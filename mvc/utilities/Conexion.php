<?php

class Conexion
{

    public static function getConexion(){
        $conn = null;
        try {
            //conexión pobres:
            $conn = new PDO("mysql:host=localhost;dbname=sigcomap", "sigcomap", "sigcomap", array(PDO::MYSQL_ATTR_LOCAL_INFILE => 'TRUE', PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            //conexión lucho:
            //$conn = new PDO("mysql:host=localhost;unix_socket=/var/run/mysqld/mysqld.sock;dbname=sigco", "sigco2015", "sigco2015*", array(PDO::MYSQL_ATTR_LOCAL_INFILE => 'TRUE', PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $conn->setAttribute(PDO:: ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex  ){
            echo 'ERROR: '.$ex->getMessage();
        }
        return $conn;
    }

}