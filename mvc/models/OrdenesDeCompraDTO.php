<?php

/**
 * Created by PhpStorm.
 * User: EDWIN
 * Date: 29/08/2015
 * Time: 7:32 PM
 */
class OrdenesDeCompraDTO
{

    private $cotizacionId;
    private $estado;
    private $valorTotal;
    private $observaciones;
    private $remisionado;
    private $facturado;
    private $certificado;
    private $fichaTecnica;
    private $fichaSeguridad;
    private $ordenCompraCliente=0;
    private $direccion;

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }



    /**
     * @return int
     */
    public function getOrdenCompraCliente()
    {
        return $this->ordenCompraCliente;
    }

    /**
     * @param int $ordenCompraCliente
     */
    public function setOrdenCompraCliente($ordenCompraCliente)
    {
        $this->ordenCompraCliente = $ordenCompraCliente;
    }



    /**
     * @return mixed
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * @param mixed $valorTotal
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;
    }

    /**
     * @return mixed
     */
    public function getRemisionado()
    {
        return $this->remisionado;
    }

    /**
     * @param mixed $remisionado
     */
    public function setRemisionado($remisionado)
    {
        $this->remisionado = $remisionado;
    }

    /**
     * @return mixed
     */
    public function getFacturado()
    {
        return $this->facturado;
    }

    /**
     * @param mixed $facturado
     */
    public function setFacturado($facturado)
    {
        $this->facturado = $facturado;
    }

    /**
     * @return mixed
     */
    public function getCertificado()
    {
        return $this->certificado;
    }

    /**
     * @param mixed $certificado
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
    }

    /**
     * @return mixed
     */
    public function getFichaTecnica()
    {
        return $this->fichaTecnica;
    }

    /**
     * @param mixed $fichaTecnica
     */
    public function setFichaTecnica($fichaTecnica)
    {
        $this->fichaTecnica = $fichaTecnica;
    }

    /**
     * @return mixed
     */
    public function getFichaSeguridad()
    {
        return $this->fichaSeguridad;
    }

    /**
     * @param mixed $fichaSeguridad
     */
    public function setFichaSeguridad($fichaSeguridad)
    {
        $this->fichaSeguridad = $fichaSeguridad;
    }



    /**
     * @return mixed
     */
    public function getCotizacionId()
    {
        return $this->cotizacionId;
    }

    /**
     * @param mixed $cotizacionId
     */
    public function setCotizacionId($cotizacionId)
    {
        $this->cotizacionId = $cotizacionId;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getDescuentoTotal()
    {
        return $this->descuentoTotal;
    }

    /**
     * @param mixed $descuentoTotal
     */
    public function setDescuentoTotal($descuentoTotal)
    {
        $this->descuentoTotal = $descuentoTotal;
    }


    /**
     * @return mixed
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param mixed $observaciones
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    }

   
}