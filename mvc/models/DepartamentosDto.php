<?php
class DepartamentosDto
{

    private $idDepartmento = 0;

    private $nombreDepartamento ="";

    /**
     * @return int
     */
    public function getIdDepartmento()
    {
        return $this->idDepartmento;
    }

    /**
     * @param int $idDepartmento
     */
    public function setIdDepartmento($idDepartmento)
    {
        $this->idDepartmento = $idDepartmento;
    }

    /**
     * @return string
     */
    public function getNombreDepartamento()
    {
        return $this->nombreDepartamento;
    }

    /**
     * @param string $nombreDepartamento
     */
    public function setNombreDepartamento($nombreDepartamento)
    {
        $this->nombreDepartamento = $nombreDepartamento;
    }




}