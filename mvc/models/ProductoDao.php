<?php


class ProductoDao
{

    private $mensaje = "";
    private $estado = 'Activo';

    public function registrarProducto(productosDto $productoDto, PDO $cnn)
    {


        try {
            $cnn->beginTransaction();
            $query = $cnn->prepare('INSERT INTO productos (IdProducto, NombreProducto, DescripcionProducto, IdIvaProductos, IdCategoriaProductos,
                                    EstadoProductos,rutaImagen,FechaCreacionProducto)VALUES(?,?,?,?,?,?,?,CURRENT_TIMESTAMP )');
            $query->bindParam(1, $productoDto->getIdProducto());
            $query->bindParam(2, $productoDto->getNombreProducto());
            $query->bindParam(3, $productoDto->getDescripcion());
            $query->bindParam(4, $productoDto->getIva());
            $query->bindParam(5, $productoDto->getCategoriaProducto());
            $query->bindParam(6, $this->estado);
            $query->bindParam(7, $productoDto->getImagenProducto());
            $query->execute();
            $query2 = $cnn->prepare('INSERT INTO valorpresentacion (idpresentacion, idProducto, valorPresentacion)values(?,?,?)  ');
            $query2->bindParam(1, $productoDto->getPresentacion());
            $query2->bindParam(2, $productoDto->getIdProducto());
            $query2->bindParam(3, $productoDto->getValorUnitario());
            $query2->execute();
            foreach ($productoDto->getAroma() as $aroma) {
                $query3 = $cnn->prepare('INSERT INTO aromasproductos VALUES (?,?)');
                $query3->bindParam(1, $aroma);
                $query3->bindParam(2, $productoDto->getIdProducto());
                $query3->execute();
            }
            foreach ($productoDto->getColor() as $color) {
                $query4 = $cnn->prepare('INSERT INTO coloresproductos VALUES (?,?)');
                $query4->bindParam(1, $color);
                $query4->bindParam(2, $productoDto->getIdProducto());
                $query4->execute();
            }
            foreach ($productoDto->getConcentracion() as $concentracion) {
                $query5 = $cnn->prepare('INSERT INTO concentracionesproductos VALUES (?,?)');
                $query5->bindParam(1, $concentracion);
                $query5->bindParam(2, $productoDto->getIdProducto());
                $query5->execute();
            }
            $cnn->commit();
            $this->mensaje = 1;
        } catch (Exception $ex) {
            $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&error=true&mensaje';
            $cnn->rollBack();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function modificarProducto(productosDto $productoDto, PDO $cnn, $idProducto)
    {
        try {
            $cnn->beginTransaction();
            $query = $cnn->prepare('UPDATE productos SET IdProducto=?, NombreProducto=?,DescripcionProducto=?,IdIvaProductos=?,
                                    IdCategoriaProductos=?,EstadoProductos=?,rutaImagen=? WHERE IdProducto=?');
            $query->bindParam(1, $productoDto->getIdProducto());
            $query->bindParam(2, $productoDto->getNombreProducto());
            $query->bindParam(3, $productoDto->getDescripcion());
            $query->bindParam(4, $productoDto->getIva());
            $query->bindParam(5, $productoDto->getCategoriaProducto());
            $query->bindParam(6, $this->estado);
            $query->bindParam(7, $productoDto->getImagenProducto());
            $query->bindParam(8, $idProducto);
            $query->execute();
            $query2 = $cnn->prepare('UPDATE valorpresentacion  set idpresentacion=?, valorPresentacion=? where idProducto=?  ');
            $query2->bindParam(1, $productoDto->getPresentacion());
            $query2->bindParam(2, $productoDto->getValorUnitario());
            $query2->bindParam(3, $productoDto->getIdProducto());
            $query2->execute();
            $cnn->commit();
            $this->mensaje = 1;
        } catch (Exception $ex) {
            $this->mensaje = $this->mensaje = '&detalleerror=' . $ex->getMessage() . '&error=true&mensaje';
            $cnn->rollBack();

        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarproductos(PDO $cnn)
    {

        try {
            $query = $cnn->prepare('SELECT productos.*,valorpresentacion.*,presentaciones.*,(case WHEN FechaCreacionProducto >= NOW() - INTERVAL 2 WEEK  then (SELECT 1 ) else (select 0) end ) as nuevo  from productos
                                         join valorpresentacion on productos.IdProducto = valorpresentacion.idProducto
                                         JOIN presentaciones ON presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                         ORDER by productos.FechaCreacionProducto DESC ');
            $query->execute();
            return $query->fetchAll();

        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarproductosCotizacion(PDO $cnn)
    {

        try {
            $query = $cnn->prepare('SELECT productos.*,valorpresentacion.*,presentaciones.*,(case WHEN FechaCreacionProducto >= NOW() - INTERVAL 2 WEEK  then (SELECT 1 ) else (select 0) end ) as nuevo  from productos
                                         join valorpresentacion on productos.IdProducto = valorpresentacion.idProducto
                                         JOIN presentaciones ON presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                      group by productos.IdProducto
                                      ORDER by productos.IdProducto ASC
                                      ');
            $query->execute();
            return $query->fetchAll();

        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function obtenerProducto($id, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('SELECT productos.*, aromasproductos.*, coloresproductos.*, concentracionesproductos.*,
  aromas.*, colores.*, concentraciones.*, valorpresentacion.*, presentaciones.*,
  impuestos.*, categoriasproductos.*,(case WHEN FechaCreacionProducto >= NOW() - INTERVAL 2 WEEK  then (SELECT 1 ) else (select 0)
                                     end ) as nuevo
  from productos
  join aromasproductos ON productos.IdProducto=aromasproductos.idProductoAromasProd
  JOIN aromas on aromas.idAroma=aromasproductos.idAromaAromasProd
  join coloresproductos on coloresproductos.idProductoColoresProd=productos.IdProducto
  join colores on colores.idColor=coloresproductos.idColorColoresProd
  JOIN concentracionesproductos on concentracionesproductos.idProductoConcProd=productos.IdProducto
  JOIN concentraciones ON concentraciones.idConcentracion=concentracionesproductos.idConcentracionConcProd
  join valorpresentacion on productos.IdProducto=valorpresentacion.idProducto
  join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
  join impuestos on productos.IdIvaProductos=impuestos.IdIva
  JOIN categoriasproductos on productos.IdCategoriaProductos=categoriasproductos.IdCategoria
    and productos.IdProducto=?');
            $query->bindParam(1, $id);
            $query->execute();
            return $query->fetch();

        } catch (Exception $ex) {
            print $ex->getMessage();
        }
        $cnn = null;
    }

    public function obtenerProductoPresentacion($idProducto, $idPresentacion, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('SELECT productos.*,valorpresentacion.*,presentaciones.*,categoriasproductos.*
                                   from productos join valorpresentacion on productos.IdProducto = valorpresentacion.idProducto
                                    JOIN presentaciones ON presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                    join categoriasproductos on IdCategoriaProductos=IdCategoria
                                    and productos.IdProducto=? and presentaciones.IdPresentacion=?');
            $query->bindParam(1, $idProducto);
            $query->bindParam(2, $idPresentacion);
            $query->execute();
            return $query->fetch();

        } catch (Exception $ex) {
            print $ex->getMessage();
        }
        $cnn = null;
    }

    public function cancelarProducto($idProducto, PDO $cnn)
    {
        $cancelar = 'Inactivo';
        try {
            $query = $cnn->prepare('UPDATE productos  SET EstadoProductos=?  where IdProducto=?');
            $query->bindParam(1, $cancelar);
            $query->bindParam(2, $idProducto);
            $query->execute();
            $this->mensaje = 'Producto Eliminado';
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage() . ' No se esta eliminando';
        }
        return $this->mensaje;
    }

    public function  seleccionarColor(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from colores');
            $query->execute();
            $this->mensaje = $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;

    }

    public function  seleccionarAroma(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from aromas');
            $query->execute();
            $this->mensaje = $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function seleccionarConcetracion(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from concentraciones');
            $query->execute();
            $this->mensaje = $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function presentacionId($id, PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select NombrePresentacion from presentaciones where IdPresentacion=?');
            $query->bindParam(1, $id);
            $query->execute();
            $this->mensaje= $query->fetch();
        } catch (Exception $ex) {
            $this->mensaje= $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function obtenerCategoriaProducto(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from categoriasproductos');
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje=$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function obtenerIvaProducto(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('select * from impuestos');
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje=$ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function buscarProducto(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('SELECT productos.*, aromasproductos.*, coloresproductos.*, concentracionesproductos.*,
  aromas.*, colores.*, concentraciones.*, valorpresentacion.*, presentaciones.*,
  impuestos.*, categoriasproductos.*,(case WHEN FechaCreacionProducto >= NOW() - INTERVAL 2 WEEK  then (SELECT 1 ) else (select 0)
                                     end ) as nuevo
  from productos
  join aromasproductos ON productos.IdProducto=aromasproductos.idProductoAromasProd
  JOIN aromas on aromas.idAroma=aromasproductos.idAromaAromasProd
  join coloresproductos on coloresproductos.idProductoColoresProd=productos.IdProducto
  join colores on colores.idColor=coloresproductos.idColorColoresProd
  JOIN concentracionesproductos on concentracionesproductos.idProductoConcProd=productos.IdProducto
  JOIN concentraciones ON concentraciones.idConcentracion=concentracionesproductos.idConcentracionConcProd
  join valorpresentacion on productos.IdProducto=valorpresentacion.idProducto
  join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
  join impuestos on productos.IdIvaProductos=impuestos.IdIva
  JOIN categoriasproductos on productos.IdCategoriaProductos=categoriasproductos.IdCategoria
  ORDER  BY productos.FechaCreacionProducto DESC ');
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }


    public function buscarProductoCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare('SELECT productos.*, aromasproductos.*, coloresproductos.*, concentracionesproductos.*,
  aromas.*, colores.*, concentraciones.*, valorpresentacion.*, presentaciones.*,
  impuestos.*, categoriasproductos.*,(case WHEN FechaCreacionProducto >= NOW() - INTERVAL 2 WEEK  then (SELECT 1 ) else (select 0)
                                     end ) as nuevo
  from productos
  join aromasproductos ON productos.IdProducto=aromasproductos.idProductoAromasProd
  JOIN aromas on aromas.idAroma=aromasproductos.idAromaAromasProd
  join coloresproductos on coloresproductos.idProductoColoresProd=productos.IdProducto
  join colores on colores.idColor=coloresproductos.idColorColoresProd
  JOIN concentracionesproductos on concentracionesproductos.idProductoConcProd=productos.IdProducto
  JOIN concentraciones ON concentraciones.idConcentracion=concentracionesproductos.idConcentracionConcProd
  join valorpresentacion on productos.IdProducto=valorpresentacion.idProducto
  join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
  join impuestos on productos.IdIvaProductos=impuestos.IdIva
  JOIN categoriasproductos on productos.IdCategoriaProductos=categoriasproductos.IdCategoria
                                             AND '.$criterio.' = "'.$busqueda.'"
                                           group by productos.IdProducto ORDER  BY productos.FechaCreacionProducto DESC ');
                    $query->execute();
                    if (isset($_SESSION['conteo'])) {
                        $_SESSION['conteo'] = $query->rowCount();
                    }
                    return $query->fetchAll();
                } catch (Exception $ex) {
                    echo '&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;

            case 2:
                try {
                    $query = $cnn->prepare('SELECT productos.*, aromasproductos.*, coloresproductos.*, concentracionesproductos.*,
  aromas.*, colores.*, concentraciones.*, valorpresentacion.*, presentaciones.*,
  impuestos.*, categoriasproductos.*,(case WHEN FechaCreacionProducto >= NOW() - INTERVAL 2 WEEK  then (SELECT 1 ) else (select 0)
                                     end ) as nuevo
  from productos
  join aromasproductos ON productos.IdProducto=aromasproductos.idProductoAromasProd
  JOIN aromas on aromas.idAroma=aromasproductos.idAromaAromasProd
  join coloresproductos on coloresproductos.idProductoColoresProd=productos.IdProducto
  join colores on colores.idColor=coloresproductos.idColorColoresProd
  JOIN concentracionesproductos on concentracionesproductos.idProductoConcProd=productos.IdProducto
  JOIN concentraciones ON concentraciones.idConcentracion=concentracionesproductos.idConcentracionConcProd
  join valorpresentacion on productos.IdProducto=valorpresentacion.idProducto
  join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
  join impuestos on productos.IdIvaProductos=impuestos.IdIva
  JOIN categoriasproductos on productos.IdCategoriaProductos=categoriasproductos.IdCategoria
                                              AND ' . $criterio . ' like "%' . $busqueda . '%"
                                            group by productos.IdProducto ORDER  BY productos.FechaCreacionProducto DESC ');
                    $query->execute();
                    if (isset($_SESSION['conteo'])) {
                        $_SESSION['conteo'] = $query->rowCount();
                    }
                    return $query->fetchAll();
                } catch (Exception $ex) {
                    echo '&ex=' . $ex->getMessage() . '&encontrados=0';
                };

                break;
        }

    }

    public function validateProductIdExist($id, PDO $conexion)
    {
        try {
            $query = $conexion->prepare('select count(*) existe from productos where IdProducto=?');
            $query->bindParam(1, $id);
            $query->execute();
            $this->mensaje = $query->fetch();
        } catch (Exception $e) {
            $this->mensaje = $e->getMessage();
        }
        $conexion=null;
        return $this->mensaje;
    }

    public function obtenerValorPresentacion(PDO $conexion)
    {
        try {
            $query = $conexion->prepare('select * from presentaciones');
            $query->execute();
            $this->mensaje = $query->fetchAll();
        } catch (Exception $e) {
            $this->mensaje = $e->getMessage();
        }
        $conexion=null;
        return $this->mensaje;
    }
    public function obtenerPresentacionesPrecios($id, PDO $conexion)
    {
        try {
            $query = $conexion->prepare('select presentaciones.*,productos.*,valorpresentacion.* from productos
                                          join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                          JOIN presentaciones on presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                           and productos.IdProducto=?');
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje = $query->fetchAll();
        } catch (Exception $e) {
            $this->mensaje = $e->getMessage();
        }
        $conexion=null;
        return $this->mensaje;
    }

}
