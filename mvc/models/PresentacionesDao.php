<?php

class PresentacionesDao
{
private $mensaje="";

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("Select presentaciones.*, valorpresentacion.*, productos.*
                                          FROM presentaciones JOIN valorpresentacion
                                          ON presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                          JOIN productos
                                          ON valorpresentacion.idProducto = productos.IdProducto");
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function traerPresentacion($id,PDO $cnn){
        try{
            $query = $cnn->prepare("Select * FROM presentaciones where idPresentacion=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }




    public function buscarConCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare("Select presentaciones.*, valorpresentacion.*, productos.*
                                          FROM presentaciones JOIN valorpresentacion
                                          ON presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                          JOIN productos
                                          ON valorpresentacion.idProducto = productos.IdProducto
                                          WHERE $criterio='$busqueda' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            case 2:
                try {
                    $query = $cnn->prepare("Select presentaciones.*, valorpresentacion.*, productos.*
                                          FROM presentaciones JOIN valorpresentacion
                                          ON presentaciones.IdPresentacion = valorpresentacion.idpresentacion
                                          JOIN productos
                                          ON valorpresentacion.idProducto = productos.IdProducto
                                            where $criterio like '%$busqueda%' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            default:
                $this->mensaje='No se han recibido parámetros correctos';
        }
        $cnn=null;
        return $this->mensaje;
    }





}