<?php

class ConcentracionesDao
{
private $mensaje="";

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT * FROM concentraciones");
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarConCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare("Select concentraciones.*, concentracionesproductos.*, productos.*
                                            FROM concentraciones JOIN concentracionesproductos
                                            ON concentraciones.idConcentracion = concentracionesproductos.idConcentracionConcProd
                                            JOIN productos
                                            ON concentracionesproductos.idProductoConcProd = productos.IdProducto
                                            WHERE $criterio='$busqueda' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            case 2:
                try {
                    $query = $cnn->prepare("Select concentraciones.*, concentracionesproductos.*, productos.*
                                          FROM concentraciones JOIN concentracionesproductos
                                          ON concentraciones.idConcentracion = concentracionesproductos.idConcentracionConcProd
                                          JOIN productos
                                          ON concentracionesproductos.idProductoConcProd = productos.IdProducto
                                            where $criterio like '%$busqueda%' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            default:
                $this->mensaje='No se han recibido parámetros correctos';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function concentracionesProducto($id,PDO $cnn){
        try{
            $query = $cnn->prepare("select concentraciones.* from productos
                                    join concentracionesproductos on concentracionesproductos.idProductoConcProd=productos.IdProducto
                                    join concentraciones on concentraciones.idConcentracion=concentracionesproductos.idConcentracionConcProd
                                    where productos.IdProducto=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function traerConcentracion($id,PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT * FROM concentraciones where idConcentracion=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }





}