<?php

class GestionDao
{

    private $mensaje = " ";

    /**
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    public function registrarGestion(GestionDto $gestionDto, PDO $cnn)
    {


        try {
            $query = $cnn->prepare("insert into gestiones (TipoGestiones,Asistentes,ObservacionesGestiones,LugarGestiones,
                                    FechaProgramada,NitClienteGestiones,CedulaEmpleadoGestiones,creado,Actualizado) VALUES (?,?,?,?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP) ");
            $query->bindParam(1, $gestionDto->getTipoVisita());
            $query->bindParam(2, $gestionDto->getAsistentes());
            $query->bindParam(3, $gestionDto->getObservaciones());
            $query->bindParam(4, $gestionDto->getLugar());
            $query->bindParam(5, $gestionDto->getFechaVisita());
            $query->bindParam(6, $gestionDto->getIdCliente());
            $query->bindParam(7, $gestionDto->getIdUsuario());
            $query->execute();
            foreach($gestionDto->getTemaProducto() as $asunto) {
                $query2 = $cnn->prepare('insert into detallesgestion (IdDetallesGestion ,Asunto) values((select MAX(IdGestion) FROM gestiones),?)');
                $query2->bindParam(1, $asunto);
                $query2->execute();
            }
            $this->mensaje = "Visita Registrada Correctamente";
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }


    public function modificarGestion(GestionDto $gestionDto, PDO $cnn, $idGestion)
    {

        try {
            $query = $cnn->prepare('update gestiones set EstadoGestiones=? ,ResultadoGestiones=?,Actualizado=CURRENT_TIMESTAMP WHERE IdGestion=?');

            $query->bindParam(1, $gestionDto->getEstado());
            $query->bindParam(2, $gestionDto->getObservaciones());
            $query->bindParam(3, $idGestion);
            $query->execute();
            $this->mensaje = "Registro Actualizado";
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }
    public function comentarGestion(GestionDto $gestionDto, PDO $cnn)
    {
        try {
            $query = $cnn->prepare("insert into comentariosGetiones (Comentario,IdComentarioGestiones,PersonaComenta) VALUES (?,?,?) ");
            $query->bindParam(1, $gestionDto->getObservaciones());
            $query->bindParam(2,$gestionDto->getIdGestion());
            $query->bindParam(3,$gestionDto->getIdUsuario());
            $query->execute();
            $this->mensaje = "Observación agregada correctamente";
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function getallEvents(PDO $cnn,$busqueda,$id)
    {


        try {
             if(($_SESSION['rol']['rol'] == 4 ||$_SESSION['rol']['rol'] ==  1) && $busqueda==1) {
                $query = $cnn->prepare('SELECT gestiones.FechaProgramada as start, detallesgestion.Asunto as title ,(case WHEN gestiones.TipoGestiones="Asesoría"
                        then (SELECT "#00a65a") else (SELECT "#f39c12") end ) as color ,
                        concat("ModificarGestion.php?id=",IdGestion) as url , ".$id." as id from gestiones
                        JOIN detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion
                        where FechaProgramada >=CURRENT_DATE  and gestiones.EstadoGestiones="pendiente" and gestiones.CedulaEmpleadoGestiones=?
                        GROUP BY gestiones.IdGestion');
                        $query->bindParam(1, $id);
            }else{
                  $query = $cnn->prepare('SELECT gestiones.FechaProgramada as start, detallesgestion.Asunto as title ,(case WHEN gestiones.TipoGestiones="Asesoría"
                        then (SELECT "#00a65a") else (SELECT "#f39c12") end ) as color ,
                        concat("ModificarGestion.php?id=",IdGestion) as url , ".$id." as id from gestiones
                        JOIN detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion
                        where FechaProgramada >=CURRENT_DATE  and gestiones.EstadoGestiones="pendiente" and gestiones.CedulaEmpleadoGestiones=?
                         GROUP BY gestiones.IdGestion');
                  $query->bindParam(1, $_SESSION['datosLogin']['id']);
            }
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        return $this->mensaje;
    }

    public function listarGestion($idUsuario,$opt, PDO $cnn)
    {

        try {
            if (($_SESSION['datosLogin']['NombreRol'] == 'Coordinador') or ($_SESSION['datosLogin']['NombreRol'] == 'Administrador')) {
                $query = $cnn->prepare("select gestiones.IdGestion,gestiones.TipoGestiones,gestiones.NitClienteGestiones,
                            date_format(gestiones.FechaProgramada,'%a %d %b') as FechaProgramada  ,gestiones.Asistentes,
  gestiones.CedulaEmpleadoGestiones,gestiones.EstadoGestiones, gestiones.ObservacionesGestiones,
  gestiones.LugarGestiones ,(case WHEN concat('',detallesgestion.Asunto * 1)=detallesgestion.Asunto
  then (SELECT productos.IdProducto from productos  where detallesgestion.Asunto=productos.IdProducto )
                             else detallesgestion.Asunto end ) as Asunto,count(detallesgestion.IdDetallesGestion) cantidad from gestiones
                         join detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion GROUP BY IdGestion");
            } else {
                $query = $cnn->prepare("select gestiones.IdGestion,gestiones.TipoGestiones,gestiones.NitClienteGestiones,
                            date_format(gestiones.FechaProgramada,'%a %d %b') as FechaProgramada  ,gestiones.Asistentes,
  gestiones.CedulaEmpleadoGestiones,gestiones.EstadoGestiones, gestiones.ObservacionesGestiones,
  gestiones.LugarGestiones ,(case WHEN concat('',detallesgestion.Asunto * 1)=detallesgestion.Asunto
  then (SELECT productos.IdProducto from productos  where detallesgestion.Asunto=productos.IdProducto )
                             else detallesgestion.Asunto end ) as Asunto,count(detallesgestion.IdDetallesGestion) cantidad from gestiones
                         join detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion and  CedulaEmpleadoGestiones=?");
                $query->bindParam(1, $idUsuario);
            }
            $query->execute();

            $_SESSION['conteo'] = $query->rowCount();
            if($opt==1){
                $this->mensaje= $query->fetch();
            }else{
                $this->mensaje= $query->fetchAll();
            }

        } catch (Exception $ex) {
            echo 'Error' . $ex->getMessage();
        }
        return $this->mensaje;
    }

    public function obtenerEmpresaById($criteria, PDO $cnn)
    {

        try {
            $query = $cnn->prepare("select clientes.RazonSocial from clientes WHERE Nit=? ");
            $query->bindParam(1, $criteria);
            $query->execute();
            return $query->fetchall();

        } catch (Exception $ex) {
            $this->mensaje = $criteria;
        }
        return $this->mensaje;
    }

    public function obtenerEmpresas(PDO $cnn)
    {

        try {
            $query = $cnn->prepare("select * from clientes");
            $query->execute();
            return $query->fetchall();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        return $this->mensaje;
    }

    public function completeGestion($idGestion, PDO $cnn)
    {

        try {
            $query = $cnn->prepare("select gestiones.IdGestion,gestiones.NitClienteGestiones,date_format(gestiones.FechaProgramada,'%a %d %b')
                                    as FechaProgramada,gestiones.Asistentes,gestiones.CedulaEmpleadoGestiones,gestiones.EstadoGestiones ,
                                    gestiones.ObservacionesGestiones,gestiones.LugarGestiones ,
                                    (case WHEN concat('',gestiones.Asunto * 1)=gestiones.Asunto  then
                                    (SELECT productos.NombreProducto from productos  where gestiones.Asunto=productos.IdProducto )
                                    else gestiones.Asunto end ) as Asunto from gestiones where IdGestion=? ORDER BY FechaProgramada");
            $query->bindParam(1, $idGestion);
            $query->execute();
            return $query->fetch();
        } catch (Exception $ex) {
            echo 'Error' . $ex->getMessage();
        }
        $cnn = null;
    }

    public function buscarGestion($id,$option, PDO $cnn)
    {
        try {
            $query = $cnn->prepare("select gestiones.*,detallesgestion.*,(select count(*) from comentariosGetiones
                                    WHERE IdComentarioGestiones=?) existe,(select Comentario from comentariosGetiones
                                    WHERE IdComentarioGestiones=?) Comentario,(SELECT PersonaComenta from comentariosGetiones
                                    WHERE IdComentarioGestiones=?) persona from gestiones
                                    JOIN  detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion
                                    where IdGestion=?");
            $query->bindParam(1, $id);
            $query->bindParam(2, $id);
            $query->bindParam(3, $id);
            $query->bindParam(4, $id);
            $query->execute();
            $this->mensaje=$query->fetch();
            if($option){
                $this->mensaje=$query->fetchAll();
            }


        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        return $this->mensaje;
    }

    public function obtenerCapacitaciones(PDO $cnn)
    {
        try {
            $query = $cnn->prepare('SELECT gestiones.FechaProgramada, sum(case WHEN gestiones.TipoGestiones="Asesoría" then 1 else 0 end)
                                    as asesorias,sum(case WHEN gestiones.Tipogestiones="Capacitación"  then 1 else 0 end) as capacitaciones
                                    from gestiones where gestiones.TipoGestiones is not NULL GROUP BY month(FechaProgramada)');
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
    }

    public function buscarGestionCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {

            case 1:
                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try {
                        $query = $cnn->prepare("select gestiones.*,detallesgestion.*,(select count(*) from comentariosGetiones
                                    WHERE IdComentarioGestiones='$busqueda') existe,(select Comentario from comentariosGetiones
                                    WHERE IdComentarioGestiones='$busqueda') Comentario,(SELECT PersonaComenta from comentariosGetiones
                                    WHERE IdComentarioGestiones='$busqueda') persona,count(detallesgestion.IdDetallesGestion) cantidad  from gestiones
                                    JOIN  detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion
                                     where $criterio='$busqueda'");
                        $query->execute();
                        if (isset($_SESSION['conteo'])) {
                            $_SESSION['conteo'] = $query->rowCount();
                        }
                        return $query->fetchAll();

                    } catch (Exception $ex) {
                        echo '&ex=' . $ex->getMessage() . '&encontrados=0';
                    }
                } else {
                    try {
                        $query = $cnn->prepare("  select gestiones.*,detallesgestion.*,(select count(*) from comentariosGetiones
                                                WHERE IdComentarioGestiones='$busqueda') existe,(select Comentario from comentariosGetiones
                                                WHERE IdComentarioGestiones='$busqueda') Comentario,(SELECT PersonaComenta from comentariosGetiones
                                                WHERE IdComentarioGestiones='$busqueda') persona,count(detallesgestion.IdDetallesGestion) cantidad  from gestiones
                                                JOIN  detallesgestion on gestiones.IdGestion = detallesgestion.IdDetallesGestion
                                                where $criterio='$busqueda'
                                                and CedulaEmpleadoGestiones=? ORDER BY FechaProgramada ");
                        $query->bindParam(1, $_SESSION['datosLogin']['id']);
                        $query->execute();
                        return $query->fetchAll();
                    } catch (Exception $ex) {
                        echo '&ex=' . $ex->getMessage() . '&encontrados=0';
                    }
                }
                break;

            case 2:

                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try {
                        $query = $cnn->prepare("Select clientes.*,gestiones.* from gestiones JOIN clientes on clientes.Nit=gestiones.NitClienteGestiones
                                                where $criterio like '%$busqueda%' ORDER BY FechaProgramada");
                        $query->execute();
                        if (isset($_SESSION['conteo'])) {
                            $_SESSION['conteo'] = $query->rowCount();
                        }
                        return $query->fetchAll();
                    } catch (Exception $ex) {
                       $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                    }

                } else{
                        try{
                            $query=$cnn->prepare("Select clientes.*,gestiones.* from gestiones JOIN clientes on clientes.Nit=gestiones.NitClienteGestiones
                                                  where $criterio like '%$busqueda%' and CedulaEmpleadoGestiones=? ORDER BY FechaProgramada" );
                            $query->bindParam(1,$_SESSION['datosLogin']['id']);
                            $query->execute();
                            $this->mensaje=$query->fetchAll();
                        }catch(Exception $ex){
                            $this->mensaje='&ex=' . $ex->getMessage().'&encontrados=0';
                        }
                    return $this->mensaje;
                }
                break;
        }

    }

}