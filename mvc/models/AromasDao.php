<?php

class AromasDao
{
private $mensaje="";

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT * FROM aromas");
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarConCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare("SELECT aromas.*, aromasproductos.*, productos.* from aromas
                                            JOIN aromasproductos ON aromas.idAroma=aromasproductos.idAromaAromasProd
                                            JOIN productos ON aromasproductos.idProductoAromasProd = productos.IdProducto
                                            WHERE $criterio='$busqueda' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            case 2:
                try {
                    $query = $cnn->prepare("SELECT aromas.*, aromasproductos.*, productos.* from aromas
                                            JOIN aromasproductos ON aromas.idAroma=aromasproductos.idAromaAromasProd
                                            JOIN productos ON aromasproductos.idProductoAromasProd = productos.IdProducto
                                            where $criterio like '%$busqueda%' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            default:
                $this->mensaje='No se han recibido parámetros correctos';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function aromasProducto($id,PDO $cnn){
        try{
            $query = $cnn->prepare("select aromas.* from productos
                                    join aromasproductos on aromasproductos.idProductoAromasProd=productos.IdProducto
                                    join aromas on aromas.idAroma=aromasproductos.idAromaAromasProd
                                    where productos.IdProducto=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function traerAroma($id,PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT * FROM aromas where idAroma=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }



}