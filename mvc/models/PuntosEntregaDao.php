<?php

class PuntosEntregaDao
{
    public function registrarPunto(PuntosEntregaDto $puntosEntregaDto, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("INSERT INTO puntosentrega VALUES(?,?,?,?,?,?,?,?)");
            $query->bindParam(1, $puntosEntregaDto->getNombrePuntoEntrega());
            $query->bindParam(2, $puntosEntregaDto->getDireccionPuntoEntrega());
            $query->bindParam(3, $puntosEntregaDto->getNombreContactoPuntoEntrega() );
            $query->bindParam(4, $puntosEntregaDto->getCorreoPuntoEntrega());
            $query->bindParam(5, $puntosEntregaDto->getTelefonoPuntoEntrega());
            $query->bindParam(6, $puntosEntregaDto->getNitEmpresa());
            $query->bindParam(7, $puntosEntregaDto->getIdLugarPuntoEntrega());
            $query->bindParam(8, $puntosEntregaDto->getObservacionesPuntoEntrega());
            $query->execute();
            $mensaje="Información registrada con éxito en la base de datos.&error=false";
        } catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La información NO ha sido registrada en la base de datos.';
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("select puntosentrega.*, lugares.*, departamentos.*, clientes.* from puntosentrega
                                    join lugares on puntosentrega.idLugarPuntoEntrega=lugares.idLugar
                                    join departamentos on departamentos.idDepartamento=lugares.idDepartamentoLugar
                                    join clientes on puntosentrega.nitEmpresaPuntoEntrega=clientes.Nit
                                    ");
            $query->execute();
            $mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $mensaje= '&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function obtenerPunto($nombrePunto, $nitEmpresa, PDO $cnn){
        try {
            $query = $cnn->prepare("select puntosentrega.*, lugares.*, departamentos.*, clientes.*,
                                    personas.*
                                    from puntosentrega
                                    join lugares on puntosentrega.idLugarPuntoEntrega=lugares.idLugar
                                    join departamentos on departamentos.idDepartamento=lugares.idDepartamentoLugar
                                    join clientes on puntosentrega.nitEmpresaPuntoEntrega=clientes.Nit
                                    join personas on personas.CedulaPersona=clientes.CedulaCliente
                                    and puntosentrega.nombrePuntoEntrega=?
                                    and puntosentrega.nitEmpresaPuntoEntrega=?
                                    ");
            $query->bindParam(1, $nombrePunto);
            $query->bindParam(2, $nitEmpresa);
            $query->execute();
            $mensaje=$query->fetch();
        } catch (Exception $ex) {
            $mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function buscarPunto($criterio, $busqueda, $comobuscar, PDO $cnn){
        switch ($comobuscar) {
            case 1:
                try{
                    $query = $cnn->prepare('select puntosentrega.*, lugares.*, departamentos.*, clientes.* from puntosentrega
                                    join lugares on puntosentrega.idLugarPuntoEntrega=lugares.idLugar
                                    join departamentos on departamentos.idDepartamento=lugares.idDepartamentoLugar
                                    join clientes on puntosentrega.nitEmpresaPuntoEntrega=clientes.Nit
                                    and '.$criterio.' = "'.$busqueda.'"');
                    $query->execute();
                    $mensaje=$query->fetchAll();
                } catch (Exception $ex){
                    $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                };
                break;
            case 2;
                try{
                    $query = $cnn->prepare('select puntosentrega.*, lugares.*, departamentos.*, clientes.* from puntosentrega
                                    join lugares on puntosentrega.idLugarPuntoEntrega=lugares.idLugar
                                    join departamentos on departamentos.idDepartamento=lugares.idDepartamentoLugar
                                    join clientes on puntosentrega.nitEmpresaPuntoEntrega=clientes.Nit
                                    and '.$criterio.' like "%'.$busqueda.'%"');
                    $query->execute();
                    $mensaje=$query->fetchAll();
                } catch (Exception $ex){
                    $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                };
                break;
            default:
                $mensaje='Opción inválida para Cómo búscar';
        }
        $cnn=null;
        return $mensaje;
    }

    public function modificarPunto(PuntosEntregaDto $puntosEntregaDto, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("UPDATE puntosentrega set
                                    puntosentrega.correoPuntoEntrega=?,
                                    puntosentrega.direccionPuntoEntrega=?,
                                    puntosentrega.idLugarPuntoEntrega=?,
                                    puntosentrega.nombreContactoPuntoEntrega=?,
                                    puntosentrega.nombrePuntoEntrega=?,
                                    puntosentrega.observacionesPuntoEntrega=?,
                                    puntosentrega.telefonoPuntoEntrega=?
                                    WHERE puntosentrega.nitEmpresaPuntoEntrega=?
                                    and puntosentrega.nombrePuntoEntrega=?
                                    ");
            $query->bindParam(1, $puntosEntregaDto->getCorreoPuntoEntrega());
            $query->bindParam(2, $puntosEntregaDto->getDireccionPuntoEntrega());
            $query->bindParam(3, $puntosEntregaDto->getIdLugarPuntoEntrega());
            $query->bindParam(4, $puntosEntregaDto->getNombreContactoPuntoEntrega());
            $query->bindParam(5, $puntosEntregaDto->getNombrePuntoEntrega());
            $query->bindParam(6, $puntosEntregaDto->getObservacionesPuntoEntrega());
            $query->bindParam(7, $puntosEntregaDto->getTelefonoPuntoEntrega());
            $query->bindParam(8, $puntosEntregaDto->getNitEmpresa());
            $query->bindParam(9, $puntosEntregaDto->getNombrePuntoEntregaAntiguo());
            $query->execute();
            $mensaje="Información actualizada con éxito.&error=false";
        } catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La información NO ha sido actualizada en la base de datos.';
        }
        $cnn = null;
        return $mensaje;
    }





}