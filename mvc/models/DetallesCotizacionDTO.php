<?php

class DetallesCotizacionDTO
{
    private $idCotizacion;
    private $idProducto;
    private $cantidad;
    private $total;
    private $presentacion;
    private $aroma;
    private $color;
    private $concentracion;
    private $presentacionDetalles;
    private $descuento;
    private $presentacionDespacho;
    private $cantidadDespacho;
    private $valorfacturacion;

    /**
     * @return mixed
     */
    public function getValorfacturacion()
    {
        return $this->valorfacturacion;
    }

    /**
     * @param mixed $valorfacturacion
     */
    public function setValorfacturacion($valorfacturacion)
    {
        $this->valorfacturacion = $valorfacturacion;
    }



    /**
     * @return mixed
     */
    public function getPresentacionDespacho()
    {
        return $this->presentacionDespacho;
    }

    /**
     * @param mixed $presentacionDespacho
     */
    public function setPresentacionDespacho($presentacionDespacho)
    {
        $this->presentacionDespacho = $presentacionDespacho;
    }

    /**
     * @return mixed
     */
    public function getCantidadDespacho()
    {
        return $this->cantidadDespacho;
    }

    /**
     * @param mixed $cantidadDespacho
     */
    public function setCantidadDespacho($cantidadDespacho)
    {
        $this->cantidadDespacho = $cantidadDespacho;
    }



    /**
     * @return mixed
     */
    public function getPresentacionDetalles()
    {
        return $this->presentacionDetalles;
    }

    /**
     * @param mixed $presentacionDetalles
     */
    public function setPresentacionDetalles($presentacionDetalles)
    {
        $this->presentacionDetalles = $presentacionDetalles;
    }

    /**
     * @return mixed
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param mixed $descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }



    /**
     * @return mixed
     */
    public function getPresentacion()
    {
        return $this->presentacion;
    }

    /**
     * @param mixed $presentacion
     */
    public function setPresentacion($presentacion)
    {
        $this->presentacion = $presentacion;
    }




    /**
     * @return mixed
     */
    public function getIdCotizacion()
    {
        return $this->idCotizacion;
    }

    /**
     * @param mixed $idCotizacion
     */
    public function setIdCotizacion($idCotizacion)
    {
        $this->idCotizacion = $idCotizacion;
    }

    /**
     * @return mixed
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * @param mixed $idProducto
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getAroma()
    {
        return $this->aroma;
    }

    /**
     * @param mixed $aroma
     */
    public function setAroma($aroma)
    {
        $this->aroma = $aroma;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getConcentracion()
    {
        return $this->concentracion;
    }

    /**
     * @param mixed $concentracion
     */
    public function setConcentracion($concentracion)
    {
        $this->concentracion = $concentracion;
    }




}