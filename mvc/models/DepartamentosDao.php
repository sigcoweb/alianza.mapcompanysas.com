<?php

class DepartamentosDao
{
    public function registrarLugar(DepartamentosDto $DepartamentosDto, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("INSERT INTO departamentos VALUES(DEFAULT,?)");
            $query->bindParam(1, $DepartamentosDto->getNombreCiudad());
            $query->execute();
            $mensaje="Información registrada con éxito en la base de datos.&error=false";
        } catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La información NO ha sido registrada en la base de datos.';
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("select * from departamentos ORDER BY idDepartamento");
            $query->execute();
            $mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $mensaje= '&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function obtenerDepartamento($idDepartamento, PDO $cnn){

        try {
            $query = $cnn->prepare('select * from departamentos where departamentos.idDepartamento = ? ORDER BY idDepartamento');
            $query->bindParam(1, $idDepartamento);
            $query->execute();
            $mensaje=$query->fetch();
        } catch (Exception $ex) {
            $mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function buscarIdDepartamento($idDepartamento, PDO $cnn){
        try{
            $query = $cnn->prepare('select * from departamentos where departamentos.idDepartamento = "'.$idDepartamento.'"
                                    ORDER BY departamentos.idDepartamento ASC');
            $query->execute();
            $mensaje=$query->fetch();
        } catch (Exception $ex){
            $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=false&error=true';
        };
        $cnn=null;
        return $mensaje;
    }

    public function buscarDepartamento($criterio, $busqueda, $comobuscar, PDO $cnn){
        switch ($comobuscar) {
            case 1:
                try{
                    $query = $cnn->prepare('select * from departamentos where '.$criterio.' = "'.$busqueda.'"
                                            ORDER BY departamentos.idDepartamento ASC');
                    $query->execute();
                    $mensaje=$query->fetchAll();
                } catch (Exception $ex){
                    $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                };
                break;
            case 2;
                try{
                    $query = $cnn->prepare('select * from departamentos where '.$criterio.' like "%'.$busqueda.'%"
                                            ORDER BY departamentos.idDepartamento ASC');
                    $query->execute();
                    $mensaje=$query->fetchAll();
                } catch (Exception $ex){
                    $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                };
                break;
            default:
                echo 'Opción inválida para Cómo búscar';
        }
        $cnn=null;
        return $mensaje;
    }

    public function modificarDepartamento(DepartamentosDto $DepartamentosDto, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("UPDATE departamentos set nombreDepartamento = ? WHERE departamentos.idDepartamento = ?");
            $query->bindParam(1, $DepartamentosDto->getNombreDepartamento());
            $query->bindParam(2, $DepartamentosDto->getIdDepartmento());
            $query->execute();
            $mensaje="Información actualizada con éxito.&error=false";
        } catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La información NO ha sido actualizada en la base de datos.';
        }
        $cnn = null;
        return $mensaje;
    }

    public function existeIdDepartamento($idDepartamento, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("SELECT COUNT(*) as existente from departamentos where departamentos.idDepartamento = ?");
            $query->bindParam(1, $idDepartamento);
            $query->execute();
            $mensaje = $query->fetch();
        }catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'$error=true&mensaje=Error en la consulta';
        }
        $cnn=null;
        return $mensaje;
    }





}