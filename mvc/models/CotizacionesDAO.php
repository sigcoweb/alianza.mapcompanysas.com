<?php

class CotizacionesDAO
{
    private $mensaje;

    public function registrarCotizaciones (CotizacionesDTO $cotizacionesDTO,PDO $conexion){
        try {
            $query = $conexion->prepare("INSERT INTO cotizaciones  VALUES (DEFAULT,?,now(),0,?,?,?,?,?)");
            $query->bindParam(1, $cotizacionesDTO->getEstado());
            $query->bindParam(2, $cotizacionesDTO->getObservaciones());
            $query->bindParam(3, $cotizacionesDTO->getValorDescuento());
            $query->bindParam(4, $cotizacionesDTO->getIdUsuario());
            $query->bindParam(5, $cotizacionesDTO->getIdCliente());
            $query->bindParam(6, $cotizacionesDTO->getEmpresaCotiza());
            $query->execute();
            $query2 = $conexion->prepare('SELECT MAX(IdCotizacion) AS "idc" FROM cotizaciones');
            $query2->execute();
            return $query2->fetch();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }

        $conexion=null;
        return $this->mensaje;
    }

    public function agregarProducto(DetallesCotizacionDTO $dto,PDO $conexion){
        try {
            $query = $conexion->prepare("INSERT INTO detallescotizacion  VALUES (?,?,?,?,?,?,?,?,?,?,'0000',?)");
            $query->bindParam(1,$dto->getIdCotizacion());
            $query->bindParam(2,$dto->getIdProducto());
            $query->bindParam(3,$dto->getCantidad());
            $query->bindParam(4,$dto->getTotal());
            $query->bindParam(5,$dto->getPresentacionDespacho());
            $query->bindParam(6,$dto->getAroma());
            $query->bindParam(7,$dto->getColor());
            $query->bindParam(8,$dto->getConcentracion());
            $query->bindParam(9,$dto->getPresentacionDetalles());
            $query->bindParam(10,$dto->getDescuento());
            $query->bindParam(11,$dto->getCantidadDespacho());
            $query->execute();
            $updateCotizacion= $conexion->prepare("update cotizaciones set ValorTotalCotizacion=ValorTotalCotizacion+?
                                                   where IdCotizacion=?");
            $updateCotizacion->bindParam(1,$dto->getTotal());
            $updateCotizacion->bindParam(2,$dto->getIdCotizacion());
            $updateCotizacion->execute();
            $this->mensaje= "Cotización registrada exitosamente";
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }

        $conexion=null;
        return $this->mensaje;
    }



    public function listarCotizaciones(PDO $cnn,$user){

        if($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador'){
            try{
                $query = $cnn->prepare("Select * from cotizaciones join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        where cotizaciones.EstadoCotizacion='Vigente' and cotizaciones.CedulaEmpleadoCotizaciones=?
                                        group by cotizaciones.IdCotizacion");
                $query->bindParam(1,$user);
                $query->execute();
                $_SESSION['conteo'] = $query->rowCount();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex) {
                $this->mensaje = $ex->getMessage();
            }
        }else{

            try{
                $query = $cnn->prepare("Select * from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                        and cotizaciones.CedulaEmpleadoCotizaciones=?
                                        group by cotizaciones.IdCotizacion");
                $query->bindParam(1,$user);
                $query->execute();
                $_SESSION['conteo'] = $query->rowCount();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex) {
                $this->mensaje = $ex->getMessage();
            }
        }
        $cnn=null;
        return $this->mensaje;

    }
    public function traerCotizacionPedido($idPedido,PDO $cnn){
        try{
            $query = $cnn->prepare("Select * from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idpresentacion=detallescotizacion.idPresentacionDetalles and valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                        join pedidos on pedidos.IdCotizacionPedidos=cotizaciones.IdCotizacion
                                        join personas on personas.CedulaPersona=cotizaciones.CedulaEmpleadoCotizaciones
                                        where pedidos.IdPedido=?");
            $query->bindParam(1,$idPedido);
            $query->execute();
            $this->mensaje= $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;

    }


    public function traerDespacho($idPedido,PDO $cnn){
        try{
            $query = $cnn->prepare("Select * from cotizaciones
join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
join presentaciones on presentaciones.IdPresentacion=detallescotizacion.idPresentacionFacturacion
join pedidos on pedidos.IdCotizacionPedidos=cotizaciones.IdCotizacion
where pedidos.IdPedido=? ");
            $query->bindParam(1,$idPedido);
            $query->execute();
            $this->mensaje= $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;

    }


    public function buscarCotizacion($id,PDO $cnn){
        try{
            $query = $cnn->prepare("Select * from cotizaciones
                                    join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                    join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                    join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                    join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                    join valorpresentacion on valorpresentacion.idpresentacion=detallescotizacion.idPresentacionDetalles
                                    AND valorpresentacion.idProducto=productos.IdProducto
                                    join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                    join personas on personas.CedulaPersona=cotizaciones.CedulaEmpleadoCotizaciones
                                    where cotizaciones.IdCotizacion=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje= $query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje = $ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarCotizacionCriterio($criterio, $busqueda, $comobuscar,PDO $cnn){
        switch ($comobuscar){
            case 1:
                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try {
                        $query = $cnn->prepare('
                  Select cotizaciones.*, clientes.*, empleados.*, personas.* from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join empleados on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado
                                        join personas on empleados.CedulaEmpleado=personas.CedulaPersona
                 join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                 join lugares on lugares.IdLugar=clientes.IdLugarCliente
                 join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                 join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                 join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                 where '.$criterio.'="'.$busqueda.'"group by cotizaciones.IdCotizacion
                 ORDER BY cotizaciones.FechaCreacionCotizacion DESC ');
                        $query->execute();
                        $_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&ex=' . $ex->getMessage() . '&encontrados=0';
                    };
                }elseif ($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador'){
                    try {
                        $query = $cnn->prepare('
                                        Select cotizaciones.*, clientes.*, empleados.*, personas.* from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join empleados on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado
                                        join personas on empleados.CedulaEmpleado=personas.CedulaPersona

                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                            where '.$criterio.'="'.$busqueda.'" and cotizaciones.CedulaEmpleadoCotizaciones='.$_SESSION['datosLogin']['id'].'
                                            group by cotizaciones.IdCotizacion
                                            ORDER BY cotizaciones.FechaCreacionCotizacion DESC ');
                        $query->execute();
                        $_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&ex=' . $ex->getMessage() . '&encontrados=0';
                    };
                }

                break;

            case 2:

                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try{
                        $query = $cnn->prepare('
                                            Select cotizaciones.*, clientes.*, empleados.*, personas.* from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join empleados on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado
                                        join personas on empleados.CedulaEmpleado=personas.CedulaPersona
                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                            where ' . $criterio . ' like "%' . $busqueda . '%"
                                            group by cotizaciones.IdCotizacion
                                            ORDER BY cotizaciones.FechaCreacionCotizacion DESC ');
                        $query->execute();
                        $_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex){
                        $this->mensaje='&ex='.$ex->getMessage().'&encontrados=0';
                    };
                }elseif($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador'){
                    try{
                        $query = $cnn->prepare('
                                            Select cotizaciones.*, clientes.*, empleados.*, personas.* from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join empleados on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado
                                        join personas on empleados.CedulaEmpleado=personas.CedulaPersona
                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                            and ' . $criterio . ' like "%' . $busqueda . '%" and cotizaciones.CedulaEmpleadoCotizaciones='.$_SESSION['datosLogin']['id'].'
                                            group by cotizaciones.IdCotizacion
                                            ORDER BY cotizaciones.FechaCreacionCotizacion DESC ');
                        $query->execute();
                        $_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex){
                        $this->mensaje='&ex='.$ex->getMessage().'&encontrados=0';
                    };
                }
                break;

        }
        $cnn=null;
        return $this->mensaje;

    }

    public function cancelarCoti($user,PDO $cnn){
        try{
            $query=$cnn->prepare("Update cotizaciones set EstadoCotizacion='Cancelada' where IdCotizacion=?");
            $query->bindParam(1,$user);
            $query->execute();
            return "Cotización cancelada exitosamente";
        }catch (Exception $ex){
            return $mensaje=$ex->getMessage();
        }

    }


    public function listarTodas(PDO $cnn){

        if($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador'){
            try{
                $query = $cnn->prepare('
                                        Select cotizaciones.*, clientes.*, empleados.*, personas.* from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join empleados on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado
                                        join personas on empleados.CedulaEmpleado=personas.CedulaPersona
                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                        and cotizaciones.CedulaEmpleadoCotizaciones='.$_SESSION['datosLogin']['id'].'
                                        group by cotizaciones.IdCotizacion
                                        ORDER BY cotizaciones.FechaCreacionCotizacion DESC
                                        ');
                $query->execute();
                $_SESSION['conteo'] = $query->rowCount();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex) {
                $this->mensaje = $ex->getMessage();
            }
        }elseif($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
            try{
                $query = $cnn->prepare("Select cotizaciones.*, clientes.*, empleados.*, personas.* from cotizaciones
                                        join clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                        join empleados on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado
                                        join personas on empleados.CedulaEmpleado=personas.CedulaPersona
                                        join detallescotizacion on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
                                        join lugares on lugares.IdLugar=clientes.IdLugarCliente
                                        join productos on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                        join valorpresentacion on valorpresentacion.idProducto=productos.IdProducto
                                        join presentaciones on presentaciones.IdPresentacion=valorpresentacion.idpresentacion
                                        group by cotizaciones.IdCotizacion
                                        ORDER BY cotizaciones.FechaCreacionCotizacion DESC
                                        ");
                $query->execute();
                $_SESSION['conteo'] = $query->rowCount();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex) {
                $this->mensaje = $ex->getMessage();
            }
        }
        $cnn=null;
        return $this->mensaje;

    }

}