<?php

/**
 * Created by PhpStorm.
 * User: strahm
 * Date: 4/11/15
 * Time: 2:39 PM
 */
include_once'../utilities/Conexion.php';
class dao
{


    public function listarTodas(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = 'select *,sum(pedidos.ValorTotal) as "ventas" from pedidos
                                  join cotizaciones on IdCotizacionPedidos=cotizaciones.IdCotizacion
                                  join empleados on empleados.CedulaEmpleado=cotizaciones.CedulaEmpleadoCotizaciones
                                  join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                  group by personas.CedulaPersona';
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }


    public function actividadesPorAsesor(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select *,count(pedidos.IdPedido) as 'pedidos',count(cotizaciones.IdCotizacion) as 'cotizaciones',
                                  count(gestiones.IdGestion) as 'gestiones'from pedidos
                                  join cotizaciones on IdCotizacionPedidos=cotizaciones.IdCotizacion
                                  join empleados on empleados.CedulaEmpleado=cotizaciones.CedulaEmpleadoCotizaciones
                                  join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                  join gestiones on gestiones.CedulaEmpleadoGestiones=empleados.CedulaEmpleado
                                  group by personas.CedulaPersona";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }

    public function asesoresConActividades(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select personas.* from pedidos
                                  join cotizaciones on IdCotizacionPedidos=cotizaciones.IdCotizacion
                                  join empleados on empleados.CedulaEmpleado=cotizaciones.CedulaEmpleadoCotizaciones
                                  join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                  join gestiones on gestiones.CedulaEmpleadoGestiones=empleados.CedulaEmpleado
                                  group by personas.CedulaPersona";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }

    public function pedidos(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select count(pedidos.IdPedido) as 'pedidos'from pedidos
                                  join cotizaciones on IdCotizacionPedidos=cotizaciones.IdCotizacion
                                  join empleados on empleados.CedulaEmpleado=cotizaciones.CedulaEmpleadoCotizaciones
                                  join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                  group by personas.CedulaPersona";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }

    public function cotizaciones(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select count(cotizaciones.IdCotizacion) as 'cotizaciones'from cotizaciones
                                  join empleados on empleados.CedulaEmpleado=cotizaciones.CedulaEmpleadoCotizaciones
                                  join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                  group by personas.CedulaPersona";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }

    public function gestiones(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select count(gestiones.IdGestion) as 'gestiones'from gestiones
                                  join empleados on empleados.CedulaEmpleado=gestiones.CedulaEmpleadoGestiones
                                  join personas on personas.CedulaPersona=empleados.CedulaEmpleado
                                  group by personas.CedulaPersona";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }


    public function traerProductos(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select productos.NombreProducto from productos
                                  join detallescotizacion on productos.IdProducto=detallescotizacion.IdProductoDetallesCotizacion
                                  join cotizaciones on cotizaciones.IdCotizacion=detallescotizacion.IdCotizacionDetallesCotizacion
                                  where cotizaciones.EstadoCotizacion='Pedido'
                                  group by productos.IdProducto";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }


    public function cantidadProductosVendidos(){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select sum(detallescotizacion.cantidadFacturacion) as CantidadVendidos, productos.IdProducto, presentaciones.NombrePresentacion, productos.NombreProducto
FROM detallescotizacion
join productos on detallescotizacion.IdProductoDetallesCotizacion=productos.IdProducto
join valorpresentacion on productos.IdProducto=valorpresentacion.idProducto
join presentaciones on valorpresentacion.idpresentacion=presentaciones.IdPresentacion
join cotizaciones on detallescotizacion.IdCotizacionDetallesCotizacion=cotizaciones.IdCotizacion
join pedidos on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
and detallescotizacion.idPresentacionDetalles=presentaciones.IdPresentacion
and detallescotizacion.idPresentacionDetalles=1
and pedidos.EstadoPedido!='Cancelado'
GROUP by productos.IdProducto
order by CantidadVendidos Desc
LIMIT 10";
            $query = $cnn->prepare($listarActividades);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }

    public function ventasAsesores($fechaMin,$fechaMax){
        $cnn = Conexion::getConexion();

        try{
            $listarActividades = "select sum(pedidos.ValorTotal) as TotalVendido, personas.Nombres from personas join empleados on personas.CedulaPersona = empleados.CedulaEmpleado join cotizaciones on cotizaciones.CedulaEmpleadoCotizaciones = empleados.CedulaEmpleado join pedidos on cotizaciones.IdCotizacion = pedidos.IdCotizacionPedidos and pedidos.EstadoPedido!='Cancelado' and pedidos.FechaElaboracionPedido BETWEEN ? and ? GROUP by personas.CedulaPersona";
            $query = $cnn->prepare($listarActividades);
            $query->bindParam(1,$fechaMin);
            $query->bindParam(2,$fechaMax);
            $query->execute();
            return $query->fetchAll();
        } catch (Exception $ex) {
            echo 'Error :'.$ex->getMessage();
        }
        $cnn=null;
    }









}
