<?php

class AreasClienteDao
{
    public function registrarArea(AreasClienteDto $areasClienteDto, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("INSERT INTO areascliente VALUES(?,?,?,?,?,?)");
            $query->bindParam(1, $areasClienteDto->getNombreArea());
            $query->bindParam(2, $areasClienteDto->getNitCliente());
            $query->bindParam(3, $areasClienteDto->getNombreContactoArea());
            $query->bindParam(4, $areasClienteDto->getTelefonoArea());
            $query->bindParam(5, $areasClienteDto->getEmailArea());
            $query->bindParam(6, $areasClienteDto->getObservacionesArea());
            $query->execute();
            $mensaje="Información registrada con éxito en la base de datos.&error=false";
        } catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La información NO ha sido registrada en la base de datos.';
        }
        $cnn = null;
        return $mensaje;
    }

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT areascliente.*, clientes.* from areascliente join clientes
                                    on clientes.Nit = areascliente.nitClienteAreas
                                    ");
            $query->execute();
            $mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $mensaje= '&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function obtenerArea($nombreArea, $nitEmpresa, PDO $cnn){
        try {
            $query = $cnn->prepare("SELECT areascliente.*, clientes.* from areascliente join clientes
                                    on clientes.Nit = areascliente.nitClienteAreas
                                    AND areascliente.nombreAreas=? and areascliente.nitClienteAreas=?
                                    ");
            $query->bindParam(1, $nombreArea);
            $query->bindParam(2, $nitEmpresa);
            $query->execute();
            $mensaje=$query->fetch();
        } catch (Exception $ex) {
            $mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function buscarArea($criterio, $busqueda, $comobuscar, PDO $cnn){
        switch ($comobuscar) {
            case 1:
                try{
                    $query = $cnn->prepare('SELECT areascliente.*, clientes.* from areascliente join clientes
                                    on clientes.Nit = areascliente.nitClienteAreas
                                    and '.$criterio.' = "'.$busqueda.'"');
                    $query->execute();
                    $mensaje=$query->fetchAll();
                } catch (Exception $ex){
                    $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                };
                break;
            case 2;
                try{
                    $query = $cnn->prepare('SELECT areascliente.*, clientes.* from areascliente join clientes
                                    on clientes.Nit = areascliente.nitClienteAreas
                                    and '.$criterio.' like "%'.$busqueda.'%"');
                    $query->execute();
                    $mensaje=$query->fetchAll();
                } catch (Exception $ex){
                    $mensaje='&detalleerror='.$ex->getMessage().'&encontrados=0';
                };
                break;
            default:
                $mensaje='Opción inválida para Cómo búscar';
        }
        $cnn=null;
        return $mensaje;
    }

    public function modificarArea(AreasClienteDto $areasClienteDto, PDO $cnn){
        $mensaje = "";
        try{
            $query = $cnn->prepare("UPDATE areascliente set
                                    areascliente.nombreAreas=?,
                                    areascliente.nitClienteAreas=?,
                                    areascliente.nombreContactoAreas=?,
                                    areascliente.telefonoArea=?,
                                    areascliente.emailArea=?,
                                    areascliente.observacionesArea=?
                                    WHERE areascliente.nitClienteAreas=?
                                    and areascliente.nombreAreas=?
                                    ");
            $query->bindParam(1, $areasClienteDto->getNombreArea());
            $query->bindParam(2, $areasClienteDto->getNitCliente());
            $query->bindParam(3, $areasClienteDto->getNombreContactoArea());
            $query->bindParam(4, $areasClienteDto->getTelefonoArea());
            $query->bindParam(5, $areasClienteDto->getEmailArea());
            $query->bindParam(6, $areasClienteDto->getObservacionesArea());
            $query->bindParam(7, $areasClienteDto->getNitAntiguo());
            $query->bindParam(8, $areasClienteDto->getNombreAntiguo());
            $query->execute();
            $mensaje="Información actualizada con éxito.&error=false";
        } catch (Exception $ex){
            $mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La información NO ha sido actualizada en la base de datos.';
        }
        $cnn = null;
        return $mensaje;
    }





}