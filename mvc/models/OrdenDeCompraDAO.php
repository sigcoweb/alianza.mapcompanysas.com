<?php

class OrdenDeCompraDAO {
    private $mensaje;
    public function registrarOrden(OrdenesDeCompraDTO $dto,PDO $cnn){
        try {
            $query = $cnn->prepare("INSERT INTO pedidos VALUES (DEFAULT,?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?)");
            $query->bindParam(1, $dto->getCotizacionId());
            $query->bindParam(2, $dto->getEstado());
            $query->bindParam(3, $dto->getValorTotal());
            $query->bindParam(4, $dto->getObservaciones());
            $query->bindParam(5, $dto->getRemisionado());
            $query->bindParam(6, $dto->getFacturado());
            $query->bindParam(7, $dto->getCertificado());
            $query->bindParam(8, $dto->getFichaTecnica());
            $query->bindParam(9, $dto->getFichaSeguridad());
            $query->bindParam(10, $dto->getDireccion());

            $query->execute();
            if ($dto->getOrdenCompraCliente()!=null){
                $query2 = $cnn->prepare('SELECT MAX(IdPedido) AS "idp" FROM pedidos');
                $query2->execute();
                $pedido=$query2->fetch();

                $query3 = $cnn->prepare("INSERT INTO ordencompracliente VALUES (?,?)");
                $query3->bindParam(1, $pedido['idp']);
                $query3->bindParam(2, $dto->getOrdenCompraCliente());
                $query3->execute();
            }
            $estado= $cnn->prepare('Update cotizaciones set EstadoCotizacion="Pedido" where IdCotizacion=?');
            $estado->bindParam(1,$dto->getCotizacionId());
            $estado->execute();
            $this->mensaje= "Orden de compra registrada, Cotizacion cambiada&error=false";
        }catch (Exception $ex){
            $this->mensaje = '&detalleerror='.$ex->getMessage().'&error=true&mensaje=La orden de compra NO pudo ser registrada';
        }
        $cnn = null;
        return $this->mensaje;
    }

    public function listarOrdenes(PDO $cnn){
        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
            try{
                $query = $cnn->prepare("Select * from pedidos JOIN cotizaciones on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                            JOIN clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                            ORDER BY pedidos.FechaElaboracionPedido DESC ");
                $query->execute();
                $_SESSION['conteo'] = $query->rowCount();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex){
                $this->mensaje='&ex='.$ex->getMessage().'&encontrados=0$error=true';
            };
        }elseif($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador'){
            try{
                $query = $cnn->prepare('Select * from pedidos JOIN cotizaciones on
                                      cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                            JOIN clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                            AND cotizaciones.CedulaEmpleadoCotizaciones='.$_SESSION['datosLogin']['id'].'
                                            ORDER BY pedidos.FechaElaboracionPedido DESC');
                $query->execute();
                $_SESSION['conteo'] = $query->rowCount();
                $this->mensaje=$query->fetchAll();
            } catch (Exception $ex){
                $this->mensaje='&ex='.$ex->getMessage().'&encontrados=0$error=true';
            };
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarCotizacionCriterio($criterio, $busqueda, $comobuscar,PDO $cnn){
        switch ($comobuscar) {
            case 1:
                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try {
                        $query = $cnn->prepare('Select * from pedidos JOIN cotizaciones on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                            JOIN clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                            where '.$criterio.'="'.$busqueda.'"
                                            ORDER BY pedidos.FechaElaboracionPedido DESC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                    };
                }elseif($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador') {
                    try {
                        $query = $cnn->prepare('Select * from pedidos JOIN cotizaciones on cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                            JOIN clientes on clientes.Nit=cotizaciones.NitClienteCotizaciones
                                            where '.$criterio.'="'.$busqueda.'"
                                            AND cotizaciones.CedulaEmpleadoCotizaciones='.$_SESSION['datosLogin']['id'].'
                                            ORDER BY pedidos.FechaElaboracionPedido DESC');
                        $query->execute();
                        //$_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje=$query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                    };
                }
                break;

            case 2:
                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                    try {
                        $query = $cnn->prepare('SELECT * FROM pedidos JOIN cotizaciones ON
                                            cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                            JOIN clientes ON clientes.Nit=cotizaciones.NitClienteCotizaciones
                                            WHERE ' . $criterio . ' LIKE "%' . $busqueda . '%"
                                            ORDER BY pedidos.FechaElaboracionPedido DESC');
                        $query->execute();
                        $_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&ex=' . $ex->getMessage() . '&encontrados=0';
                    };
                }elseif($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador') {
                    try {
                        $query = $cnn->prepare('SELECT * FROM pedidos JOIN cotizaciones ON
                                            cotizaciones.IdCotizacion=pedidos.IdCotizacionPedidos
                                            JOIN clientes ON clientes.Nit=cotizaciones.NitClienteCotizaciones
                                            WHERE ' . $criterio . ' LIKE "%' . $busqueda . '%"
                                            AND cotizaciones.CedulaEmpleadoCotizaciones='.$_SESSION['datosLogin']['id'].'
                                            ORDER BY pedidos.FechaElaboracionPedido DESC');
                        $query->execute();
                        $_SESSION['conteo'] = $query->rowCount();
                        $this->mensaje = $query->fetchAll();
                    } catch (Exception $ex) {
                        $this->mensaje = '&ex=' . $ex->getMessage() . '&encontrados=0';
                    };
                }
                break;
        }
        $cnn=null;
        return $this->mensaje;
    }


    public function cancelarOrden($user,PDO $cnn){
        try{
            $query=$cnn->prepare("UPDATE pedidos SET EstadoPedido = 'Cancelado' WHERE pedidos.IdPedido = ?");
            $query->bindParam(1,$user);
            $query->execute();
            $mensaje="Pedido cancelado exitosamente";
        }catch (Exception $ex){
            $mensaje='Error '. $ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function autorizarOrden($user,PDO $cnn){
        try{
            $query=$cnn->prepare("UPDATE pedidos SET EstadoPedido = 'Autorizado' WHERE pedidos.IdPedido = ?");
            $query->bindParam(1,$user);
            $query->execute();
            $mensaje="Pedido autorizado exitosamente";
        }catch (Exception $ex){
            $mensaje='Error '. $ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function actualizarPresentacionDespacho($producto,$presentacion,PDO $cnn){
        try{
            $query=$cnn->prepare("UPDATE detallescotizacion SET idPresentacionFacturacion = ? WHERE IdProductoDetallesCotizacion = ?");
            $query->bindParam(1,$presentacion);
            $query->bindParam(2,$producto);
            $query->execute();
            $mensaje="Facturacion modificada";
        }catch (Exception $ex){
            $mensaje='Error '. $ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }

    public function buscarOrdenCliente($pedido,PDO $cnn){
        try{
            $query=$cnn->prepare("select * from ordencompracliente where ordencompracliente.idPedido=?");
            $query->bindParam(1,$pedido);
            $query->execute();
            $mensaje=$query->fetch();
        }catch (Exception $ex){
            $mensaje='Error '. $ex->getMessage();
        }
        $cnn=null;
        return $mensaje;
    }


}