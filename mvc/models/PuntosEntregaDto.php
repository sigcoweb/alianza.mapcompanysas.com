<?php

class PuntosEntregaDto
{

    private $nombrePuntoEntrega = "";
    private $nombrePuntoEntregaAntiguo = "";
    private $direccionPuntoEntrega = "";
    private $nombreContactoPuntoEntrega = "";
    private $correoPuntoEntrega = "";
    private $telefonoPuntoEntrega = 0;
    private $nitEmpresa = 0;
    private $idLugarPuntoEntrega = 0;
    private $observacionesPuntoEntrega = "";
    

    /**
     * PuntosEntregaDto constructor.
     * @param string $nombrePuntoEntrega
     * @param string $direccionPuntoEntrega
     * @param string $nombreContactoPuntoEntrega
     * @param string $correoPuntoEntrega
     * @param int $telefonoPuntoEntrega
     * @param int $nitEmpresa
     * @param int $idLugarPuntoEntrega
     * @param int $observacionesPuntoEntrega
     */
    public function __construct($nombrePuntoEntrega, $direccionPuntoEntrega, $nombreContactoPuntoEntrega,
                                $correoPuntoEntrega, $telefonoPuntoEntrega, $nitEmpresa, $idLugarPuntoEntrega,
                                $observacionesPuntoEntrega)
    {
        $this->nombrePuntoEntrega = $nombrePuntoEntrega;
        $this->direccionPuntoEntrega = $direccionPuntoEntrega;
        $this->nombreContactoPuntoEntrega = $nombreContactoPuntoEntrega;
        $this->correoPuntoEntrega = $correoPuntoEntrega;
        $this->telefonoPuntoEntrega = $telefonoPuntoEntrega;
        $this->nitEmpresa = $nitEmpresa;
        $this->idLugarPuntoEntrega = $idLugarPuntoEntrega;
        $this->observacionesPuntoEntrega = $observacionesPuntoEntrega;
    }


    /**
     * @return string
     */
    public function getNombrePuntoEntregaAntiguo()
    {
        return $this->nombrePuntoEntregaAntiguo;
    }

    /**
     * @param string $nombrePuntoEntregaAntiguo
     */
    public function setNombrePuntoEntregaAntiguo($nombrePuntoEntregaAntiguo)
    {
        $this->nombrePuntoEntregaAntiguo = $nombrePuntoEntregaAntiguo;
    }

    /**
     * @return string
     */
    public function getNombrePuntoEntrega()
    {
        return $this->nombrePuntoEntrega;
    }

    /**
     * @param string $nombrePuntoEntrega
     */
    public function setNombrePuntoEntrega($nombrePuntoEntrega)
    {
        $this->nombrePuntoEntrega = $nombrePuntoEntrega;
    }

    /**
     * @return string
     */
    public function getDireccionPuntoEntrega()
    {
        return $this->direccionPuntoEntrega;
    }

    /**
     * @param string $direccionPuntoEntrega
     */
    public function setDireccionPuntoEntrega($direccionPuntoEntrega)
    {
        $this->direccionPuntoEntrega = $direccionPuntoEntrega;
    }

    /**
     * @return string
     */
    public function getNombreContactoPuntoEntrega()
    {
        return $this->nombreContactoPuntoEntrega;
    }

    /**
     * @param string $nombreContactoPuntoEntrega
     */
    public function setNombreContactoPuntoEntrega($nombreContactoPuntoEntrega)
    {
        $this->nombreContactoPuntoEntrega = $nombreContactoPuntoEntrega;
    }

    /**
     * @return string
     */
    public function getCorreoPuntoEntrega()
    {
        return $this->correoPuntoEntrega;
    }

    /**
     * @param string $correoPuntoEntrega
     */
    public function setCorreoPuntoEntrega($correoPuntoEntrega)
    {
        $this->correoPuntoEntrega = $correoPuntoEntrega;
    }

    /**
     * @return int
     */
    public function getTelefonoPuntoEntrega()
    {
        return $this->telefonoPuntoEntrega;
    }

    /**
     * @param int $telefonoPuntoEntrega
     */
    public function setTelefonoPuntoEntrega($telefonoPuntoEntrega)
    {
        $this->telefonoPuntoEntrega = $telefonoPuntoEntrega;
    }

    /**
     * @return int
     */
    public function getNitEmpresa()
    {
        return $this->nitEmpresa;
    }

    /**
     * @param int $nitEmpresa
     */
    public function setNitEmpresa($nitEmpresa)
    {
        $this->nitEmpresa = $nitEmpresa;
    }

    /**
     * @return int
     */
    public function getIdLugarPuntoEntrega()
    {
        return $this->idLugarPuntoEntrega;
    }

    /**
     * @param int $idLugarPuntoEntrega
     */
    public function setIdLugarPuntoEntrega($idLugarPuntoEntrega)
    {
        $this->idLugarPuntoEntrega = $idLugarPuntoEntrega;
    }

    /**
     * @return int
     */
    public function getObservacionesPuntoEntrega()
    {
        return $this->observacionesPuntoEntrega;
    }

    /**
     * @param int $observacionesPuntoEntrega
     */
    public function setObservacionesPuntoEntrega($observacionesPuntoEntrega)
    {
        $this->observacionesPuntoEntrega = $observacionesPuntoEntrega;
    }

}