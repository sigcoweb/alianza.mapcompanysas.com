<?php

class ColoresDao
{
private $mensaje="";

    public function listarTodos(PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT * FROM colores");
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function buscarConCriterio($criterio, $busqueda, $comobuscar, PDO $cnn)
    {
        switch ($comobuscar) {
            case 1:
                try {
                    $query = $cnn->prepare("Select colores.*, coloresproductos.*, productos.*
                                          FROM colores JOIN coloresproductos
                                          ON colores.idColor = coloresproductos.idColorColoresProd
                                          JOIN productos
                                          ON coloresproductos.idProductoColoresProd = productos.IdProducto
                                          WHERE $criterio='$busqueda' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            case 2:
                try {
                    $query = $cnn->prepare("Select colores.*, coloresproductos.*, productos.*
                                          FROM colores JOIN coloresproductos
                                          ON colores.idColor = coloresproductos.idColorColoresProd
                                          JOIN productos
                                          ON coloresproductos.idProductoColoresProd = productos.IdProducto
                                            where $criterio like '%$busqueda%' ");
                    $query->execute();
                    $this->mensaje=$query->fetchAll();
                } catch (Exception $ex) {
                    $this->mensaje='&ex=' . $ex->getMessage() . '&encontrados=0';
                };
                break;
            default:
                $this->mensaje='No se han recibido parámetros correctos';
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function coloresProducto($id,PDO $cnn){
        try{
            $query = $cnn->prepare("select colores.* from productos
                                    join coloresproductos on coloresproductos.idProductoColoresProd=productos.IdProducto
                                    join colores on colores.idColor=coloresproductos.idColorColoresProd
                                    where productos.IdProducto=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetchAll();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }

    public function traerColor($id,PDO $cnn){
        try{
            $query = $cnn->prepare("SELECT * FROM colores where idColor=?");
            $query->bindParam(1,$id);
            $query->execute();
            $this->mensaje=$query->fetch();
        } catch (Exception $ex) {
            $this->mensaje='&detalleerror='.$ex->getMessage();
        }
        $cnn=null;
        return $this->mensaje;
    }





}