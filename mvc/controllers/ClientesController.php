<?php
include_once '../models/ClientesDto.php';
include_once '../models/PuntosEntregaDto.php';
include_once '../facades/ClienteFacade.php';
include_once '../facades/PuntosEntregaFacade.php';
session_start();
$puntoFacha=new PuntosEntregaFacade();
$fachada = new ClienteFacade();
if(isset($_GET['controlar'])) {
    $accion = $_GET['controlar'];
    switch ($accion) {
        case 'activarVarios':
            $siza=0;
            $paila=0;
            foreach($_POST['activame'] as $uno){
                $fachada->cambiarEstado($uno);
                $cli=$fachada->buscarCliente('clientes.Nit',$uno,1);
                if($cli['0']['estadoCliente']=='Activo'){
                    $siza++;
                }else{
                    $paila++;
                }
            }
            if($siza>0){
                $menso='Se ha(n) activado exitosamente '.$siza.' cliente(s).';
            }
            if($paila>0){
                $menso=$menso.'No ha(n) sido activado(s) '.$paila.' cliente(s).&error=true';
            }
            header("Location: ../views/activarClientes.php?mensaje=".$menso);
            break;
        case 'crear':
            if(!isset($_POST['activarCliente'])){
                $_POST['activarCliente']="Inactivo";
            }else{
                $_POST['activarCliente']="Activo";
            }
            if(!isset($_POST['IdClasificacion'])){
                $_POST['IdClasificacion']=1;
            }
            $clienteDto = new ClientesDto($_POST['Cedula'], $_POST['Nombres'], $_POST['Apellidos'],
                mb_strtolower($_POST['Email1']), $_POST['Celular'],
                $_POST['Nit'], $_POST['RazonSocial'], $_POST['Direccion'],
                $_POST['Telefono'], mb_strtolower($_POST['Email2']),
                $_POST['IdTipo'], $_POST['IdActividad'], $_POST['IdLugar'], $_POST['activarCliente']
            );
            $mensajePunto="";
            $clienteDto->setDiaCierre($_POST['diaCierreFacturacion']);
            $clienteDto->setPlazoPago($_POST['plazoPago']);
            $clienteDto->setIdClasificacion($_POST['IdClasificacion']);
            $clienteDto->setNombreComercial(ucwords(strtolower($_POST['NombreComercial'])));
            $mensaje = $fachada->registrarCliente($clienteDto);
            if($_POST['puntoEntrega']){
                $punto=new PuntosEntregaDto($_POST['nombrePunto'],$_POST['Direccion'],$_POST['nombreContactoPunto'],
                    $_POST['correoPunto'],$_POST['telefonoPunto'],$_POST['Nit'],$_POST['IdLugar'],
                    $_POST['observacionesPunto']);
                $mensajePunto=$puntoFacha->registrarPunto($punto);
            }
            header("Location: ../views/buscarClientes.php?mensaje=".$mensajePunto.$mensaje);
            break;
        case 'crearSoloEmpresa':
            if(!isset($_POST['activarCliente'])){
                $_POST['activarCliente']="Inactivo";
            }else{
                $_POST['activarCliente']="Activo";
            }
            if(!isset($_POST['IdClasificacion'])){
                $_POST['IdClasificacion']=1;
            }
            $clienteDto = new ClientesDto($_POST['Cedula'], $_POST['Nombres'], $_POST['Apellidos'],
                mb_strtolower($_POST['Email1']), $_POST['Celular'],
                $_POST['Nit'], $_POST['RazonSocial'], $_POST['Direccion'],
                $_POST['Telefono'], mb_strtolower($_POST['Email2']),
                $_POST['IdTipo'], $_POST['IdActividad'], $_POST['IdLugar'], $_POST['activarCliente']
            );
            $mensajePunto="";
            $clienteDto->setDiaCierre($_POST['diaCierreFacturacion']);
            $clienteDto->setPlazoPago($_POST['plazoPago']);
            $clienteDto->setIdClasificacion($_POST['IdClasificacion']);
            $clienteDto->setNombreComercial(ucwords(strtolower($_POST['NombreComercial'])));
            $mensaje = $fachada->registrarSoloEmpresa($clienteDto);
            if($_POST['puntoEntrega']){
                $punto=new PuntosEntregaDto($_POST['nombrePunto'],$_POST['Direccion'],
                    $_POST['nombreContactoPunto'],$_POST['correoPunto'],$_POST['telefonoPunto'],$_POST['Nit'],
                    $_POST['IdLugar'],$_POST['observacionesPunto']);
                $mensajePunto=$puntoFacha->registrarPunto($punto);
            }
            header("Location: ../views/buscarClientes.php?mensaje=".$mensaje.$mensajePunto);
            break;
        case 'modificar':
            if(!isset($_POST['activarCliente'])) {
                $cliente = $fachada->obtenerCliente($_GET['IdCliente']);
                $_POST['activarCliente']=$cliente['estadoCliente'];
            }
            if(!isset($_POST['IdClasificacion'])) {
                $clasificacion = $fachada->obtenerCliente($_GET['IdCliente']);
                $_POST['IdClasificacion']=$clasificacion['IdClasificacionCliente'];
            }
            if (isset($_POST['reestablecerContrasenia']) && $_POST['reestablecerContrasenia'] == 'on') {
                $contrasenia = MD5($_POST['Cedula']);
            } else {
                $person = $fachada->obtenerPersona($_POST['Cedula']);
                $contrasenia=$person['Contrasenia'];
            };
            $clienteDto = new ClientesDto($_POST['Cedula'], $_POST['Nombres'], $_POST['Apellidos'],
                mb_strtolower($_POST['Email1']), $_POST['Celular'],
                $_POST['Nit'], $_POST['RazonSocial'], $_POST['Direccion'],
                $_POST['Telefono'], mb_strtolower($_POST['Email2']),
                $_POST['IdTipo'], $_POST['IdActividad'], $_POST['IdLugar'], $_POST['activarCliente']
            );
            $clienteDto->setDiaCierre($_POST['diaCierreFacturacion']);
            $clienteDto->setPlazoPago($_POST['plazoPago']);
            $clienteDto->setNombreComercial($_POST['nombreComercial']);
            $clienteDto->setIdClasificacion($_POST['IdClasificacion']);
            $clienteDto->setIdCliente($_GET['IdCliente']);
            $clienteDto->setIdPersona($_GET['IdPersona']);
            $clienteDto->setContrasenia($contrasenia);
            $mensaje = $fachada->modificarCliente($clienteDto);
            header("Location: ../views/buscarClientes.php?mensaje=" . $mensaje);
            break;
        case 'buscar':
            $criterio = $_POST['criterio'];
            $busqueda = $_POST['busqueda'];
            $comobuscar = $_POST['comobuscar'];
            $mensaje = $fachada->buscarCliente($criterio, $busqueda, $comobuscar);
            if ($mensaje == null) {
                header("Location: ../views/buscarClientes.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
            } else {
                header("Location: ../views/buscarClientes.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
            };
            break;
        case 'buscarCedula':
            $mensaje = $fachada->buscarCedulaPersona($_POST['cedulaPersona']);
            if ($mensaje == null) {
                header("Location: ../views/buscarClienteNuevaEmpresa.php?encontrados=false&cedulaPersona=".$_POST['cedulaPersona']);
            } else {
                header("Location: ../views/nuevoClienteMismaPersona.php?encontrados=true&cedulaPersona=".$_POST['cedulaPersona']);
            };
            break;
        case 'buscarNit':
            $mensaje=$fachada->buscarCliente('clientes.Nit', $_POST['nit'],1);
            if ($mensaje == null) {
                header("Location: ../views/buscarClienteNuevoPuntoEntrega.php?encontrados=false&nit=".$_POST['nit']);
            } else {
                header("Location: ../views/nuevoPuntoEntrega.php?encontrados=true&IdCliente=".$mensaje['0']['IdCliente']);
            };
            break;
        case 'buscarNitNuevaArea':
            $mensaje=$fachada->buscarCliente('clientes.Nit', $_POST['nit'],1);
            if ($mensaje == null) {
                header("Location: ../views/nuevaAreaMismaEmpresa.php?encontrados=false&nit=".$_POST['nit']);
            } else {
                header("Location: ../views/nuevaArea.php?encontrados=true&IdCliente=".$mensaje['0']['IdCliente']);
            };
            break;
        case 'todos':
            $mensaje = $fachada->listarTodos();
            unset($_SESSION['consulta']);
            if ($mensaje == null) {
                header("Location: ../views/buscarClientes.php?encontrados=false");
            } else {
                header("Location: ../views/buscarClientes.php?encontrados=true&todos=todos");
            };
            break;
        default:
            echo 'Valor incorrecto enviado por el método get a la variable controlar';
    }
}


if(isset($_POST['idDetalleCliente'])){
    $mensaje = $fachada->obtenerCliente($_POST['idDetalleCliente']);
    echo json_encode($mensaje);
};

if(isset($_POST['existeNit'])){
    $mensaje = $fachada->existeNit($_POST['existeNit']);
    echo json_encode($mensaje);
};

if(isset($_POST['existeCedula'])){
    $mensaje = $fachada->existeCedula($_POST['existeCedula']);
    echo json_encode($mensaje);
};

if (isset ($_POST['reestablecerContrasenia'])){
    $msg=$fachada->reestablecerContrasenia($_POST['reestablecerContrasenia']);
    echo json_encode($msg);
}

if (isset ($_POST['cambiarEstado'])){
    $msg=$fachada->cambiarEstado($_POST['cambiarEstado']);
    print json_decode($msg);
}

if (isset ($_POST['cargarLugar'])){
    include_once '../facades/LugaresFacade.php';
    $facLug = new LugaresFacade();
    $msg=$facLug->listarPorDepto($_POST['idDepto']);
    echo json_encode($msg);
}