<?php

include_once '../models/EmpleadoDto.php';
include_once '../facades/FacadeEmpleado.php';
session_start();

$fachada = new FacadeEmpleado();
$dto= new EmpleadoDto();


if (isset($_POST['documento'])) {
    if($_POST['changepass']==1){
        $dto->setContrasenia(md5($_POST['pass1']));
    }else{
        $dto->setContrasenia(md5($_POST['documento']));
    }
    $dto->setIdUsuario($_POST['documento']);
    $dto->setNombres($_POST['nombres']);
    $dto->setApellidos($_POST['apellidos']);
    $dto->setEmpleo($_POST['cargo']);
    $dto->setEmail($_POST['email']);

    $dto->setEstado($_POST['estadoEmpleado']);

    if($_FILES['imagen']['size']>0){
        $file = $_FILES['imagen'];
        $name = $file['name'];
        $path = "../imgages/" . basename($name);;
        //Ruta de la original
        $rtOriginal = $file['tmp_name'];

//Crear variable de imagen a partir de la original
        $original = imagecreatefromjpeg($rtOriginal);
        list($ancho, $alto) = getimagesize($rtOriginal);

//Definir tamaño máximo y mínimo
        if ($ancho < $alto) {
            $max_alto = 200;
            $max_ancho = 250;
            echo 'entre';
        } else {
            $max_ancho = 200;
            $max_alto = 200;
        }

//Recoger ancho y alto de la original

//Calcular proporción ancho y alto
        $x_ratio = $max_ancho / $ancho;
        $y_ratio = $max_alto / $alto;


        if (($ancho <= $max_ancho) && ($alto <= $max_alto)) {
//Si es más pequeña que el máximo no redimensionamos
            $ancho_final = $ancho;
            $alto_final = $alto;
        } //si no calculamos si es más alta o más ancha y redimensionamos
        elseif (($x_ratio * $alto) < $max_alto) {
            $alto_final = ceil($x_ratio * $alto);
            $ancho_final = $max_ancho;
        } else {
            $ancho_final = ceil($y_ratio * $ancho);
            $alto_final = $max_alto;
        }

//Crear lienzo en blanco con proporciones
        $lienzo = imagecreatetruecolor($max_ancho, $max_alto);

//Copiar $original sobre la imagen que acabamos de crear en blanco ($tmp)
        imagecopyresampled($lienzo, $original, 0, 0, 0, 0, $max_ancho, $max_alto, $ancho, $alto);

//Limpiar memoria
        imagedestroy($original);

//Definimos la calidad de la imagen final
        $cal = 90;

//Se crea la imagen final en el directorio indicado
        imagejpeg($lienzo, "../imgages/" . $_POST['documento'] . $file['name'], $cal);
        // move_uploaded_file($file['tmp_name'], $path );



        $dto->setRutaimagen($name);
    }else{
        $dto->setRutaimagen('sinImagen.jpg');
    }


    $dto->setCelular($_POST['celular']);
    $dto->setFechaNacimiento($_POST['fechaNacimiento']);
    $dto->setIdLugar($_POST['IdLugar']);


    $mensaje= $fachada->registrarEmpleado($dto,$_POST['rol']);
    header('location: ../views/RegistrarEmpleado.php?mensaje='.$mensaje);



}

if (isset($_GET['buscar'])) {
    unset($_SESSION['consulta']);
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $fachada->buscarCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarEmpleado.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarEmpleado.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['controlar'])) {
    $resul = $fachada->listarUsuarios();
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarEmpleado.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarEmpleado.php?encontrados=true&todos=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['modificar'])) {

    if(isset($_POST['changepass'])&&$_POST['changepass']==1){
        $dto->setContrasenia(md5($_POST['pass1']));
    }else{
        include_once '../facades/FacadeEmpleado.php';
        $facaEmpl=new FacadeEmpleado();
        $empl=$facaEmpl->obtenerUsuario($_GET['idPersona']);
        $dto->setContrasenia($empl['Contrasenia']);
    };

    if(!isset($_POST['imagen'])){
        $dto->setRutaimagen('sinImagen.jpg');
    }else{
        $dto->setRutaimagen($_POST['imagen']);
    };

    $dto->setIdUsuario($_POST['cc']);
    $dto->setNombres($_POST['nombres']);
    $dto->setApellidos($_POST['apellidos']);
    $dto->setCelular($_POST['celular']);
    $dto->setEmpleo($_POST['cargo']);
    $dto->setEmail($_POST['email']);
    $dto->setEstado($_POST['estadoEmpleado']);
    $dto->setFechaNacimiento($_POST['fechaNacimiento']);
    $dto->setIdLugar($_POST['IdLugar']);

    $mensaje=$fachada->modificarUsuario($dto,$_GET['idPersona'],$_POST['rol']);
    header("Location: ../views/buscarEmpleado.php?mensaje=" . $mensaje);

}

if (isset($_GET['cambiarEstado'])) {
    $estado=$_GET['estado'];

    if ($estado=="Activo"){
        $estado="Inactivo";
    }else{
        $estado="Activo";
    }
    $mensaje=$fachada->cambiarEstado($_GET['id'],$estado);
    header("Location: ../views/buscarEmpleado.php?mensaje=" . $mensaje);
}



