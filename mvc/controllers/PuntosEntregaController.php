<?php
session_start();
include_once '../models/PuntosEntregaDto.php';
include_once '../facades/PuntosEntregaFacade.php';
$fachada = new PuntosEntregaFacade();
if(isset($_GET['controlar'])) {
    $accion = $_GET['controlar'];
    switch ($accion) {
        case 'crear':
            $puntoDto = new PuntosEntregaDto($_POST['nombrePuntoEntrega'],$_POST['direccionPuntoEntrega'],$_POST['nombreContactoPuntoEntrega'],
                $_POST['correoPuntoEntrega'],$_POST['telefonoPuntoEntrega'],$_POST['nitEmpresaPuntoEntrega'],$_POST['idLugarPuntoEntrega'],
                $_POST['observacionesPuntoEntrega']);
            $mensaje=$fachada->registrarPunto($puntoDto);
            header("Location: ../views/buscarPuntoEntrega.php?mensaje=".$mensaje);
            break;
        case 'modificar':
            $puntoDto = new PuntosEntregaDto($_POST['nombrePuntoEntrega'],$_POST['direccionPuntoEntrega'],$_POST['nombreContactoPuntoEntrega'],
                $_POST['correoPuntoEntrega'],$_POST['telefonoPuntoEntrega'],$_POST['nitEmpresaPuntoEntrega'],$_POST['idLugarPuntoEntrega'],
                $_POST['observacionesPuntoEntrega']);
            $puntoDto->setNombrePuntoEntregaAntiguo($_POST['nombrePuntoEntregaAntiguo']);
            $mensaje=$fachada->modificarPunto($puntoDto);
            header("Location: ../views/buscarPuntoEntrega.php?mensaje=".$mensaje);
            break;
        case 'buscar':
            $criterio = $_POST['criterio'];
            $busqueda = $_POST['busqueda'];
            $comobuscar = $_POST['comobuscar'];
            $mensaje = $fachada->buscarPunto($criterio, $busqueda, $comobuscar);
            if ($mensaje == null) {
                header("Location: ../views/buscarPuntoEntrega.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
            } else {
                header("Location: ../views/buscarPuntoEntrega.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
            }
            break;
        case 'todos':
            $mensaje = $fachada->listarTodos();
            if ($mensaje == null) {
                header("Location: ../views/buscarPuntoEntrega.php?encontrados=false");
            } else {
                header("Location: ../views/buscarPuntoEntrega.php?encontrados=true&todos=todos");
            }
            break;
        default:
            echo 'Valor incorrecto enviado por el método get a la variable controlar';
    }
}

if(isset($_POST['nombrePunto'])){
    $mensaje = $fachada->obtenerPunto($_POST['nombrePunto'],$_POST['nit']);
    $_SESSION['prueba']=$_POST['nombrePunto'].' '.$_POST['nit'];
    print json_encode($mensaje);
};


