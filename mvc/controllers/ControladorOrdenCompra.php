<?php
require'../models/OrdenesDeCompraDTO.php';
require'../facades/FacadeOrdenCompra.php';
session_start();
$dto= new OrdenesDeCompraDTO();
$facade= new FacadeOrdenCompra();
if (isset($_GET['crear'])) {
    $dto->setCotizacionId($_POST['idcoti']);
    $dto->setEstado("Pendiente");
    $dto->setValorTotal($_POST['valorpedido']);
    $dto->setObservaciones($_POST['observaciones']);
    $dto->setRemisionado($_POST['remisionado']);
    $dto->setFacturado($_POST['facturado']);
    $dto->setCertificado($_POST['certificado']);
    $dto->setFichaTecnica($_POST['fichaTecnica']);
    $dto->setFichaSeguridad($_POST['fichaSeguridad']);
    $dto->setDireccion($_POST['direccion']);

    if (isset($_POST['ordenDeCompra'])){
        $dto->setOrdenCompraCliente($_POST['ordenDeCompra']);
    }

    if (isset($_GET['change'])){
        $_SESSION['pedido']=$dto;
        header("Location: ../views/crearPedido.php?coti=".$_POST['idcoti']);
    }else{
        $mensaje=$facade->registrarOrden($dto);
       header("Location: ../views/buscarOrdenes.php?mensaje=" . $mensaje);
    }
}
if (isset($_GET['finalizar'])) {
    $cantidad=$_POST['cantidadPresentaciones'];
    $mensaje=$facade->registrarOrden($_SESSION['pedido']);
    unset($_SESSION['pedido']);
    for($i=0;$i<$cantidad;$i++){
        $test=$facade->actualizarPresentacionDespacho($_POST['idProducto'.$i],$_POST['nuevaPresentacion'.$i]);
    }
    header("Location: ../views/buscarOrdenes.php?mensaje=" . $mensaje);
}




if (isset($_GET['buscar'])) {
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];

        header("Location: ../views/buscarOrdenes.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);

}

if (isset($_GET['listar'])) {
        header("Location: ../views/buscarOrdenes.php?todos=todos");
}

if (isset($_GET['cancelar'])){
    $id=$_GET['id'];
    $mensaje=$facade->cancelarOrden($id);
    header("Location: ../views/buscarOrdenes.php?mensaje=" . $mensaje);
}

if (isset($_GET['autorizarPedido'])){
    $id=$_GET['idpedido'];
    $mensaje=$facade->autorizarOrden($id);
    header("Location: ../views/buscarOrdenes.php?mensaje=" . $mensaje);
}

if (isset($_POST['cancelarPedido'])){
    $id=$_POST['cancelarPedido'];
    $mensaje=$facade->cancelarOrden($id);
    header("Location: ../views/buscarOrdenes.php?mensaje=" . $mensaje);
}