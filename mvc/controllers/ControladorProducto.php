<?php
session_start();
include_once '../models/ProductoDao.php';
include_once '../models/ProductoDto.php';
include_once '../controllers/ControladorProducto.php';
include_once '../utilities/Conexion.php';
include_once '../facades/FacadeProducto.php';
include_once '../utilities/mpdf60/mpdf.php';


$fachada = new FacadeProducto();

if (isset($_POST['guardar'])) {
    $file = $_FILES['ImagenProducto'];
    $name='../images/placeholder.png';
    if (!($file['name'] == "" && $file['size'] == 0)) {
        $name ="../images/" . uniqid();
        $path = "../images/" . basename($name);
    }
    $producto = new ProductosDto();
    $producto->setIdProducto($_POST['idProducto']);
    $producto->setNombreProducto($_POST['nombreProducto']);
    $producto->setDescripcion($_POST['descriptionProducto']);
    $producto->setIva($_POST['ivaProducto']);
    $producto->setCategoria($_POST['categoriaProducto']);
    $producto->setValorUnitario($_POST['precio']);
    $producto->setPresentacion($_POST['presentacion']);
    $producto->setImagenProducto( $name);
    $producto->setColor($_POST['color']);
    $producto->setAroma($_POST['aroma']);
    $producto->setConcentracion($_POST['concentracion']);
    $mensaje = $fachada->registrarProducto($producto);
    if ($mensaje == 1) {
        move_uploaded_file($file['tmp_name'], $path);


        $mensaje = 'mensaje=Producto Registrado Correctamente &error=false'.$resul;
        header("Location: ../views/productoListar.php?" . $mensaje);
    } else {
        print $mensaje ;//= 'mensaje=Ocurrio un error al registrar el producto &error=true';
        print_r($_POST['aroma']);
        print_r($_POST['color']);
        print_r($_POST['concentracion']);
        //header("Location: ../views/productoListar.php?" . $mensaje);
    }

}
if (isset($_POST['modificar'])) {
    $file = $_FILES['ImagenProducto'];
    $baseUrl = '../images/';
    if (!($_FILES['ImagenProducto']['name'] == "")) {
        $name = $baseUrl . uniqid();
        $path = $baseUrl . basename($name);
    } else {
        $name = $fachada->obtenerProducto($_POST['idProducto'])['rutaImagen'];
    }
    $idviejo = $_GET['id'];
    $producto = new ProductosDto();
    $producto->setIdProducto($_POST['idProducto']);
    $producto->setNombreProducto($_POST['nombreProducto']);
    $producto->setDescripcion($_POST['descriptionProducto']);
    $producto->setIva($_POST['ivaProducto']);
    $producto->setCategoria($_POST['categoriaProducto']);
    $producto->setImagenProducto($name);
    $producto->setValorUnitario($_POST['precio']);
    $producto->setPresentacion($_POST['presentacion']);
    $mensaje = $fachada->actualizarProducto($producto, $idviejo);
    if ($mensaje == 1) {
        move_uploaded_file($file['tmp_name'], $path);
        $mensaje = 'mensaje=Producto Modificado Correctamente &error=false';
        header("Location: ../views/productoListar.php?" . $mensaje);


    } else {
        print $mensaje = 'mensaje=Ocurrio un error al Modificar el producto &error=true';
        header("Location: ../views/productoListar.php?" . $mensaje);
    }

}

if (isset ($_POST['deleteProducto'])) {
    $fachada = new FacadeProducto();
    $msg = $fachada->cancelarProducto($_POST['deleteProducto']);
    echo json_encode($msg);
}

if (isset ($_POST['search'])) {
    $mensaje = $_POST['searchProduct'];
    header("Location: ../views/productoListar.php?resultado=" . $mensaje);
}
if (isset($_GET['buscar'])) {
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    if (isset($criterio) && isset($busqueda) && isset($comobuscar)) {

        header("Location: ../views/productoListar.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    } else {
        header("Location: ../views/productoListar.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    }
}

if (isset($_GET['listar'])) {
    /*if (0) {
        header("Location: ../views/productoListar.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    } else {
        header("Location: ../views/productoListar.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    }*/
}

if (isset($_POST['detailProduct'])) {
    $response = $fachada->obtenerProducto($_POST['detailProduct']);
    echo json_encode($response);
}

if (isset($_POST['data'])) {

    $mensaje = $fachada->validarExistenciaDeProducto($_POST['data']);
    if ($mensaje['existe'] == '1') {
        echo json_encode(1);
    }


}
if (isset($_POST['buscar2'])) {
     $criterio = $_POST['criterio'];
     $busqueda = $_POST['busqueda'];
     $comobuscar = $_POST['comobuscar2'];

     header("Location: ../views/exportarProductosPdf.php?criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);

}
if(isset($_POST['exportar'])){
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $data=$fachada->buscarConCriterio($criterio,$busqueda,$comobuscar);
    $html = '<html><body><h2 style="text-align: center;color: #3c8dbc">Listado de Productos</h2><hr><table><thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Nombre</th>
                                    <th>Categoria</th>
                                    <th>Precio</th>
                                </tr>
                                </thead>';
    foreach ($data as $details)
    {   $html.='<tbody><tr><td>'.'<img heigth="80" width="80" src='.$details['rutaImagen'].'>'. "<td>" ;
        $html.='<td>'. $details['NombreProducto'] . "<td>";
        $html.='<td>'. $details['NombreCategoria'] . "<td>";
        $html.='<td><span class="badge label-success">$'.number_format($details['valorPresentacion'])."<span><td><tr>";
    }
    $html .= '</tbody></table></body></html>';
    $mpdf=new mPDF('','', 0, 'helvetica', 15, 15, 16, 16, 9, 9, 'L');
    $stylesheet = file_get_contents('../../bootstrap/css/bootstrap.css');

    $mpdf->list_indent_first_level = 1;  // 1 or 0 - whether to indent the first level of a list
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    $mpdf->Output();

}