<?php

include_once '../facades/FacadeEmpleado.php';
session_start();
$fachada = new FacadeEmpleado();
if (isset($_GET['login'])){
    $user=$_POST['email'];
    $pass=$_POST['clave'];
    $_SESSION['datosLogin']=$fachada->comprobarUsuario($user,$pass);
    if ($_SESSION['datosLogin']==false){
        session_unset();
        session_destroy();
        header('location: ../../login.php?login=false');
    }else{
        $_SESSION['hora']=time();
        header('location: ../views/index.php');
    }
}


if (isset($_GET['forget'])){
    $user=$_POST['usuario'];
    $existe=$fachada->verificarExistencia($user);
    if ($existe==false){
        header('location: ../../login.php?correo=false');
    }else{
        $_SESSION['correo']=$existe;
        $pass=rand(1111111,999999);
        $_SESSION['pass']=$pass;
        if ($fachada->cambiarClave($pass,$user)){
            header('location: ../utilities/resetpass.php?sent=true');
        }else{
            header('location: ../../login.php?fail=true');
        }
    }
}

if (isset($_GET['unlock'])){
    $login=$fachada->unlock($_POST['pass']);
    if ($login==false){
        header('location: ../../lock_screen.php?login=false');
    }else{
        $_SESSION['hora']=time();
        header('location: ../views/'.$_SESSION['pagina']);
    }

}



if (isset($_GET['account'])){
    unset($_SESSION['datosLogin']);
    unset($_SESSION['rol']);
    unset($_SESSION['hora']);
    session_destroy();
    session_unset();
    header('location: ../../login.php?cerrar=true');
}