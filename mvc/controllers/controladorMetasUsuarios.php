<?php
/**
 * Created by PhpStorm.
 * User: iStam
 * Date: 3/09/15
 * Time: 7:53 PM
 */
include_once '../models/MetaUsuarioDto.php';
include_once '../facades/FacadeMetaUsuario.php';
include_once '../utilities/mailgun-php-master/vendor/autoload.php';
include_once '../facades/FacadeEmpleado.php';

session_start();
$facade = new FacadeMetaUsuario();
$dto = new MetaUsuarioDto();
$empleado = new FacadeEmpleado();
if (isset($_POST['cedula'])) {
    $dto->setEmpleado($_POST['cedula']);
    $dto->setMeta($_POST['meta']);
    $mg = new \Mailgun\Mailgun('key-94e5c87dd8ca10dbc366bc759833daa7');
    $domain = "sandbox15819fc8307a4e94bcd2034c57385a01.mailgun.org";
    $mensaje = $facade->asignarMeta($dto);
    foreach ($_POST['cedula'] as $user) {

        $data = $empleado->buscarCriterio('CedulaEmpleado', $user, 1);
        foreach ($data as $info) {
            # Make the call to the client.
            $result = $mg->sendMessage($domain, array(
                'from' => 'Notificaciones Mapcompany <notificaciones@mapcompany.com>',
                'to' => $info['Nombres'] . ' ' . $info['Apellidos'] . '<' . $info['EmailPersona'] . '>',
                'subject' => 'Asignacion de Meta',
                'html' => '<!doctype html>
   <head>
       <meta charset="utf-8">
   </head>
<html>
<body>
<div id=":40t" class="ii gt m1516ec2c7408a467 adP adO">
    <div id=":3zq" class="a3s" style="overflow: hidden;"><u></u>


        <div style="background-color:#f2f2f2">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
                       style="background-color:#f2f2f2">
                    <tbody>
                    <tr>
                        <td align="center" valign="top" style="padding:40px 20px">
                            <table border="0" cellpadding="0" cellspacing="0" style="width:600px">
                                <tbody>
                                <tr>
                                    <td align="center" valign="top" style="padding-bottom:30px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               style="background-color:#ffffff;border-collapse:separate!important;border-radius:4px">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top"
                                                    style="padding-top:40px;padding-right:40px;padding-bottom:0;padding-left:40px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top" style="padding-right:20px">
                                                                <a href="
                                                                   title="Mapcompany" style="text-decoration:none"
                                                                   target="_blank"><img
                                                                        src="http://s22.postimg.org/52ex74b1d/logo.png"
                                                                        alt="mapcompany" height="72" width="237"
                                                                        style="border:0;color:#6dc6dd!important;font-family:Helvetica,Arial,sans-serif;font-size:60px;font-weight:bold;min-height:auto!important;letter-spacing:-4px;line-height:100%;outline:none;text-align:center;text-decoration:none"
                                                                        class="CToWUd"></a>
                                                            </td>
                                                            <td valign="middle" width="100%"
                                                                style="color:#606060;font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:150%;text-align:left">
                                                                <h3 style="color:#606060!important;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:bold;letter-spacing:-1px;line-height:115%;margin:0;padding:0;text-align:left;@charset utf-8">
                                                                    Más de 20 años de evolución e innovación nos permiten ofrecerle un nuevo producto.</h3>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"
                                                    style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="border-top:1px dotted #cccccc;border-bottom:1px dotted #cccccc;padding-top:10px;padding-bottom:10px">
                                                                <h2 style="color:#606060!important;font-family:Helvetica,Arial,sans-serif;font-size:20px;letter-spacing:-.5px;line-height:115%;margin:0;padding:0;text-align:center">
                                                                    </h2> <img src="https://ci6.googleusercontent.com/proxy/5M3_WtoM0mRtWnsLyZoameW1h0TrEB4fqbs8aKIUt5G0M-qz85JlaNx-hWUiWP9qNKJmh9yBXzY1p9JEUxeugSWDQhR16qSRbZexPThKiDxO1iP0BMEIyORkwF7VbCeLdp5CaHsf0hAVEyL3Onmjxo5gcshrAeSJZakS1FfS-KA0O1qBD_jETmj2-lhSv59aWNQdHYV8Wzmjt-Cz65PzwfVRPYHcf_O5Dlb-B_eFByq_A56Wxbws3AE=s0-d-e1-ft#http://mi.udemy.com/p/rp/ea34ef6aca9125c1.png?mi_u=431662325&lego=friends&currency=usd&lang=es&mi_name=Get+Courses+for+$14.+Hurry,+Prices+Inflate+Every+Other+Day!" alt="">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="middle"
                                                    style="padding-right:40px;padding-bottom:40px;padding-left:40px">
                                                    <table border="0" cellpadding="0" cellspacing="0"
                                                           style="background-color:#6dc6dd;border-collapse:separate!important;border-radius:3px">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="middle"
                                                                style="color:#ffffff;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:bold;line-height:100%;padding-top:18px;padding-right:15px;padding-bottom:15px;padding-left:15px">
                                                                <a href="http://mapcompanysas.com/"
                                                                   style="color:#ffffff;text-decoration:none"
                                                                   target="_blank">Visita nuestra Página</a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top"
                                                    style="color:#606060;font-family:Helvetica,Arial,sans-serif;font-size:13px;line-height:125%">
                                                    © 2015 Mapcompany SAS<span
                                                        style="font-size:10px!important;vertical-align:super">®</span>,
                                                    All Rights Reserved.
                                                    <br>
                                                    <a href="#1516ec2c7408a467_"
                                                       style="color:#606060!important;text-decoration:none!important"><span
                                                            style="color:#606060!important">Calle 68 # 93 - 52 Bogotá - Colombia</span></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="padding-top:30px">
                                                      <img
                                                            src="https://bitbucket.org/account/sigcoweb/avatar/16/?ts=1448012213"
                                                            height="25" width="25"
                                                            style="border:0;outline:none;text-decoration:none"
                                                            class="CToWUd"></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </center>
            <img
                src="https://ci4.googleusercontent.com/proxy/Rsl4HWciT6xqzuKuKzFj-jqc8pfvAEb7D5uD66e8ZMG0ZTUjw2UGk2gWf5QUWunMlXcEDcE7tk-VH7tXbsPaNGBT6WZluEZ_71dqfb2RdgQTb3ffz-2715zAgdthk0I43Ln50F2QKcx1dhnHn1dHX_w=s0-d-e1-ft#http://click.mailchimpapp.com/track/open.php?u=10612303&amp;id=d76c383a73ea46f59b40be9da1979b88"
                height="1" width="1" class="CToWUd"></div>
        <div class="yj6qo"></div>
        <div class="adL">

        </div>
    </div>
</div>
</body>
</html>' ));
        }
    }
header('location: ../views/buscarMetas.php?mensaje='.$mensaje);
}

if (isset($_GET['buscar'])) {
    unset($_SESSION['consulta']);
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $facade->buscarConCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta'] = $resul;
    if ($resul == null) {
        header("Location: ../views/buscarMetas.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    } else {
        header("Location: ../views/buscarMetas.php?encontrados=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    }
}

if (isset($_GET['listar'])) {
    unset($_SESSION['consulta']);
    $resul = $facade->listarMetas();
    $_SESSION['consulta'] = $resul;
    if ($resul == null) {
        header("Location: ../views/buscarMetas.php?encontrados=false&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    } else {
        header("Location: ../views/buscarMetas.php?encontrados=true&todos=true&criterio=" . $criterio . "&busqueda=" . $busqueda . "&comobuscar=" . $comobuscar);
    }
}

