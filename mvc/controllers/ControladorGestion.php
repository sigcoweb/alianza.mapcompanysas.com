<?php
session_start();
include_once '../models/GestionDao.php';
include_once '../models/GestionDto.php';
include_once '../utilities/Conexion.php';
include_once '../facades/FacadeGestion.php';
include_once '../facades/PuntosEntregaFacade.php';
include_once '../facades/ClienteFacade.php';

$fachada = new FacadeGestion();
$puntosEntrega = new PuntosEntregaFacade();
$cliente = new ClienteFacade();
if (isset($_POST['registrar'])) {

    $gestion = new GestionDto();
    $gestion->setIdCliente ($_POST['idCliente']);
    $gestion->setTipoVisita($_POST['tipoVisita']);
    if($_POST['tipoVisita']=='Capacitación'){
        $gestion->setTemaProducto($_POST['temaproducto']);
    }else{
        $gestion->setTemaProducto($_POST['tema']);
    }
    $gestion->setAsistentes($_POST['asistentes']);
    $gestion->setObservaciones($_POST['observaciones']);
    $gestion->setLugar($_POST['lugar']);
    $gestion->setFechaVisita($_POST['fechaVisita']);
    $gestion->setIdUsuario($_SESSION['datosLogin']['id']);
    $mensaje = $fachada->registrarGestion($gestion);
    header("Location: ../views/buscarGestion.php?mensaje=".$mensaje);
}

if (isset ($_GET['idproducto'])){

    $mensaje=$fachada->cancelarGestion($_GET['idproducto']);
    header("Location: ../views/listarGestion.php?mensaje=".$mensaje);
}

if(isset($_POST['reload'])){
    $nit=$_POST['reload'];
    $mensaje=$puntosEntrega->buscarPunto('clientes.Nit',$nit,1);
    if(count($mensaje)==0){
      $mensaje=$cliente->buscarCliente('clientes.Nit',$nit,1);
    }
    print json_encode($mensaje);

}
if(isset($_POST['dateValid'])){
    $mensaje=$fachada->buscarGestionPorFecha($_POST['dateValid']);
    foreach($mensaje as $m){
        print json_encode($m['FechaProgramada']);
    }

}
if(isset($_POST['events'])){

        print json_encode($fachada->getEvents(0,0));

}
if(isset($_POST['searchBy'])){

    print json_encode($fachada->getEvents(1,$_POST['searchBy']));

}
if(isset($_POST['detail'])){
     $rows=$fachada->obtenerGestion($_POST['detail'],0);
        print json_encode($rows);

}
if(isset($_POST['getTrainings'])){
    $rows=$fachada->obtenerCapacitacion();
    print "".json_encode($rows);

}
if (isset($_POST['modificar'])) {

    $gestion = new GestionDto();
    $idviejo=$_GET['idv'];
    $gestion->setObservaciones($_POST['resultadoGestion']);
    $gestion->setLugar($_POST['lugar']);
    $gestion->setFechaVisita($_POST['fechaVisita']);
    $gestion->setEstado($_POST['estado']);
    $gestion->setIdUsuario($_SESSION['datosLogin']['id']);
    $mensaje = $fachada->modificarGestion($gestion,$idviejo);

    header("Location: ../views/buscarGestion.php?mensaje=".$mensaje);

}
if (isset($_GET['buscar'])) {
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $fachada->buscarConCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarGestion.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarGestion.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['listar'])) {
    $resul = $fachada->getGestiones($_SESSION['datosLogin']['id'],0);
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarGestion.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarGestion.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_POST['comentar'])) {
    $gestion = new GestionDto();
    $gestion->setIdGestion($_POST['idGestion']);
    $gestion->setObservaciones($_POST['comentario']);
    $gestion->setIdUsuario($_SESSION['datosLogin']['id']);
    $resul = $fachada->comentarGestion($gestion);
    if($resul==null){
        header("Location: ../views/buscarGestion.php?mensaje=".$resul);
    }else{
        header("Location: ../views/buscarGestion.php?mensaje=".$resul);
    }
}
