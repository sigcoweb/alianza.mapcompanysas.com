<?php
include_once '../models/CotizacionesDTO.php';
include_once'../models/DetallesCotizacionDTO.php';
include_once '../facades/FacadeCotizaciones.php';
include_once '../facades/FacadeProducto.php';
include_once'../models/ProductosCotizados.php';
session_start();
$dto=new CotizacionesDTO();
$facade=new FacadeCotizaciones();
$facadeProducto=new FacadeProducto();
$detalles=new DetallesCotizacionDTO();

if (isset($_GET['buscar'])) {
    $criterio = $_POST['criterio'];
    $busqueda = $_POST['busqueda'];
    $comobuscar = $_POST['comobuscar'];
    $resul = $facade->buscarConCriterio($criterio, $busqueda, $comobuscar);
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarCotizaciones.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarCotizaciones.php?encontrados=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['listar'])) {
    unset($_SESSION['consulta']);
    $resul = $facade->listarCotizaciones();
    $_SESSION['consulta']=$resul;
    if($resul==null){
        header("Location: ../views/buscarCotizaciones.php?encontrados=false&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }else{
        header("Location: ../views/buscarCotizaciones.php?encontrados=true&todos=true&criterio=".$criterio."&busqueda=".$busqueda."&comobuscar=".$comobuscar);
    }
}

if (isset($_GET['agregar'])) {
    $select=$_POST['idproducto'];
    $cantidad=$_POST['cantidad'];
    $cliente=$_POST['idcliente'];
    $explode=explode(",",$select);
    $idProducto=$_POST['idproducto'];
    $idPresentacion=$_POST['idpresentacion'];
    $producto=$facadeProducto->obtenerProductoPresentacion($idProducto,$idPresentacion);
    $dto=new ProductosCotizados();
    $dto->setId($idProducto);
    $dto->setNombre($producto['NombreProducto']);
    $dto->setValorBase($producto['valorPresentacion']);
    $dto->setCantidad($cantidad);
    $dto->setIva("0");
    $dto->setSubtotal($cantidad*$dto->getValorBase());
    $dto->setPresentacion($_POST['idpresentacion']);
    $dto->setAroma($_POST['idaroma']);
    $dto->setColor($_POST['idcolor']);
    $dto->setConcentracion($_POST['idconcentracion']);
    $nomas='';
    if (!isset($_SESSION['productoDatos']) ){
        $_SESSION['productoDatos']=array();
        array_push($_SESSION['productoDatos'],$dto);
    }else{
        $rep=0;
        foreach($_SESSION['productoDatos'] as $product){
            if($_POST['idproducto']==$product->getId()&&$_POST['idpresentacion']==$product->getPresentacion()&&$_POST['idaroma']==$product->getAroma()&&$_POST['idcolor']==$product->getColor()&&$_POST['idconcentracion']==$product->getConcentracion()){
                $rep++;
            }
        }
        if($rep==0){
            array_push($_SESSION['productoDatos'],$dto);
        }else{
            $nomas='&error=true&detalleerror=El producto NO ha sido añadido porque ya se encuentra en la cotización';
        }
    }
    header('location: ../views/crearCotizacion2.php?idcliente='.$cliente.'&empresaCotiza='.$_GET['empresaCotiza'].$nomas);
}


if (isset($_GET['finalizar'])) {
    $mensaje="";
    $dto->setObservaciones($_POST['observaciones']);
    $dto->setEstado("Vigente");
    $dto->setIdCliente($_GET['idcliente']);
    $dto->setEmpresaCotiza($_GET['cotiza']);

    if (isset($_POST['asesor'])){
        $dto->setIdUsuario($_POST['asesor']);
    }else{
        $dto->setIdUsuario($_SESSION['datosLogin']['id']);
    }
    $dto->setValorDescuento("0");
    $idCotizacion=$facade->registrarCotizaciones($dto)['idc'];
    $i=0;
    foreach ($_SESSION['productoDatos'] as $productos ) {
        $detalles->setIdCotizacion($idCotizacion);
        $detalles->setCantidad($productos->getCantidad());
        $detalles->setIdProducto($productos->getId());
        $detalles->setCantidadDespacho($_POST['cantidadDespacho'.$i.'']);
        $valorProducto=$_POST['descuento'.$i.''];
        $total=$valorProducto*$detalles->getCantidadDespacho();
        $detalles->setTotal($total);
        $detalles->setPresentacion($productos->getPresentacion());
        $detalles->setAroma($productos->getAroma());
        $detalles->setColor($productos->getColor());
        $detalles->setConcentracion($productos->getConcentracion());
        $detalles->setPresentacionDetalles($productos->getPresentacion());
        $detalles->setDescuento($_POST['descuento'.$i.'']);
        $detalles->setPresentacionDespacho($_POST['presentacionDespacho'.$i.'']);

        $mensaje=$facade->agregarProducto($detalles);
        $i++;
    }
    unset($_SESSION['productoDatos']);
    if($_POST['enviar']) {
        $all = $facade->listarTodas();
        require '../utilities/fpdf/fpdf.php';
        include_once '../facades/FacadeCotizaciones.php';
        include_once '../utilities/ConvertirNumero.php';
        $convertir = new ConvertirNumero();
        $facade = new FacadeCotizaciones();
        $resultados = $facade->buscarCotizacion($all[0]['IdCotizacion']);
        $pdf = new FPDF("P");
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 12);
        switch ($resultados[0]['empresaCotiza']) {
            case "MAP":
                $pdf->Image('../images/map.jpg', 0, 0, 218);
                break;

            case "BIO":
                $pdf->Image('../images/bio.jpg', 0, 0, 218);
                break;

            case "MAT":
                $pdf->Image('../images/mat.jpg', 0, 0, 218);
                break;
        }
        $pdf->Image('../views/cotizacion.new.png', 0, 0, 218);
        $pdf->Ln(25);
        $pdf->Cell(70, 0, utf8_decode('SEÑOR(ES): ' . $resultados[0]['RazonSocial']), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(115, 0, utf8_decode('DIRECCIÓN: ' . $resultados[0]['Direccion']), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(130, 0, utf8_decode('TELÉFONO: ' . $resultados[0]['Telefono']), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 16);
        $pdf->SetTextColor(220, 20, 60);
        $pdf->Cell(110, 0, utf8_decode('Cotización Nº: ' . $resultados[0]['IdCotizacion']), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(156, 0, utf8_decode('E-MAIL: ' . $resultados[0]['EmailCliente']), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(115, 0, utf8_decode('CIUDAD: ' . $resultados[0]['NombreLugar']), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(60, 0, utf8_decode('NIT: ' . $resultados[0]['Nit']), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(60, 0, utf8_decode('ASESOR TÉCNICO COMERCIAL: ' . ($resultados[0]['Nombres'] . ' ' . $resultados[0]['Apellidos'])), 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(60, 0, utf8_decode('EMAIl: ' . $resultados[0]['EmailPersona']), 0, 0, 'L');
        $pdf->Ln(25);
        $totalsub = 0;
        $totaliva = 0;
        $totalDescuento = 0;
        $pdf->Ln(15);
        $totalProductos = 0;
        $pdf->SetFont('Arial', '', 10);
        $prod = 0;
        $lin = 0;
        foreach ($resultados as $datos) {
            if ($lin > 13) {
                $pdf->AddPage();
                $pdf->SetFont('Arial', '', 12);
                switch ($resultados[0]['empresaCotiza']) {
                    case "MAP":
                        $pdf->Image('../images/map.jpg', 0, 0, 218);
                        break;
                    case "BIO":
                        $pdf->Image('../images/bio.jpg', 0, 0, 218);
                        break;
                    case "MAT":
                        $pdf->Image('../images/mat.jpg', 0, 0, 218);
                        break;
                }
                $pdf->Image('cotizacion.new.png', 0, 0, 218);
                $pdf->Ln(25);
                $pdf->Cell(70, 0, utf8_decode('SEÑOR(ES): ' . $resultados[0]['RazonSocial']), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->Cell(115, 0, utf8_decode('DIRECCIÓN: ' . $resultados[0]['Direccion']), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->Cell(130, 0, utf8_decode('TELÉFONO: ' . $resultados[0]['Telefono']), 0, 0, 'L');
                $pdf->SetFont('Arial', '', 16);
                $pdf->SetTextColor(220, 20, 60);
                $pdf->Cell(110, 0, utf8_decode('Cotización Nº: ' . $resultados[0]['IdCotizacion']), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->SetFont('Arial', '', 12);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(156, 0, utf8_decode('E-MAIL: ' . $resultados[0]['EmailCliente']), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->Cell(115, 0, utf8_decode('CIUDAD: ' . $resultados[0]['NombreLugar']), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->Cell(60, 0, utf8_decode('NIT: ' . $resultados[0]['Nit']), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->Cell(60, 0, utf8_decode('ASESOR TÉCNICO COMERCIAL: ' . ($resultados[0]['Nombres'] . ' ' . $resultados[0]['Apellidos'])), 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->Cell(60, 0, utf8_decode('EMAIl: ' . $resultados[0]['EmailPersona']), 0, 0, 'L');
                $pdf->Ln(25);
                $pdf->Ln(15);
                $pdf->SetFont('Arial', '', 10);
                $prod = 0;
                $lin = 0;
            }
            $totalsub = $totalsub + ($datos['TotalDetalle']);
            $pdf->Cell(26, 8, $datos['IdProducto'], 0, 0, 'L');
            $palabra = "";
            $palabra2 = "";
            if (strlen($datos['NombreProducto']) > 45) {
                $test = str_split($datos['NombreProducto']);
                for ($i = 0; $i < 45; $i++) {
                    $palabra = $palabra . $test[$i];
                }
                $pdf->Cell(85, 8, utf8_decode($palabra . '-'), 0, 0, 'L');
                $pdf->Ln(5.5);
                $restante = strlen($datos['NombreProducto']) - 45;
                $restante = $restante + 45;
                for ($i = 45; $i < $restante; $i++) {
                    $palabra2 = $palabra2 . $test[$i];
                }
                $pdf->Cell(26, 8, "", 0, 0, 'L');
                $pdf->Cell(85, 8, utf8_decode($palabra2), 0, 0, 'L');
                $totalProductos++;
                $lin = $lin + 2;
            } else {
                $pdf->Cell(85, 8, utf8_decode($datos['NombreProducto']), 0, 0, 'L');
                $lin++;
            }
            $pdf->Cell(10, 8, $datos['CantidadProductos'], 0, 0, 'L');
            $pdf->Cell(20, 8, $datos['Abreviatura'], 0, 0, 'C');
            $pdf->Cell(26, 8, '$' . number_format($datos['TotalDetalle'] / $datos['CantidadProductos']), 0, 0, 'R');
            $pdf->Cell(30, 8, '$' . number_format($datos['TotalDetalle']), 0, 0, 'R');
            $pdf->Ln(5.5);
            $totalProductos++;
            $prod++;
        }
        if ($lin < 14) {
            $diferencia = 14 - $lin;
            for ($i = 1; $i < $diferencia - 1; $i++) {
                $pdf->Cell(20, 8, "", 0, 0, 'L');
                $pdf->Cell(107, 8, "", 0, 0, 'L');
                $pdf->Cell(10, 8, "", 0, 0, 'L');
                $pdf->Cell(18, 8, "", 0, 0, 'C');
                $pdf->Cell(22, 8, "", 0, 0, 'L');
                $pdf->Cell(60, 8, "", 0, 0, 'L');
                $pdf->Ln(5.5);
            }
            $pdf->Ln(20);
        } else {
            $pdf->Ln(10);
        }
        $pdf->Cell(197, 0, "$" . number_format($resultados[0]['ValorTotalCotizacion']), 0, 0, 'R');
        $guardar = '../documents/cotNo.' . $all[0]['IdCotizacion'] . 'de' . date("d-m-y-h-m-s", time()) . '.pdf';
        $pdf->Output($guardar);
        require '../utilities/mailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->setLanguage('es');// Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                     // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'siangeldice@gmail.com';   // SMTP username
        $mail->Password = 'Angels871207';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable encryption, only 'tls' is accepted
        $mail->addReplyTo($all[0]['EmailPersona']);
        //$mail->From = $all[0]['EmailPersona'];
        //$mail->FromName = strtoupper($all[0]['Nombres'].' '.$all[0]['Apellidos']);
        $mail->setFrom($all[0]['EmailPersona'], strtoupper($all[0]['Nombres'] . ' ' . $all[0]['Apellidos']));
        //$mail->addAddress($_POST['correo']);                 // Add a recipient
        $var=$_POST['correo'];
        foreach($var as $key=>$ma){
            $mail->addBCC($var[$key]);
        }
        $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
        $mail->Subject = utf8_decode('Grupo MAT, Cotización No.' . $all[0]['IdCotizacion']);
        $mail->isHTML(true);
        $mail->Body = '<h4>Respetados señores:</h4><p>En calidad de su asesor técnico y comercial me permito adjuntar
a ustedes la cotización de nuestra línea de productos según solicitud.</p><br><p>Estaremos atentos ante cualquier comentario.</p><br>Atentamente,<br>
<h5>' . strtoupper($all[0]['Nombres'] . ' ' . $all[0]['Apellidos']) . '</h5>
<h6>Asesor Comercial Grupo MAT</h6>
';
        $mail->addAttachment($guardar);
        if (!$mail->send()) {
            $mensaje .= ' El mensaje no fue enviado.';
            $mensaje .= '&error=true&detalleerror=: ' . $mail->ErrorInfo;
        } else {
            $mensaje .= ' Se ha enviado un correo electrónico al cliente con una copia de la cotización.';
        }
        unlink($guardar);
    };
    header("Location: ../views/buscarCotizaciones.php?mensaje=" . $mensaje);
}



if (isset($_GET['cancelar'])){
    $id=$_GET['id'];
    $mensaje=$facade->cancelarCoti($id);
    header("Location: ../views/buscarCotizaciones.php?mensaje=" . $mensaje);
}




