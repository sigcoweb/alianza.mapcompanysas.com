<!DOCTYPE html> <?php session_start(); ?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Nueva empresa para un cliente</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons --
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>-->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons --
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>-->
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>


    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->


    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Empresas de clientes
                <small>Formulario de registro</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Clientes</a></li>
                <li class="active">Nueva empresa</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    include_once '../facades/ClienteFacade.php';
                    if(isset($_POST['cedulaPersona'])){
                        $clienteFac = new ClienteFacade();
                        $cliente = $clienteFac->listarPersonasConRol();
                    }

                    if (isset($cliente[0]['CedulaPersona'])){
                        ?>

                        <form action="../controllers/ClientesController.php?controlar=crearSoloEmpresa"
                              method="post" class="validacion" id="formValidacion">

                            <div class="box box-default box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Indicaciones de registro</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <p>
                                            Por favor diligencie el siguiente formulario para registrar la información.<br>
                                            Recuerde que este formulario contiene campos obligatorios(*).
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="box box-default box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Información registrada</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label id="labelCedula" for="Cedula">Cédula*</label>
                                                <input type="text" name="Cedula" id="Cedula" class="form-control exists"
                                                       value="<?php echo $cliente[0]['CedulaPersona'] ?>" required readonly/>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Nombres">Nombres*</label>
                                                <input type="text" name="Nombres" id="Nombres" class="form-control"
                                                       value="<?php echo $cliente[0]['Nombres'] ?>" required readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Apellidos">Apellidos*</label>
                                                <input type="text" name="Apellidos" id="Apellidos" class="form-control"
                                                       value="<?php echo $cliente[0]['Apellidos'] ?>" required readonly/>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Celular">Número fijo o celular*</label>
                                                <input type="text" name="Celular" id="Celular" class="form-control"
                                                       value="<?php echo $cliente[0]['CelularPersona'] ?>" required readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Email1">Email*</label>
                                                <input type="email" name="Email1" id="Email1" class="form-control"
                                                       value="<?php echo $cliente[0]['EmailPersona'] ?>" required readonly/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="box box-info box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Información corporativa</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Nit" id="labelNit">Nit(sin dígito de verificacción)*</label>
                                                <input type="text" name="Nit" id="Nit" class="form-control exists"
                                                       placeholder="9024552452" required tabindex="6"/>
                                                <a id="botonCotizar" class="hidden"><span class="label label-info">
                                                    <i class="fa fa-calculator">
                                                    </i>   añadir cotización a este cliente</span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="RazonSocial">Razón social*</label>
                                                <input type="text" name="RazonSocial" id="RazonSocial" class="form-control"
                                                       placeholder="Mi Empresa Inc" required tabindex="7"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="NombreComercial">Nombre comercial*</label>
                                                <input type="text" name="NombreComercial" id="NombreComercial" class="form-control"
                                                       placeholder="EMP" required tabindex="8"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Email2">Email corporativo*</label>
                                                <input type="email" name="Email2" id="Email2" class="form-control"
                                                       placeholder="info@empresa.com" required tabindex="9"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Telefono">Teléfono principal (fijo o celular)*</label>
                                                <input type="text" name="Telefono" id="Telefono" class="form-control"
                                                       placeholder="6343429" required tabindex="10"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="IdActividad">Actividad del cliente*</label>
                                                <select class="form-control select2" name="IdActividad" id="IdActividad" required
                                                        tabindex="11">
                                                    <option selected="selected" disabled>Seleccione...</option>
                                                    <?php
                                                    include_once '../facades/ActividadesEmpresasFacade.php';
                                                    $actividadesCliente = new ActividadesEmpresasFacade();
                                                    $todasActividadesCliente = $actividadesCliente->listarTodos();
                                                    foreach ($todasActividadesCliente as $actividad) {
                                                        ?>
                                                        <option
                                                            value="<?php echo $actividad['IdActividad'] ?>"><?php echo $actividad['NombreActividad'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="IdTipo">Tipo de cliente*</label>
                                                <select class="form-control select2" name="IdTipo" id="IdTipo" required tabindex="12">
                                                    <option selected="selected" disabled>Seleccione...</option>
                                                    <?php
                                                    include_once '../facades/TiposEmpresasFacade.php';
                                                    $tiposCliente = new TiposEmpresasFacade();
                                                    $todosTiposCliente = $tiposCliente->listarTodos();
                                                    foreach ($todosTiposCliente as $tipo) {
                                                        ?>
                                                        <option
                                                            value="<?php echo $tipo['IdTipo'] ?>"><?php echo $tipo['NombreTipo'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.form-group -->

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="plazoPago">Plazo de pago (en días)*</label>
                                                <select class="form-control select2" name="plazoPago" id="plazoPago" required tabindex="13">
                                                    <option selected="selected" disabled>Seleccione...</option>
                                                    <?php
                                                    for ($plazo=1;$plazo<121;$plazo++) {
                                                        ?>
                                                        <option value="<?php echo $plazo; ?>"><?php echo $plazo; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="diaCierreFacturacion">Cierre facturación (día del mes)*</label>
                                                <select class="form-control select2" name="diaCierreFacturacion" id="diaCierreFacturacion" required tabindex="14">
                                                    <option selected="selected" disabled>Seleccione...</option>
                                                    <?php
                                                    for ($cierre=1;$cierre<31;$cierre++) {
                                                        ?>
                                                        <option value="<?php echo $cierre; ?>"><?php echo $cierre; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                            ?>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="IdClasificacion">Clasificación del cliente*(sólo coordinador)</label>
                                                    <select class="form-control select2" name="IdClasificacion" id="IdClasificacion"
                                                            required tabindex="15">
                                                        <option selected="selected" disabled>Seleccione...</option>
                                                        <?php
                                                        include_once '../facades/ClasificacionesFacade.php';
                                                        $classFaca = new ClasificacionesFacade();
                                                        $todasClasifCliente = $classFaca->listarTodos();
                                                        foreach ($todasClasifCliente as $clasif) {
                                                            ?>
                                                            <option value="<?php echo $clasif['IdClasificacion'] ?>">
                                                                <?php echo $clasif['NombreClasificacion'] ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="box box-info box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ubicación</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="IdDepartamento">Departamento*</label>
                                                <select class="form-control select2 depto" name="IdDepartamento" id="IdDepartamento" required tabindex="16">
                                                    <option value="0" disabled selected>Seleccione...</option>
                                                    <?php
                                                    include_once '../facades/DepartamentosFacade.php';
                                                    $deptosFac = new DepartamentosFacade();
                                                    $todos = $deptosFac->listarTodos();
                                                    foreach ($todos as $depto) {
                                                        ?>
                                                        <option
                                                            value="<?php echo $depto['idDepartamento'] ?>"><?php echo $depto['nombreDepartamento'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="IdLugar">Lugar*</label>
                                                <select class="form-control select2" disabled name="IdLugar" id="IdLugar" required tabindex="17">
                                                    <option value="0" disabled selected>Seleccione primero un departamento...</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="Direccion">Dirección*</label>
                                                <input type="text" name="Direccion" id="Direccion" class="form-control"
                                                       placeholder="Calle 34 No. 32 - 42" required tabindex="18"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>¿Registrar ésta ubicación como "punto de entrega"?</label>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-lg-3" id="punto">
                                                        <label for="no"> No <input required type="radio" name="puntoEntrega"
                                                                                   id="no" value="0" tabindex="19" onchange="javascript:noPunto()">
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label for="si"> Si <input required type="radio" name="puntoEntrega"
                                                                                   id="si" value="1" tabindex="20" onchange="javascript:siPunto()">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nuevoPunto" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="nombrePunto">Nombre del punto*</label>
                                                    <input type="text" name="nombrePunto" id="nombrePunto" class="form-control"
                                                           placeholder="Punto Principal" required tabindex="21"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="nombreContactoPunto">Persona de contacto*</label>
                                                    <input type="text" name="nombreContactoPunto" id="nombreContactoPunto" class="form-control"
                                                           placeholder="John Smith" required tabindex="22"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="correoPunto">Correo electrónico*</label>
                                                    <input type="text" name="correoPunto" id="correoPunto" class="form-control"
                                                           placeholder="jhon@smith.com" required tabindex="23"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="telefonoPunto">Teléfono (fijo o celular)*</label>
                                                    <input type="text" name="telefonoPunto" id="telefonoPunto" class="form-control"
                                                           placeholder="3214123443" required tabindex="24"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="observacionesPunto">Observaciones del punto de entrega*</label>
                                                    <input type="text" name="observacionesPunto" id="observacionesPunto" class="form-control"
                                                           placeholder="Recepción de pedidos de lunes a viernes de 8am a 5pm" required tabindex="25"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
                            ?>
                            <div class="box box-primary box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Asignación comercial (Sólo coordinador)</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="CedulaEmpleado">Asesor encargado del cliente*</label>
                                                <select class="form-control select2" name="CedulaEmpleado" id="CedulaEmpleado" required
                                                        tabindex="26">
                                                    <option selected="selected" disabled>Seleccione...</option>
                                                    <?php
                                                    include_once '../facades/FacadeEmpleado.php';
                                                    $empleadoFac = new FacadeEmpleado();
                                                    $empleados = $empleadoFac->listarUsuarios();
                                                    foreach ($empleados as $empleado) {
                                                        ?>
                                                        <option <?php if($empleado['EstadoPersona']=="Inactivo"){echo 'disabled';} ?>
                                                            value="<?php echo $empleado['CedulaEmpleado'] ?>">
                                                            <?php echo $empleado['CedulaEmpleado'].' | '.$empleado['Nombres'].' '.$empleado['Apellidos'] ?>
                                                            <?php if($empleado['EstadoPersona']=="Inactivo"){echo '| Inactivo';} ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="ac">Activar el cliente para realizar cotizaciones*</label><br>
                                                <label>
                                                    <input class="flat-red" type="checkbox" name="activarCliente" id="activarCliente" tabindex="27" checked>
                                                    ¿Activar cliente?
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="box-footer">
                                        <button type="submit" class="btn bg-green-gradient" tabindex="28"
                                                value="guardar" name="guardar" id="guardar">Guardar cliente
                                        </button>
                                        <input type="button" class="btn bg-yellow-gradient pull-right" tabindex="29"
                                               onclick="location.href='buscarClientes.php'" value="Cancelar"/>
                                    </div>
                                    <!-- /.box-footer -->
                                </div>
                                <!-- /.box-body -->
                            </div>

                        </form>
                        <!-- /.box -->
                        <?php
                    }else{
                        ?>
                        <div class="box box-warning box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">La cédula del cliente no es válida o el cliente no está asignado a su cuenta</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p>Debe ingresar nuevamente una cédula válida para asociarle una empresa.</p>
                            </div>
                            <div class="box-footer">
                                <input type="button" class="btn bg-green-gradient pull-right" tabindex="16"
                                       onclick="location.href='buscarClienteNuevaEmpresa.php'" value="Buscar otra cédula"/>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>
    <!-- jQuery 2.1.4--
        <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
        <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
    <script type="text/javascript">
        nuevoPunton = document.getElementById("nuevoPunto");
        function siPunto() {
            nuevoPunton.style.display='block';
            if($("#NombreComercial").val() == ''){
                $("#NombreComercial").val($('#RazonSocial').val());
            }
            if($("#nombreContactoPunto").val() == ''){
                $("#nombreContactoPunto").val($('#Nombres').val()+' '+$('#Apellidos').val());
            }
            if($("#telefonoPunto").val() == ''){
                $("#telefonoPunto").val($('#Celular').val());
            }
            if($("#correoPunto").val() == ''){
                $("#correoPunto").val($('#Email1').val());
            }
        }
        function noPunto() {
            nuevoPunton.style.display='none';
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#RazonSocial").change(function () {
                if($("#NombreComercial").val() == ''){
                    $("#NombreComercial").val($('#RazonSocial').val());
                }
            });

            $(".select2").select2();

            $('#Nit').on('keyup', function () {
                $.post("../controllers/ClientesController.php",
                    {
                        existeNit: $('#Nit').val()
                    },
                    function (nit) {
                        var json = $.parseJSON(nit);
                        if(json.existente==1){
                            nitExist=true;
                            $("#Nit").removeClass("has-success").addClass("has-error").addClass("alert-danger");
                            $(".form-control-feedback").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                            $("#guardar").addClass("disabled").addClass("hidden");
                            $("#labelNit").text("Este nit ya existe. Por favor indique otro número*");
                            nit = $('#Nit').val();
                            dirCotizaNit = 'crearCotizacion2.php?guardar=guardar&idcliente='+nit;
                            $("#botonCotizar").removeClass("hidden").attr('href',dirCotizaNit);
                        }else if(json.existente==0){
                            nitExist=false;
                            $("#Nit").addClass("has-success").removeClass("has-error").removeClass("alert-danger");
                            $("#guardar").removeClass("disabled").removeClass("hidden");
                            $("#labelNit").text("Nit*");
                            $("#botonCotizar").addClass("hidden");
                        }
                    });
            });

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            $("#IdDepartamento").change(function () {
                $('#IdLugar').removeAttr("disabled");
                $("#IdDepartamento option:selected").each(function () {
                    idDepto = $(this).val();
                    //$("#IdLugar").selectedIndex=0;
                    //document.getElementById("#IdLugar").selectedIndex=0;
                    $('#IdLugar').val('0');
                    $('#IdLugar').change();
                    $.post("lugaresDepto.php", { idDepto: idDepto }, function(data){
                        $("#IdLugar").html(data);
                    });
                });
            });

            $('#formValidacion').formValidation({
                message: 'Este valor no es admitido',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    Nit: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 5,
                                max: 15,
                                message: 'Este campo debe tener mínimo 5 carácteres y máximo 15.'
                            },
                            between: {
                                min: 9999,
                                max: 1000000000000000,
                                message: 'Debe ser un nit válido'
                            }
                        }
                    },
                    nombrePunto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 3,
                                max: 45,
                                message: 'Este campo debe tener mínimo 3 carácteres y máximo 45.'
                            }
                        }
                    },
                    RazonSocial: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 100,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 100.'
                            }
                        }
                    },
                    NombreComercial: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 50,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 50.'
                            }
                        }
                    },
                    nombreContactoPunto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 50,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 50.'
                            }
                        }
                    },
                    Direccion: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 100,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 100.'
                            }
                        }
                    },
                    Telefono: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 13,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 13.'
                            },
                            between: {
                                min: 999999,
                                max: 10000000000,
                                message: 'Debe ser un número telefónico válido'
                            }
                        }
                    },
                    Email2: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            }
                        }
                    },
                    puntoEntrega: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdDepartamento: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdLugar: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    CedulaEmpleado: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdTipo: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdActividad: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    Cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 5,
                                max: 15,
                                message: 'Este campo debe tener mínimo 5 carácteres y máximo 15.'
                            },
                            between: {
                                min: 9999,
                                max: 1000000000000000,
                                message: 'Debe ser un número de documento válido'
                            }

                        }
                    },
                    Nombres: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    Apellidos: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    correoPunto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            }
                        }
                    },
                    telefonoPunto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            },
                            between: {
                                min: 999999,
                                max: 10000000000,
                                message: 'Debe ser un número de teléfono válido'
                            }
                        }
                    },
                    Celular: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            },
                            between: {
                                min: 999999,
                                max: 10000000000,
                                message: 'Debe ser un número de teléfono válido'
                            }
                        }
                    },
                    Email1: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            }
                        }
                    }

                }
            });
        });
    </script>
</body>
</html>