<?php
include_once '../facades/FacadeCotizaciones.php';
$cotFaca = new FacadeCotizaciones();
session_start();?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Buscar cotizaciones</title>
    <link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Cotizaciones
                <small>Buscar</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li>Cotizaciones</li>
                <li class="active">Buscar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">


                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>

                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Indicaciones para la búsqueda</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Use las siguientes opciones para realizar la búsqueda de la información.
                                Recuerde que en este formulario hay campos obligatorios marcados (*).
                            </p>
                        </div>
                    </div>


                    <div class="box box-solid box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Opciones de búsqueda</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">


                            <form role="form" id="defaultForm" action="../controllers/controladorCotizacion.php?buscar=true" method="post">

                                <div class="form-group">
                                    <label for="criterio">Seleccione un criterio de búsqueda*</label>
                                    <select class="form-control select2" name="criterio" id="criterio" style="width: 100%"
                                            data-toggle="tooltip" title="Indique una categoría" required tabindex="1" autofocus>
                                        <option value="cotizaciones.EstadoCotizacion" selected>Estado Cotización</option>
                                        <option value="clientes.RazonSocial" >Razon social cliente</option>
                                        <option value="cotizaciones.IdCotizacion" >ID Cotización</option>
                                        <option value="cotizaciones.NitClienteCotizaciones" >Nit cliente</option>
                                        <?php
                                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                            <option value="cotizaciones.CedulaEmpleadoCotizaciones" >Cédula asesor</option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>

                                <label for="comobuscar" >¿Cómo desea consultar? y ¿Qué desea encontrar?*</label>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select name="comobuscar" id="comobuscar" class="btn btn-default dropdown-toggle"
                                                    style="width: 100%" data-toggle="dropdown" tabindex="2">
                                                <option value="1">Una búsqueda exacta de</option>
                                                <option selected value="2">Cualquier coincidencia de</option>
                                            </select>
                                        </div><!-- /btn-group -->
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="busqueda" class="form-control" placeholder="Número Nit | Razón Social | Lugar" required
                                                   data-toggle="tooltip" title="Indique lo que desea buscar" tabindex="3">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                    <span class="input-group-btn">
                      <button class="btn bg-aqua-gradient btn-flat" type="submit" tabindex="4" data-toggle="tooltip" title="Clic para consultar">
                          <i class="fa fa-search">     </i>     Buscar cotización(es)</button>
                    </span>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                    <?php
                    if(isset($_GET['encontrados'])&&$_GET['encontrados']=='false') {
                        ?>


                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Consulta sin coincidencias</h4>

                            <p>No se han encontrado resultados para la consulta de:</p>
                            <p>Criterio: <?php echo $_GET['criterio'] ?></p>
                            <p>Búsqueda: <?php echo $_GET['busqueda'] ?></p>
                        </div>


                        <?php
                    }
                    if((isset($_GET['criterio'])&&isset($_GET['busqueda'])&&isset($_GET['comobuscar']))) {
                        $consulta = $cotFaca->buscarConCriterio($_GET['criterio'], $_GET['busqueda'], $_GET['comobuscar']);
                    }
                    if(isset($_GET['todos'])){
                        $consulta = $cotFaca->listarTodas();
                    }

                    if(isset($consulta)) {

                        if (count($consulta)>0) {

                            ?>

                            <div class="box box-primary box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Resultados de la búsqueda</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        Se han encontrado <span class="badge label-info"><?php echo count($consulta); ?></span>
                                        registros para esta consulta.
                                    </p>
                                    <br>
                                    <table id="example1" class="table table-bordered table-striped table-responsive table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>#Cot.</th>
                                            <th>#Nit</th>
                                            <th>Razón social</th>
                                            <?php
                                            if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                                ?>
                                                <th>Asesor</th>
                                                <?php
                                            }
                                            ?>
                                            <th>Valor</th>
                                            <th>Estado</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $_SESSION['exportar'] = $consulta;
                                        foreach ($consulta as $respuesta){
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo date("Y-m-d", strtotime($respuesta['FechaCreacionCotizacion'])); ?>
                                            </td>
                                            <td>
                                                <?php echo $respuesta['IdCotizacion']; ?>
                                            </td>
                                            <td>
                                                <?php echo $respuesta['NitClienteCotizaciones']; ?>
                                            </td>
                                            <td>
                                                <?php echo $respuesta['RazonSocial']; ?>
                                            </td>
                                            <?php
                                            if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                                ?>
                                                <td>
                                                    <?php echo $respuesta['Nombres'].' '.substr($respuesta['Apellidos'],0,1).'.'; ?>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                            <td>
                                                <a class="label label-info label-xs"
                                                   data-toggle="tooltip" title="Ver detalle" target="_blank"
                                                   href="ex2.php?coti=<?php echo $respuesta['IdCotizacion'] ?>">
                                                                <span
                                                                    class="fa fa-search-plus">  $<?php echo number_format($respuesta['ValorTotalCotizacion']); ?>
                                                                </span>
                                                </a>

                                            </td>
                                            <td>

                                                <?php if($respuesta['EstadoCotizacion']=='Vigente'){ ?>
                                                    <div class="btn-group" data-toggle="tooltip" title="Cambiar estado">
                                                        <button type="button" class="btn btn-xs btn-success">
                                                            <?php echo $respuesta['EstadoCotizacion']; ?>
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="crearOrden.php?idcliente=<?php echo $respuesta['IdCotizacion']; ?>">
                                                                    Generar pedido
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="../controllers/controladorCotizacion.php?id=<?php echo $respuesta['IdCotizacion'] ?>&cancelar=true">
                                                                    Cancelar
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php }else{ ?>
                                                    <span class="label label-<?php if($respuesta['EstadoCotizacion']=='Cancelada'){
                                                        echo 'warning';
                                                    }else if($respuesta['EstadoCotizacion']=='Vigente'){
                                                        echo 'success';
                                                    }else if($respuesta['EstadoCotizacion']=='Pedido'){
                                                        echo 'default';
                                                    }
                                                    ?>">
                                                    <?php echo $respuesta['EstadoCotizacion']; ?>
                                                        </span>
                                                <?php } ?>

                                            </td>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>#Cot.</th>
                                            <th>#Nit</th>
                                            <th>Razón social</th>
                                            <?php
                                            if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                                ?>
                                                <th>Asesor</th>
                                                <?php
                                            }
                                            ?>
                                            <th>Valor</th>
                                            <th>Estado</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    if (!isset($_GET['todos'])){ ?>
                        <!-- general form elements disabled -->
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver todos los registros</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <p>
                                    Si desea, puede ver todos los registros usando el botón "Ver todos"
                                    (esta opción puede tardar un poco).
                                </p>
                            </div>
                            <div class="box-footer">
                                <form action="../views/buscarCotizaciones.php?todos=todos" method="post">
                                    <button type="submit" class="btn bg-green-gradient pull-right" tabindex="14"
                                    > <i class="fa fa-plus-square-o"> </i>   Ver todos
                                    </button>
                                </form>
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div><!--/.col (right) -->
            </div>   <!-- /.row -->




        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function() {

        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                busqueda: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0,1, 'desc' ]]
        });
    });
</script>
</html>
