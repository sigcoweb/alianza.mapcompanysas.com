<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
include_once'../models/ProductosCotizados.php';
include_once'../facades/FacadeProducto.php';
include_once'../facades/ColoresFacade.php';
include_once'../facades/AromasFacade.php';
include_once'../facades/ConcentracionesFacade.php';
include_once'../facades/PresentacionesFacade.php';
include_once '../facades/FacadeCotizaciones.php';
$facadeCotizaciones=new FacadeCotizaciones();
$facadePresentaciones=new PresentacionesFacade();
$facadeProducto= new FacadeProducto();
$facadeColores= new ColoresFacade();
$facadeAromas= new AromasFacade();
$facadeConcentraciones=new ConcentracionesFacade();
$despacho=$facadeCotizaciones->traerCotizacionPedido($_GET['pedido']);
//print_r($despacho);
$facturacion=$facadeCotizaciones->traerDespacho($_GET['pedido']);
$comosedespacha=0;
$comosefactura=0;
$cantidadLitrosDespacho=0;
$valorMinimo=0;


?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Detalles pedido</title>



    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Detalles
                <small>Pedido</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Cotizaciones</a></li>
                <li class="active">Crear cotización</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-12">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <form id="defaultForm" action="../controllers/controladorCotizacion.php?agregar=true&idcliente=<?php echo $cliente?>" method="post">


                        <?php
                        include_once '../facades/ClienteFacade.php';
                        if(isset($_GET['idcliente'])){
                            $clienteFac = new ClienteFacade();
                            $cliente = $clienteFac->obtenerCliente($_GET['idcliente']);
                        }
                        ?>


                        <div class="box box-default collapsed-box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver información corporativa registrada</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="nitEmpresaPuntoEntrega" id="labelNit">Nit*</label>
                                            <input type="text" name="nitEmpresaPuntoEntrega" id="nitEmpresaPuntoEntrega" class="form-control exists" readonly
                                                   value="<?php echo $cliente['Nit'] ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="RazonSocial">Razón social*</label>
                                            <input type="text" name="RazonSocial" id="RazonSocial" class="form-control"
                                                   value="<?php echo $cliente['RazonSocial'] ?>" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="NombreComercial">Nombre comercial*</label>
                                            <input type="text" name="NombreComercial" id="NombreComercial" class="form-control"
                                                   value="<?php echo $cliente['nombreComercial'] ?>" readonly />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Direccion">Dirección*</label>
                                            <input type="text" name="Direccion" id="Direccion" class="form-control"
                                                   value="<?php echo $cliente['Direccion'] ?>" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Telefono">Teléfono principal*</label>
                                            <input type="text" name="Telefono" id="Telefono" class="form-control"
                                                   value="<?php echo $cliente['Telefono'] ?>" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Email2">Email corporativo*</label>
                                            <input type="email" name="Email2" id="Email2" class="form-control"
                                                   value="<?php echo $cliente['EmailCliente'] ?>" readonly  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdDep">Departamento*</label>
                                            <input type="text" name="IdDep" id="IdDep" class="form-control"
                                                   value="<?php echo $cliente['nombreDepartamento'] ?>" readonly />
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdLu">Lugar*</label>
                                            <input type="text" name="IdLu" id="IdLu" class="form-control"
                                                   value="<?php echo $cliente['NombreLugar'] ?>" readonly  />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdTipo">Tipo de cliente*</label>
                                            <input type="text" name="IdTipo" id="IdTipo" class="form-control"
                                                   value="<?php echo $cliente['NombreClasificacion'] ?>" readonly  />
                                        </div>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdActividad">Actividad del cliente*</label>
                                            <input type="text" name="IdActividad" id="IdActividad" class="form-control"
                                                   value="<?php echo $cliente['NombreActividad'] ?>" readonly required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="box box-default collapsed-box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver información de contacto registrada</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Cedula">Cédula*</label>
                                            <input type="text" name="Cedula" id="Cedula" class="form-control" readonly
                                                   value="<?php echo $cliente['CedulaPersona']; ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Nombres">Nombres*</label>
                                            <input type="text" name="Nombres" id="Nombres" class="form-control" readonly
                                                   value="<?php echo $cliente['Nombres']; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Apellidos">Apellidos*</label>
                                            <input type="text" name="Apellidos" id="Apellidos" class="form-control" readonly
                                                   value="<?php echo $cliente['Apellidos']; ?>" />
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Celular">Celular*</label>
                                            <input type="text" name="Celular" id="Celular" class="form-control" readonly
                                                   value="<?php echo $cliente['CelularPersona']; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Email1">Email*</label>
                                            <input type="email" name="Email1" id="Email1" class="form-control" readonly
                                                   value="<?php echo $cliente['EmailPersona']; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <?php
                        if($cliente['EstadoPersona']=='Inactivo'){
                            ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-warning"></i> Advertencia</h4>
                                Este cliente se encuentra inactivo. El estado sólo puede ser modificado por un coordinador.
                            </div>
                            <?php
                        }
                        ?>









                        <div class="box box-solid box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Productos para facturación:</h3>
                            </div>
                            <div class="box-body">


                                <table id="example1" class="table table-condensed table-responsive table-striped table-hover text-center">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Unidad</th>
                                        <th>Cantidad</th>
                                        <th>V/unitario</th>
                                        <th>V/venta</th>
                                        <th>Diferencia</th>
                                        <th>SubTotal venta</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    $cambio=0;
                                    $subtotal=0;
                                    $totalbase=0;
                                    $total=0;
                                    foreach ($despacho as $test) {
                                        if($despacho[$i]['Abreviatura']!=$facturacion[$i]['Abreviatura']){

                                            switch ($despacho[$i]['Abreviatura']) {
                                                Case "Lt":
                                                    $comosedespacha = 1;
                                                    $cantidadLitrosDespacho = 1;
                                                    break;
                                                Case "Gl":
                                                    $comosedespacha = 2;
                                                    $cantidadLitrosDespacho = 4;
                                                    break;
                                                Case "Cñ":
                                                    $comosedespacha = 3;
                                                    $cantidadLitrosDespacho = 20;
                                                    break;
                                                Case "Cn":
                                                    $comosedespacha = 4;
                                                    $cantidadLitrosDespacho = 60;

                                                    break;
                                                Case "Tm":
                                                    $comosedespacha = 5;
                                                    $cantidadLitrosDespacho = 208;
                                                    break;
                                            }
                                            $cantidadLitrosFacturacion = 0;

                                            switch ($facturacion[$i]['Abreviatura']) {
                                                Case "Lt":
                                                    switch ($comosedespacha) {
                                                        Case 2:
                                                            $cantidadLitrosFacturacion = 4;
                                                            break;
                                                        Case 3:
                                                            $comosedespacha = 3;
                                                            $cantidadLitrosFacturacion = 20;
                                                            break;
                                                        Case 4:
                                                            $comosedespacha = 4;
                                                            $cantidadLitrosFacturacion = 60;
                                                            break;
                                                        Case 5:
                                                            $comosedespacha = 5;
                                                            $cantidadLitrosFacturacion = 208;
                                                            break;
                                                    }
                                                    break;

                                                Case "Gl":
                                                    switch ($comosedespacha) {
                                                        Case 2:
                                                            $cantidadLitrosFacturacion = 4;
                                                            break;
                                                        Case 3:
                                                            $comosedespacha = 3;
                                                            $cantidadLitrosFacturacion = 20 / 4;
                                                            break;
                                                        Case 4:
                                                            $comosedespacha = 4;
                                                            $cantidadLitrosFacturacion = 60 / 4;
                                                            break;
                                                        Case 5:
                                                            $comosedespacha = 5;
                                                            $cantidadLitrosFacturacion = 208 / 4;
                                                            break;
                                                    }
                                                    break;


                                                Case "Cñ":
                                                    switch ($comosedespacha) {
                                                        Case 2:
                                                            $cantidadLitrosFacturacion = 4;
                                                            break;
                                                        Case 3:
                                                            $comosedespacha = 3;
                                                            $cantidadLitrosFacturacion = 20 / 20;
                                                            break;
                                                        Case 4:
                                                            $comosedespacha = 4;
                                                            $cantidadLitrosFacturacion = 60 / 20;
                                                            break;
                                                        Case 5:
                                                            $comosedespacha = 5;
                                                            $cantidadLitrosFacturacion = 208 / 20;
                                                            break;
                                                    }
                                                    break;
                                                Case "Cn":
                                                    $comosefactura = 4;
                                                    $cantidadLitrosFacturacion = 60;
                                                    break;
                                                Case "Tm":
                                                    $comosefactura = 5;
                                                    $cantidadLitrosFacturacion = 208;
                                                    break;
                                            }

                                            if ($comosedespacha > $comosefactura) {
                                                $valorMinimo = $despacho[$i]['valorPresentacion'] / $cantidadLitrosFacturacion;
                                            }

                                        }else{
                                            $valorMinimo=$despacho[$i]['valorPresentacion'];
                                        }

                                        $totalbase=$totalbase+($valorMinimo*$facturacion[$i]['cantidadFacturacion']);
                                        $valorVendido=$test['ValorVentaProducto'];
                                        $diferencia=$valorVendido-round($valorMinimo);
                                        ?>
                                        <tr>
                                            <td><?php echo $test['IdProducto']?></td>
                                            <td><?php echo $test['NombreProducto']?></td>
                                            <td><?php echo $facturacion[$i]['Abreviatura']?></td>
                                            <td><?php echo $facturacion[$i]['cantidadFacturacion']?></td>
                                            <td>$ <?php echo number_format($valorMinimo)?></td>
                                            <td>$ <?php echo number_format($test['ValorVentaProducto']);?>
                                            </td>
                                            <td>
                                                <?php
                                                $porcentaje=round(($diferencia*100)/$valorMinimo);
                                                if($porcentaje<0){
                                                    $porcentaje=$porcentaje*-1;
                                                }

                                                if($diferencia==0){
                                                    ?>
                                                    <label for="" class="label bg-yellow-gradient"><?php

                                                        echo "0";

                                                        ?></label>
                                                    <?php
                                                }

                                                if($diferencia<0){?>

                                                    <label for="" class="label bg-red-gradient"><?php

                                                        echo "$".number_format($diferencia);

                                                        ?></label>

                                                    <label for="" class="label bg-red-gradient"><?php

                                                        echo $porcentaje."%";

                                                        ?></label>

                                                    <?php
                                                }elseif($diferencia>0){?>
                                                    <label for="" class="label bg-green-gradient"><?php
                                                        echo "$".number_format($diferencia);

                                                        ?></label>
                                                    <label for="" class="label bg-red-gradient"><?php

                                                        echo $porcentaje."%";

                                                        ?></label>
                                                <?php
                                                }


                                                ?>


                                            </td>

                                            <td>$ <?php echo number_format($test['TotalDetalle']);
                                                ?></td>




                                        </tr>

                                        <?php
                                        $i++;
                                        $total=$total+$test['TotalDetalle'];
                                    }

                                    ?>

                                    </tbody>
                                </table>
                                <br>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                            <b>
                                                Observaciones cotización No. <?php echo $despacho[0]['IdCotizacion'] ?>:
                                            </b>
                                            <p>
                                                <?php
                                                echo $despacho[0]['ObservacionesCotizacion'];
                                                ?>
                                            </p>

                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                            <b>
                                                Observaciones pedido No. <?php echo $despacho[0]['IdPedido'] ?>:
                                            </b>
                                            <p>
                                                <?php
                                                echo $despacho[0]['ObservacionesPedido'];
                                                ?>
                                            </p>

                                        </div>
                                    </div>
                                    <div class="col-lg-4 pull-right">
                                        <table id="example4" class="table table-condensed table-responsive table-striped table-hover">
                                            <tr>
                                                <td>
                                                    <b class="text-center text-light-blue">
                                                        Total Base
                                                    </b>
                                                </td>
                                                <td><b>$
                                                        <?php
                                                        echo number_format($totalbase);
                                                        ?>
                                                    </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <b class="text-center text-light-blue">
                                                        Total Venta
                                                    </b>
                                                </td>
                                                <td><b>$
                                                        <?php
                                                        echo number_format($despacho[0]['ValorTotalCotizacion']);
                                                        ?>
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php
                                            if($totalbase>$total){?>
                                                <tr>
                                                    <td>
                                                        <b class="text-center text-light-blue">
                                                            Descuento
                                                        </b>
                                                    </td>
                                                    <td><b>$
                                                            <?php
                                                            echo number_format($totalbase-$total);
                                                            ?>
                                                        </b>
                                                    </td>
                                                </tr>
                                               <?php
                                            }
                                            ?>



                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>






                        <div class="box box-solid box-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Productos para despacho:</h3>
                            </div>
                            <div class="box-body">

                                <table id="example1" class="table table-condensed table-responsive table-striped table-hover text-center">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Unidad</th>
                                        <th>Cantidad</th>

                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    $cambio=0;
                                    $subtotal=0;
                                    $totalbase=0;
                                    $total=0;
                                    foreach ($despacho as $test) {

                                        ?>
                                        <tr>
                                            <td><?php echo $test['IdProducto']?></td>
                                            <td><?php echo $test['NombreProducto']?></td>

                                            <td><?php



                                                if($facturacion[$i]['Abreviatura']==$test['Abreviatura']){
                                                    echo $test['Abreviatura'];
                                                }else{?>

                                                <label for="" class="label bg-red-gradient"><?php  echo $test['Abreviatura']?></label>
                                                <?php
                                                }

                                                ?>


                                            </td>
                                            <td><?php echo $test['CantidadProductos']?></td>





                                        </tr>

                                        <?php
                                        $i++;
                                    }

                                    ?>

                                    </tbody>
                                </table>
                                <br>


                            </div>

                            <div class="box-footer">
                                <a href="../controllers/ControladorOrdenCompra.php?autorizarPedido=true&idpedido=<?php echo $_GET['pedido']?>" class="btn bg-green-gradient pull-left" type="button" tabindex="2"
                                   value="Finalizar" name="finalizar" id="finish" >Aprobar pedido
                                </a>
                                <input type="button" class="btn bg-yellow-gradient pull-right" tabindex="1"
                                       onclick="location.href='buscarOrdenes.php'" value="Cancelar"/>
                            </div>
                        </div>





                    </form>






                </div>

                <!-- /.box -->
            </div>



            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

    <script type="text/javascript">
        $(document).ready(function() {

            $("#idproducto").change(function () {
                $('#idpresentacion').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idpresentacion').val('0');
                    $('#idpresentacion').change();
                    $.post("presentacionesProducto.php", { idproducto: idproducto }, function(data){
                        $("#idpresentacion").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idaroma').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idaroma').val('0');
                    $('#idaroma').change();
                    $.post("aromasProducto.php", { idproducto: idproducto }, function(data){
                        $("#idaroma").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idcolor').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idcolor').val('0');
                    $('#idcolor').change();
                    $.post("coloresProducto.php", { idproducto: idproducto }, function(data){
                        $("#idcolor").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idconcentracion').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idconcentracion').val('0');
                    $('#idconcentracion').change();
                    $.post("concentracionesProducto.php", { idproducto: idproducto }, function(data){
                        $("#idconcentracion").html(data);
                    });
                });
            });


            $('#datosCotizacion').hide();

            function randomNumber(min, max) {
                return Math.floor(Math.random() * (max - min + 1) + min);
            }

            function generateCaptcha() {
                $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
            }

            generateCaptcha();
            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    idproducto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    idaroma:{
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }

                    },

                    idcolor:{
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }

                    },

                    meta: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    cantidad: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    }


                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>
</body>
</html>
