<?php
include_once'../models/ProductosCotizados.php';
include_once'../facades/FacadeProducto.php';
include_once'../facades/ClienteFacade.php';
include_once'../facades/ColoresFacade.php';
include_once'../facades/AromasFacade.php';
include_once'../facades/ConcentracionesFacade.php';
include_once'../facades/PresentacionesFacade.php';
include_once'../facades/FacadeEmpleado.php';
$faca = new PresentacionesFacade();
$empleadoFac = new FacadeEmpleado();
$clienteFac = new ClienteFacade();
$facadePresentaciones=new PresentacionesFacade();
$facadeProducto= new FacadeProducto();
$facadeColores= new ColoresFacade();
$facadeAromas= new AromasFacade();
$facadeConcentraciones=new ConcentracionesFacade();
$cliente=$_GET['idcliente'];
$empresaCotiza=$_GET['cotiza'];
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registrar cotización</title>

    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons --
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>-->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons --
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!--<link rel="stylesheet" href="../../plugins/ionic/css/ionic.css" type="text/css">
    <script type="text/javascript" src="../../plugins/ionic/js/ionic.js"></script>-->
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Cotización</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Cotizaciones</a></li>
                <li class="active">Crear cotización</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-12">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <form id="defaultForm" action="../controllers/controladorCotizacion.php?finalizar=true&idcliente=<?php echo $cliente?>&cotiza=<?php echo $empresaCotiza?>" method="post">

                        <div class="box box-info box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Indicaciones de registro</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para registrar una nueva
                                        cotización.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->


                        <!--<div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Datos Cliente</h3>
                            </div>

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="cantidad">ID cliente:*</label>
                                    <input class="form-control" name="idcliente" id="idcliente" type="text" value="<?php echo $cliente?>" readonly>
                                </div>

                            </div>

                        </div>-->


                        <?php
                        if(isset($_GET['idcliente'])) {
                            $datacliente = $clienteFac->buscarCliente('clientes.Nit', $_GET['idcliente'], 1);
                        }
                        ?>


                        <div class="box box-default collapsed-box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver información corporativa registrada</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="idcliente" id="labelNit">Nit*</label>
                                            <input type="text" name="idcliente" id="idcliente" class="form-control exists" readonly
                                                   value="<?php echo $datacliente[0]['Nit'] ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="RazonSocial">Razón social*</label>
                                            <input type="text" name="RazonSocial" id="RazonSocial" class="form-control"
                                                   value="<?php echo $datacliente[0]['RazonSocial'] ?>" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="NombreComercial">Nombre comercial*</label>
                                            <input type="text" name="NombreComercial" id="NombreComercial" class="form-control"
                                                   value="<?php echo $datacliente[0]['nombreComercial'] ?>" readonly />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="Direccion">Dirección*</label>
                                            <input type="text" name="Direccion" id="Direccion" class="form-control"
                                                   value="<?php echo $datacliente[0]['Direccion'] ?>" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="Telefono">Teléfono principal*</label>
                                            <input type="text" name="Telefono" id="Telefono" class="form-control"
                                                   value="<?php echo $datacliente[0]['Telefono'] ?>" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="Email2">Email corporativo*</label>
                                            <input type="email" name="Email2" id="Email2" class="form-control"
                                                   value="<?php echo $datacliente[0]['EmailCliente'] ?>" readonly  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="IdDep">Departamento*</label>
                                            <input type="text" name="IdDep" id="IdDep" class="form-control"
                                                   value="<?php echo $datacliente[0]['nombreDepartamento'] ?>" readonly />
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="IdLu">Lugar*</label>
                                            <input type="text" name="IdLu" id="IdLu" class="form-control"
                                                   value="<?php echo $datacliente[0]['NombreLugar'] ?>" readonly  />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="IdTipo">Tipo de cliente*</label>
                                            <input type="text" name="IdTipo" id="IdTipo" class="form-control"
                                                   value="<?php echo $datacliente[0]['NombreClasificacion'] ?>" readonly  />
                                        </div>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="IdActividad">Actividad del cliente*</label>
                                            <input type="text" name="IdActividad" id="IdActividad" class="form-control"
                                                   value="<?php echo $datacliente[0]['NombreActividad'] ?>" readonly required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <?php if(!isset($total)&&isset($_SESSION['productoDatos'])){ ?>


                            <div id="mensajeError" class="box box-warning box-solid collapsed-box" hidden>
                                <div class="box-header with-border">
                                    <h3 class="box-title">Error de presentación (Las cantidades deben ser exactas)</h3>
                                    <div class="box-tools pull-right">

                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">

                                    <div class="form-group">
                                        <p>
                                            No es posible cambiar la presentación para despacho.<br>
                                            Las cantidades deben ser exactas.
                                        </p>
                                    </div>
                                </div>
                            </div>


                            <div class="box box-solid box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Descuentos por producto:</h3>
                                </div>
                                <div class="box-body">


                                    <table id="example1" class="table table-bordered table-striped table-hover text-left table-condensed">
                                        <thead>
                                        <tr>
                                            <th>Cód</th>
                                            <th>Producto</th>
                                            <th>Unidad Despacho</th>
                                            <th>Cantidad Despacho</th>
                                            <th>Valor base</th>
                                            <th>Unidad Facturación</th>
                                            <th>Cantidad Facturación</th>
                                            <th>Precio venta</th>
                                            <th>$ Total</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i=0;
                                        $total=0;
                                        foreach ($_SESSION['productoDatos'] as $test) {

                                            ?>
                                            <tr>
                                                <td><?php echo $test->getId()?></td>
                                                <td><?php echo $test->getNombre()?></td>
                                                <td>
                                                    <?php $presentacion=$facadePresentaciones->traerPresentacion($test->getPresentacion());
                                                    echo $presentacion['Abreviatura'];

                                                    ?>
                                                </td>
                                                <td><?php echo $test->getCantidad()?></td>
                                                <td>$ <?php echo number_format($test->getValorBase())?></td>

                                                <td>

                                                    <?php
                                                    $facs=$faca->buscarConCriterio("productos.IdProducto",$test->getId(),1);
                                                    $fact="'".$presentacion['Abreviatura']."'";
                                                    $productoID="'".$test->getId();
                                                    ?>
                                                    <select name="presentacionDespacho<?php echo $i ?>" class="select2" id="presentacionDespacho<?php echo $i ?>" onchange="calcular(<?php echo $i.','.$test->getCantidad().','.$fact?>)"      >


                                                        <?php
                                                        foreach ($facs as $presentaciones) {?>
                                                            <option <?php if ($presentacion['IdPresentacion']==$presentaciones['IdPresentacion']){?>
                                                                selected
                                                                <?php
                                                                }


                                                                ?>value="<?php echo $presentaciones['IdPresentacion']?>|<?php echo $presentaciones['valorPresentacion']?>"><?php echo $presentaciones['Abreviatura']?></option>
                                                            <?php
                                                        }
                                                        ?>


                                                    </select>



                                                </td>

                                                <td>
                                                    <input class="form-horizontal" readonly name="cantidadDespacho<?php echo $i ?>" id="cantidadDespacho<?php echo $i ?>"  type="text" value="<?php echo $test->getCantidad() ?>">
                                                </td>



                                                <td>
                                                    <input hidden readonly type="text" width="20%" id="descc<?php echo $i?>"name="descuento2<?php echo $i ?>" id="descuento" class="form-horizontal" value="<?php echo $test->getValorBase()?>" >

                                                    <input type="text" width="20%" id="desc<?php echo $i?>"name="descuento<?php echo $i ?>" id="descuento" class="form-horizontal" value="<?php echo $test->getValorBase()?>" onchange="descuentoAplicar(<?php echo $i.','.$test->getCantidad()?>)">
                                                </td>
                                                <td>$<label class="control-label" id="base<?php echo $i?>" ><?php echo $test->getSubtotal()?></label></td>




                                            </tr>

                                            <?php
                                            $i++;
                                            $total=$total+$test->getSubtotal();
                                        }

                                        ?>



                                        </tbody>
                                    </table>

                                    <hr>
                                    <?php if (isset($_SESSION['productoDatos'])) {?>


                                    <?php }
                                    ?>
                                </div>


                            </div>

                        <?php } ?>

                        <div class="box box-solid box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Observaciones:</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">

                                    <label for="cantidad">Observaciones Cotización:*</label>
                                    <input class="form-control" name="observaciones" id="cantidad" type="text" placeholder="Se garantiza descuento de $100.000 si se confirma la compra dentro de los siguientes 5 días hábiles">
                                </div>
                            </div>
                        </div>

                        <div class="box box-solid box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registrar</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>¿Adjuntar la cotización por correo?</label>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-lg-3" id="enviar">
                                                    <label for="no"> No <input required type="radio" name="enviar"
                                                                               id="no" value="0" tabindex="19" onchange="javascript:noCorreo()">
                                                    </label>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label for="si"> Si <input required type="radio" name="enviar"
                                                                               id="si" value="1" tabindex="20" onchange="javascript:siCorreo()">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="correo" style="display: none;">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="correo[]">Seleccione el correo del cliente*</label><br>
                                                <select class="form-control select2" multiple="multiple" style="width: 100%" name="correo[]" id="correo[]" required tabindex="16">
                                                    <?php $correos=$clienteFac->listarCorreosNit($_GET['idcliente']); foreach($correos as $correo){ ?>
                                                        <option value="<?php echo $correo['emails']; ?>"><?php echo $correo['detalle'].' | '.$correo['emails']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php
                                    if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
                                        ?>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="asesor">Asesor encargado del cliente*</label>
                                                <select class="form-control select2" name="asesor" id="asesor" required tabindex="16">
                                                    <option value="" >Seleccione...</option>
                                                    <?php
                                                    $empleados = $empleadoFac->listarUsuarios();
                                                    foreach ($empleados as $empleado) {
                                                        ?>
                                                        <option <?php if($empleado['EstadoPersona']=="Inactivo"||$empleado['NombreRol']=="Coordinador"){echo 'disabled';} ?>
                                                            value="<?php echo $empleado['CedulaEmpleado'] ?>">
                                                            <?php echo $empleado['CedulaEmpleado'].' | '.$empleado['Nombres'].' '.$empleado['Apellidos'] ?>
                                                            <?php if($empleado['EstadoPersona']=="Inactivo"){echo '| Inactivo';} ?>
                                                            <?php if($empleado['NombreRol']=="Coordinador"){echo '| Coordinador';} ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="box-footer">
                                    <button class="btn bg-yellow-gradient btn-block btn-flat" type="submit"
                                            value="Finalizar" name="finalizar" id="finish" ><i class="fa fa-floppy-o">   </i>     Finalizar Cotización
                                    </button>
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box-body -->
                        </div>


                    </form>



                </div>

                <!-- /.box -->
            </div>



            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
    </footer>
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>

    <script type="text/javascript">
        correo = document.getElementById("correo");
        function siCorreo() {
            correo.style.display='block';
        }
        function noCorreo() {
            correo.style.display='none';
        }
    </script>
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2({
                language: "es",
                placeholder: "Seleccione...",
                theme: "classic",
                tags: true,
                tokenSeparators: [',', ' ']
            });
        });

        $(document).ready(function() {


            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    asesor: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    meta: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    }


                }
            });
        });
    </script>

    <script type="text/javascript">




        function calcular(i,cantidad,presentacion){
            var numeroselect="presentacionDespacho"+i;
            var input_despacho="cantidadDespacho"+i;
            var cantidad=cantidad;
            var input="desc"+i;
            var input_original="descc"+i;
            var valorBase=parseFloat(document.getElementById(input_original).value);
            var combo = document.getElementById(numeroselect);
            var valueOriginal=combo.value;
            var separar=valueOriginal.split("|");
            var valorNuevo=separar[1];
            var selected = combo.options[combo.selectedIndex].text;
            var presentacionDespacho=0;
            if(presentacion!=selected){

                switch (presentacion){
                    case "Gl":
                        cantidad=cantidad*4;
                        presentacionDespacho=2;
                        break;

                    case "Lt":
                        cantidad=cantidad;
                        presentacionDespacho=1;

                        break;

                    case "Cnx20":
                        cantidad=cantidad*20;
                        presentacionDespacho=3;
                        break;

                    case "Cnx60":
                        cantidad=cantidad*60;
                        presentacionDespacho=4;
                        break;

                    case "Tm":
                        cantidad=cantidad*208;
                        presentacionDespacho=5;
                        break;

                    case "IsoTm":
                        cantidad=cantidad*1000;
                        presentacionDespacho=6;
                        break;
                }

                switch(selected){
                    case "Lt":
                        document.getElementById(input_despacho).value=cantidad;
                        switch (presentacionDespacho){
                            case 2:
                                valorTotal=valorBase/4;
                                break;
                            case 3:
                                valorTotal=valorBase/20;
                                break;
                            case 4:
                                valorTotal=valorBase/60;
                                break;
                            case 5:
                                valorTotal=valorBase/208;
                                break;

                        }
                        document.getElementById(input).value=Math.round(valorTotal);

                        break;


                    case "Gl":
                        var galon=4;
                        var total=cantidad/galon;
                        var residuo=cantidad % galon;

                        if(residuo==0){
                            switch (presentacionDespacho){
                                case 3:
                                    valorTotal=valorBase/total;
                                    break;
                                case 4:
                                    valorTotal=valorBase/total;
                                    break;
                                case 5:
                                    valorTotal=valorBase/total;
                                    break;

                            }
                            document.getElementById(input_despacho).value=total;
                            document.getElementById(input).value=Math.round(valorTotal);


                            ocultarBoton(total);

                        }else{
                            $('#mensajeError').show();
                            document.getElementById(input_despacho).value="0";
                            ocultarBoton(0);

                        }
                        break;

                    case "Cnx20":
                        var canecax20=20;
                        var total=cantidad/canecax20;
                        var residuo=cantidad % canecax20;

                        if(residuo==0){
                            switch (presentacionDespacho){
                                case 4:
                                    valorTotal=valorBase/total;
                                    break;
                                case 5:
                                    valorTotal=valorBase/total;
                                    break;

                            }
                            document.getElementById(input_despacho).value=total;
                            document.getElementById(input).value=valorTotal;


                            ocultarBoton(total);

                        }else{
                            $('#mensajeError').show();
                            document.getElementById(input_despacho).value="0";
                            ocultarBoton(0);

                        }
                        break;

                    case "Cnx60":
                        var canecax60=60;
                        var total=cantidad/canecax60;
                        var residuo=cantidad % canecax60;

                        if(residuo==0){
                            switch (presentacionDespacho){
                                case 5:
                                    valorTotal=valorBase/total;
                                    break;

                            }
                            document.getElementById(input_despacho).value=total;
                            document.getElementById(input).value=valorTotal;
                            ocultarBoton(total);


                        }else{
                            $('#mensajeError').show();

                            document.getElementById(input_despacho).value="0";
                            ocultarBoton(0);

                        }
                        break;

                    case "Tm":
                        var tambor=208;
                        var total=cantidad/tambor;
                        var residuo=cantidad % tambor;
                        if(residuo==0){
                            document.getElementById(input_despacho).value=total;
                            document.getElementById(input).value=valorBase/tambor;
                            ocultarBoton(total);


                        }else{
                            $('#mensajeError').show();

                            document.getElementById(input_despacho).value="0";
                            ocultarBoton(0);

                        }
                        break;

                }

            }else{
                document.getElementById(input).value=valorBase;
                document.getElementById(input_despacho).value=cantidad;
                ocultarBoton(cantidad);


            }




        }

        function ocultarBoton(cantidadActual){
            var hayCeros=false;
            var cantidad=cantidadActual;
            if(cantidad==0){
                hayCeros=true;
            }else{
                for(var i=0;i<10;i++){
                    if($("#cantidadDespacho"+i).length==0){
                    }else{
                        if(document.getElementById("cantidadDespacho"+i).value==0){
                            hayCeros=true;


                        }
                    }
                }
            }




            if(hayCeros==true) {
                $('#finish').hide();
            }

            if(hayCeros==false) {
                $('#finish').show();
            }



        }



        function decimal(i,valor){
            var total_input="total"+i;
            var descuento=valor;
            var base=document.getElementById(base_input).textContent;
            var baseFloat= parseFloat(base);
            if (descuento>baseFloat||descuento<0){
                document.getElementById("desc"+i).value="0";

            }else{
                var total=baseFloat-descuento;
                document.getElementById(total_input).textContent=format(total,0,',');

            }

        }

        function format(numero)
        {
            var  num= numero;
            //var num = input.value.replace(/\./g,'');
            if(!isNaN(num)){
                num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,');
                num = num.split('').reverse().join('').replace(/^[\.]/,'');
                return num;
            }
        }


        function porcentaje(i,porc){
            var total_input="total"+i;
            var base_input="base"+i;
            var base=document.getElementById(base_input).textContent;
            var baseFloat= parseFloat(base);
            var porcentaje=baseFloat*(porc/100);
            var total=baseFloat-porcentaje;
            document.getElementById(total_input).textContent=currency(total,0,',');
            document.getElementById("desc"+i).value=porcentaje;



        }

        function descuentoAplicar(i,cantidad){
            var input="desc"+i;
            var base_input="base"+i;
            var cantidadReal=document.getElementById("cantidadDespacho"+i).value;
            if(document.getElementById(input).value<=0){
                document.getElementById(input).value="0";
            }else {
                var v1 = document.getElementById(input).value;
                var valor = parseFloat(v1);
                var total = valor * cantidadReal;
                document.getElementById(base_input).textContent = format(total, 0, ',');
            }


        }

        function currency(value, decimals, separators) {
            decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
            separators = separators || ['.', "'", ','];
            var number = (parseFloat(value) || 0).toFixed(decimals);
            if (number.length <= (4 + decimals))
                return number.replace('.', separators[separators.length - 1]);
            var parts = number.split(/[-.]/);
            value = parts[parts.length > 1 ? parts.length - 2 : 0];
            var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
                separators[separators.length - 1] + parts[parts.length - 1] : '');
            var start = value.length - 6;
            var idx = 0;
            while (start > -3) {
                result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                    + separators[idx] + result;
                idx = (++idx) % 2;
                start -= 3;
            }
            return (parts.length == 3 ? '-' : '') + result;
        }
    </script>
</body>
</html>
