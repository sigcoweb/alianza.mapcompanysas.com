<?php session_start()?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
if ($_SESSION['datosLogin']['EstadoPersona'] == "Inactivo" or !isset($_SESSION['datosLogin'])) {
    header('location: Invalido.php');

}
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Buscar producto</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>

    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link href="../../plugins/animate/animate.css" rel="stylesheet" type="text/css"/>
    <script src="../../plugins/messajes/jquery.notynoty.packaged.min.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.css"/>

    <script>
        $(function () {
            $("#datepicker").datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels --> <span class="logo-mini"><b>M</b>AT</span>
            <!-- logo for regular state and mobile devices --> <span class="logo-lg"><b>Grupo</b>MAT</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The iterator image in the navbar-->
                            <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                                 class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span
                                class="hidden-xs"><?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The iterator image in the menu -->
                            <li class="user-header">
                                <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                                     class="img-circle" alt="User Image"/>

                                <p>
                                    <?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?>
                                    <small><?php echo $_SESSION['datosLogin']['NombreRol'] ?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../controllers/controladorCerrarSesion.php"
                                       class="btn btn-default btn-flat">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                         class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i
                            class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol'] ?>
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu</li>
                <!-- Optionally, you can add icons to the links -->
                <li><a href="index.php"><i class="fa fa-desktop"></i> <span>Inicio</span></a></li>
                <?php
                include_once '../facades/FacadeEmpleado.php';
                include_once '../facades/FacadeProducto.php';
                $facade = new FacadeEmpleado();
                $titulos = $facade->obtenerMenu($_SESSION['rol']['rol']);
                $nombre_archivo = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                //verificamos si en la ruta nos han indicado el directorio en el que se encuentra
                if (strpos($nombre_archivo, '/') !== FALSE)
                    //de ser asi, lo eliminamos, y solamente nos quedamos con el nombre y su extension
                    $nombre_archivo = array_pop(explode('/', $nombre_archivo));
                foreach ($titulos as $menu) {
                    $i = 0;
                    $subtitulos = $facade->obtenerSubMenu($menu['IdCategoria'], $_SESSION['rol']['rol']);
                    foreach ($subtitulos as $submenu) {
                        if ($nombre_archivo == $submenu['Url']) {
                            $i++;
                        };
                    }
                    ?>
                    <li class="treeview <?php if ($i >= 1) {
                        echo 'active';
                    }; ?>">
                        <a href="#"><i class="<?php echo $menu['Icono'] ?>"></i> <span><?php echo $menu['Nombre'] ?>
                            </span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <?php
                            $subtitulos = $facade->obtenerSubMenu($menu['IdCategoria'], $_SESSION['rol']['rol']);
                            foreach ($subtitulos as $submenu) { ?>
                                <li class="<?php if ($nombre_archivo == $submenu['Url']) {
                                    echo 'active';
                                }; ?>">
                                    <a href="<?php echo $submenu['Url'] ?>"><?php echo $submenu['NombrePagina'] ?></a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </li>

                    <?php
                }
                ?>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Catálogo de Productos
                <!--            <small>Optional description</small>-->
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Productos</a></li>
                <li class="active">Buscar</li>
            </ol>
            <form action="../controllers/ControladorProducto.php" method="post">
                <div class="row">

                </div>
                <!-- /.row -->
            </form>
        </section>


        <!-- Main content -->
        <section class="content">
            <!-- Horizontal Form -->


            <div class="row">
                <div class="col-md-10">
                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 'true') {
                            echo 'alert-error';
                        } else {
                            echo 'alert-success';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="fa fa-<?php if ($_GET['error'] == 'true') {
                                    echo 'warning';
                                } else {
                                    echo 'check';
                                }; ?>"> </i> Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 'true') {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <div class="box box-info box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Indicaciones para la búsqueda</h3>

                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Use las siguientes opciones para realizar la búsqueda de un producto.
                                Recuerde que en este formulario hay campos obligatorios(*).<br><br>
                            </p>

                        </div>
                    </div>

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Opciones de búsqueda</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            <form role="form" action="../controllers/ControladorProducto.php?buscar=true" method="post">

                                <div class="form-group">
                                    <label for="criterio">Seleccione un criterio de búsqueda*</label>
                                    <select class="form-control select2" name="criterio" id="criterio" required
                                            tabindex="1" autofocus>
                                        <option
                                            value="productos.IdProducto" <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'productos.IdProducto') {
                                            echo 'selected';
                                        } ?>>Código de Producto
                                        </option>
                                        <option
                                            value="productos.NombreProducto" <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'productos.NombreProducto') {
                                            echo 'selected';
                                        } ?>>Nombre Producto
                                        </option>
                                        <option
                                            value="valorpresentacion.valorPresentacion" <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'valorpresentacion.valorPresentacion') {
                                            echo 'selected';
                                        } ?>>Precio Producto
                                        </option>
                                    </select>
                                </div>

                                <label for="comobuscar" class="margin">¿Qué desea encontrar?*</label>

                                <div class="input-group input-group-sm margin">
                                    <div class="input-group-btn">
                                        <select name="comobuscar" id="comobuscar"
                                                class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                                tabindex="2">
                                            <option
                                                value="1" <?php if (isset($_GET['comobuscar']) && $_GET['comobuscar'] == 1) {
                                                echo 'selected';
                                            } ?>>Una búsqueda exacta de
                                            </option>
                                            <option
                                                value="2" <?php if (isset($_GET['comobuscar']) && $_GET['comobuscar'] == 2) {
                                                echo 'selected';
                                            } ?>>Cualquier coincidencia de
                                            </option>
                                        </select>
                                    </div>
                                    <!-- /btn-group -->
                                    <input type="text" name="busqueda" class="form-control"
                                           placeholder="Número Nit | Razón Social | Lugar" required tabindex="3">
                    <span class="input-group-btn">
                      <button class="btn btn-info btn-flat" type="submit" tabindex="4">Buscar Producto</button>
                    </span>
                                </div>
                                <!-- /input-group -->

                            </form>

                        </div>

                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog">
                                <div class="modal-content" style="border-radius: 5px;">
                                    <div class="modal-header"
                                         style="background-color: #3c8dbc;border-radius: 5px 5px 0px 0px;color:#FFF;text-align: center">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 id="title" class="modal-title">Detalle de Producto</h4>
                                    </div>

                                    <div class="modal-body">

                                        <table class="pull-left col-md-8">
                                            <table class="pull-left col-md-8 ">
                                                <tbody>
                                                <tr>
                                                    <td><strong>Código</strong></td>
                                                    <td></td>
                                                    <td class=""><span id="codigo" class="badge label-success price ">nkdfsmdfkls</span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><strong>Nombre del Producto</strong></td>
                                                    <td></td>
                                                    <td id="nombre">descrição do produto</td>
                                                </tr>

                                                <tr>
                                                    <td><strong>Presentación</strong></td>
                                                    <td></td>
                                                    <td id="presentacion">Marca do produto</td>
                                                </tr>

                                                <tr>
                                                    <td><strong>Categoria</strong></td>
                                                    <td></td>
                                                    <td id="category">0230316</td>
                                                </tr>

                                                <td><strong>Precio Unitario</strong></td>
                                                <td></td>
                                                <td id="price">R$ 35,00</td>
                                                </tr>

                                                </tbody>
                                            </table>
                                            <div class="col-md-4">
                                                <img class="img-thumbnail" id="image"
                                                     src="../images/55e920f1d7044placeholder.png" alt=""/>
                                            </div>
                                            <div class="clearfix">
                                                <strong>Descripción</strong>

                                                <p class="info_open" id="description"> Ut enim ad minim veniam, quis
                                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit
                                                    esse cillum dolore eu fugiat nulla pariatur.</p>
                                            </div>

                                            <!--<p><input type="text" class="input-sm" id="txtfname"/></p>
                                            <p><input type="text" class="input-sm" id="txtlname"/></p>-->
                                    </div>

                                    <div class="modal-footer">

                                        <div class="text-right pull-right col-md-3">
                                            Precio: <br>
                                            <span class="h3 text-muted"><strong id="precioU">R$35,00</strong></span>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <?php
                    if (isset($_GET['encontrados']) && $_GET['encontrados'] == 'false') {
                        ?>

                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Consulta sin coincidencias</h4>

                            <p>No se han encontrado resultados para la consulta de:</p>

                            <p>Criterio: <?php echo $_GET['criterio'] ?></p>

                            <p>Búsqueda: <?php echo $_GET['busqueda'] ?></p>
                        </div>


                        <?php
                    }
                    ?>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Todos los registros</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Si desea, puede ver todos los registros usando el botón "Ver todos"
                                (esta opción puede tardar un poco).
                            </p>
                        </div>
                        <div class="box-footer">
                            <div col-md-4 text-center>
                                <button type="submit" class="btn btn-success pull-left"
                                        onclick="location.href='productoListar.php?criterio=productos.IdProducto&busqueda=MP&comobuscar=2'"
                                " tabindex="14"
                                ><i class="fa fa-plus-square-o"> </i> Ver productos de Map Company
                                </button>
                            </div>
                            <div class="col-md-4 text-center">
                                <button type="submit" class="btn btn-warning pull-right "
                                        onclick="location.href='productoListar.php?criterio=productos.IdProducto&busqueda=BS&comobuscar=2'"
                                " tabindex="14"
                                ><i class="fa fa-plus-square-o"> </i> Ver productos de Biosupport
                                </button>
                            </div>
                            <div col-md-4 text-center>
                                <button type="submit" class="btn btn-primary"
                                        onclick="location.href='productoListar.php?criterio=productos.IdProducto&busqueda=MAT&comobuscar=2'"
                                " tabindex="14"
                                ><i class="fa fa-plus-square-o"> </i> Ver productos Grupo MAT
                                </button>
                            </div>
                        </div>
                    </div>



                    <?php

                    $producto = new FacadeProducto();
                    if (isset($_GET['criterio']) && isset($_GET['busqueda']) && isset($_GET['comobuscar'])) {
                        $criterio = $_GET['criterio'];
                        $busqueda = $_GET['busqueda'];
                        $comobuscar = $_GET['comobuscar'];
                        $resultado = $producto->buscarConCriterio($criterio, $busqueda, $comobuscar);
                    } else {
                        $resultado = [];
                    }
                    if (count($resultado) > 0) { ?>
                    <div class="box box-info" style="background-image: url('../../assets/img/confectionary.png');">
                        <div class="box-header with-border">

                            <p>

                            <h2 class="box-title">Listado de Productos </h2></p>
                            <table id="example" class="table hover order-column compact display">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php /**/
                                $present= new FacadeProducto();
                                $var=0;
                                foreach ($resultado as $counter => $iterator) { ?>
                                    <tr>
                                        <td><div class="thumbnail " style="margin: 5%">
                                                <?php if ($resultado[$var]['nuevo']) { ?>
                                                    <div class="pull-left" style="margin: auto">
                                                        <img style="max-width: 25%;"  src="../../dist/img/labelNew.png" alt="">
                                                    </div>
                                                <?php } ?>
                                                <span class=""><?php echo $resultado[$var]['NombreProducto']; ?></span>
                                                <span class="badge  label-success pull-right price "><?php
                                                    echo $resultado[$var]['IdProducto']; ?></span>
                                                <div class="thumbnail product"
                                                     style="margin-top: 50px ;border: none;">
                                                    <?php if (strpos($resultado[$var]['rutaImagen'], 'sinImagen.jpg') !== false) { ?>

                                                        <img src="../images/placeholder.png"
                                                             alt="Unicorn Flux"
                                                             style=" border-radius: 5px ;border:  solid 1px #f1f1f1;max-height:80%;max-width:70%"
                                                             class="img-responsive">

                                                    <?php } else { ?>
                                                        <img class="detail"
                                                             style=" border-radius: 5px;height: auto;max-height: 300px;min-width: 150px;min-height: 100px;"
                                                             src="../images/<?php echo $resultado[$var]['rutaImagen'] ?>"
                                                             alt="Imagen del producto" class="">
                                                    <?php } ?>


                                                </div>

                                                <div class=" top-left sticky red shadow">


                                                    <h3 class="clearfix">

                                                        <?php

                                                        $precioI=$present->obtenerPresentacionesPrecios($resultado[$var]['IdProducto']);

                                                        ?>
                                                        <span class="pull-left">
                                                                <?php
                                                                $x=0;
                                                                foreach($precioI as $respond){


                                                                    ?>
                                                                    <?php echo $respond['NombrePresentacion'];



                                                                    ?>
                                                                    <small>$<?php print number_format($respond['valorPresentacion']); ?></small>
                                                                    <?php
                                                                    if(isset($precioI[$x]['NombrePresentacion'])){
                                                                        print '<br>';
                                                                    }
                                                                    $x++;
                                                                }?>
                                                              </span>


                                                    </h3>

                                                    <p>
                                                        <?php echo $resultado[$var]['DescripcionProducto'] ?>
                                                    </p>
                                                </div>
                                                <div class="clearfix btn-toolbar">
                                                    <?php if (($_SESSION['datosLogin']['NombreRol'] == 'Administrador') or ($_SESSION['datosLogin']['NombreRol'] == 'Coordinador')) { ?>
                                                        <a href="../views/ModificarProducto.php?id=<?php echo $resultado[$var]['IdProducto']; ?>">
                                                            <button class="btn btn-primary btn-xs  pull-right"><i
                                                                    data-toggle="tooltip" title="Modificar"
                                                                    class="fa fa-pencil"></i></button>
                                                        </a>

                                                        <button data-toggle="tooltip" title="Eliminar"
                                                                class="btn btn-danger btn-xs click pull-right"
                                                                value="<?php echo $resultado[$var]['IdProducto']; ?>"><i
                                                                class="fa fa-trash "></i>
                                                        </button>
                                                    <?php } ?>
                                                    <button class="btn btn-warning   btn-xs detail pull-right"
                                                            data-toggle="tooltip" title="Detalle"
                                                            value="<?php echo $resultado[$var]['IdProducto']; ?>">
                                                        <i class="fa fa-eye "></i>
                                                    </button>


                                                </div>
                                            </div>

                                        </td>

                                        <td>


                                            <div class="thumbnail " style="margin: 5%">
                                                <?php
                                                if (isset($resultado[($var+1)]['IdProducto'])){
                                                    $var++;
                                                    if ($resultado[$var]['nuevo']) { ?>
                                                        <div class="" style="margin: 0">
                                                            <img style="max-width: 25%"  src="../../dist/img/labelNew.png" alt="">
                                                        </div>
                                                    <?php } ?>
                                                    <span class="">
                                                                <?php echo $resultado[$var]['NombreProducto'] ?>
                                                              </span>
                                                    <span class="badge  label-success pull-right price "><?php
                                                        echo $resultado[$var]['IdProducto']; ?></span>
                                                    <div class="thumbnail product"
                                                         style="margin-top: 50px ;border: none;">
                                                        <?php if (strpos($resultado[$var]['rutaImagen'], 'sinImagen.jpg') !== false) { ?>

                                                            <img src="../images/placeholder.png"
                                                                 alt="Unicorn Flux"
                                                                 style=" border-radius: 5px ;border:  solid 1px #f1f1f1;max-height:80%;max-width:70%"
                                                                 class="img-responsive">

                                                        <?php } else { ?>
                                                            <img class="detail"
                                                                 style=" border-radius: 5px;height: auto;max-height: 300px;min-width: 150px;min-height: 100px;"
                                                                 src="../images/<?php echo $resultado[$var]['rutaImagen'] ?>"
                                                                 alt="Imagen del producto" class="">
                                                        <?php } ?>


                                                    </div>

                                                    <div class=" top-left sticky red shadow">



                                                        <h3 class="clearfix">

                                                            <?php

                                                            $precioI=$present->obtenerPresentacionesPrecios($resultado[$var]['IdProducto']);

                                                            ?>
                                                            <span class="pull-left">
                                                                <?php
                                                                $x=0;
                                                                foreach($precioI as $respond){


                                                                    ?>
                                                                    <?php echo $respond['NombrePresentacion'];



                                                                    ?>
                                                                    <small>$<?php print number_format($respond['valorPresentacion']); ?></small>
                                                                    <?php
                                                                    if(isset($precioI[$x]['NombrePresentacion'])){
                                                                        print '<br>';
                                                                    }
                                                                    $x++;
                                                                }?>
                                                              </span>



                                                        </h3>

                                                        <p>
                                                            <?php echo $resultado[$var]['DescripcionProducto'] ?>
                                                        </p>
                                                    </div>
                                                    <div class="clearfix btn-toolbar">
                                                        <?php if (($_SESSION['datosLogin']['NombreRol'] == 'Administrador') or ($_SESSION['datosLogin']['NombreRol'] == 'Coordinador')) { ?>
                                                            <a href="../views/ModificarProducto.php?id=<?php echo $resultado[$var]['IdProducto']; ?>">
                                                                <button class="btn btn-primary btn-xs  pull-right"><i
                                                                        data-toggle="tooltip" title="Modificar"
                                                                        class="fa fa-pencil"></i></button>
                                                            </a>

                                                            <button data-toggle="tooltip" title="Eliminar"
                                                                    class="btn btn-danger btn-xs click pull-right"
                                                                    value="<?php echo $resultado[$var]['IdProducto']; ?>"><i
                                                                    class="fa fa-trash "></i>
                                                            </button>
                                                        <?php } ?>
                                                        <button class="btn btn-warning   btn-xs detail pull-right"
                                                                data-toggle="tooltip" title="Detalle"
                                                                value="<?php echo $resultado[$var]['IdProducto']; ?>">
                                                            <i class="fa fa-eye "></i>
                                                        </button>

                                                    </div>
                                                <?php }else{break 1;} ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    if (isset($resultado[($var+1)]['IdProducto'])){
                                        $var++; }else{break 1;}
                                }
                                ?>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>

                                </tr>
                                </tfoot>

                            </table>
                            <!-- /content-panel -->
                        </div>

                    </div>

                </div>
                <?php }?>
        </section>
        <!-- /.content -->
    </div>


    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo
                MAT</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/s/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.js"></script>

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/Moneyformat/money.js"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function() {
        $(function () {
            $("#example").DataTable({
                responsive: true,
                stateSave: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                },
                'sPageEllipsis': 'paginate_ellipsis',
                'sPageNumber': 'paginate_number',
                'sPageNumbers': 'paginate_numbers',
                "processing": true,
                "deferLoading": 57,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    ["10", "20", "50", "100", "Todos"]
                ]
            });
        });
    });
</script>
<script>

    $(".click").click(function () {
        var btnId = $(this).attr("value");
        var n = noty({
            text: 'Desea eliminar este Registro?',
            theme: 'relax',
            layout: 'center',
            closeWith: ['click', 'hover'],
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {

                    $.post("../controllers/ControladorProducto.php",

                        {
                            deleteProducto: btnId


                        },
                        function (data) {

                            $noty.close();
                            noty({text: data, type: 'success'});
                            location.reload();
                        });
                }
                },
                {
                    addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();

                }
                }
            ],
            type: 'confirm',
            animation: {
                open: 'animated wobble', // Animate.css class names
                close: 'animated flipOutX', // Animate.css class names
            }

        });

    });
    $(".detail").click(function () {
        $.post("../controllers/ControladorProducto.php",

            {
                detailProduct: $(this).attr("value")


            },
            function (data) {
                var json = $.parseJSON(data);
                $('#nombre').text(json.NombreProducto);
                $('#title').text(json.NombreProducto);
                $('#description').text(json.DescripcionProducto);
                $('#category').text(json.NombreCategoria);
                var format = accounting.formatMoney(json.valorPresentacion);
                $('#price').text(format);
                $('#precioU').text(format);
                $('#presentacion').text(json.NombrePresentacion);
                $('#fecha').text(json.EstadoProducto);
                $('#codigo').text(json.IdProducto);
                $('#image').attr('src', '../images/' + json.rutaImagen);
                $("#myModal").modal("show");

            });


    });
    function ResizeToLargestElement(element) {
        var maxHeight = -1;
        if ($(element).length > 0) {
            $(element).each(function () {
                maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
            });

            $(element).each(function () {
                $(this).height(maxHeight);
            });
        }
    }


</script>

</html>
