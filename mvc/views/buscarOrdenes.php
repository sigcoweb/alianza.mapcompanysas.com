<?php
include_once '../facades/FacadeOrdenCompra.php';
include_once '../facades/FacadeEmpleado.php';
$facaEmpl=new FacadeEmpleado();
$facaPedi=new FacadeOrdenCompra();
session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> Buscar pedido</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <!-- sweet alert lucho-->
    <link href="../../plugins/animate/animate.css" rel="stylesheet" type="text/css"/>
    <script src="../../plugins/messajes/jquery.noty.packaged.min.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Pedidos
                <small>Buscar</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li>Pedidos</li>
                <li class="active">Buscar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">


                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>

                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Indicaciones para la búsqueda</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Use las siguientes opciones para realizar la búsqueda de la información.
                                Recuerde que en este formulario hay campos obligatorios marcados (*).
                            </p>
                        </div>
                    </div>


                    <div class="box box-solid box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Opciones de búsqueda</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">


                            <form role="form" id="form" action="../controllers/ControladorOrdenCompra.php?buscar=true" method="post">

                                <div class="form-group">
                                    <label for="criterio">Seleccione un criterio de búsqueda*</label>
                                    <select class="form-control select2" name="criterio" style="width: 100%"
                                            id="criterio" required tabindex="1" autofocus>
                                        <option value="pedidos.EstadoPedido" selected>Estado del pedido</option>
                                        <option value="clientes.RazonSocial" >Razón social</option>
                                        <option value="clientes.Nit" >Nit del cliente</option>
                                        <option value="pedidos.IdPedido" >Número del pedido</option>
                                        <option value="cotizaciones.IdCotizacion" >Número de cotización</option>
                                        <?php
                                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                            ?>
                                            <option value="cotizaciones.CedulaEmpleadoCotizaciones">Cédula del asesor</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <label for="comobuscar" class="margin">¿Qué desea encontrar?*</label>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select name="comobuscar" id="comobuscar" class="btn btn-default dropdown-toggle"
                                                    style="width: 100%" data-toggle="dropdown" tabindex="2">
                                                <option selected value="1">Una búsqueda exacta de</option>
                                                <option value="2">Cualquier coincidencia de</option>
                                            </select>
                                        </div><!-- /btn-group -->
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input type="text" name="busqueda" class="form-control" placeholder="Número Nit | Razón Social | Lugar" required tabindex="3">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                    <span class="input-group-btn">
                      <button class="btn bg-blue-gradient btn-flat" type="submit" tabindex="4"><i class="fa fa-search-plus"> </i> Buscar pedido(s)</button>
                    </span>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                    <?php
                    if(isset($_GET['criterio'])&&isset($_GET['busqueda'])&&isset($_GET['comobuscar'])) {
                        $consulta = $facaPedi->buscarConCriterio($_GET['criterio'], $_GET['busqueda'], $_GET['comobuscar']);
                        $conteo = count($consulta);
                    }
                    if(isset($_GET['todos'])&&!isset($_GET['criterio'])&&!isset($_GET['busqueda'])&&!isset($_GET['comobuscar'])){
                        $consulta=$facaPedi->listarOrdenes();
                        $conteo = count($consulta);
                    }


                    if(isset($conteo)&&$conteo<1) {
                        ?>
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Consulta sin coincidencias</h4>

                            <p>No se han encontrado resultados para la consulta de:</p>
                            <p>Criterio: <?php echo $_GET['criterio'] ?></p>
                            <p>Búsqueda: <?php echo $_GET['busqueda'] ?></p>
                        </div>
                        <?php
                    }

                    if(isset($conteo)&&$conteo>0) {
                        ?>
                        <div class="box box-solid box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Resultados de la búsqueda</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p>
                                    Se han encontrado <span class="badge label-info"><?php echo $conteo; ?></span>
                                    registros para esta consulta.
                                </p>
                                <br>
                                <table id="example1" class="table table-bordered table-striped table-responsive table-hover table-condensed">
                                    <thead>
                                    <th>Fecha</th>
                                    <th>#Ped.</th>
                                    <th>#Nit</th>
                                    <th>Razón social</th>
                                    <th>Documentos</th>
                                    <?php
                                    if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                        ?>
                                        <th>Asesor</th>
                                        <?php
                                    }
                                    ?>
                                    <th>Total</th>
                                    <th>Estado</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($consulta as $respuesta){
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date("Y-m-d" , strtotime($respuesta['FechaElaboracionPedido'])) ?>
                                        </td>
                                        <td>
                                            <?php echo $respuesta['IdPedido'] ?>
                                        </td>
                                        <td>
                                            <?php echo $respuesta['Nit'] ?>
                                        </td>
                                        <td>
                                            <?php echo $respuesta['RazonSocial'] ?>
                                        </td>
                                        <td>
                                            <?php if($respuesta['Remisionado']=="Si"){?>
                                                <span class="label badge bg-yellow-gradient" data-toggle="tooltip" title="Remisionado">Re</span>
                                            <?php };
                                            if($respuesta['Facturado']=="Si"){?>
                                                <span class="label badge bg-red-gradient" data-toggle="tooltip" title="Facturado">Fa</span>
                                            <?php };
                                            if($respuesta['Certificado']=="Si"){?>
                                                <span class="label badge bg-blue-gradient" data-toggle="tooltip" title="Certificaciones de productos">Ce</span>
                                            <?php };
                                            if($respuesta['FichaTecnica']=="Si"){?>
                                                <span class="label badge bg-light-blue-gradient" data-toggle="tooltip" title="Fichas técnicas">FT.</span>
                                            <?php };
                                            if($respuesta['FichaSeguridad']=="Si"){?>
                                                <span class="label badge bg-teal-gradient" data-toggle="tooltip" title="Fichas de seguridad">FS.</span>
                                            <?php };
                                            if($respuesta['FichaTecnica']=="No"&&$respuesta['Facturado']=="No"&&$respuesta['Certificado']=="No"&&$respuesta['Remisionado']=="No"&&$respuesta['FichaSeguridad']=="No"){?>
                                                <span class="label label-default" data-toggle="tooltip"
                                                      title="Sin documentos asignados"><i class="fa fa-exclamation-triangle"></i>  Sin docs.</span>
                                            <?php };
                                            $ocCli=$facaPedi->buscarOrdenCliente($respuesta['IdPedido']);
                                            if(isset($ocCli['codigoOrdenCompraCliente'])){
                                                ?>
                                                <span class="label label-default" data-toggle="tooltip"
                                                      title="Órden de compra del cliente">OC <?php echo $ocCli['codigoOrdenCompraCliente']; ?></span>
                                            <?php } ?>
                                        </td>
                                        <?php
                                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                            $nn=$facaEmpl->buscarCriterio('empleados.CedulaEmpleado',$respuesta['CedulaEmpleadoCotizaciones'],1);
                                            ?>
                                            <td>
                                                <?php echo $nn[0]['Nombres'].' '.substr($nn[0]['Apellidos'],0,1).'.'; ?>
                                            </td>
                                            <?php
                                        }
                                        ?>
                                        <td>
                                            <a class="label label-info" href="expedido.php?idpedido=<?php echo $respuesta['IdPedido'] ?>" target="_blank"
                                               data-toggle="tooltip" title="Ver detalle">
                                                <span class="fa fa-search-plus"></span>
                                                $ <?php echo number_format($respuesta['ValorTotal']); ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
                                                if($respuesta['EstadoPedido']=='Pendiente'||$respuesta['EstadoPedido']=='Autorizado') {
                                                    ?>
                                                    <div class="btn-group" data-toggle="tooltip" title="Cambiar estado">
                                                        <button type="button"
                                                                class="btn btn-xs btn-<?php if ($respuesta['EstadoPedido'] == 'Pendiente') {
                                                                    echo 'warning';
                                                                } elseif ($respuesta['EstadoPedido'] == 'Autorizado') {
                                                                    echo 'success';
                                                                } else {
                                                                    echo 'default';
                                                                } ?>">
                                                            <?php echo $respuesta['EstadoPedido']; ?>
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-xs btn-<?php if ($respuesta['EstadoPedido'] == 'Pendiente') {
                                                                    echo 'warning';
                                                                } elseif ($respuesta['EstadoPedido'] == 'Autorizado') {
                                                                    echo 'success';
                                                                } else {
                                                                    echo 'default';
                                                                } ?> dropdown-toggle" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <?php if ($respuesta['EstadoPedido'] != 'Autorizado') { ?>
                                                                    <a href="detallesPedido.php?pedido=<?php echo $respuesta['IdPedido']; ?>&idcliente=<?php echo $respuesta['IdCliente']; ?>">Autorizar</a>
                                                                <?php } ?>
                                                            </li>
                                                            <li>
                                                                <?php if ($respuesta['EstadoPedido'] != 'Cancelado') { ?>
                                                                    <button
                                                                        class="btn btn-block btn-sm btn-default cancelarPedido"
                                                                        value="<?php echo $respuesta['IdPedido'] ?>">
                                                                        Cancelar
                                                                    </button>
                                                                <?php } ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php }else{ ?>
                                                    <span class="label label-default"><?php echo $respuesta['EstadoPedido'] ?></span>
                                                <?php } ?>
                                            <?php }else{ ?>
                                                <span class="label flat label-<?php if($respuesta['EstadoPedido']=='Cancelado'){
                                                    echo 'warning';}elseif($respuesta['EstadoPedido']=='Autorizado'){
                                                    echo 'success';}else{echo 'info';} ?>">
                                                    <?php echo $respuesta['EstadoPedido']; ?>
                                                    </span>
                                            <?php } ?>
                                        </td>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>#Ped.</th>
                                        <th>#Nit</th>
                                        <th>Razón social</th>
                                        <th>Documentos</th>
                                        <?php
                                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                            ?>
                                            <th>Asesor</th>
                                            <?php
                                        }
                                        ?>
                                        <th>Total</th>
                                        <th>Estado</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <?php

                    }
                    if (!isset($_GET['todos'])){ ?>
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver todos los registros</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p>
                                    Si desea, puede ver todos los registros usando el botón "Ver todos"
                                    (esta opción puede tardar un poco).
                                </p>
                            </div>
                            <div class="box-footer">
                                <form action="../controllers/ControladorOrdenCompra.php?listar=todos" method="post">
                                    <button type="submit" class="btn bg-green-gradient pull-right" tabindex="14"
                                    > <i class="fa fa-plus-square-o"> </i>   Ver todos
                                    </button>
                                </form>
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div><!--/.col (right) -->
            </div>   <!-- /.row -->




        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function() {

        $(".cancelarPedido").click(function () {
            var cambEst=$(this).attr("value");
            var n = noty({
                text: '¿Desea cancelar el pedido?',
                theme: 'relax',
                layout: 'center',
                closeWith: ['click', 'hover'],
                buttons: [
                    {
                        addClass: 'btn btn-primary', text: 'Cancelar este pedido', onClick: function ($cancelar) {
                        $.post("../controllers/ControladorOrdenCompra.php",
                            {
                                cancelarPedido: cambEst
                            },
                            function (data) {
                                location.href='buscarOrdenes.php?mensaje=Se ha cancelado el pedido de forma correcta.&error=false';
                                noty({text: data, type: 'success'});
                                $cancelar.close();
                            });
                    }
                    },
                    {
                        addClass: 'btn btn-danger', text: 'Dejar como está', onClick: function ($cancelar) {
                        $cancelar.close();
                    }
                    }
                ],
                type: 'confirm',
                animation: {
                    open: 'animated wobble', // Animate.css class names
                    close: 'animated flipOutX' // Animate.css class names
                }
            });
        });

        $(".autorizarPedido").click(function () {
            var cambEst=$(this).attr("value");
            var n = noty({
                text: '¿Desea autorizar el pedido?',
                theme: 'relax',
                layout: 'center',
                closeWith: ['click', 'hover'],
                buttons: [
                    {
                        addClass: 'btn btn-primary', text: 'Autorizar el pedido', onClick: function ($confirmar) {
                        $.post("../controllers/ControladorOrdenCompra.php",
                            {
                                autorizarPedido: cambEst
                            },
                            function (data) {
                                location.href='buscarOrdenes.php?mensaje=Se ha autorizado el pedido de forma correcta.&error=false';
                                noty({text: data, type: 'success'});
                                $confirmar.close();
                            });
                    }
                    },
                    {
                        addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($confirmar) {
                        $confirmar.close();
                    }
                    }
                ],
                type: 'confirm',
                animation: {
                    open: 'animated wobble', // Animate.css class names
                    close: 'animated flipOutX' // Animate.css class names
                }
            });
        });

        function randomNumber(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function generateCaptcha() {
            $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
        }

        generateCaptcha();
        $('#form').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                criterio: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                comobuscar: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                }


            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0,1, 'desc' ]]
        });
    });
</script>
</html>
