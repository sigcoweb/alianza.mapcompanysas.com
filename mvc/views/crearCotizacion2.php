<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
include_once'../models/ProductosCotizados.php';
include_once'../facades/FacadeProducto.php';
include_once'../facades/ColoresFacade.php';
include_once'../facades/AromasFacade.php';
include_once'../facades/ConcentracionesFacade.php';
include_once'../facades/PresentacionesFacade.php';
session_start();
$facadePresentaciones=new PresentacionesFacade();
$facadeProducto= new FacadeProducto();
$facadeColores= new ColoresFacade();
$facadeAromas= new AromasFacade();
$facadeConcentraciones=new ConcentracionesFacade();
$cliente=$_GET['idcliente'];
$empresaCotiza=$_GET['empresaCotiza'];

?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Registrar cotización</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons --
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>-->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons --
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!--<link rel="stylesheet" href="../../plugins/ionic/css/ionic.css" type="text/css">
    <script type="text/javascript" src="../../plugins/ionic/js/ionic.js"></script>-->
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Cotización</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Cotizaciones</a></li>
                <li class="active">Crear cotización</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-xs-12">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <form id="defaultForm" action="../controllers/controladorCotizacion.php?agregar=true&idcliente=<?php echo $cliente?>&empresaCotiza=<?php echo $empresaCotiza?>" method="post">

                        <div class="box box-info box-default collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Indicaciones de registro</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para registrar una nueva
                                        cotización.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->


                        <!--<div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Datos Cliente</h3>
                            </div>

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="cantidad">ID cliente:*</label>
                                    <input class="form-control" name="idcliente" id="idcliente" type="text" value="<?php echo $cliente?>" readonly>
                                </div>

                            </div>

                        </div>-->


                        <?php
                        include_once '../facades/ClienteFacade.php';
                        if(isset($_GET['idcliente'])) {
                            $clienteFac = new ClienteFacade();
                            $datacliente = $clienteFac->buscarCliente('clientes.Nit', $_GET['idcliente'], 1);
                        }
                        ?>


                        <div class="box box-default collapsed-box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver información corporativa registrada</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="idcliente" id="labelNit">Nit*</label>
                                            <input type="text" name="idcliente" id="idcliente" class="form-control exists" readonly
                                                   value="<?php echo $datacliente[0]['Nit'] ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="RazonSocial">Razón social*</label>
                                            <input type="text" name="RazonSocial" id="RazonSocial" class="form-control"
                                                   value="<?php echo $datacliente[0]['RazonSocial'] ?>" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="NombreComercial">Nombre comercial*</label>
                                            <input type="text" name="NombreComercial" id="NombreComercial" class="form-control"
                                                   value="<?php echo $datacliente[0]['nombreComercial'] ?>" readonly />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Direccion">Dirección*</label>
                                            <input type="text" name="Direccion" id="Direccion" class="form-control"
                                                   value="<?php echo $datacliente[0]['Direccion'] ?>" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Telefono">Teléfono principal*</label>
                                            <input type="text" name="Telefono" id="Telefono" class="form-control"
                                                   value="<?php echo $datacliente[0]['Telefono'] ?>" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Email2">Email corporativo*</label>
                                            <input type="email" name="Email2" id="Email2" class="form-control"
                                                   value="<?php echo $datacliente[0]['EmailCliente'] ?>" readonly  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdDep">Departamento*</label>
                                            <input type="text" name="IdDep" id="IdDep" class="form-control"
                                                   value="<?php echo $datacliente[0]['nombreDepartamento'] ?>" readonly />
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdLu">Lugar*</label>
                                            <input type="text" name="IdLu" id="IdLu" class="form-control"
                                                   value="<?php echo $datacliente[0]['NombreLugar'] ?>" readonly  />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdTipo">Tipo de cliente*</label>
                                            <input type="text" name="IdTipo" id="IdTipo" class="form-control"
                                                   value="<?php echo $datacliente[0]['NombreClasificacion'] ?>" readonly  />
                                        </div>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdActividad">Actividad del cliente*</label>
                                            <input type="text" name="IdActividad" id="IdActividad" class="form-control"
                                                   value="<?php echo $datacliente[0]['NombreActividad'] ?>" readonly required/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="box box-solid box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Añadir productos (Como serán despachados):</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="cantidad">Cantidad:*</label>
                                            <input type="text" class="form-control" name="cantidad" id="cantidad" required autofocus tabindex="1" placeholder="208">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="idproducto">Seleccione el producto*</label>
                                            <select class="form-control select2 sm" name="idproducto" id="idproducto" autofocus required tabindex="2">
                                                <option value="" disabled selected>Seleccione un producto</option>
                                                <?php
                                                include_once '../facades/FacadeProducto.php';
                                                $facade=new FacadeProducto();
                                                $listado=$facade->getProductosCotizacion();
                                                foreach ($listado as $clientes ) {

                                                    ?>
                                                    <option class="pro" value="<?php echo $clientes['IdProducto']?>" <?php if($clientes['EstadoProductos']=='Inactivo'){echo ' disabled';} ?>>
                                                        <?php echo $clientes['IdProducto'].' | '.$clientes['NombreProducto'];
                                                        if($clientes['EstadoProductos']=='Inactivo'){echo ' | Inactivo';} ?>
                                                    </option>
                                                    <?php

                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="idpresentacion">Presentación*</label>
                                            <select class="form-control select2" disabled name="idpresentacion" id="idpresentacion" required tabindex="3">
                                                <option value="1" selected>Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="idaroma">Aroma*</label>
                                            <select class="form-control select2" disabled  name="idaroma" id="idaroma"  tabindex="4">
                                                <option value="0"  disabled selected>Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label for="idcolor">Color*</label>
                                            <select class="form-control select2" disabled name="idcolor" id="idcolor"  tabindex="5">
                                                <option value="0"  disabled selected>Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="idconcentracion">Concentración*</label>
                                            <select class="form-control select2" disabled name="idconcentracion" id="idconcentracion" required tabindex="6">
                                                <option value="0" disabled selected>Seleccionar</option>
                                            </select>
                                        </div>
                                    </div>




                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Agregar</label>
                                            <button class="btn bg-aqua-gradient btn-block" type="submit" tabindex="7"
                                                    value="agregar" name="agregar" id="agregar"><i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>



                        <?php if(!isset($total)&&isset($_SESSION['productoDatos']) && count($_SESSION['productoDatos'])>0){ ?>

                            <div class="box box-solid box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Detalle de la cotización:</h3>
                                </div>
                                <div class="box-body">

                                    <?php
                                    if (isset($_SESSION['productoDatos']) && count($_SESSION['productoDatos'])>0) {

                                        ?>
                                        <table id="example1" class="table table-bordered table-responsive table-striped table-hover text-center">
                                            <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Descripción</th>
                                                <th>Aroma</th>
                                                <th>Color</th>
                                                <th>Concen.</th>
                                                <th>Unidad</th>
                                                <th>Cantidad</th>
                                                <th>$ Unitario</th>
                                                <th>$ Total</th>
                                                <th>Elim.</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $i=0;
                                            $total=0;
                                            foreach ($_SESSION['productoDatos'] as $test) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $test->getId()?></td>
                                                    <td><?php echo substr($test->getNombre(),0,25);
                                                        if(strlen($test->getNombre())>25){echo '...';};
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php $aroma=$facadeAromas->traerAroma($test->getAroma());
                                                        echo substr($aroma['aroma'],0,10);
                                                        if(strlen($aroma['aroma'])>10){echo '...';};
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php $color=$facadeColores->traerColor($test->getColor());
                                                        echo substr($color['color'],0,10);
                                                        if(strlen($color['color'])>10){echo '...';};

                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php $conc=$facadeConcentraciones->traerConcentracion($test->getConcentracion());
                                                        echo $conc['concentracion'];

                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php $presentacion=$facadePresentaciones->traerPresentacion($test->getPresentacion());
                                                        echo $presentacion['Abreviatura'];

                                                        ?>
                                                    </td>
                                                    <td><?php echo $test->getCantidad()?></td>
                                                    <td>$ <?php echo number_format($test->getValorBase())?></td>
                                                    <td>$ <?php echo number_format($test->getSubtotal())?></td>

                                                    <td> <a class="label badge label-warning" data-toggle="tooltip" title="Eliminar" href="crearCotizacion2.php?idcliente=<?php echo $cliente?>&removerproducto=true&posicion=<?php echo $i?>&empresaCotiza=<?php echo $empresaCotiza?>">
                                                            <i class="fa fa-fw fa-remove" ></i>
                                                        </a></td>
                                                </tr>

                                                <?php
                                                $i++;
                                                $total=$total+$test->getSubtotal();
                                            }

                                            ?>



                                            <?php
                                            if (isset($_GET['removerproducto'])){
                                                array_splice($_SESSION['productoDatos'],$_GET['posicion'],1);?>
                                                <meta http-equiv="Refresh" content="0;url=crearCotizacion2.php?idcliente=<?php echo $cliente?>&empresaCotiza=<?php echo $empresaCotiza?>">
                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <?php
                                    }?>
                                    <hr>
                                    <?php if (isset($_SESSION['productoDatos']) && count($_SESSION['productoDatos'])>0) {?>

                                        <h2 class="text-center text-light-blue">Total: <?php echo '$ '.number_format($total);?></h2>

                                    <?php }
                                    ?>
                                </div>
                                <div class="box-footer">
                                    <a href="crearCotizacion3.php?idcliente=<?php echo $cliente?>&cotiza=<?php echo $empresaCotiza?>" class="btn bg-green-gradient btn-block" type="button"
                                       value="Finalizar" name="finalizar" id="finish" >Continuar con la cotización
                                    </a>
                                </div>

                            </div>

                        <?php } ?>



                    </form>






                </div>

                <!-- /.box -->
            </div>



            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

    <script type="text/javascript">
        $(document).ready(function() {

            $(function () {
                //Initialize Select2 Elements
                $(".select2").select2();
            });

            $("#idproducto").change(function () {
                $('#idpresentacion').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idpresentacion').val('0');
                    $('#idpresentacion').change();
                    $.post("presentacionesProducto.php", { idproducto: idproducto }, function(data){
                        $("#idpresentacion").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idaroma').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idaroma').val('0');
                    $('#idaroma').change();
                    $.post("aromasProducto.php", { idproducto: idproducto }, function(data){
                        $("#idaroma").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idcolor').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idcolor').val('0');
                    $('#idcolor').change();
                    $.post("coloresProducto.php", { idproducto: idproducto }, function(data){
                        $("#idcolor").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idconcentracion').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idconcentracion').val('0');
                    $('#idconcentracion').change();
                    $.post("concentracionesProducto.php", { idproducto: idproducto }, function(data){
                        $("#idconcentracion").html(data);
                    });
                });
            });

            $('#datosCotizacion').hide();

            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    idproducto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    idaroma:{
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }

                    },

                    idcolor:{
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }

                    },

                    meta: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    cantidad: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 1,
                                max: 20,
                                message: 'Este campo debe tener mínimo 1 y máximo 20 carácteres.'
                            },
                            between: {
                                min: 1,
                                max: 9999999999999999999,
                                message: 'Debe ser un número válido'
                            }
                        }
                    }
                }
            });
        });
    </script>
</body>
</html>
