<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->


<html>
<head>
    <meta charset="UTF-8">
    <title>Modificar producto</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../plugins/select2/select2.css">
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function () {
            $("#datepicker").datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->             <span class="logo-mini"><b>M</b>AT</span>             <!-- logo for regular state and mobile devices -->             <span class="logo-lg"><b>Grupo</b>MAT</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The iterator image in the navbar-->
                            <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                                 class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span
                                class="hidden-xs"><?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The iterator image in the menu -->
                            <li class="user-header">
                                <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                                     class="img-circle" alt="User Image"/>

                                <p>
                                    <?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?>
                                    <small><?php echo $_SESSION['datosLogin']['NombreRol'] ?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../controllers/controladorCerrarSesion.php"
                                       class="btn btn-default btn-flat">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                         class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i
                            class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol'] ?>
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu</li>
                <!-- Optionally, you can add icons to the links -->
                <li><a href="index.php"><i class="fa fa-desktop"></i> <span>Inicio</span></a></li>
                <?php
                include_once '../facades/FacadeEmpleado.php';
                $facade = new FacadeEmpleado();
                $titulos = $facade->obtenerMenu($_SESSION['rol']['rol']);
                $nombre_archivo = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                //verificamos si en la ruta nos han indicado el directorio en el que se encuentra
                if (strpos($nombre_archivo, '/') !== FALSE)
                    //de ser asi, lo eliminamos, y solamente nos quedamos con el nombre y su extension
                    $nombre_archivo = array_pop(explode('/', $nombre_archivo));
                foreach ($titulos as $menu) {
                    $i = 0;
                    $subtitulos = $facade->obtenerSubMenu($menu['IdCategoria'], $_SESSION['rol']['rol']);
                    foreach ($subtitulos as $submenu) {
                        if ($nombre_archivo == $submenu['Url']) {
                            $i++;
                        };
                    }
                    ?>
                    <li class="treeview <?php if ($i >= 1) {
                        echo 'active';
                    }; ?>">
                        <a href="#"><i class="<?php echo $menu['Icono'] ?>"></i> <span><?php echo $menu['Nombre'] ?>
                            </span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <?php
                            $subtitulos = $facade->obtenerSubMenu($menu['IdCategoria'], $_SESSION['rol']['rol']);
                            foreach ($subtitulos as $submenu) { ?>
                                <li class="<?php if ($nombre_archivo == $submenu['Url']) {
                                    echo 'active';
                                }; ?>">
                                    <a href="<?php echo $submenu['Url'] ?>"><?php echo $submenu['NombrePagina'] ?></a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </li>

                    <?php
                }
                ?>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Productos
                <small> Formulario de modificación</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Productos</a></li>
                <li class="active">Modificar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">


                <!-- right column -->
                <div class="col-md-10">
                    <?php $idviejo = $_GET['id']; ?>
                    <form id="defaultForm" action="../controllers/ControladorProducto.php?id=<?php echo $idviejo ?>"
                          method="post" enctype="multipart/form-data">

                        <div class="box box-info box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modificar Producto</h3>

                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para actualizar la información
                                        de un producto.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->
                        <?php
                        include_once '../facades/FacadeProducto.php';

                        $productos = new FacadeProducto();
                        $p = $productos->obtenerProducto($idviejo);

                        ?>

                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modificar Producto</h3>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cc">Codigo producto*</label>
                                            <input class="form-control" name="idProducto"
                                                   value="<?php echo $p['IdProducto'] ?>" id="cc" type="text"
                                                   placeholder="10311433222" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="names">Nombre de producto*</label>
                                            <input class="form-control" name="nombreProducto"
                                                   value="<?php echo $p['NombreProducto'] ?>" id="names" type="text"
                                                   placeholder="Map-234556" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="apellido">Descripción*</label>
                                    <textarea class="form-control" name="descriptionProducto" id="descriptionProducto"
                                              type="text" maxlength="100" placeholder=""
                                              rows="5"><?php echo $p['DescripcionProducto'] ?></textarea>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="placeind">
                                            <label for="precio">Precio*</label>
                                            <input class="form-control validation"
                                                   value="<?php echo $p['valorPresentacion'] ?>"
                                                   name="precio" id="precio" type="number" min="1" placeholder="50000"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <label for="cargo">IVA*</label>
                                            <select class="form-control select2" name="ivaProducto" id="cargo">

                                                <?php

                                                $producto = new FacadeProducto();
                                                $s = $producto->obtenerImpuestosProducto();

                                                foreach ($s as $iterator) { ?>
                                                    <option value="<?php echo $iterator['IdIva'];
                                                    ?>" <?php if ($p['IdIvaProductos'] == $iterator['IdIva']) {
                                                        echo 'selected';
                                                    } ?>><?php echo $iterator['PorcentajeIva']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="imagen">Imágen</label> <br>
                                        <span class="btn btn-primary btn-file btn-block">
                                            Buscar imagen...<input name="ImagenProducto" id="imagen" type="file"
                                                                   multiple=true class="file"
                                                                   accept="image/gif, image/jpeg, image/png  title="
                                                                   Este campo es requerido">
                                        </span>
                                        </div>
                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="categoriaProducto">Categoria producto*</label>
                                            <select class="form-control select2" name="categoriaProducto" id="cargo">
                                                <?php
                                                $producto = new FacadeProducto();
                                                $Productos = $producto->obtenerCategoriaProducto();
                                                foreach ($Productos as $iterator) { ?>
                                                    <option value="<?php echo $iterator['IdCategoria'];
                                                    ?>" <?php if ($iterator['IdCategoria'] == $p['IdCategoria']) {
                                                        echo 'selected';
                                                    } ?>><?php echo $iterator['NombreCategoria']; ?></option>
                                                    <?php
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                  <div class="form-group thumbnail" style=" max-width: 50%" id="img"><img id="blah"
                                                                                                          style="border-radius: 5px;height: auto;max-height: 300px;min-width: 150px;min-height: 100px;"
                                                                                                                src="<?php if ($p['rutaImagen']) {
                                                                                                                    echo $p['rutaImagen'];
                                                                                                                } else {
                                                                                                                    echo "#";
                                                                                                                } ?>"
                                                                                                                alt="producto"/></div>


                                    <div class="box-footer">

                                    </div>



                        </div>
                        </div>
                        <div class="box box-default" id="formulario2">
                            <div class="box-header with-border">
                                <h4>Propiedades Especificas</h4>
                            </div>

                            <div class="box-body">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="cargo">Presentacion*</label>
                                        <select class="form-control select2" name="presentacion" id="cargo">
                                            <option value="">Seleccionar</option>
                                            <?php

                                            $producto = new FacadeProducto();
                                            $Productos = $producto->obtenerPresentacionProducto();
                                            foreach ($Productos as $iterator) { ?>
                                                <option
                                                    value="<?php echo $iterator['IdPresentacion']; ?>" <?php if ($iterator['IdPresentacion'] == $p['IdPresentacion']) {
                                                    echo 'selected';
                                                } ?>><?php echo $iterator['NombrePresentacion']; ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                        </div>

                                    <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="categoriaProducto">Aroma*</label>
                                        <select class="form-control select2" multiple name="aroma" id="">
                                            <option value="">Seleccionar</option>
                                            <?php
                                            $producto = new FacadeProducto();
                                            $Productos = $producto->obtenerAromas();
                                            foreach ($Productos as $iterator) { ?>
                                                <option
                                                    value="<?php echo $iterator['IdAroma']; ?>"><?php echo $iterator['aroma']; ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                    <div class="form-group">

                                        <label for="categoriaProducto">Color*</label>
                                        <select class="form-control select2" multiple name="color" id="">
                                            <option value="">Seleccionar</option>
                                            <?php
                                            $producto = new FacadeProducto();
                                            $Productos = $producto->obtenerColores();
                                            foreach ($Productos as $iterator) { ?>
                                                <option
                                                    value="<?php echo $iterator['idColor']; ?>"><?php echo $iterator['color']; ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                        </div>
                                        <div class="col-lg-6">
                                    <div class="form-group">

                                        <label for="categoriaProducto">Concentración*</label>
                                        <select class="form-control select2" multiple name="concentracion" id="">
                                            <option value="">Seleccionar</option>
                                            <?php
                                            $producto = new FacadeProducto();
                                            $Productos = $producto->obtenerConcetraciones();
                                            foreach ($Productos as $iterator) { ?>
                                                <option
                                                    value="<?php echo $iterator['idConcentracion']; ?>"><?php echo $iterator['concentracion']; ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                     </div>
                                   </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success btn-block" tabindex="14"
                                                value="guardar" name="modificar" id="guardar">Actualizar Producto
                                        </button>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </form>

                </div>

                <!-- /.box -->

                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/select2/select2.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function () {

        function randomNumber(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function generateCaptcha() {
            $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
        }

        generateCaptcha();
        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                cedula: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                meta: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },


            }
        });
    });
    $('.select2').select2();
    $('#imagen').on('change', function () {
        if (document.getElementsByTagName(img).clientWidth <= 0) {
            $('#img').hidden();
            console.log('no file');
        }
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)

                console.log(e);
            };
            $('#img').show();
            reader.readAsDataURL(this.files[0]);
        }

    });
</script>
</html>
