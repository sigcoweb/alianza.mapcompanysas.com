<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
session_start();
if ($_SESSION['datosLogin']['EstadoPersona']=="Inactivo" or !isset($_SESSION['datosLogin'])){

    header('location: Invalido.php');
}
?>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Registrar gestión</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

      <link rel="stylesheet" href="../../plugins/datepicker/bootstrap-datetimepicker.css">
      <link rel="icon" type="image/png" href="../../favicon.ico">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../../plugins/select2/select2.css" rel="stylesheet" type="text/css" />
    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>
    <script type="text/javascript" src="../../plugins/select2/select2.js"></script>

<!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
        <header class="main-header">

            <?php include_once 'header.php'; ?>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar iterator panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                        <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                    </div>
                </div>

                <?php include_once 'menu.php' ?>
            </section>
            <!-- /.sidebar -->
        </aside>

      <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Visitas
                    <small> Formulario de registro</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li><a href="#">Visitas</a></li>
                    <li class="active">Registrar</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">

                    <!-- right column -->
                    <div class="col-md-10">
                        <form id="defaultForm" action="../controllers/ControladorGestion.php" method="post">

                                <div class="box box-info box-solid collapsed-box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Indicaciones de registro</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                            </button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <p>
                                                Por favor diligencie el siguiente formulario para registrar una nueva
                                                Gestión.<br><br>
                                                Recuerde que este formulario contiene campos obligatorios(*).
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            <!-- general form elements disabled -->


                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Registrar Visita</h3>
                                </div>

                                <div class="box-body">


                                    <div class="form-group">
                                        <label for="cc">Nit*</label>
                                        <select class="form-control select2" style="width: 100%" tabindex="1" data-placeholder="Seleccione una Empresa" name="idCliente" id="idCliente" >
                                            <option value="">seleccionar</option>
                                            <?php
                                            include_once '../facades/FacadeGestion.php';
                                            include_once '../facades/FacadeProducto.php';
                                            $empresa = new FacadeGestion();
                                            $empresas = $empresa->obtenerEmpresas();
                                            foreach($empresas as $iterator) { ?>
                                                <option value="<?php echo $iterator['Nit']; ?>"><?php echo $iterator['Nit']."|".$iterator['nombreComercial']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="apellido">Tipo visita*</label>
                                        <select class="form-control select2" style="width: 100%" data-placeholder="Seleccione una Empresa" name="tipoVisita" id="tipoVisita">
                                            <option value="Asesoría">Asesoria Técnica</option>
                                            <option value="Capacitación">Capacitación </option>
                                        </select>

                                    </div>
                                    <div class="form-group"  id="producto" style="@media (min-width: 768px)">
                                        <label for="cargo">Producto*</label>
                                        <select  class="form-control select2" multiple style="width: 100%;height: auto" data-placeholder="Seleccione una Empresa" name="temaproducto[]">
                                            <?php
                                            $producto = new FacadeProducto();
                                            $Productos = $producto->getProductos();
                                            foreach($Productos as $iterator) { ?>
                                                <option value="<?php echo $iterator['IdProducto']; ?>"><?php echo $iterator['IdProducto']; ?></option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                    <div class="form-group"  id="tema">
                                        <label for="email" >Tema*</label>
                                        <input class="form-control" name="tema[]"  type="text" maxlength="20" placeholder="Desengrasantes">
                                    </div>

                                    <div class="form-group">
                                        <label for="pass1">Asistentes*</label>
                                        <input class="form-control" name="asistentes" id="asistentes" type="number" placeholder="" min="1">
                                    </div>

                                    <div class="form-group">
                                        <label for="pass2">Descripccion*</label>
                                        <textarea class="form-control" name="observaciones" id="observaciones"  maxlength="100" placeholder="" rows="5"></textarea>

                                    </div>
                                    <div class="form-group ">
                                        <label for="pass1">Fecha*</label>
                                        <div class='input-group date'  >
                                            <input type='text' class="form-control"  maxlength="16" name="fechaVisita" value="" id="fechaVisita" placeholder="YYYY/MM/DD h:m A" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="imagen">Lugar*</label>
                                        <select class="form-control select2" name="lugar" disabled type="text" id="lugar" style="width: 100%" required>
                                            <option value="0"  disabled selected>Seleccionar</option>
                                        </select>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success btn-block" tabindex="14"
                                                value="registrar" name="registrar" id="guardar">Guardar Gestión
                                        </button>
                                    </div>

                                </div>

                            </div>
                        </form>



                    <!-- /.box -->
                </div>
        </div>

        <!--/.col (right) -->

    <!-- /.row -->
    </section>

    <!-- /.content -->
      <!-- Main Footer -->

    </div><!-- ./wrapper -->
        <footer class="main-footer">
            <!-- To the right -->

            <!-- Default to the left -->
            <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
        </footer>

        <!-- Control Sidebar -->

        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../plugins/daterangepicker/moment.js"></script>
    <script src="../../plugins/datepicker/bootstrap-datetimepicker.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->
  </body>
  <script type="text/javascript">
      var responce=false;
      $(document).ready(function() {

          $('#defaultForm').formValidation({
              message: 'This value is not valid',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },

              locale: 'es_ES',

              fields: {
                  idCliente: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          },
                          integer: {
                              message: 'Solo se permite el ingreso de números'
                          }
                      }
                  },
                  temaproducto: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          }

                      }
                  },
                  tema: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          }

                      }
                  },
                  tipoVisita: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          },
                          stringLength: {
                              min: 3,
                              max: 30,
                              message: 'Este campo debe tener mínimo 3 caracteres y máximo 30'
                          }
                      }
                  },

                  observaciones: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          }

                      }


                  },

                     asistentes: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          },
                          integer: {
                              message: 'solo se permiten números'
                          }
                      }
                  },

                  fechaVisita: {
                      validators: {
                          notEmpty: {
                              message: 'Este campo es requerido'
                          },
                          date: {
                              format: 'YYYY-MM-DD HH:mm',
                              message: 'Fecha no Valida'
                          }

                      }

                  }

              }
          }).on('success.validator.fv', function (e, data) {
              // data.field     --> The field name
              // data.element   --> The field element
              // data.result    --> The result returned by the validator
              // data.validator --> The validator name

              if (data.field === 'fechaVisita'
                  && data.validator === 'notEmpty') {
                  async();

                  if(responce) {
                      console.log(data);
                      data.element                    // Get the field element
                          .closest('.form-group')     // Get the field parent
                          // Add has-warning class
                          .removeClass('has-success')
                          .addClass('has-error')

                          // Show message
                          .find('small[data-fv-validator="notEmpty"][data-fv-for="fechaVisita"]')
                          .show();
                      data.fv.disableSubmitButtons(true);
                      data.fv.updateMessage('fechaVisita', 'notEmpty', 'se ha encontrado un evento en esta fecha');

                      responce=false;
                  }
              }
      });
      });

      $.post("../controllers/ControladorGestion.php",
          {
              reload: $('#idCliente').val()
          },
          function (data) {
              $('#cliente').val(data);
          });
      $( function () {
          if($('#tipoVisita').val()=='Capacitación'){
              $('#tema').hide();
              $('#producto').show();
          }else{
              $('#producto').hide();
              $('#tema').show();

          }

      });
      function getUrlVars() {
          var vars = {};
          var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
              vars[key] = value;
          });
          return vars;
      }
      function getCalendarData() {
          var first = decodeURI(getUrlVars()["date"]);
          var second = decodeURI(getUrlVars()["type"]);
          if(first&&second!=""){
              if(second=="Capacitación"){
                $('#tipoVisita').val(second);


              }
              $('#fechaVisita').val(first);

          }
      }
      function async()
      {
          $.ajax({
              url: '../controllers/ControladorGestion.php',
              type: "POST",
              data: {dateValid: $('#fechaVisita').val()},
              success: function (r) {
                  if(!r=='') {
                      validate(r);
                  }
              }
          });
      }
      function validate(result){
          console.log(result);
          if(!result=="") {
              responce = true;
          }
      }
      getCalendarData();

  </script>
  <script>
      var deft='<option value="0"  disabled selected>Seleccionar</option>';
      $('#idCliente').on('change', function () {
          reload = $(this).val();
          $('#lugar').removeAttr("disabled");
          $('#lugar').val('0');
          $('#lugar').change();
          $('#lugar').empty();
              $.post("../controllers/ControladorGestion.php",

                  {
                      reload: reload

                  },
                  function (data) {

                      var json =JSON.parse(data);
                      console.log(json);
                      $('#lugar').append(deft);

                      if(countKeys(json,'direccionPuntoEntrega')>0){

                          $.each(json, function (i, item) {
                              $('#lugar').append($('<option>', {
                                  value: item.direccionPuntoEntrega,
                                  text : item.direccionPuntoEntrega

                              }));

                          });
                      }
                      $.each(json, function (i, item) {
                          $('#lugar').append($('<option>', {
                              value: item.Direccion,
                              text : item.Direccion

                          }));

                      });

                  });

      });

      $('#tipoVisita').on('change',function(){
          if($('#tipoVisita').val()=='Capacitación'){
              $('#tema').hide();
              $('#producto').show();
          }else{
              $('#producto').hide();
              $('#tema').show();

          }
      });
      $(".select2").select2();
      $(function () {
          var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
          $('#fechaVisita').datetimepicker({
              minDate:today,
              format: 'YYYY-MM-DD HH:mm'

          });

      });
      /*$(function () {
          $('#lugar').removeAttr("disabled");
          $('#lugar').val('0');
          $('#lugar').change();
          $('#lugar').empty();
          $.post("../controllers/ControladorGestion.php",

              {
                  reload: $('#idCliente').val()

              },
              function (data) {

                  console.log(json);
                  $('#lugar').append(deft);
                  $.each(json, function (i, item) {
                      $('#lugar').append($('<option>', {
                          value: item.direccionPuntoEntrega,
                          text : item.direccionPuntoEntrega,

                      }));

                  });


              });
      });*/
      function countKeys(json,word){
          var word, count = 0;
          for(word in json) {
              if(json.hasOwnProperty(word)) {
                  count++;
              }
          }
          return count;
      }
  </script>
</html>
