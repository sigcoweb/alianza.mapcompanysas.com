<!DOCTYPE html> <?php session_start();
include_once('../facades/FacadeGestion.php');
include_once('../facades/FacadeEmpleado.php');
$persona=new FacadeEmpleado();
$gestion=new FacadeGestion();
?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->


<html>
<head>
    <meta charset="UTF-8">
    <title>Buscar gestión</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../plugins/morris/morris.css" rel="stylesheet" type="text/css"/>

    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/png" href="../../favicon.ico">

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../../css/cirular-buttons.css" rel="stylesheet" type="text/css"/>
    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function () {
            $("#datepicker").datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper supreme-container">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->             <span class="logo-mini"><b>M</b>AT</span>             <!-- logo for regular state and mobile devices -->             <span class="logo-lg"><b>Grupo</b>MAT</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-danger">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-red"></i> 5 new members joined
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> You changed your username
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">

                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The iterator image in the navbar-->
                            <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                                 class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span
                                class="hidden-xs"><?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The iterator image in the menu -->
                            <li class="user-header">
                                <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                                     class="img-circle" alt="User Image"/>

                                <p>
                                    <?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?>
                                    <small><?php echo $_SESSION['datosLogin']['NombreRol'] ?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->


                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../controllers/controladorCerrarSesion.php"
                                       class="btn btn-default btn-flat">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"
                         class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'] . ' ' . $_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i
                            class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol'] ?>
                    </a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Gestiones
                <small>Buscar</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li>Gestión</li>
                <li class="active">Buscar</li>
            </ol>
        </section>


        <!-- Main content -->
        <?php $f=new FacadeGestion();
        $gest=$f->getGestiones($_SESSION['datosLogin']['id'],1);
        if ($gest['cantidad'] > 0){
        ?>
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">


                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-success';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i>Resultado del proceso:</h4>
                            <?php echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <div class="box box-info box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Indicaciones para la búsqueda</h3>

                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Use las siguientes opciones para realizar la búsqueda de un gestión.
                                Recuerde que en este formulario hay campos obligatorios(*).<br><br>
                            </p>

                        </div>
                    </div>

                    <!-- general form elements disabled -->


                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Opciones de búsqueda</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            <form role="form" action="../controllers/ControladorGestion.php?buscar=true" method="post">

                                <div class="form-group">
                                    <label for="criterio">Seleccione un criterio de búsqueda*</label>
                                    <select class="form-control select2" name="criterio" id="criterio" required
                                            tabindex="1" autofocus>
                                        <option
                                            value="gestiones.IdGestion" <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'gestiones.IdGestion') {
                                            echo 'selected';
                                        } ?>
                                            >código de Gestión
                                        </option>
                                        <option
                                            value="gestiones.LugarGestiones" <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'gestiones.LugarGestiones') {
                                            echo 'selected';
                                        } ?> >Lugar Gestión
                                        </option>
                                        <option
                                            value="gestiones.FechaProgramada" <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'gestiones.FechaProgramada') {
                                            echo 'selected';
                                        } ?>>Fecha
                                        </option>
                                    </select>
                                </div>

                                <label for="comobuscar" class="margin">¿Qué desea encontrar?*</label>

                                <div class="input-group input-group-sm margin">
                                    <div class="input-group-btn">
                                        <select name="comobuscar" id="comobuscar"
                                                class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                                tabindex="2">
                                            <option selected
                                                    value="1" <?php if (isset($_GET['comobuscar']) && $_GET['comobuscar'] == 1) {
                                                echo 'selected';
                                            } ?>>Una búsqueda exacta de
                                            </option>
                                            <option
                                                value="2" <?php if (isset($_GET['comobuscar']) && $_GET['comobuscar'] == 2) {
                                                echo 'selected';
                                            } ?>>Cualquier coincidencia de
                                            </option>
                                        </select>
                                    </div>
                                    <!-- /btn-group -->
                                    <input type="text" name="busqueda" class="form-control" value="<?php if(isset($_GET['busqueda'])) print $_GET['busqueda'] ?>"
                                           placeholder="Número Nit | Razón Social | Lugar" required tabindex="3">
                    <span class="input-group-btn">
                      <button class="btn btn-info btn-flat" type="submit" tabindex="4">Buscar Gestión</button>
                    </span>
                                </div>
                                <!-- /input-group -->

                            </form>

                        </div>
                    </div>

                    <?php
                    if (isset($_GET['encontrados']) && $_GET['encontrados'] == 'false') {
                        ?>


                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Consulta sin coincidencias</h4>

                            <p>No se han encontrado resultados para la consulta de:</p>

                            <p>Criterio: <?php echo $_GET['criterio'] ?></p>

                            <p>Búsqueda: <?php echo $_GET['busqueda'] ?></p>
                        </div>


                        <?php
                    }
                    ?>


                    <?php

                    if (isset($_GET['encontrados']) && isset($_SESSION['consulta'])) {

                        if ($_GET['encontrados'] == 'true') {
                            ?>

                            <div class="box box-default box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Herramientas de la consulta</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-default btn-sm" data-widget="collapse"
                                                data-toggle="tooltip" title="Ampliar información"
                                                data-original-title="Collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-default btn-sm" data-widget="remove"
                                                data-toggle="tooltip" title="Quitar información"
                                                data-original-title="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <dl class="dl-horizontal">
                                        <dt><span class="label label-success">Realizada</span>
                                            <span class="label label-warning">Pendiente</span>

                                        </dt>
                                        <dt><span class="label label-danger">Cancelada</span></dt>
                                        <dd>Cambia el estado de una visita.</dd>
                                        <dt><i class="fa fa-search-plus"></i></dt>
                                        <dd>Muestra la información detallada de un visita.</dd>
                                        <dt><i class="fa fa-edit"></i></dt>
                                        <dd>Permite editar la información de una visita.</dd>
                                        <dt><span class="label label-default">  <i class="fa fa-file-excel-o"></i>
                                  Exportar consulta completa</span></dt>
                                        <dd>Guardar la consulta con toda la información en formato XLS.</dd>
                                    </dl>
                                </div>
                                <!-- /.box-body -->
                            </div><!-- /.box -->

                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Resultados de la búsqueda</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        Se han encontrado <span
                                            class="badge label-info"><?php echo $_SESSION['conteo']; ?></span>
                                        registros para esta consulta.
                                    </p>
                                    <br>

                                    <div class="box-footer">
                                        <form role="form" action="../utilities/exportarGestion.php?busqueda=<?php
                                        if (isset($_GET['busqueda'])) {
                                            echo $_GET['busqueda'];
                                        } else {
                                            echo 'todos';
                                        } ?>" method="post">
                                            <button type="submit" class="btn btn-default pull-left" tabindex="14"
                                                    value="exportar" name="exportar" id="todos"><i
                                                    class="fa fa-file-excel-o"></i> Exportar consulta completa
                                            </button>
                                        </form>
                                    </div>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Actividad</th>
                                            <th>Tema Relacionado</th>
                                            <th>Lugar</th>
                                            <th>Estado</th>
                                            <th>Fecha</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        $consulta = $_SESSION['consulta'];

                                        $_SESSION['GestionExportar'] = $consulta;

                                        foreach ($consulta as $respuesta) {

                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $respuesta['TipoGestiones']; ?>
                                            </td>
                                            <td>
                                                <?php

                                                echo $respuesta['Asunto'];
                                                ?>
                                                <?php if ($respuesta['cantidad'] > 1) { ?>
                                                    <?php if ($respuesta['cantidad'] > 1) { ?>
                                                        <button class="btn btn-info pop pull-right" style="width: 30px;
                                                    height: 30px;
                                                    padding: 6px 0;
                                                    border-radius: 15px;
                                                    text-align: center;
                                                    font-size: 13px;
                                                    color: white;
                                                    margin-left: 3%;
                                                    line-height: 1.428571429;" data-toggle="popover"
                                                                data-placement="right auto" tabindex="0"
                                                                data-html="true"
                                                                data-content="<?php foreach ($gestion->obtenerGestion($respuesta['IdGestion'], 1) as $resp) {
                                                                    print '<p>' . $resp['Asunto'] . '</p>';
                                                                } ?>" title="<?php echo $respuesta['TipoGestiones'] ?>">
                                                            +<?php print $respuesta['cantidad'] - 1 ?></button>
                                                    <?php }
                                                    ?>

                                                <?php }
                                                ?>
                                                <?php
                                                $t = $gestion->obtenerGestion($respuesta['IdGestion'], 0);
                                                if ($t['existe']) { ?>
                                                    <button class="btn btn-bitbucket pull-right pop" style="width: 30px;
                                                    height: 30px;
                                                    padding: 6px 0;
                                                    border-radius: 15px;
                                                    text-align: center;
                                                    font-size: 14px;
                                                    color: white;
                                                    margin-left: 3%;
                                                    line-height: 1.428571429;" data-toggle="popover"
                                                            data-placement="left"
                                                            title="Observación: <?php $name = $persona->getUserData($t['persona']);
                                                            print '<br>' . $name['Nombres'] . ' ' . $name['Apellidos']; ?>"
                                                            tabindex="0" data-html="true"
                                                            data-content="
                                                        <?php
                                                            print '<p>' . $t['Comentario'] . '</p>';
                                                            ?>"><i class="fa fa-weixin"></button>
                                                    <?php
                                                } ?>

                                            </td>
                                            <td>
                                                <?php echo $respuesta['LugarGestiones']; ?>
                                            </td>
                                            <td>
                                                <span
                                                    class="label  label-mini <?php if ($respuesta['EstadoGestiones'] == 'Pendiente') {
                                                        print 'label-warning';
                                                    } elseif ($respuesta['EstadoGestiones'] == 'Realizada') {
                                                        print 'label-success';
                                                    } else {
                                                        print 'label-danger';
                                                    }
                                                    ?>
                                                    "><?php echo $respuesta['EstadoGestiones']; ?></span>
                                            </td>
                                            <td>
                                                <?php echo $respuesta['FechaProgramada']; ?>
                                            </td>

                                            <td>
                                                <button data-toggle="tooltip" title="Ver Detalle"
                                                        class="btn btn-primary btn-xs fa fa-search-plus click"
                                                        value="<?php echo $respuesta['IdGestion']; ?>">

                                                </button>
                                                <a data-toggle="tooltip" title="Editar estado"
                                                   href="ModificarGestion.php?id=<?php echo $respuesta['IdGestion']; ?>" <?php if (!$_SESSION['datosLogin']['NombreRol'] == 'Coordinador' or (!$_SESSION['datosLogin']['NombreRol'] == 'Asesor') or (!$_SESSION['datosLogin']['NombreRol'] == 'Administrador')) {
                                                    echo 'hidden=';
                                                } ?> >
                                                    <i class="fa fa-fw fa-edit"></i>
                                                </a>
                                            </td>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Actividad</th>
                                            <th>Tema Relacionado</th>
                                            <th>Lugar</th>
                                            <th>Estado</th>
                                            <th>Fecha</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                            <?php
                        }
                    }
                    if (!isset($_GET['todos'])) { ?>
                        <!-- general form elements disabled -->
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Todos los registros</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p>
                                    Si desea, puede ver todos los registros usando el botón "Ver todos"
                                    (esta opción puede tardar un poco).
                                </p>
                            </div>
                            <div class="box-footer">
                                <form action="../controllers/ControladorGestion.php?listar=todos" method="post">
                                    <button type="submit" class="btn btn-success pull-right" tabindex="14"
                                        ><i class="fa fa-plus-square-o"> </i> Ver todos
                                    </button>
                                </form>
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->

            <?php
            }

            else{ ?>
            <div class="row" style="margin-top: 30%;opacity: 0.5">
                <div class="data-wrapper" id="data_table"><div class="no-data-section  text-center">
                        <i class="fa fa-calendar fa-5x"></i>
                        <h4 class="text-center"> Sin gestiones registradas</h4>
                    </div>
                </div>
            </div>
            <?php }?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    <!-- REQUIRED JS SCRIPTS -->

</div>
<div class="modal fade " id="myModal">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header"
                 style="background-color: #3c8dbc;border-radius: 5px 5px 0px 0px;color:#FFF;text-align: center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modaltitle">Detalle de Gestión</h4>
            </div>
            <div class="modal-body" class="pull-left col-md-8">
                <dl class="dl-horizontal">
                    <dt>Nit Cliente</dt>
                    <dd id="Cliente">A description list is perfect for defining terms.</dd>
                    <dt>Tema</dt>
                    <dd id="tema">Vestibulum id ligula porta felis euismod semper eget lacinia odio
                        sem nec elit.
                    </dd>
                    <dt>Asistentes</dt>
                    <dd id="asistentes">Etiam porta sem malesuada magna mollis euismod.</dd>
                    <dt>Obsevaciones</dt>
                    <dd id="observaciones">Fusce dapibus, tellus ac cursus commodo, tortor mauris
                        condimentum nibh, ut fermentum massa justo sit amet risus.
                    </dd>
                    <dt>Lugar</dt>
                    <dd id="lugar"></dd>
                    <dt>Fecha</dt>
                    <dd id="fecha"></dd>
                    <dt>Modificado por:</dt>
                    <dd id="modificacion"></dd>
                    <dt>Resultado de visita</dt>
                    <dd id="actividad">A description list is perfect for defining terms.</dd>
                </dl>
                <!--<p><input type="text" class="input-sm" id="txtfname"/></p>
                <p><input type="text" class="input-sm" id="txtlname"/></p>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../plugins/morris/morris.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="../../plugins/knob/jquery.knob.js" type="text/javascript"></script>
<script src="../../plugins/morris/raphael-min.js%20"></script>
<script src="../../plugins/topdf/xepOnline.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

</body>
<script type="text/javascript">
    $(document).ready(function () {

            $('[data-toggle="popover"]').popover();



        $('.click').click(function () {
            $.post("../controllers/ControladorGestion.php",

                {
                    detail: $(this).attr("value")
                },
                function (data) {
                    var json = $.parseJSON(data);
                    $("#modaltitle").text(json.TipoGestiones);
                    $('#actividad').text(json.ResultadoGestiones);
                    $('#tema').text(json.Asunto);
                    $('#asistentes').text(json.Asistentes);
                    $('#observaciones').text(json.ObservacionesGestiones);
                    $('#lugar').text(json.LugarGestiones);
                    $('#fecha').text(json.FechaProgramada);
                    $('#modificacion').text(json.CedulaEmpleadoGestiones);
                    $('#Cliente').text(json.NitClienteGestiones);
                    $("#myModal").modal("show");

                });
        });
    });


</script>
<script type="text/javascript">

    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            responsive: true
        });
    });

</script>
</html>
