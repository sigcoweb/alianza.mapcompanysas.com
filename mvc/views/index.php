<?php
include_once '../facades/ClienteFacade.php';
include_once '../facades/FacadeCotizaciones.php';
include_once '../facades/FacadeProducto.php';
$producFaca=new FacadeProducto();
$cotizacionesFac=new FacadeCotizaciones();
$clientesActivosFac=new ClienteFacade();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SIGCO | Inicio</title>
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">


        <?php
        include_once 'header.php';
        ?>

    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inicio
                <!--            <small>Optional description</small>-->
            </h1>
            <ol class="breadcrumb">
                <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">


            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <?php
                                    $cantidadActivos = count($clientesActivosFac->obtenerClientesActivos());
                                    $cantidadInactivos= count($clientesActivosFac->obtenerClientesInactivos());
                                    ?>
                                    <h3><?php echo $cantidadActivos ?></h3>
                                    <p>Clientes activos</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                <a href="buscarClientes.php?criterio=clientes.estadoCliente&busqueda=Activo&comobuscar=1" class="small-box-footer">
                                    <?php }else{ ?>
                                    <a href="buscarClientes.php" class="small-box-footer">
                                        <?php } ?>
                                        Buscar <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><?php echo $cantidadInactivos ?></h3>
                                    <p>Clientes inactivos</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user-times"></i>
                                </div>
                                <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                <a href="buscarClientes.php?criterio=clientes.estadoCliente&busqueda=Inactivo&comobuscar=1" class="small-box-footer">
                                    <?php }else{ ?>
                                    <a href="buscarClientes.php" class="small-box-footer">
                                        <?php } ?>
                                        Buscar <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <?php
                                    $cotVigentes=count($cotizacionesFac->buscarConCriterio("cotizaciones.EstadoCotizacion", "Vigente",1));
                                    $cotCanceladas=count($cotizacionesFac->buscarConCriterio("cotizaciones.EstadoCotizacion", "Cancelada",1));
                                    ?>
                                    <h3><?php echo $cotVigentes ?></h3>
                                    <p>Cotizaciones vigentes</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cart-plus"></i>
                                </div>
                                <a href="buscarCotizaciones.php?criterio=cotizaciones.EstadoCotizacion&busqueda=Vigente&comobuscar=1" class="small-box-footer">Buscar <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3><?php echo $cotCanceladas ?></h3>
                                    <p>Cotizaciones canceladas</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-exclamation-circle"></i>
                                </div>
                                <a href="buscarCotizaciones.php?criterio=cotizaciones.EstadoCotizacion&busqueda=Cancelada&comobuscar=1" class="small-box-footer">Buscar <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-8">
                            <!-- TABLE: LATEST ORDERS -->

                            <?php
                            if($cantidadActivos<1) {
                                ?>
                                <div class="box box-warning box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Crear clientes</h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <p>
                                            No cuenta con clientes creados y activos para iniciar su gestión.
                                            Puede crear uno aquí (el cliente posteriormente debe ser activado por un
                                            coordinador).<br><br>
                                            <a class="label label-primary" href="nuevoCliente.php">
                                                <i class="fa fa-calculator"></i> Crear un cliente</a>
                                        </p>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                <?php
                            }else{
                                ?>


                                <div class="box box-info box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Últimas cotizaciones</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-minus"></i></button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-times"></i></button>
                                        </div>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <?php
                                        $ultimasCotizaciones = $cotizacionesFac->listarTodas();
                                        if (count($ultimasCotizaciones) > 0) {
                                            ?>
                                            <div class="table-responsive">
                                                <table class="table no-margin table-responsive table-condensed hover">
                                                    <thead>
                                                    <tr>
                                                        <th>#Cot.</th>
                                                        <th>Fecha</th>
                                                        <th>#Nit</th>
                                                        <th>Razon social</th>
                                                        <th>Estado</th>
                                                        <th>Valor</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($ultimasCotizaciones as $cotizacion) {
                                                        $i++;
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <a class="label label-info label-xs badge"
                                                                   data-toggle="tooltip" title="Ver detalle" target="_blank"
                                                                   href="ex2.php?coti=<?php echo $cotizacion['IdCotizacion'] ?>">
                                                                <span
                                                                    class="fa fa-search-plus"><?php echo $cotizacion['IdCotizacion'] ?></span></a>

                                                            </td>
                                                            <td><?php echo date("Y-m-d", strtotime($cotizacion['FechaCreacionCotizacion'])) ?>
                                                            </td>
                                                            <td><?php echo $cotizacion['NitClienteCotizaciones'] ?></td>
                                                            <td><?php echo $cotizacion['RazonSocial'] ?></td>
                                                            <td><span class="label label-<?php
                                                                if ($cotizacion['EstadoCotizacion'] == 'Vigente') {
                                                                    echo 'success ';
                                                                } elseif ($cotizacion['EstadoCotizacion'] == 'Cancelada') {
                                                                    echo 'warning ';
                                                                } else {
                                                                    echo 'default ';
                                                                } ?>">
                                                        <?php echo $cotizacion['EstadoCotizacion'] ?></span></td>
                                                            <td>
                                                                <?php echo '$' . number_format($cotizacion['ValorTotalCotizacion']) ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        if ($i > 5) {
                                                            break;
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div><!-- /.table-responsive -->
                                            <?php
                                        } else {
                                            ?>
                                            <p>
                                                No tiene cotizaciones creadas.<br>
                                                Empiece creando una con el siguiente botón:
                                            </p>
                                            <?php
                                        }
                                        ?>
                                    </div><!-- /.box-body -->
                                    <div class="box-footer clearfix">
                                        <a href="crearCotizacion.php" class="btn btn-sm btn-info btn-flat pull-left">Crear
                                            nueva cotización</a>
                                        <a href="buscarCotizaciones.php?encontrados=true&todos=true&criterio=&busqueda=&comobuscar="
                                           class="btn btn-sm btn-default btn-flat pull-right">Ver todas las cotizaciones</a>
                                    </div><!-- /.box-footer -->
                                </div><!-- /.box -->

                                <?php
                            }
                            ?>


                        </div><!-- /.col -->



                        <div class="col-md-4">
                            <!-- Info Boxes Style 2 -->
                            <!-- PRODUCT LIST -->
                            <div class="box box-primary box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Productos recientes</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div><!-- /.box-header -->
                                <?php
                                $ultimosProductos=$producFaca->getProductos();
                                ?>
                                <div class="box-body">
                                    <ul class="products-list product-list-in-box">
                                        <?php
                                        $i=0;
                                        foreach($ultimosProductos as $producto) {
                                            $i++;
                                            ?>
                                            <div class="" style="margin: auto">

                                            </div>
                                            <li class="item">
                                                <img class="pull-right" style="max-width: 15%;"  src="../../dist/img/labelNewDer.png" alt="">
                                                <div class="product-img">

                                                    <img src="<?php echo $producto['rutaImagen'] ?>"
                                                         alt="Imagen del producto"/>
                                                </div>
                                                <div class="product-info">

                                                    <a href="javascript::;" class="product-title"><?php echo $producto['NombreProducto'] ?>
                                                        <span data-toggle="tooltip" title="Producto <?php if($producto['EstadoProductos']=="Activo")
                                                        {echo "disponible";}else{echo "inactivo";} ?>" class="label label-<?php if($producto['EstadoProductos']=="Activo")
                                                        {echo "success";}else{echo "warning";} ?> pull-right">$<?php echo number_format($producto['valorPresentacion']) ?>

                                                        </span></a>
                                                    <p></p>
												<span class="product-description pull-left">
													<p><?php echo $producto['DescripcionProducto']; ?></p>
                                                    <a class="label label-default badge" style="font-size: 70%" href="../documents/<?php echo $producto['certificadoProducto']; ?>" target="_blank">Cert.</a>
                                                    <a class="label label-info badge" style="font-size: 70%" href="../documents/<?php echo $producto['fichaSeguridad']; ?>" target="_blank">F.Seg.</a>
                                                    <a class="label label-primary badge" style="font-size: 70%" href="../documents/<?php echo $producto['fichaTecnica']; ?>" target="_blank">F.Tec.</a>
												</span>
                                                </div>
                                            </li><!-- /.item -->
                                            <?php
                                            if($i>=4){
                                                break;
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div><!-- /.box-body -->
                                <div class="box-footer text-center">
                                    <a href="productoListar.php?encontrados=true&criterio=&busqueda=&comobuscar=" class="uppercase">Ver todos los productos</a>
                                </div><!-- /.box-footer -->
                            </div><!-- /.box -->
                        </div><!-- Info Boxes Style 2 -->
                    </div><!--row-->




                </div><!-- Left col -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      iterator experience. Slimscroll is required when using the
      fixed layout. -->
</body>
</html>
