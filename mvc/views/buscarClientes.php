<?php
include_once '../facades/ClienteFacade.php';
include_once '../facades/PuntosEntregaFacade.php';
include_once '../facades/FacadeCotizaciones.php';
include_once '../facades/AreasClienteFacade.php';
$puntosFaca = new PuntosEntregaFacade();
$areasFaca = new AreasClienteFacade();
$cotizFaca = new FacadeCotizaciones();
$clienteFaca = new ClienteFacade();
session_start(); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Clientes</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>

    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/ju-1.11.4/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,af-2.1.0,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.css"/>

    <link href="../../plugins/animate/animate.css" rel="stylesheet" type="text/css"/>
    <script src="../../plugins/messajes/jquery.noty.packaged.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php
            include_once 'menu.php';
            ?>

        </section>
        <!-- /.sidebar -->
    </aside>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clientes
                <small>Buscar</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li>Clientes</li>
                <li class="active">Buscar</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
                     id="verDetalle">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content" style="border-radius: 5px">
                            <div class="modal-header"
                                 style="background: #00c0ef; color: #fff; border-radius: 5px 5px 0 0; text-align: center">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span
                                        aria-hidden="true">×</span></button>
                                <h3 class="modal-title" id="nombreCliente">Detalle del cliente</h3>
                            </div>
                            <div class="modal-body">
                                <!-- info row -->
                                <div class="row invoice-info">
                                    <div class="col-sm-5 invoice-col">
                                        <dl class="dl-horizontal">
                                            <dt>Razón social</dt>
                                            <dd id="razonsocial1"></dd>
                                            <dt>Nombre comercial</dt>
                                            <dd id="nombrecomercial1"></dd>
                                            <dt>Nit</dt>
                                            <dd id="nit1"></dd>
                                            <dt>Dirección</dt>
                                            <dd id="direccion1"></dd>
                                            <dt>Ubicación</dt>
                                            <dd id="departamento1"></dd>
                                            <dd id="lugar1"></dd>
                                            <dt>Teléfono corporativo</dt>
                                            <dd id="telefono1"></dd>
                                            <dt>Email corporativo</dt>
                                            <dd id="email1"></dd>
                                            <dt>Tipo de empresa</dt>
                                            <dd id="tipo1"></dd>
                                            <dt>Actividad económica</dt>
                                            <dd id="actividad1"></dd>
                                            <dt>Clasificación del cliente</dt>
                                            <dd id="clasificacion1"></dd>
                                            <dt>Estado del cliente</dt>
                                            <dd id="estado0"></dd>
                                            <dt>Día cierre</dt>
                                            <dd id="diaCierre"></dd>
                                            <dt>Plazo pago</dt>
                                            <dd id="plazoPago"></dd>
                                        </dl>
                                    </div><!-- /.col -->
                                    <div class="col-sm-6 invoice-col">
                                        <dl class="dl-horizontal">
                                            <dt>Nombres</dt>
                                            <dd id="nombres1"></dd>
                                            <dt>Apellidos</dt>
                                            <dd id="apellidos1"></dd>
                                            <dt>Cédula</dt>
                                            <dd id="cedula1"></dd>
                                            <dt>Email</dt>
                                            <dd id="email2"></dd>
                                            <dt>Celular</dt>
                                            <dd id="celular1"></dd>
                                            <dt>Inscrito desde</dt>
                                            <dd id="fechaCliente1"></dd>
                                            <dt>Estado del usuario</dt>
                                            <dd id="estado1"></dd>
                                        </dl>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- right column -->
                <div class="col-md-10">

                    <?php if (isset($_GET['mensaje'])) { ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 'true') {
                            echo 'alert-error';
                        } else {
                            echo 'alert-success';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="fa fa-<?php if ($_GET['error'] == 'true') {
                                    echo 'warning';
                                } else {
                                    echo 'check';
                                }; ?>">
                                </i> Resultado del proceso:</h4>
                            <p>
                                <?php echo $mensaje = $_GET['mensaje'] ?>
                            </p>
                            <?php if (isset($_GET['activo']) && $_GET['activo'] == 'true') { ?>
                                <a href="crearCotizacion.php?nit=<?php echo $_GET['nit'] ?>"
                                   class="btn btn-primary btn-xs">Añadir cotización</a>

                            <?php } ?>
                        </div>

                        <?php if (isset($_GET['detalleerror']) && $_GET['error'] == 'true') { ?>

                            <div class="box box-danger box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    if(count($clienteFaca->listarTodos())==0) {
                        ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Sin clientes registrados </h4>
                            <p>No cuenta con clientes creados. Para incluir uno vaya a: <a class="label bg-green-gradient"
                                                                                           href="nuevoCliente.php"><i
                                        class="fa fa-plus-circle"></i> Añadir cliente</a></p>
                        </div>
                    <?php }else{ ?>

                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Indicaciones para la búsqueda</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Reducir/Ampliar"
                                        data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Ocultar"
                                        data-widget="remove"><i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                Use las siguientes opciones para realizar la búsqueda de la información.
                                Recuerde que en este formulario hay campos obligatorios(*).
                            </p>
                        </div>
                    </div>
                    <form role="form" id="defaultForm" action="../controllers/ClientesController.php?controlar=buscar" method="post">
                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Elementos para la búsqueda</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-toggle="tooltip" title="Reducir/Ampliar"
                                            data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                    <div class="form-group">
                                        <label for="criterio">Seleccione el cliente para consultar*</label>
                                        <select class="form-control select2 sinKey" name="criterio" id="criterio" style="width: 100%"
                                                data-toggle="tooltip" title="Indique una categoría" required tabindex="1"
                                                autofocus>
                                            <optgroup label="Información corporativa">Información corporativa</optgroup>
                                            <option value="clientes.Nit"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'clientes.Nit') {
                                                    echo 'selected';
                                                } ?>
                                            >NIT del cliente (Ej. 917284)
                                            </option>
                                            <option value="clientes.RazonSocial"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'clientes.RazonSocial') {
                                                    echo 'selected';
                                                } ?>
                                            >Razón Social del cliente (Ej. Duicom Incorporated)
                                            </option>
                                            <option value="clientes.nombreComercial"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'clientes.nombreComercial') {
                                                    echo 'selected';
                                                } ?>
                                            >Nombre comercial (Ej. DU CO)
                                            </option>
                                            <option value="clientes.estadoCliente"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'clientes.estadoCliente') {
                                                    echo 'selected';
                                                } ?>
                                            >Estado del cliente (Ej. Activo/Inactivo)
                                            </option>
                                            <option value="departamentos.nombreDepartamento"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'departamentos.nombreDepartamento') {
                                                    echo 'selected';
                                                } ?>
                                            >Departamento de ubicación (Ej. Cundinamarca)
                                            </option>
                                            <option value="lugares.NombreLugar"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'lugares.NombreLugar') {
                                                    echo 'selected';
                                                } ?>
                                            >Ubicación del cliente (Ej. Bogotá)
                                            </option>
                                            <optgroup label="Información personal">Información personal</optgroup>
                                            <option value="personas.CedulaPersona"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'personas.CedulaPersona') {
                                                    echo 'selected';
                                                } ?>
                                            >Cédula Ciudadanía del cliente (Ej. 51125309)
                                            </option>
                                            <option value="personas.Nombres"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'personas.Nombres') {
                                                    echo 'selected';
                                                } ?>
                                            >Nombres del contacto (Ej. Carlos)
                                            </option>
                                            <option value="personas.Apellidos"
                                                <?php if (isset($_GET['criterio']) && $_GET['criterio'] == 'personas.Apellidos') {
                                                    echo 'selected';
                                                } ?>
                                            >Nombres del contacto (Ej. Casas)
                                            </option>
                                        </select>
                                    </div>

                                    <label for="comobuscar">¿Cómo desea consultar? y ¿Qué desea encontrar?*</label>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select name="comobuscar" id="comobuscar" style="width: 100%"
                                                        class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                                        tabindex="2">
                                                    <option <?php if (isset($_GET['comobuscar']) && $_GET['comobuscar'] == 2) {
                                                        echo 'selected';
                                                    } ?> title="Hace una búsqueda por cualquier coincidencia"
                                                         value="2"> Cualquier registro que contenga
                                                    </option>
                                                    <option <?php if (isset($_GET['comobuscar']) && $_GET['comobuscar'] == 1) {
                                                        echo 'selected';
                                                    } ?> title="Hace una búsqueda de la coincidencia única"
                                                         value="1"> Una búsqueda exacta de
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input type="txt" name="busqueda" class="form-control" data-toggle="tooltip"
                                                       title="Indique lo que desea buscar"
                                                    <?php if (isset($_GET['busqueda'])) {
                                                        echo 'value="' . $_GET['busqueda'] . '"';
                                                    } elseif (isset($_GET['todos']) && $_GET['todos'] == 'todos') {
                                                        echo 'placeholder="Número Nit | Razón Social | Lugar"';
                                                    } else {
                                                        echo 'placeholder="Número Nit | Razón Social | Lugar"';
                                                    }
                                                    ?>
                                                       required tabindex="3">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                          <span class="input-group-btn">
                      <button class="btn bg-aqua-gradient btn-flat" type="submit" tabindex="4" data-toggle="tooltip"
                              title="Clic para consultar">
                          <i class="fa fa-search"> </i> Buscar cliente(s)
                      </button>
                    </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <input type="text" class="hidden" hidden name="criterio" value="clientes.Nit">
                                                <input type="text" class="hidden" hidden name="comobuscar" value="1">
                                                <select class="form-control select2" name="busqueda" id="busqueda" style="width: 100%" required>
                                                    <option selected disabled>Seleccione un cliente para consultar...</option>
                                                    <?php
                                                    $listado=$clienteFaca->listarTodos();
                                                    foreach ($listado as $clientes ) { ?>
                                                        <option <?php if(isset($_GET['criterio'])&&$_GET['busqueda']==$clientes['Nit']){echo 'selected';} ?>
                                                            value="<?php echo $clientes['Nit']?>" >
                                                            <?php echo $clientes['RazonSocial'].' | '.$clientes['Nit']; if($clientes['estadoCliente']=='Inactivo'){echo ' | Inactivo';} ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                            <span class="input-group-btn">
                      <button class="btn bg-aqua-gradient btn-flat btn-block" type="submit" tabindex="4" data-toggle="tooltip"
                              title="Clic para consultar">
                          <i class="fa fa-search"> </i> Consultar cliente
                      </button>
                    </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                    <?php
                    if ((isset($_GET['criterio']) && isset($_GET['busqueda']) && isset($_GET['comobuscar']))) {
                        $consulta = $clienteFaca->buscarCliente($_GET['criterio'], $_GET['busqueda'], $_GET['comobuscar']);
                    }
                    if (isset($_GET['todos'])) {
                        $consulta = $clienteFaca->listarTodos();
                    }
                    if ((isset($_GET['busqueda']) || isset($_GET['todos'])) && ((isset($consulta) && count($consulta)) <= 0)) {
                        ?>
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-triangle"></i> Consulta sin coincidencias</h4>
                            <p>No se han encontrado resultados para la consulta</p>
                        </div>
                        <?php
                    }
                    if (isset($consulta)) {

                        if (count($consulta) > 0) {
                            ?>

                            <div class="box box-primary box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Resultados de la búsqueda</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        Se han encontrado <span
                                            class="badge label-info"><?php echo count($consulta); ?></span>
                                        registros para esta consulta.
                                    </p>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#Empresas" aria-controls="Empresas" role="tab" data-toggle="tab">Empresas</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#Representantes" aria-controls="Representantes" role="tab"
                                               data-toggle="tab">Representantes</a>
                                        </li>
                                    </ul>
                                    <br>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="Empresas">
                                            <table id="example1"
                                                   class="table table-striped hover order-column row-border compact display">
                                                <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>#Nit</th>
                                                    <th>Razón social</th>
                                                    <th>Ubicación</th>
                                                    <th>Teléfono</th>
                                                    <th>Estado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($consulta as $respuesta){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo date('Y-m-d', strtotime($respuesta['FechaCreacionPersona'])) ?>

                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['Nit']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['RazonSocial']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['NombreLugar']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['Telefono']; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (($_SESSION['rol'][0] == 1) || $_SESSION['rol'][0] == 4) {
                                                            ?>
                                                            <button
                                                                class="btn btn-xs <?php if ($respuesta['estadoCliente'] == 'Activo') {
                                                                    echo 'bg-green-gradient';
                                                                } else {
                                                                    echo 'bg-yellow-gradient';
                                                                }; ?> cambiarEstado"
                                                                value="<?php echo $respuesta['Nit']; ?>"
                                                                data-toggle="tooltip" title="Cambiar estado">
                                                                <?php echo($respuesta['estadoCliente']); ?>
                                                            </button>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button
                                                                class="btn btn-xs <?php if ($respuesta['estadoCliente'] == 'Activo') {
                                                                    echo 'bg-green-gradient';
                                                                } else {
                                                                    echo 'bg-yellow-gradient';
                                                                }; ?>"
                                                                data-toggle="tooltip"
                                                                title="Sólo el coordinador puede cambiar el estado del cliente">
                                                                <?php echo($respuesta['estadoCliente']); ?>
                                                            </button>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <div class="btn-group dropup">
                                                            <button type="button" class="btn btn-xs btn-default click"
                                                                    value="<?php echo $respuesta['IdCliente']; ?>">Ver
                                                                más
                                                            </button>
                                                            <button type="button"
                                                                    class="btn btn-xs btn-default dropdown-toggle" aria-haspopup="true" aria-expanded="false"
                                                                    data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                                                    <li>
                                                                        <a href="modificarCliente.php?IdPersona=<?php echo $respuesta['IdPersona'] . '&IdCliente=' . $respuesta['IdCliente']; ?>">Editar</a>
                                                                    </li>
                                                                <?php } ?>
                                                                <li>
                                                                    <a href="nuevaArea.php?IdCliente=<?php echo $respuesta['IdCliente']; ?>">Añadir
                                                                        áreas</a></li>
                                                                <?php
                                                                if (count($areasFaca->buscarArea('areascliente.nitClienteAreas', $respuesta['Nit'], 1)) > 0) {
                                                                    ?>
                                                                    <li>
                                                                        <a href="buscarAreasEmpresas.php?criterio=areascliente.nitClienteAreas&busqueda=<?php echo $respuesta['Nit'] ?>&comobuscar=1"
                                                                        >Ver áreas
                                                                            <small class="badge pull-right label-info">
                                                                                <?php
                                                                                echo count($areasFaca->buscarArea('areascliente.nitClienteAreas', $respuesta['Nit'], 1))
                                                                                ?>
                                                                            </small>
                                                                        </a>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <li>
                                                                    <a href="nuevoPuntoEntrega.php?IdCliente=<?php echo $respuesta['IdCliente']; ?>">Añadir
                                                                        puntos de entrega</a></li>
                                                                <?php
                                                                if (count($puntosFaca->buscarPunto('puntosentrega.nitEmpresaPuntoEntrega', $respuesta['Nit'], 1)) > 0) {
                                                                    ?>
                                                                    <li>
                                                                        <a href="buscarPuntoEntrega.php?criterio=puntosentrega.nitEmpresaPuntoEntrega&busqueda=<?php echo $respuesta['Nit'] ?>&comobuscar=1"
                                                                        >Ver puntos entrega
                                                                            <small class="badge pull-right label-info">
                                                                                <?php
                                                                                echo count($puntosFaca->buscarPunto('puntosentrega.nitEmpresaPuntoEntrega', $respuesta['Nit'], 1))
                                                                                ?>
                                                                            </small>
                                                                        </a>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <li>
                                                                    <a <?php if ($respuesta['estadoCliente'] == 'Activo')
                                                                        {
                                                                            echo 'href="crearCotizacion.php?nit=' . $respuesta['Nit'] . '"';
                                                                        }else{
                                                                        ?>
                                                                        class="btn bg-gray" data-toggle="tooltip"
                                                                        title="Para añadir una cotización debe activar este cliente (coordinador*)"
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                    ">Añadir cotización</a>
                                                                </li>
                                                                <?php
                                                                if (count($cotizFaca->buscarConCriterio('clientes.Nit', $respuesta['Nit'], 1)) > 0) {
                                                                    ?>
                                                                    <li>
                                                                        <a href="buscarCotizaciones.php?criterio=clientes.Nit&busqueda=<?php echo $respuesta['Nit'] ?>&comobuscar=1"
                                                                        >Ver cotizaciones
                                                                            <small class="badge pull-right label-info">
                                                                                <?php
                                                                                echo count($cotizFaca->buscarConCriterio('clientes.Nit', $respuesta['Nit'], 1))
                                                                                ?>
                                                                            </small>
                                                                        </a>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>#Nit</th>
                                                    <th>Razón social</th>
                                                    <th>Ubicación</th>
                                                    <th>Teléfono</th>
                                                    <th>Estado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div> <!-- panel activo-->
                                        <div role="tabpanel" class="tab-pane fade" id="Representantes">
                                            <table id="example2"
                                                   class="table table-striped hover order-column row-border compact">
                                                <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>#Cédula</th>
                                                    <th>Nombres</th>
                                                    <th>Apellidos</th>
                                                    <th>#Celular</th>
                                                    <th>Estado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach ($consulta as $respuesta){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo date('Y-m-d', strtotime($respuesta['FechaCreacionPersona'])) ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['CedulaPersona']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['Nombres']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['Apellidos']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $respuesta['CelularPersona']; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if (($_SESSION['rol'][0] == 1) || $_SESSION['rol'][0] == 4) {
                                                            ?>
                                                            <button
                                                                class="btn btn-xs <?php if ($respuesta['EstadoPersona'] == 'Activo') {
                                                                    echo 'bg-green-gradient';
                                                                } else {
                                                                    echo 'bg-yellow-gradient';
                                                                }; ?> cambiarEstado"
                                                                value="<?php echo $respuesta['CedulaPersona']; ?>"
                                                                data-toggle="tooltip" title="Cambiar estado">
                                                                <?php echo($respuesta['EstadoPersona']); ?>
                                                            </button>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button
                                                                class="btn btn-xs <?php if ($respuesta['EstadoPersona'] == 'Activo') {
                                                                    echo 'bg-green-gradient';
                                                                } else {
                                                                    echo 'bg-yellow-gradient';
                                                                }; ?>"
                                                                data-toggle="tooltip"
                                                                title="Sólo el coordinador puede cambiar el estado del cliente">
                                                                <?php echo($respuesta['EstadoPersona']); ?>
                                                            </button>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-xs btn-default click"
                                                                    value="<?php echo $respuesta['IdCliente']; ?>">Ver
                                                                más
                                                            </button>
                                                            <button type="button"
                                                                    class="btn btn-xs btn-default dropdown-toggle"
                                                                    data-toggle="dropdown">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                                                                    <li>
                                                                        <a href="modificarCliente.php?IdPersona=<?php echo $respuesta['IdPersona'] . '&IdCliente=' . $respuesta['IdCliente']; ?>">Editar</a>
                                                                    </li>
                                                                <?php } ?>

                                                                <li>
                                                                    <a <?php if ($respuesta['estadoCliente'] == 'Activo')
                                                                        {
                                                                            echo 'href="nuevoClienteMismaPersona.php?cedulaPersona=' . $respuesta['CedulaPersona'] . '"';
                                                                        }else{
                                                                        ?> class="btn bg-gray" data-toggle="tooltip"
                                                                           title="Para asociar una empresa debe activar este cliente (coordinador*)" <?php
                                                                    }
                                                                    ?>">Añadir empresa
                                                                    </a>
                                                                </li>
                                                                <!--<li class="divider"></li>
                                                                <li>
                                                                    <button type="button"
                                                                            class="btn btn-xs reestablecer btn-block"
                                                                            value="<?php echo $respuesta['IdPersona']; ?>">
                                                                        Reestablecer contraseña
                                                                    </button>
                                                                </li>-->
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>#Cédula</th>
                                                    <th>Nombres</th>
                                                    <th>Apellidos</th>
                                                    <th>#Celular</th>
                                                    <th>Estado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div> <!-- panel inactivo-->
                                    </div> <!-- tab content-->
                                </div>
                            </div>
                            <?php
                        }
                    }
                    if (!isset($_GET['todos'])) {
                        if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                            <div class="box box-default box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver todos los registros</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-toggle="tooltip" title="Reducir/Ampliar"
                                                data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <p>
                                        Si desea, puede ver todos los registros usando el botón "Ver todos"
                                        (esta opción puede tardar un poco).
                                    </p>
                                </div>
                                <div class="box-footer">
                                    <form action="../controllers/ClientesController.php?controlar=todos" method="post">
                                        <button type="submit" class="btn bg-green-gradient pull-right" tabindex="14"
                                                data-toggle="tooltip" title="Clic para consultar"
                                        ><i class="fa fa-plus-square-o"> </i> Ver todos
                                        </button>
                                    </form>
                                </div>
                            </div>
                        <?php } } ?>
                </div><!--/.col (right) -->
                <?php
                }
                ?>
            </div>   <!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->
<!--
<script type="text/javascript" src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
-->
<script type="text/javascript" src="https://cdn.datatables.net/s/ju-1.11.4/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,af-2.1.0,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.js"></script>


<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Select2 -->
<script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $('.click').click(function () {
        $.post("../controllers/ClientesController.php",
            {
                idDetalleCliente: $(this).attr("value")
            },
            function (data) {
                var json = $.parseJSON(data);
                $('#nombreCliente').text(json.RazonSocial);
                $('#nit1').text(json.Nit);
                $('#razonsocial1').text(json.RazonSocial);
                $('#departamento1').text(json.nombreDepartamento);
                $('#nombrecomercial1').text(json.nombreComercial);
                $('#direccion1').text(json.Direccion);
                $('#telefono1').text(json.Telefono);
                $('#email1').text(json.EmailCliente);
                $('#actividad1').text(json.NombreActividad);
                $('#clasificacion1').text(json.NombreClasificacion);
                $('#lugar1').text(json.NombreLugar);
                $('#estado0').text(json.estadoCliente);
                $('#cedula1').text(json.CedulaPersona);
                $('#nombres1').text(json.Nombres);
                $('#apellidos1').text(json.Apellidos);
                $('#email2').text(json.EmailPersona);
                $('#celular1').text(json.CelularPersona);
                $('#tipo1').text(json.NombreTipo);
                $('#estado1').text(json.EstadoPersona);
                $('#fechaCliente1').text(json.FechaCreacionPersona);
                $('#diaCierre').text(json.diaCierreFacturacion);
                $('#plazoPago').text(json.plazoPago);
                $('#verDetalle').modal('show');
                //alert(data);
            });

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('[data-toggle="popover"]').popover();

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
        });

        var table = $('#example1').DataTable({
            responsive: true,
            stateSave: true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            'sPageEllipsis': 'paginate_ellipsis',
            'sPageNumber': 'paginate_number',
            'sPageNumbers': 'paginate_numbers',
            "processing": true,
            "deferLoading": 57,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [
                [10, 25, 50, -1],
                ["10 registros", "25 registros", "50 registros", "Todos"]
            ],
            /*
             "aoColumns": [
             { "orderSequence": [ "desc", "asc", "asc" ] },
             null,
             null,
             null,
             null,
             null,
             null,
             null
             ],
             */
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6
                }
            ],
            "order": [[ 0, 'desc' ]],

            dom:
                'Bfrtip',

            buttons: [
                {
                    extend: 'pageLength',
                    text: '<i class="fa fa-filter"> </i> Cantidad',
                    titleAttr: 'Cantidad de registros'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i>',
                    titleAttr: 'Mostrar/ocultar columnas'
                }
                <?php if(isset($nadie)){ ?>
                ,{
                    extend: 'print',
                    text: '<i class="fa fa-print"> </i>',
                    titleAttr: 'Imprimir',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' )
                            .prepend(
                                '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    }
                },
                {
                    extend: 'collection',
                    text: '<i class="fa fa-file"> </i>  Exportar',
                    buttons: [
                        {
                            extend: 'copy',
                            text: '<i class="fa fa-files-o"> </i>  Copiar',
                            titleAttr: 'Copiar en el portapapeles',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            message: 'Listado de clientes a la fecha (<?php echo date("dd-mm-Y") ?>) por <?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?>',
                            text: '<i class="fa fa-file-pdf-o"> </i> PDF',
                            orientation: 'landscape',
                            pageSize: 'LETTER',
                            /*
                             customize: function ( doc ) {
                             doc.content.splice( 1, 0, {
                             margin: [ 0, 0, 0, 12 ],
                             alignment: 'center',
                             image: 'http://datatables.net/media/images/logo-fade.png'
                             } );
                             },
                             */
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-file-text-o"> </i>  CSV',
                            titleAttr: 'Exportar valores separados por comas',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        },
                        {
                            extend: 'excel',
                            text:      '<i class="fa fa-file-excel-o"></i>  Excel',
                            titleAttr: 'a Ms Excel',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        }
                    ],
                    fade: true
                }
                <?php } ?>
            ]


        });
        table.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
        });
        table.on( 'responsive-resize', function ( e, datatable, columns ) {
            var count = columns.reduce( function (a,b) {
                return b === false ? a+1 : a;
            }, 0 );
            console.log( count +' column(s) are hidden' );
        });

        var table2 = $('#example2').DataTable({
            responsive: true,
            stateSave: true,


            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },


            'sPageEllipsis': 'paginate_ellipsis',
            'sPageNumber': 'paginate_number',
            'sPageNumbers': 'paginate_numbers',

            "processing": true,
            "deferLoading": 57,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [
                [10, 25, 50, -1],
                ["10 registros", "25 registros", "50 registros", "Todos"]
            ],
            /*
             "aoColumns": [
             { "orderSequence": [ "desc", "asc", "asc" ] },
             null,
             null,
             null,
             null,
             null,
             null,
             null
             ],
             */
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6
                }
            ],
            "order": [[ 0, 'desc' ]],

            dom:
                'Bfrtip',

            buttons: [
                {
                    extend: 'pageLength',
                    text: '<i class="fa fa-filter"> </i>  Cantidad',
                    titleAttr: 'Cantidad de registros'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i>',
                    titleAttr: 'Mostrar/ocultar columnas'
                }
                <?php if(isset($nadie)){ ?>
                ,
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"> </i>',
                    titleAttr: 'Imprimir',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' )
                            .prepend(
                                '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    }
                },
                {
                    extend: 'collection',
                    text: '<i class="fa fa-file"> </i>  Exportar',
                    buttons: [
                        {
                            extend: 'copy',
                            text: '<i class="fa fa-files-o"> </i>  Copiar',
                            titleAttr: 'Copiar en el portapapeles',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            message: 'Listado de clientes a la fecha (<?php echo date("dd-mm-Y") ?>) por <?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?>',
                            text: '<i class="fa fa-file-pdf-o"> </i> PDF',
                            orientation: 'landscape',
                            pageSize: 'LETTER',
                            /*
                             customize: function ( doc ) {
                             doc.content.splice( 1, 0, {
                             margin: [ 0, 0, 0, 12 ],
                             alignment: 'center',
                             image: 'http://datatables.net/media/images/logo-fade.png'
                             } );
                             },
                             */
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-file-text-o"> </i>  CSV',
                            titleAttr: 'Exportar valores separados por comas',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        },
                        {
                            extend: 'excel',
                            text:      '<i class="fa fa-file-excel-o"></i>  Excel',
                            titleAttr: 'a Ms Excel',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        }
                    ],
                    fade: true
                }
                <?php } ?>
            ]

        });

        table2.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
        });

        table2.on( 'responsive-resize', function ( e, datatable, columns ) {
            var count = columns.reduce( function (a,b) {
                return b === false ? a+1 : a;
            }, 0 );
            console.log( count +' column(s) are hidden' );
        });
    });
</script>

<script type="text/javascript">
    $(".reestablecer").click(function () {
        var btnId=$(this).attr("value");
        var n = noty({
            text: '¿Desea reestablecer la contraseña para éste cliente?',
            theme: 'relax',
            layout: 'center',
            closeWith: ['click', 'hover'],
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Reestablecer', onClick: function ($noty) {
                    $.post("../controllers/ClientesController.php",
                        {
                            reestablecerContrasenia: btnId
                        },
                        function (data) {
                            //location.reload();
                            noty({text: data, type: 'success'});
                            $noty.close();
                        });
                }
                },
                {
                    addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                    $noty.close();
                }
                }
            ],
            type: 'confirm',
            animation: {
                open: 'animated wobble', // Animate.css class names
                close: 'animated flipOutX' // Animate.css class names
            }
        });
    });

    $(".cambiarEstado").click(function () {
        var cambEst=$(this).attr("value");
        var n = noty({
            text: '¿Desea cambiar el estado de éste cliente?',
            theme: 'relax',
            layout: 'center',
            closeWith: ['click', 'hover'],
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Cambiar estado', onClick: function ($noty) {
                    $.post("../controllers/ClientesController.php",
                        {
                            cambiarEstado: cambEst
                        },
                        function (data) {
                            noty({text: data, type: 'success'});
                            location.href='buscarClientes.php?'+data;
                            $noty.close();
                        });
                }
                },
                {
                    addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                    $noty.close();
                }
                }
            ],
            type: 'confirm',
            animation: {
                open: 'animated wobble', // Animate.css class names
                close: 'animated flipOutX' // Animate.css class names
            }
        });
    });
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>
<script type="text/javascript">
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2({
            language: "es",
            minimumInputLength: 3
        });
        $(".sinKey").select2({
            language: "es"
        });
    });
</script>

</body>
</html>
