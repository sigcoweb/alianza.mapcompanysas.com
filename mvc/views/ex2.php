<?php
require('../utilities/fpdf/fpdf.php');
require'../facades/FacadeCotizaciones.php';
require'../utilities/ConvertirNumero.php';
$convertir=new ConvertirNumero();
$facade=new FacadeCotizaciones();
$resultados=$facade->buscarCotizacion($_GET['coti']);
//print_r($resultados);
$pdf=new FPDF("P");
$pdf->AddPage();
$pdf->SetFont('Arial', '', 12);
switch($resultados[0]['empresaCotiza']){
    case "MAP":
        $pdf->Image('../images/map.jpg',0,0,218);
        break;

    case "BIO":
        $pdf->Image('../images/bio.jpg',0,0,218);
        break;

    case "MAT":
        $pdf->Image('../images/mat.jpg',0,0,218);
        break;
}
$pdf->Image('cotizacion.new.png',0,0,218);
$pdf->Ln(25);
$pdf->Cell(70,0,utf8_decode('SEÑOR(ES): '.$resultados[0]['RazonSocial']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(115,0,utf8_decode('DIRECCIÓN: '.$resultados[0]['Direccion']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(130,0,utf8_decode('TELÉFONO: '.$resultados[0]['Telefono']),0,0,'L');
$pdf->SetFont('Arial', '', 16);
$pdf->SetTextColor(220,20,60);
$pdf->Cell(110,0,utf8_decode('Cotización Nº: '.$resultados[0]['IdCotizacion']),0,0,'L');
$pdf->Ln(5);
$pdf->SetFont('Arial', '', 12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(156,0,utf8_decode('E-MAIL: '.$resultados[0]['EmailCliente']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(115,0,utf8_decode('CIUDAD: '.$resultados[0]['NombreLugar']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(60,0,utf8_decode('NIT: '.$resultados[0]['Nit']),0,0,'L');
$pdf->Ln(5);
//$pdf->Cell(176,0,utf8_decode(''.date("d/m/Y")),0,0,'R');
$pdf->Cell(60,0,utf8_decode('ASESOR TÉCNICO COMERCIAL: '.($resultados[0]['Nombres'].' '.$resultados[0]['Apellidos'])),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(60,0,utf8_decode('EMAIl: '.$resultados[0]['EmailPersona']),0,0,'L');
$pdf->Ln(25);

$totalsub=0;
$totaliva=0;
$totalDescuento=0;
$pdf->Ln(15);
$totalProductos=0;
$pdf->SetFont('Arial', '', 10 );
$prod=0;
$lin=0;
foreach ($resultados as $datos) {
    if($lin>13){
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 12);
        switch($resultados[0]['empresaCotiza']){
            case "MAP":
                $pdf->Image('../images/map.jpg',0,0,218);
                break;

            case "BIO":
                $pdf->Image('../images/bio.jpg',0,0,218);
                break;

            case "MAT":
                $pdf->Image('../images/mat.jpg',0,0,218);
                break;
        }
        $pdf->Image('cotizacion.new.png',0,0,218);
        $pdf->Ln(25);
        $pdf->Cell(70,0,utf8_decode('SEÑOR(ES): '.$resultados[0]['RazonSocial']),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(115,0,utf8_decode('DIRECCIÓN: '.$resultados[0]['Direccion']),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(130,0,utf8_decode('TELÉFONO: '.$resultados[0]['Telefono']),0,0,'L');
        $pdf->SetFont('Arial', '', 16);
        $pdf->SetTextColor(220,20,60);
        $pdf->Cell(110,0,utf8_decode('Cotización Nº: '.$resultados[0]['IdCotizacion']),0,0,'L');
        $pdf->Ln(5);
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(156,0,utf8_decode('E-MAIL: '.$resultados[0]['EmailCliente']),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(115,0,utf8_decode('CIUDAD: '.$resultados[0]['NombreLugar']),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(60,0,utf8_decode('NIT: '.$resultados[0]['Nit']),0,0,'L');
        $pdf->Ln(5);
//$pdf->Cell(176,0,utf8_decode(''.date("d/m/Y")),0,0,'R');
        $pdf->Cell(60,0,utf8_decode('ASESOR TÉCNICO COMERCIAL: '.($resultados[0]['Nombres'].' '.$resultados[0]['Apellidos'])),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(60,0,utf8_decode('EMAIl: '.$resultados[0]['EmailPersona']),0,0,'L');
        $pdf->Ln(25);
        $pdf->Ln(15);
        $pdf->SetFont('Arial', '', 10 );
        $prod=0;
        $lin=0;
    }
    $totalsub=$totalsub+($datos['TotalDetalle']);
    $pdf->Cell(26, 8, $datos['IdProducto'], 0,0,'L');
    $palabra="";
    $palabra2="";
    if(strlen($datos['NombreProducto'])>45){
        $test=str_split($datos['NombreProducto']);
        for($i=0;$i<45;$i++){
            $palabra=$palabra.$test[$i];
        }
        $pdf->Cell(85, 8, utf8_decode($palabra.'-'), 0,0,'L');
        $pdf->Ln(5.5);
        $restante=strlen($datos['NombreProducto'])-45;
        $restante=$restante+45;
        for($i=45;$i<$restante;$i++) {
            $palabra2=$palabra2.$test[$i];
        }
        $pdf->Cell(26, 8, "", 0,0,'L');
        $pdf->Cell(85, 8, utf8_decode($palabra2), 0,0,'L');
        $totalProductos++;
        $lin=$lin+2;
    }else{
        $pdf->Cell(85, 8, utf8_decode($datos['NombreProducto']), 0,0,'L');
        $lin++;
    }
    $pdf->Cell(10, 8, $datos['CantidadProductos'], 0,0,'L');
    $pdf->Cell(20, 8, $datos['Abreviatura'], 0,0,'C');
    $pdf->Cell(26, 8, '$'.number_format($datos['TotalDetalle']/$datos['CantidadProductos']), 0,0,'R');
    $pdf->Cell(30, 8, '$'.number_format($datos['TotalDetalle']), 0,0,'R');
    $pdf->Ln(5.5);
    $totalProductos++;
    $prod++;
}

if($lin<14){
    $diferencia=14-$lin;
    for($i=1;$i<$diferencia-1;$i++){
        $pdf->Cell(20, 8, "", 0,0,'L');
        $pdf->Cell(107, 8, "", 0,0,'L');
        $pdf->Cell(10, 8,"", 0,0,'L');
        $pdf->Cell(18, 8, "", 0,0,'C');
        $pdf->Cell(22, 8, "", 0,0,'L');
        $pdf->Cell(60, 8, "", 0,0,'L');
        $pdf->Ln(5.5);
    }
    $pdf->Ln(20);
}else{
    $pdf->Ln(10);
}
$pdf->Cell(197,0,"$".number_format($resultados[0]['ValorTotalCotizacion']),0,0,'R');
$pdf->Output('detalles.pdf','I');
