<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php
include_once'../models/ProductosCotizados.php';
include_once'../facades/FacadeProducto.php';
include_once'../facades/ColoresFacade.php';
include_once'../facades/AromasFacade.php';
include_once'../facades/ConcentracionesFacade.php';
include_once'../facades/PresentacionesFacade.php';
include_once '../facades/FacadeCotizaciones.php';
$facadeCotizaciones=new FacadeCotizaciones();
$facadePresentaciones=new PresentacionesFacade();
$facadeProducto= new FacadeProducto();
$facadeColores= new ColoresFacade();
$facadeAromas= new AromasFacade();
$facadeConcentraciones=new ConcentracionesFacade();
$lista=$facadeCotizaciones->buscarCotizacion($_GET['coti']);


?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Registrar pedido</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Pedido</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Cotizaciones</a></li>
                <li class="active">Crear cotización</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-12">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <form id="defaultForm" action="../controllers/controladorOrdenCompra.php?finalizar=true" method="post">

                        <div class="box box-info box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Indicaciones de registro</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para registrar una nueva
                                        cotización.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->


                        <!--<div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Datos Cliente</h3>
                            </div>

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="cantidad">ID cliente:*</label>
                                    <input class="form-control" name="idcliente" id="idcliente" type="text" value="<?php echo $cliente?>" readonly>
                                </div>

                            </div>

                        </div>-->


                        <?php
                        include_once '../facades/ClienteFacade.php';
                        if(isset($_GET['idcliente'])) {
                            $clienteFac = new ClienteFacade();
                            $datacliente = $clienteFac->buscarCliente('clientes.Nit', $_GET['idcliente'], 1);
                        }
                        ?>


                        <div class="box box-solid box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Productos a facturar:</h3>
                            </div>
                            <div class="box-body">


                                <table id="example1" class="table table-bordered table-striped table-hover text-center">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Descripción</th>
                                        <th>Aroma</th>
                                        <th>Color</th>
                                        <th>Concentracion</th>
                                        <th>Presentación</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=0;
                                    $total=0;
                                    foreach ($lista as $test) {
                                        ?>
                                        <tr>
                                            <td><?php echo $test['IdProducto']?>
                                                <input type="text" name="idProducto<?php echo $i ?>" value="<?php echo $test['IdProducto']?>" hidden>
                                            </td>
                                            <td><?php echo $test['DescripcionProducto']?></td>
                                            <td>
                                                <?php $aroma=$facadeAromas->traerAroma($test['idAromaDetalles']);
                                                echo $aroma['aroma'];

                                                ?>
                                            </td>
                                            <td>
                                                <?php $color=$facadeColores->traerColor($test['idColorDetalles']);
                                                echo $color['color'];

                                                ?>

                                            </td>
                                            <td>
                                                <?php $conc=$facadeConcentraciones->traerConcentracion($test['idConcentracionDetalles']);
                                                echo $conc['concentracion'];

                                                ?>
                                            </td>
                                            <td>
                                                <?php $presentacion=$facadePresentaciones->traerPresentacion($test['idPresentacionDetalles']);
                                                $listado=$facadePresentaciones->buscarConCriterio("productos.IdProducto",$test['IdProducto'],1);
                                                ?>
                                                <select style="width: 100%;" class="form-control select2" name="nuevaPresentacion<?php echo $i ?>" id="nuevaPresentacion">
                                                    <option value="<?php echo $test['idPresentacionDetalles']?>" selected><?php echo $presentacion['Abreviatura']?> </option>
                                                    <?php
                                                    foreach ($listado as $item) {
                                                        if ($item['IdPresentacion']==$test['idPresentacionDetalles']){

                                                        }else{
                                                            ?>
                                                            <option value="<?php echo $item['IdPresentacion']?>" selected><?php echo $item['Abreviatura']?> </option>
                                                            <?php
                                                        }


                                                    }
                                                    ?>

                                                </select>
                                            </td>
                                        </tr>

                                        <?php
                                        $i++;
                                    }

                                    ?>
                                    <input type="text" name="cantidadPresentaciones" value="<?php echo $i?>" hidden>


                                    <?php
                                    if (isset($_GET['removerproducto'])){
                                        array_splice($_SESSION['productoDatos'],$_GET['posicion'],1);?>
                                        <meta http-equiv="Refresh" content="0;url=crearCotizacion2.php?idcliente=<?php echo $cliente?>">
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>

                                <hr>
                                <?php if (isset($_SESSION['productoDatos'])) {?>

                                    <h2 class="text-center text-light-blue">Total: <?php echo '$ '.number_format($total);?></h2>

                                <?php }
                                ?>
                            </div>
                            <div class="box-footer">
                                <button class="btn bg-green-gradient btn-block btn-flat" type="submit"
                                   value="Finalizar" name="finalizar" id="finish" >Generar pedido
                                </button>
                            </div>

                        </div>




                    </form>






                </div>

                <!-- /.box -->
            </div>



            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

    <script type="text/javascript">
        $(document).ready(function() {

            $("#idproducto").change(function () {
                $('#idpresentacion').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idpresentacion').val('0');
                    $('#idpresentacion').change();
                    $.post("presentacionesProducto.php", { idproducto: idproducto }, function(data){
                        $("#idpresentacion").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idaroma').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idaroma').val('0');
                    $('#idaroma').change();
                    $.post("aromasProducto.php", { idproducto: idproducto }, function(data){
                        $("#idaroma").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idcolor').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idcolor').val('0');
                    $('#idcolor').change();
                    $.post("coloresProducto.php", { idproducto: idproducto }, function(data){
                        $("#idcolor").html(data);
                    });
                });
            });

            $("#idproducto").change(function () {
                $('#idconcentracion').removeAttr("disabled");
                $("#idproducto option:selected").each(function () {
                    idproducto = $(this).val();
                    $('#idconcentracion').val('0');
                    $('#idconcentracion').change();
                    $.post("concentracionesProducto.php", { idproducto: idproducto }, function(data){
                        $("#idconcentracion").html(data);
                    });
                });
            });


            $('#datosCotizacion').hide();

            function randomNumber(min, max) {
                return Math.floor(Math.random() * (max - min + 1) + min);
            }

            function generateCaptcha() {
                $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
            }

            generateCaptcha();
            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    idproducto: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    idaroma:{
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }

                    },

                    idcolor:{
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }

                    },

                    meta: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    cantidad: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    }


                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>
</body>
</html>
