<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php


if ( ($_SESSION['hora']+20000)> time()){
    $_SESSION['hora']=time();
}else{
    $nombre_archivo = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
    if ( strpos($nombre_archivo, '/') !== FALSE )
        $nombre_archivo = array_pop(explode('/', $nombre_archivo));
    $_SESSION['pagina']=$nombre_archivo;
    header('location: ../../lock_screen.php?expire=true');
}
include_once'../models/daoGraficas.php';
$dao=new dao();
$cantidad=$dao->cantidadProductosVendidos();
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Productos mas vendidos</title>
    <link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->


    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <?php include_once 'menu.php'; ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Panel
                <small>Productos mas vendidos</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li>Panel</li>
                <li class="active">Productos mas vendidos</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-12">

                    <script type="text/javascript">
                        $(function () {
                            $('#container').highcharts({
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: 'Top 10 productos vendidos'
                                },
                                xAxis: {
                                    categories: [
                                        <?php
                                        foreach ($cantidad as $resultado) {?>

                                        '<?php echo $resultado['NombreProducto']?>',

                                        <?php
                                        }
                                        ?>
                                    ]
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Total ventas productos'
                                    }
                                },
                                legend: {
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal'
                                    }
                                },
                                series: [{
                                    name: 'Cantidad vendida',
                                    data: [
                                        <?php
                                        foreach ($cantidad as $totales) {?>

                                        <?php echo $totales['CantidadVendidos']?>,

                                        <?php
                                        }
                                        ?>


                                    ]
                                }, ]
                            });
                        });
                    </script>



                    <!-- general form elements disabled -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Productos mas vendidos</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                En la siguiente gráfica encontrará el total de productos mas vendidos, con sus respectivas descripciones de cada una<br><br>
                            </p>

                        </div>
                    </div>


                    <div class="box">

                        <div class="box-body">

                            <div onclick="mostrar()" id="container" style="min-width: 310px; height: 600px; margin: 0 auto">

                            </div>

                        </div>

                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Elementos para la búsqueda</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-toggle="tooltip" title="Reducir/Ampliar"
                                            data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>

                            <div hidden id="jaja" style="margin-top:200px;min-width: 310px; height: 400px; margin: 0 auto">

                                <table id="example1"
                                       class="table table-striped hover order-column row-border compact display">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Cantidad Vendida</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($cantidad as $item) {?>
                                        <tr>
                                            <td><?php echo $item['IdProducto']?></td>
                                            <td><?php echo $item['NombreProducto']?></td>
                                            <td><?php echo $item['CantidadVendidos']?></td>

                                        </tr>
                                    <?php }
                                    ?>

                                    </tbody>

                                </table>



                            </div>





                        </div>
                    </div>












                </div><!--/.col (right) -->
            </div>   <!-- /.row -->




        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../plugins/highcharts/js/highcharts.js"></script>
<script src="../../plugins/highcharts/js/modules/exporting.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script>
    function mostrar(){
        $('#jaja').show();


    }



</script>
</html>
