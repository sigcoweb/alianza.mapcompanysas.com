<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->


<html>
<head>
    <meta charset="UTF-8">
    <title>Registrar meta</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->



    <script>
        $(function() {

            $( "#ffinal" ).datepicker( "option", "dateFormat", "YYYY-MM-DD" );

        });
    </script>


    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Metas</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Meta</a></li>
                <li class="active">Registrar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">
                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <form id="defaultForm" action="../controllers/ControladorMeta.php" method="post">

                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Nueva Meta</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para registrar una nueva
                                        meta.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->


                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registrar Meta</h3>
                            </div>

                            <div class="box-body">

                                <div class="form-group">
                                    <label for="tipo">Tipo Meta*</label>
                                    <select class="form-control" name="tipo" id="tipo" required>
                                        <option value="">Seleccionar</option>
                                        <option value="Ventas">Ventas</option>
                                        <option value="Visitas">Visitas</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="valor">Valor a cumplir*</label>
                                    <input class="form-control" name="valor" id="valor" type="text" placeholder="5000000" required>
                                </div>
                                <div class="form-group">
                                    <label for="finicio">Fecha Inicio*</label>
                                    <div>
                                        <div class="input-group input-append date" >
                                            <input class="form-control" name="finicio" id="startDatePicker" type="text" placeholder="2015-01-01" required />
                                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ffinal">Fecha Final*</label>
                                    <div class="input-group input-append date" >
                                        <input class="form-control" name="ffinal" id="endDatePicker" type="text" maxlength="20" placeholder="2015-01-01">
                                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>

                                </div>
                                <div class="form-group">


                                    <input class="" type="checkbox" name="asignar" value="1">
                                    <label for="asignar">  ¿Asignar esta meta a empleados?</label>

                                </div>

                                <div class="box-footer">
                                    <input type="button" class="btn btn-warning" tabindex="15"
                                           onclick="location.href='index.php'" value="Cancelar"/>
                                    <button type="submit" class="btn btn-success pull-right" tabindex="14"
                                            value="guardar" name=" guardar" id="guardar">Registrar
                                    </button>
                                </div>


                            </div>

                        </div>
                    </form>

                </div>

                <!-- /.box -->


                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div><!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function() {



        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                tipo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                valor: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        },
                        integer: {

                            message: 'El valor no es correcto'
                        },
                        stringLength: {
                            min: 1,
                            max: 10,
                            message: 'Este campo debe tener mínimo 1 carácteres y máximo 10.'
                        },
                        between: {
                            min: 1,
                            max: 10000000000,
                            message: 'Debe ser una cantidad válida'
                        }


                    }
                },
                finicio:{
                    validators:{
                        noEmpty:{
                            message: 'Este campo es requerido'
                        }
                    }
                },
                ffinal:{
                    validators:{
                        noEmpty:{
                            message: 'Este campo es requerido'
                        },
                        date: {
                            format: 'YYYY-MM-DD',
                            min: 'finicio',
                            message: 'La fecha final debe ser mayor o igual la inicial'
                        }
                    }
                }

            }


        });



        /*.on('success.field.fv', function(e, data) {
            if (data.field === 'finicio' && !data.fv.isValidField('ffinal')) {
                // We need to revalidate the end date
                data.fv.revalidateField('ffinal');
            }

            if (data.field === 'ffinal' && !data.fv.isValidField('finicio')) {
                // We need to revalidate the start date
                data.fv.revalidateField('finicio');
            }

        });*/


        $( "#startDatePicker" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0
        }).on('change', function(e) {
            // Revalidate the start date field
            $('#defaultForm').formValidation('revalidateField', 'ffinal');
        });
        $( "#endDatePicker" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0
        }).on('change', function(e) {
            $('#defaultForm').formValidation('revalidateField', 'ffinal');
        });
    });

</script>

</html>
