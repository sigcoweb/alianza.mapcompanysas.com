<?php
include_once '../facades/ClienteFacade.php';
include_once'../facades/PuntosEntregaFacade.php';
include_once'../facades/FacadeCotizaciones.php';
include_once'../facades/AreasClienteFacade.php';
$puntosFaca=new PuntosEntregaFacade();
$areasFaca=new AreasClienteFacade();
$cotizFaca=new FacadeCotizaciones();
$clienteFaca = new ClienteFacade();
session_start();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset='utf-8'>
    <title>Activar clientes</title>


    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons --
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />-->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Ionicons --
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->


    <!--<link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    -->

    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.css"/>


    <!-- sweet alert lucho-->
    <link href="../../plugins/animate/animate.css" rel="stylesheet" type="text/css"/>
    <script src="../../plugins/messajes/jquery.noty.packaged.min.js"></script>


    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <!--<script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>




            <?php
            include_once 'menu.php';
            ?>



        </section>
        <!-- /.sidebar -->
    </aside>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clientes
                <small>Inactivos</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li>Clientes</li>
                <li class="active">Buscar</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="verDetalle">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content" style="border-radius: 5px">
                                <div class="modal-header" style="background: #00c0ef; color: #fff; border-radius: 5px 5px 0 0; text-align: center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                                    <h3 class="modal-title" id="nombreCliente">Detalle del cliente</h3>
                                </div>


                                <div class="modal-body">
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <div class="col-sm-5 invoice-col">
                                            <dl class="dl-horizontal">
                                                <dt>Razón social</dt>
                                                <dd id="razonsocial1"></dd>
                                                <dt>Nombre comercial</dt>
                                                <dd id="nombrecomercial1"></dd>
                                                <dt>Nit</dt>
                                                <dd id="nit1"></dd>
                                                <dt>Dirección</dt>
                                                <dd id="direccion1"></dd>
                                                <dt>Ubicación</dt>
                                                <dd id="departamento1"></dd>
                                                <dd id="lugar1"></dd>
                                                <dt>Teléfono corporativo</dt>
                                                <dd id="telefono1"></dd>
                                                <dt>Email corporativo</dt>
                                                <dd id="email1"></dd>
                                                <dt>Tipo de empresa</dt>
                                                <dd id="tipo1"></dd>
                                                <dt>Actividad económica</dt>
                                                <dd id="actividad1"></dd>
                                                <dt>Clasificación del cliente</dt>
                                                <dd id="clasificacion1"></dd>
                                                <dt>Estado del cliente</dt>
                                                <dd id="estado0"></dd>
                                                <dt>Día cierre</dt>
                                                <dd id="diaCierre"></dd>
                                                <dt>Plazo pago</dt>
                                                <dd id="plazoPago"></dd>
                                            </dl>
                                        </div><!-- /.col -->
                                        <div class="col-sm-6 invoice-col">
                                            <dl class="dl-horizontal">
                                                <dt>Nombres</dt>
                                                <dd id="nombres1"></dd>
                                                <dt>Apellidos</dt>
                                                <dd id="apellidos1"></dd>
                                                <dt>Cédula</dt>
                                                <dd id="cedula1"></dd>
                                                <dt>Email</dt>
                                                <dd id="email2"></dd>
                                                <dt>Celular</dt>
                                                <dd id="celular1"></dd>
                                                <dt>Inscrito desde</dt>
                                                <dd id="fechaCliente1"></dd>
                                                <dt>Estado del usuario</dt>
                                                <dd id="estado1"></dd>
                                            </dl>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    <?php
                    $consulta = $clienteFaca->buscarCliente('clientes.estadoCliente', 'Inactivo', 1);
                    if(isset($consulta)&&count($consulta)<=0||isset($_GET['mensaje'])) { ?>
                        <div class="alert alert-<?php if(isset($_GET['error'])){echo 'warning';}else{echo 'success';}; ?> alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-<?php if(isset($_GET['error'])){echo 'exclamation-triangle';}else{echo 'check';}; ?>"></i> <?php if(isset($_GET['mensaje'])){echo 'Resultado del proceso:';}else{echo 'Sin activaciones pendientes';} ?></h4>
                            <?php if(isset($_GET['mensaje'])){echo $_GET['mensaje'];}else{ ?>
                                <p>No se han encontrado clientes pendientes por activar.</p> <?php } ?>
                        </div>
                    <?php }
                    if(isset($consulta)) {
                        if (count($consulta)>0) { ?>
                            <form action="../controllers/ClientesController.php?controlar=activarVarios" name="f1" method="post">
                                <div class="box box-primary box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Clientes pendientes por activar</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <p>
                                            Se han encontrado <span class="badge label-info"><?php echo count($consulta); ?></span>
                                            registros para esta consulta.
                                        </p>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#Empresas" aria-controls="Empresas" role="tab" data-toggle="tab">Empresas</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#Representantes" aria-controls="Representantes" role="tab" data-toggle="tab">Representantes</a>
                                            </li>
                                        </ul>
                                        <br>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="Empresas">
                                                <table id="example1" class="table table-striped hover order-column row-border compact display">
                                                    <thead>
                                                    <tr>
                                                        <th>Activar</th>
                                                        <th>Fecha</th>
                                                        <th>#Nit</th>
                                                        <th>Razón social</th>
                                                        <th>Ubicación</th>
                                                        <th>Teléfono</th>
                                                        <th>Estado</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $clave=0;
                                                    foreach ($consulta as $respuesta){
                                                    ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <input type="checkbox" class="sel" id="activ" value="<?php echo $respuesta['Nit']; ?>" name="activame[]">
                                                        </td>
                                                        <td>
                                                            <?php echo date('Y-m-d', strtotime($respuesta['FechaCreacionPersona']))?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['Nit']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['RazonSocial']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['NombreLugar']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['Telefono']; ?>
                                                        </td>
                                                        <td>
                                                            <label
                                                                class="label <?php if ($respuesta['estadoCliente'] == 'Activo') {
                                                                    echo 'bg-green-gradient';
                                                                } else {
                                                                    echo 'bg-yellow-gradient';
                                                                }; ?>"
                                                                data-toggle="tooltip" title="Estado del cliente">
                                                                <?php echo($respuesta['estadoCliente']); ?>
                                                            </label>
                                                        </td>

                                                        <td>
                                                            <button type="button" class="btn btn-xs btn-default click" value="<?php echo $respuesta['IdCliente']; ?>">Ver más</button>
                                                        </td>
                                                        <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Activar</th>
                                                        <th>Fecha</th>
                                                        <th>#Nit</th>
                                                        <th>Razón social</th>
                                                        <th>Ubicación</th>
                                                        <th>Teléfono</th>
                                                        <th>Estado</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div> <!-- panel activo-->
                                            <div role="tabpanel" class="tab-pane fade" id="Representantes">
                                                <table id="example2" class="table table-striped hover order-column row-border compact">
                                                    <thead>
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>#Cédula</th>
                                                        <th>Nombres</th>
                                                        <th>Apellidos</th>
                                                        <th>#Celular</th>
                                                        <th>Estado</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach ($consulta as $respuesta){
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo date('Y-m-d', strtotime($respuesta['FechaCreacionPersona']))?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['CedulaPersona']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['Nombres']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['Apellidos']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $respuesta['CelularPersona']; ?>
                                                        </td>
                                                        <td>
                                                            <label
                                                                class="label <?php if ($respuesta['EstadoPersona'] == 'Activo') {
                                                                    echo 'bg-green-gradient';
                                                                } else {
                                                                    echo 'bg-yellow-gradient';
                                                                }; ?>"
                                                                data-toggle="tooltip" title="Estado del cliente">
                                                                <?php echo($respuesta['EstadoPersona']); ?>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-xs btn-default click" value="<?php echo $respuesta['IdCliente']; ?>">Ver más</button>
                                                        </td>
                                                        <?php
                                                        $clave++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>#Cédula</th>
                                                        <th>Nombres</th>
                                                        <th>Apellidos</th>
                                                        <th>#Celular</th>
                                                        <th>Estado</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div> <!-- panel inactivo-->
                                        </div> <!-- tab content-->
                                    </div>
                                    <div class="box-footer">
                                        <a class="btn bg-yellow-gradient" href="javascript:seleccionar_todo()"><i class="fa fa-check-square-o"></i> Todos</a>
                                        <a class="btn bg-purple-gradient" href="javascript:deseleccionar_todo()"><i class="fa fa-square-o"></i> Ninguno</a>
                                        <button id="activarSeleccion" type="submit" class="btn bg-green-gradient pull-right disabled" tabindex="14" data-toggle="tooltip" title="Clic para activar"
                                        > <i class="fa fa-check"> </i>   Activar selección
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <?php
                        }
                    }
                    if (!isset($_GET['todos'])){ ?>
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver todos los registros</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-toggle="tooltip" title="Reducir/Ampliar"  data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <p>
                                    Si desea, puede ver todos los registros usando el botón "Ver todos"
                                    (esta opción puede tardar un poco).
                                </p>
                            </div>
                            <div class="box-footer">
                                <form action="buscarClientes.php?todos=todos" method="post">
                                    <button type="submit" class="btn bg-green-gradient pull-right" tabindex="14" data-toggle="tooltip" title="Clic para consultar"
                                    > <i class="fa fa-plus-square-o"> </i>   Ver todos
                                    </button>
                                </form>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div><!--/.col (right) -->
            </div>   <!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/s/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.js"></script>
<!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Select2 -->
<script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
<!-- SlimScroll 1.3.0 -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- iCheck 1.0.1 -->
<script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../dist/js/demo.js" type="text/javascript"></script>

<script type="text/javascript">
    $('.sel').click(function () {
        sele=0;
        for (i=0;i<document.f1.elements.length;i++){
            if(document.f1.elements[i].checked==1){
                sele=sele+1;
            }
        }
        menso='<i class="fa fa-check"></i>  Activar '+sele+' cliente(s)';
        if(sele>0){
            $("#activarSeleccion").removeClass("disabled");
            $("#activarSeleccion").html(menso);
        }else {
            $("#activarSeleccion").html('Seleccione clientes');
            $("#activarSeleccion").addClass("disabled");
        }
    });
    function seleccionar_todo(){
        $("#activarSeleccion").html('<i class="fa fa-check"></i>  Activar todos');
        $("#activarSeleccion").removeClass("disabled");
        for (i=0;i<document.f1.elements.length;i++)
            if(document.f1.elements[i].type == "checkbox")
                document.f1.elements[i].checked=1
    }
    function deseleccionar_todo(){
        $("#activarSeleccion").html('Seleccione clientes');
        $("#activarSeleccion").addClass("disabled");
        for (i=0;i<document.f1.elements.length;i++)
            if(document.f1.elements[i].type == "checkbox")
                document.f1.elements[i].checked=0
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.click').click(function () {
            $.post("../controllers/ClientesController.php",
                {
                    idDetalleCliente: $(this).attr("value")
                },
                function (data) {
                    var json = $.parseJSON(data);
                    $('#nombreCliente').text(json.RazonSocial);
                    $('#nit1').text(json.Nit);
                    $('#razonsocial1').text(json.RazonSocial);
                    $('#departamento1').text(json.nombreDepartamento);
                    $('#nombrecomercial1').text(json.nombreComercial);
                    $('#direccion1').text(json.Direccion);
                    $('#telefono1').text(json.Telefono);
                    $('#email1').text(json.EmailCliente);
                    $('#actividad1').text(json.NombreActividad);
                    $('#clasificacion1').text(json.NombreClasificacion);
                    $('#lugar1').text(json.NombreLugar);
                    $('#estado0').text(json.estadoCliente);
                    $('#cedula1').text(json.CedulaPersona);
                    $('#nombres1').text(json.Nombres);
                    $('#apellidos1').text(json.Apellidos);
                    $('#email2').text(json.EmailPersona);
                    $('#celular1').text(json.CelularPersona);
                    $('#tipo1').text(json.NombreTipo);
                    $('#estado1').text(json.EstadoPersona);
                    $('#fechaCliente1').text(json.FechaCreacionPersona);
                    $('#diaCierre').text(json.diaCierreFacturacion);
                    $('#plazoPago').text(json.plazoPago);
                    $('#verDetalle').modal('show');
                    //alert(data);
                });

        });


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
        });




        var table = $('#example1').DataTable({
            responsive: true,
            stateSave: true,


            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },


            'sPageEllipsis': 'paginate_ellipsis',
            'sPageNumber': 'paginate_number',
            'sPageNumbers': 'paginate_numbers',

            "processing": true,
            "deferLoading": 57,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [
                [10, 25, 50, -1],
                ["10 registros", "25 registros", "50 registros", "Todos"]
            ],
            /*
             "aoColumns": [
             { "orderSequence": [ "desc", "asc", "asc" ] },
             null,
             null,
             null,
             null,
             null,
             null,
             null
             ],
             */
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7
                }
            ],
            "order": [[ 0, 'desc' ]],

            dom:
                'Bfrtip',

            buttons: [
                {
                    extend: 'pageLength',
                    text: '<i class="fa fa-filter"> </i>  Mostrar',
                    titleAttr: 'Cantidad de registros'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i>',
                    titleAttr: 'Mostrar/ocultar columnas'
                }
            ]





        });


        table.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
        });

        table.on( 'responsive-resize', function ( e, datatable, columns ) {
            var count = columns.reduce( function (a,b) {
                return b === false ? a+1 : a;
            }, 0 );

            console.log( count +' column(s) are hidden' );
        });


        /*

         // Setup - add a text input to each footer cell
         $('#example1 tfoot th').each( function () {
         var title = $(this).text();
         $(this).html( '<input type="text" placeholder="Buscar" />' );
         });

         // DataTable
         var table = $('#example1').DataTable();

         // Apply the search
         table.columns().every( function () {
         var that = this;

         $( 'input', this.footer() ).on( 'keyup change', function () {
         if ( that.search() !== this.value ) {
         that
         .search( this.value )
         .draw();
         }
         } );
         });

         */






        var table2 = $('#example2').DataTable({
            responsive: true,
            stateSave: true,


            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },


            'sPageEllipsis': 'paginate_ellipsis',
            'sPageNumber': 'paginate_number',
            'sPageNumbers': 'paginate_numbers',

            "processing": true,
            "deferLoading": 57,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [
                [10, 25, 50, -1],
                ["10 registros", "25 registros", "50 registros", "Todos"]
            ],
            /*
             "aoColumns": [
             { "orderSequence": [ "desc", "asc", "asc" ] },
             null,
             null,
             null,
             null,
             null,
             null,
             null
             ],
             */
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6
                }
            ],
            "order": [[ 0, 'desc' ]],

            dom:
                'Bfrtip',

            buttons: [
                {
                    extend: 'pageLength',
                    text: '<i class="fa fa-filter"> </i>  Mostrar',
                    titleAttr: 'Cantidad de registros'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i>',
                    titleAttr: 'Mostrar/ocultar columnas'
                }
            ]





        });


        table2.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
        });

        table2.on( 'responsive-resize', function ( e, datatable, columns ) {
            var count = columns.reduce( function (a,b) {
                return b === false ? a+1 : a;
            }, 0 );

            console.log( count +' column(s) are hidden' );
        });









        $('[data-toggle="popover"]').popover();






        $(".reestablecer").click(function () {
            var btnId=$(this).attr("value");
            var n = noty({
                text: '¿Desea reestablecer la contraseña para éste cliente?',
                theme: 'relax',
                layout: 'center',
                closeWith: ['click', 'hover'],
                buttons: [
                    {
                        addClass: 'btn btn-primary', text: 'Reestablecer', onClick: function ($noty) {
                        $.post("../controllers/ClientesController.php",
                            {
                                reestablecerContrasenia: btnId
                            },
                            function (data) {
                                //location.reload();
                                noty({text: data, type: 'success'});
                                $noty.close();
                            });
                    }
                    },
                    {
                        addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ],
                type: 'confirm',
                animation: {
                    open: 'animated wobble', // Animate.css class names
                    close: 'animated flipOutX' // Animate.css class names
                }
            });
        });

        $(".cambiarEstado").click(function () {
            var cambEst=$(this).attr("value");
            var n = noty({
                text: '¿Desea cambiar el estado de éste cliente?',
                theme: 'relax',
                layout: 'center',
                closeWith: ['click', 'hover'],
                buttons: [
                    {
                        addClass: 'btn btn-primary', text: 'Cambiar estado', onClick: function ($noty) {
                        $.post("../controllers/ClientesController.php",
                            {
                                cambiarEstado: cambEst
                            },
                            function (data) {
                                noty({text: data, type: 'success'});
                                $noty.close();
                                location.href='buscarClientes.php?'+data;
                            });
                    }
                    },
                    {
                        addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ],
                type: 'confirm',
                animation: {
                    open: 'animated wobble', // Animate.css class names
                    close: 'animated flipOutX' // Animate.css class names
                }
            });
        });


        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                busqueda: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es necesario'
                        }
                    }
                }
            }
        });












        function confirmar() {
            if (confirm('¿Está seguro que desea cambiar el estado de este cliente?')) {
                return true;
            } else {
                return false;
            }
        }

        function contrasenia() {
            if (confirm('¿Está seguro que desea restaurar la contraseña de este cliente?')) {
                return true;
            } else {
                return false;
            }
        }


    });//document ready
</script>



</body>
</html>
