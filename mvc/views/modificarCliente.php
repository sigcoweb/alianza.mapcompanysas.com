<?php
include_once '../facades/ClienteFacade.php';
include_once '../facades/DepartamentosFacade.php';
include_once'../facades/LugaresFacade.php';
include_once '../models/TiposClienteDao.php';
include_once '../models/ActividadesClienteDao.php';
include_once '../models/ClasificacionesClienteDao.php';
include_once '../facades/FacadeEmpleado.php';
$empleadoFac = new FacadeEmpleado();
$clasificacionCliente = new ClasificacionesClienteDao();
$actividadesCliente = new ActividadesClienteDao();
$tiposCliente = new TiposClienteDao();
$lugaresFaca=new LugaresFacade();
$deptosFac = new DepartamentosFacade();
session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> Modificar cliente</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/> -->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons --
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>

    <!--<link rel="stylesheet" href="../../plugins/ionic/css/ionic.min.css" type="text/css">-->

    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>

    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->


    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <?php
    if (isset($_GET['IdCliente'])) {
    $clienteFac = new ClienteFacade();
    $cliente = $clienteFac->obtenerCliente($_GET['IdCliente']);
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de modificación
                <small>Clientes</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Clientes</a></li>
                <li class="active">Modificar cliente</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if(isset($cliente['Nit'])){
                    ?>

                    <form
                        action="../controllers/ClientesController.php?controlar=modificar&IdCliente=<?php echo $_GET['IdCliente'] . '&IdPersona=' . $_GET['IdPersona']; ?>"
                        method="post" id="formValidacion">

                        <!-- general form elements disabled -->
                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ver indicaciones de registro</h3>

                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para modificar la información del
                                        cliente.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <?php
                        if ($cliente['estadoCliente']!='Activo') {
                            ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-warning"></i> Advertencia</h4>
                                Este cliente se encuentra inactivo. El estado sólo puede ser modificado por un
                                coordinador.
                            </div>
                            <?php
                        }
                        ?>

                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Información corporativa registrada</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label id="labelNit" for="Nit">NIT*</label>
                                            <input type="text" name="Nit" id="Nit" class="form-control"
                                                   value="<?php echo $cliente['Nit']; ?>"
                                                <?php
                                                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){echo' autofocus ';
                                                }else
                                                {echo' readonly ';}?>
                                                   required tabindex="1"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="RazonSocial">Razón social*</label>
                                            <input type="text" name="RazonSocial" id="RazonSocial" class="form-control"
                                                   value="<?php echo $cliente['RazonSocial']; ?>" required tabindex="2"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="nombreComercial">Nombre comercial*</label>
                                            <input type="text" name="nombreComercial" id="nombreComercial" class="form-control"
                                                   value="<?php echo $cliente['nombreComercial']; ?>" required tabindex="2"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Direccion">Dirección*</label>
                                            <input type="text" name="Direccion" id="Direccion" class="form-control"
                                                   value="<?php echo $cliente['Direccion']; ?>" required tabindex="3"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Telefono">Teléfono principal (fijo o celular)*</label>
                                            <input type="text" name="Telefono" id="Telefono" class="form-control"
                                                   value="<?php echo $cliente['Telefono']; ?>" required tabindex="4"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Email2">Email corporativo*</label>
                                            <input type="email" name="Email2" id="Email2" class="form-control"
                                                   value="<?php echo $cliente['EmailCliente']; ?>" required tabindex="5"/>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdDepartamento">Departamento*</label>
                                            <select class="form-control select2 depto" name="IdDepartamento" id="IdDepartamento" required tabindex="5">
                                                <option value="0" disabled selected>Seleccione...</option>
                                                <?php
                                                $todos = $deptosFac->listarTodos();
                                                foreach ($todos as $depto) {
                                                    ?>
                                                    <option <?php if($depto['idDepartamento']==$cliente['idDepartamentoLugar']){echo ' selected ';}; ?>
                                                        value="<?php echo $depto['idDepartamento'] ?>">
                                                        <?php echo $depto['nombreDepartamento'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdLugar">Lugar*</label>
                                            <select class="form-control select2"  name="IdLugar" id="IdLugar" required tabindex="6">
                                                <option value="0" disabled selected>Seleccione primero un departamento...</option>
                                                <?php
                                                $lugares=$lugaresFaca->listarTodos();
                                                foreach($lugares as $lugar) {
                                                    ?>
                                                    <option <?php if ($lugar['IdLugar'] == $cliente['IdLugar']) {
                                                        echo 'selected';
                                                    }; ?>
                                                        value="<?php echo $lugar['IdLugar'] ?>">
                                                        <?php echo $lugar['NombreLugar'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="diaCierreFacturacion">Cierre facturación (día del mes)*</label>
                                            <select class="form-control select2" name="diaCierreFacturacion" id="diaCierreFacturacion" required tabindex="7">
                                                <option selected="selected" disabled>Seleccione...</option>
                                                <?php
                                                for ($cierre=1;$cierre<31;$cierre++) {
                                                    ?>
                                                    <option value="<?php echo $cierre; ?>" <?php if($cliente['diaCierreFacturacion']==$cierre){echo 'selected';} ?>><?php echo $cierre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="plazoPago">Plazo de pago (en días)*</label>
                                            <select class="form-control select2" name="plazoPago" id="plazoPago" required tabindex="7">
                                                <option selected="selected" disabled>Seleccione...</option>
                                                <?php
                                                for ($plazo=1;$plazo<131;$plazo++) {
                                                    ?>
                                                    <option value="<?php echo $plazo; ?>" <?php if($cliente['plazoPago']==$plazo){echo 'selected';} ?>><?php echo $plazo; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdTipo">Tipo de cliente*</label>
                                            <select class="form-control select2" name="IdTipo" id="IdTipo" required
                                                    tabindex="7">
                                                <?php
                                                $todosTiposCliente = $tiposCliente->listarTodos();
                                                foreach ($todosTiposCliente as $tipo) {
                                                    ?>
                                                    <option <?php if ($cliente['IdTipoCliente'] == $tipo['IdTipo']) {
                                                        echo ' selected ';
                                                    }; ?>
                                                        value="<?php echo $tipo['IdTipo'] ?>"><?php echo $tipo['NombreTipo'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label for="IdActividad">Actividad del cliente*</label>
                                            <select class="form-control select2" name="IdActividad" id="IdActividad"
                                                    required tabindex="8">
                                                <?php
                                                $todasActividadesCliente = $actividadesCliente->listarTodas();
                                                foreach ($todasActividadesCliente as $actividad) {
                                                    ?>
                                                    <option <?php if ($cliente['IdActividadCliente'] == $actividad['IdActividad']) {
                                                        echo ' selected ';
                                                    }; ?>
                                                        value="<?php echo $actividad['IdActividad'] ?>"><?php echo $actividad['NombreActividad'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador') {
                                    ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="IdClasificacion">Clasificación del cliente*(Sólo
                                                    coordinador)</label>
                                                <select class="form-control select2" name="IdClasificacion"
                                                        id="IdClasificacion"
                                                        required tabindex="9">
                                                    <?php
                                                    $todasClasificacionesCliente = $clasificacionCliente->listarTodas();
                                                    foreach ($todasClasificacionesCliente as $clasificacion) {
                                                        ?>
                                                        <option <?php if ($cliente['IdClasificacionCliente'] == $clasificacion['IdClasificacion']) {
                                                            echo ' selected ';
                                                        }; ?>
                                                            value="<?php echo $clasificacion['IdClasificacion'] ?>"><?php echo $clasificacion['NombreClasificacion'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <!-- /.form-group -->
                            </div>
                        </div>


                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Información personal registrada</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label id="labelCedula"  for="Cedula">Cédula*</label>
                                            <a id="botonAgregar" class="hidden"><span class="label label-info"><i
                                                        class="fa fa-plus"></i>   asociar otra empresa a éste mismo número</span></a>
                                            <input type="text" name="Cedula" id="Cedula" class="form-control"
                                                   value="<?php echo $cliente['CedulaPersona']; ?>"
                                                <?php
                                                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){echo' required ';
                                                }else
                                                {echo' readonly ';}?>
                                                   tabindex="10"
                                            />
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Nombres">Nombres*</label>
                                            <input type="text" name="Nombres" id="Nombres" class="form-control"
                                                   value="<?php echo $cliente['Nombres']; ?>"
                                                   required tabindex="11"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Apellidos">Apellidos*</label>
                                            <input type="text" name="Apellidos" id="Apellidos" class="form-control"
                                                   value="<?php echo $cliente['Apellidos']; ?>" required tabindex="12"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Celular">Número fijo o celular*</label>
                                            <input type="text" name="Celular" id="Celular" class="form-control"
                                                   value="<?php echo $cliente['CelularPersona']; ?>" required tabindex="13"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="Email1">Email*</label>
                                            <input type="email" name="Email1" id="Email1" class="form-control"
                                                   value="<?php echo $cliente['EmailPersona']; ?>" required tabindex="14"/>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="reestablecerContrasenia">Reestablecer la contraseña del usuario</label><br>
                                            <input class="flat-red" type="checkbox" name="reestablecerContrasenia"
                                                   id="reestablecerContrasenia">
                                            <label for="reestablecerContrasenia">Reestablecer a su estado inicial
                                                (igual al número del documento/cédula)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){
                                ?>
                            </div>
                        </div>
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Asignación comercial</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="activarCliente">Estado del cliente*</label>
                                            <select class="form-control select2" name="activarCliente" id="activarCliente"
                                                    required tabindex="16">
                                                <option <?php if ($cliente['estadoCliente'] == 'Activo') {
                                                    echo ' selected ';
                                                }; ?>
                                                    value="Activo">Activo
                                                </option>
                                                <option <?php if ($cliente['estadoCliente'] == 'Inactivo') {
                                                    echo ' selected ';
                                                }; ?>
                                                    value="Inactivo">Inactivo
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                }
                                ?>
                                <div class="form-group" hidden>
                                    <label>
                                        <input type="hidden" name="Contrasenia"
                                               value="<?php echo $cliente['Contrasenia'] ?>">
                                    </label>
                                </div>
                                <div class="box-footer">
                                    <input type="button" class="btn bg-yellow-gradient pull-right" tabindex="18"
                                           onclick="location.href='buscarClientes.php'" value="Cancelar"/>
                                    <button type="submit" class="btn bg-green-gradient pull-left" tabindex="17"
                                            value="modificar" name="modificar" id="modificar">Modificar cliente
                                    </button>
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>
                    <!-- /.box -->

                </div>

                <?php
                }else {
                    ?>
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">El cliente seleccionado no existe o no está asociado a su cuenta</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <p>Debe seleccionar nuevamente un cliente
                                para modificar su información.</p>
                        </div>
                        <div class="box-footer">
                            <input type="button" class="btn bg-green-gradient pull-right" tabindex="16"
                                   onclick="location.href='buscarClientes.php'" value="Buscar clientes"/>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <?php
                }
                ?>

                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
    <script type="text/javascript">
        $(document).ready(function () {

                    <?php if($_SESSION['datosLogin']['NombreRol']!='Administrador'&&$_SESSION['datosLogin']['NombreRol']!='Coordinador'){ ?>
                    $("#modificar").addClass("disabled").addClass("hidden");
            $(".form-control").attr('disabled',true);
            <?php } ?>

            $("#IdDepartamento").change(function () {
                $('#IdLugar').removeAttr("disabled");
                $("#IdDepartamento option:selected").each(function () {
                    idDepto = $(this).val();
                    //$("#IdLugar").selectedIndex=0;
                    //document.getElementById("#IdLugar").selectedIndex=0;
                    $('#IdLugar').val('0');
                    $('#IdLugar').change();
                    $.post("lugaresDepto.php", { idDepto: idDepto }, function(data){
                        $("#IdLugar").html(data);
                    });
                });
            });

            $("#RazonSocial").change(function () {
                raz=$('#RazonSocial').val();
                $("#NombreComercial").val(raz);
            });

            $('#Nit').on('keyup', function () {
                $.post("../controllers/ClientesController.php",
                    {
                        existeNit: $('#Nit').val()
                    },
                    function (data) {
                        antiguoNit="<?php echo $cliente['Nit'] ?>";
                        var json = $.parseJSON(data);
                        if(json.existente==1&&antiguoNit!=$('#Nit').val()){
                            $("#Nit").removeClass("has-success").addClass("has-error").addClass("alert-danger").removeClass("alert-success");
                            $(".form-control-feedback").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                            $("#modificar").addClass("disabled").addClass("hidden");
                            $("#labelNit").text("Este nit ya existe. Por favor indique otro número*");
                        }else if(json.existente==0){
                            $("#Nit").removeClass("has-error").removeClass("alert-danger").removeClass("alert-success");
                            //$(".form-control-feedback").removeClass("glyphicon-remove");
                            $("#modificar").removeClass("disabled").removeClass("hidden");
                            $("#labelNit").text("Nit*");
                        }else if($('#Nit').val()==antiguoNit){
                            $('#Nit').addClass("alert-success").removeClass("alert-danger");
                            $("#labelNit").text("Nit*");
                            $("#modificar").removeClass("disabled").removeClass("hidden");
                        }
                    });
            });

            $('#Cedula').on('keyup', function () {
                $.post("../controllers/ClientesController.php",
                    {
                        existeCedula: $('#Cedula').val()
                    },
                    function (data) {
                        antiguaCedula="<?php echo $cliente['CedulaPersona'] ?>";
                        var json = $.parseJSON(data);
                        cedula = $('#Cedula').val();
                        dir = 'nuevoClienteMismaPersona.php?cedulaPersona='+cedula;
                        if(json.existente==1&&antiguaCedula!=$('#Cedula').val()){
                            $("#Cedula").removeClass("has-success").addClass("has-error").addClass("alert-danger").removeClass("alert-success");
                            $(".form-control-feedback").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                            $("#modificar").addClass("disabled").addClass("hidden");
                            $("#labelCedula").text("Esta cédula ya existe*. Por favor indique otro número o si desea puede  ");
                            $("#botonAgregar").removeClass("hidden").attr('href',dir);
                        }else if(json.existente==0){
                            $("#Cedula").removeClass("has-error").removeClass("alert-danger").removeClass("alert-success");
                            //$(".form-control-feedback").removeClass("glyphicon-remove");
                            $("#modificar").removeClass("disabled").removeClass("hidden");
                            $("#labelCedula").text("Cédula*");
                            $("#botonAgregar").addClass("hidden");
                        }else if($('#Cedula').val()==antiguaCedula){
                            $('#Cedula').addClass("alert-success").removeClass("alert-danger");
                            $("#labelCedula").text("Cédula*");
                            $("#modificar").removeClass("disabled").removeClass("hidden");
                        }
                    });
            });


            $('#formValidacion').formValidation({
                message: 'Este valor no es admitido',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    Nit: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 15.'
                            }
                        }
                    },
                    RazonSocial: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 50,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 50.'
                            }
                        }
                    },
                    Direccion: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            stringLength: {
                                min: 6,
                                max: 100,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 100.'
                            }
                        }
                    },
                    Telefono: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 13,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 13.'
                            }
                        }
                    },
                    Email2: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            }
                        }
                    },
                    IdLugar: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdTipo: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    IdActividad: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            }
                        }
                    },
                    Cedula: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 15.'
                            }
                        }
                    },
                    Nombres: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    Apellidos: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 50,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 50'
                            }
                        }
                    },
                    Celular: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 13,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 13.'
                            }
                        }
                    },
                    Email1: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            }
                        }
                    }

                }
            });
        });
    </script>
    <!-- Page script-->
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
</body>
</html>
<?php
};
?>