<?php
include_once 'tiempo.php';
?>
<!-- Logo -->
<a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>M</b>AT</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Grupo</b>MAT</span>  
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Tasks: style can be found in dropdown.less -->



            <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-calculator"></i>
                    <span class="label label-danger"><?php
                        include_once '../facades/FacadeCotizaciones.php';
                        $facaCotiza=new FacadeCotizaciones();
                        $cotizTodas=$facaCotiza->listarTodas();
                        $cotizVigente=$facaCotiza->buscarConCriterio('cotizaciones.EstadoCotizacion','Vigente',1);
                        $cotizCancelada=$facaCotiza->buscarConCriterio('cotizaciones.EstadoCotizacion','Cancelada',1);
                        $cotizPedido=$facaCotiza->buscarConCriterio('cotizaciones.EstadoCotizacion','Pedido',1);
                        if(count($cotizVigente)>0){echo count($cotizVigente);};
                        ?></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Cotizaciones realizadas: <?php echo count($cotizTodas); ?></li>
                    <li>

                        <ul class="menu">
                            <li>
                                <a href="buscarCotizaciones.php?criterio=cotizaciones.EstadoCotizacion&busqueda=Vigente&comobuscar=1">
                                    <h3>
                                        Cotizaciones vigentes: <?php echo count($cotizVigente);?>
                                        <small class="pull-right"><?php if(count($cotizTodas)>0){echo substr(((count($cotizVigente))*100/(count($cotizTodas))),0,4);}else{echo 0;} ?>%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-aqua" style="width: <?php if(count($cotizTodas)>0){echo (count($cotizVigente))*100/(count($cotizTodas));}else{echo 0;} ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><?php if(count($cotizTodas)>0){echo substr(((count($cotizVigente))*100/(count($cotizTodas))),0,4);}else{echo 0;} ?>% vigentes</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="buscarOrdenes.php?criterio=pedidos.EstadoPedido&busqueda=Cancelado&comobuscar=1">
                                    <h3>
                                        Cotizaciones canceladas: <?php echo count($cotizCancelada);?>
                                        <small class="pull-right"><?php if(count($cotizTodas)>0){echo substr(((count($cotizCancelada))*100/(count($cotizTodas))),0,4);}else{echo 0;} ?>%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: <?php if(count($cotizTodas)>0){echo (count($cotizCancelada))*100/(count($cotizTodas));}else{echo 0;} ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><?php if(count($cotizTodas)>0){echo substr(((count($cotizCancelada))*100/(count($cotizTodas))),0,4);}else{echo 0;} ?>% canceladas</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="buscarOrdenes.php?criterio=pedidos.EstadoPedido&busqueda=Autorizado&comobuscar=1">
                                    <h3>
                                        Cotizaciones en pedidos: <?php echo count($cotizPedido);?>
                                        <small class="pull-right"><?php if(count($cotizTodas)>0){echo substr(((count($cotizPedido))*100/(count($cotizTodas))),0,4);}else{echo 0;} ?>%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-red" style="width: <?php if(count($cotizTodas)>0){echo (count($cotizPedido))*100/(count($cotizTodas));}else{echo 0;}; ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><?php if(count($cotizTodas)>0){echo substr(((count($cotizPedido))*100/(count($cotizTodas))),0,4);}else{echo 0;} ?>% autorizados</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer">
                        <a href="buscarCotizaciones.php?todos=todos">Ver todas las cotizaciones</a>
                    </li>
                </ul>
            </li>




            <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="label label-danger"><?php
                        include_once '../facades/FacadeOrdenCompra.php';
                        $facaPedidos=new FacadeOrdenCompra();
                        $pedidPendien=$facaPedidos->buscarConCriterio('pedidos.EstadoPedido','Pendiente',1);
                        $pedidCancela=$facaPedidos->buscarConCriterio('pedidos.EstadoPedido','Cancelado',1);
                        $pedidAutoriza=$facaPedidos->buscarConCriterio('pedidos.EstadoPedido','Autorizado',1);
                        $pedidTodos=$facaPedidos->listarOrdenes();
                        if(count($pedidPendien)>0){echo count($pedidPendien);}
                        ?></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Pedidos realizados: <?php echo count($pedidTodos); ?></li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- Task item -->
                                <a href="buscarOrdenes.php?criterio=pedidos.EstadoPedido&busqueda=Pendiente&comobuscar=1">
                                    <h3>
                                        Pedidos pendientes: <?php echo count($pedidPendien);?>
                                        <small class="pull-right"><?php if(count($pedidTodos)>0){echo substr(((count($pedidPendien))*100/(count($pedidTodos))),0,4);}else{echo 0;} ?>%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-aqua" style="width: <?php if(count($pedidTodos)>0){echo (count($pedidPendien))*100/(count($pedidTodos));}else{echo 0;} ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><?php if(count($pedidTodos)>0){echo substr(((count($pedidPendien))*100/(count($pedidTodos))),0,4);}else{echo 0;} ?>% pendientes</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                            <li><!-- Task item -->
                                <a href="buscarOrdenes.php?criterio=pedidos.EstadoPedido&busqueda=Cancelado&comobuscar=1">
                                    <h3>
                                        Pedidos cancelados: <?php echo count($pedidCancela);?>
                                        <small class="pull-right"><?php if(count($pedidTodos)>0){echo substr(((count($pedidCancela))*100/(count($pedidTodos))),0,4);}else{echo 0;} ?>%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: <?php if(count($pedidTodos)>0){echo (count($pedidCancela))*100/(count($pedidTodos));}else{echo 0;} ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><?php if(count($pedidTodos)>0){echo substr(((count($pedidCancela))*100/(count($pedidTodos))),0,4);}else{echo 0;} ?>% cancelados</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                            <li><!-- Task item -->
                                <a href="buscarOrdenes.php?criterio=pedidos.EstadoPedido&busqueda=Autorizado&comobuscar=1">
                                    <h3>
                                        Pedidos autorizados: <?php echo count($pedidAutoriza);?>
                                        <small class="pull-right"><?php if(count($pedidTodos)>0){echo substr(((count($pedidAutoriza))*100/(count($pedidTodos))),0,4);}else{echo 0;} ?>%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-red" style="width: <?php if(count($pedidTodos)>0){echo (count($pedidAutoriza))*100/(count($pedidTodos));}else{echo 0;} ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only"><?php if(count($pedidTodos)>0){echo substr(((count($pedidAutoriza))*100/(count($pedidTodos))),0,4);}else{echo 0;} ?>% autorizados</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                        </ul>
                    </li>
                    <li class="footer">
                        <a href="buscarOrdenes.php?todos=todos">Ver todos los pedidos</a>
                    </li>
                </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The iterator image in the navbar-->
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="user-image" alt="User Image" />
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs"><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- The iterator image in the menu -->
                    <li class="user-header">
                        <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                        <p>
                            <?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?>
                            <small><?php echo $_SESSION['datosLogin']['NombreRol']?></small>
                        </p>
                    </li>
                    <!-- Menu Body -->

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <!--<div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Perfil</a>
                        </div> -->
                        <div class="pull-right">
                            <a href="../controllers/controladorCerrarSesion.php" class="btn btn-default btn-flat">Cerrar Sesión</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
