<?php
require('../utilities/fpdf/fpdf.php');
require'../facades/FacadeCotizaciones.php';
require'../utilities/ConvertirNumero.php';
$convertir=new ConvertirNumero();
$facade=new FacadeCotizaciones();
$resultados=$facade->buscarCotizacion($_GET['coti']);
$pdf=new FPDF("P");
$pdf->AddPage();
$pdf->SetFont('Arial', '', 12);
$pdf->Image('test_old.png',0,0,218);
$pdf->Ln(52);
$pdf->Cell(70,0,utf8_decode('SEÑORES: '.$resultados[0]['RazonSocial']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(115,0,utf8_decode('DIRECCIÓN: '.$resultados[0]['Direccion']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(60,0,utf8_decode('E-MAIL: '.$resultados[0]['EmailCliente']),0,0,'L');
$pdf->SetFont('Arial', '', 18);
$pdf->SetTextColor(220,20,60);
$pdf->Cell(110,0,utf8_decode('Nº: '.$resultados[0]['IdCotizacion']),0,0,'R');
$pdf->Ln(5);
$pdf->SetFont('Arial', '', 12);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(115,0,utf8_decode('CIUDAD: '.$resultados[0]['NombreLugar']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(60,0,utf8_decode('NIT O CC: '.$resultados[0]['Nit']),0,0,'L');
$pdf->Cell(100,0,utf8_decode('TELÉFONO: '.$resultados[0]['Telefono']),0,0,'L');
$pdf->Ln(5);
$pdf->Cell(160,0,utf8_decode(''.date("d/m/Y")),0,0,'R');
$pdf->Cell(32,0,utf8_decode('17/12/2015'),0,0,'R');
$totalsub=0;
$totaliva=0;
$pdf->Ln(15);
$totalProductos=0;
foreach ($resultados as $datos ) {
    $totalsub=$totalsub+$datos['TotalDetalle'];
    $pdf->Cell(20, 8, $datos['IdProducto'], 0,0,'L');
    $pdf->Cell(107, 8, $datos['NombreProducto'], 0,0,'L');
    $pdf->Cell(10, 8, $datos['Abreviatura'], 0,0,'L');
    $pdf->Cell(18, 8, $datos['CantidadProductos'], 0,0,'C');
    $pdf->Cell(22, 8, '$'.$datos['valorPresentacion'], 0,0,'L');
    $pdf->Cell(60, 8, '$'.$datos['TotalDetalle'], 0,0,'L');
    $pdf->Ln(6);
    $totalProductos++;
}
if($totalProductos<10){
    $diferencia=10-$totalProductos;
    for($i=1;$i<$diferencia;$i++){
        $pdf->Cell(20, 8, "", 0,0,'L');
        $pdf->Cell(107, 8, "", 0,0,'L');
        $pdf->Cell(10, 8,"", 0,0,'L');
        $pdf->Cell(18, 8, "", 0,0,'C');
        $pdf->Cell(22, 8, "", 0,0,'L');
        $pdf->Cell(60, 8, "", 0,0,'L');
        $pdf->Ln(6);
    }

}
$iva=$totalsub*0.16;
$total=$totalsub-$iva;
$pdf->Ln(30);
$pdf->Cell(182,0,"$".$total,0,0,'R');
$pdf->Ln(6);
$pdf->Cell(182,0,"$".$datos['ValorDescuento'],0,0,'R');
$pdf->Ln(16);
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(182,0,$convertir->to_word($totalsub,"COP"),0,0,'L');
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 12);

$pdf->Cell(182,0,"$".$iva,0,0,'R');
$pdf->Ln(6);
$pdf->Cell(182,0,"$".$totalsub,0,0,'R');







$pdf->Output('Cotizacion'.uniqid().'.pdf','D');
?>
