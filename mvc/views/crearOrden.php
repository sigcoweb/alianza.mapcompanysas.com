<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php

/**/
?>

<html>
<head>
    <meta charset="UTF-8">
    <title> Registrar pedido</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Pedido</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Pedido</a></li>
                <li class="active">Crear pedido</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if(!isset($_POST['idcliente'])&&!isset($_GET['idcliente'])) {
                        ?>
                        <div class="box box-warning box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    La cotización no es válida o no está asignada a su cuenta</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <p>Debe seleccionar una cotización válida para generar un pedido.</p>
                            </div>
                            <div class="box-footer">
                                <input type="button" class="btn bg-green-gradient pull-right" tabindex="16"
                                       onclick="location.href='crearOrden1.php'" value="Seleccionar otra cotización"/>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <?php
                    }else {
                        if(isset($_GET['idcliente'])){
                            $_POST['idcliente']=$_GET['idcliente'];
                        }
                        ?>

                        <?php
                        if (isset($_GET['mensaje'])) {
                            ?>
                            <div class="alert
                      <?php if ($_GET['error'] == 1) {
                                echo 'alert-error';
                            } else {
                                echo 'alert-info';
                            } ?>
                      alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <h4>Resultado del proceso:</h4>
                                <?php
                                echo $mensaje = $_GET['mensaje'] ?>
                            </div>

                            <?php
                            if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                                ?>

                                <div class="box box-danger collapsed-box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Ver detalle del error</h3>

                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i
                                                    class="fa fa-plus"></i>
                                            </button>
                                            <button class="btn btn-box-tool" data-widget="remove"><i
                                                    class="fa fa-remove"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <p>
                                            <?php echo $mensaje = $_GET['detalleerror'] ?>
                                        </p>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                    </div>
                                </div><!-- /.box -->
                                <?php
                            }
                        }
                        ?>
                        <form id="defaultForm" action="../controllers/ControladorOrdenCompra.php?crear=true"
                              method="post">

                            <div class="box box-default box-solid collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Indicaciones de registro</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <p>
                                            Por favor diligencie el siguiente formulario para registrar la
                                            información.<br>
                                            Recuerde que este formulario contiene campos obligatorios(*).
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- general form elements disabled -->
                            <?php
                            include_once '../facades/FacadeCotizaciones.php';
                            $coti = new FacadeCotizaciones();
                            $cotizacion = $coti->buscarCotizacion($_POST['idcliente']);
                            ?>
                            <input name="idcoti" hidden type="text"
                                   value="<?php echo $cotizacion[0]['IdCotizacion'] ?>">
                            <div class="box box-solid box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Registrar Pedido</h3>
                                </div>

                                <div class="box-body">


                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="valorcoti">Total a pagar:*</label>
                                                <input type="text" hidden name="valorpedido" value="<?php echo $cotizacion[0]['ValorTotalCotizacion'] ?>">
                                                <input class="form-control" name="valorcoti" id="valorcoti" type="text"
                                                       value="$ <?php echo number_format($cotizacion[0]['ValorTotalCotizacion']) ?>"
                                                       readonly>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">

                                                <label for="buttonOrd">O.C. cliente</label>
                                                <button type="button" id="mostrarOrden" class="btn btn-info off" onclick="orden()">Agregar
                                                    orden
                                                </button>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="form-group" id="ordenCompra">
                                                <label for="ordenDeCompra">Cód. Orden de compra:*</label>
                                                <input class="form-control" name="ordenDeCompra" id="ordenDeCompra"
                                                       type="text" placeholder="24312">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="remisionado">Remisionado:*</label>
                                                <select class="form-control select2" name="remisionado" id="remisionado"
                                                        required autofocus tabindex="1">
                                                    <option value="" disabled selected>Seleccionar</option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="facturado">Facturado:*</label>
                                                <select class="form-control select2" name="facturado" id="facturado" required tabindex="2">
                                                    <option value="" disabled selected>Seleccionar</option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="certificado">Certificado:*</label>
                                                <select class="form-control select2" name="certificado" id="certificado"
                                                        required tabindex="3">
                                                    <option value="" disabled selected>Seleccionar</option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for="fichaTecnica">Ficha técnica:*</label>
                                                <select class="form-control select2" name="fichaTecnica" id="fichaTecnica"
                                                        required tabindex="4">
                                                    <option value="" disabled selected>Seleccionar</option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="fichaSeguridad">Ficha seguridad:*</label>
                                                <select class="form-control select2" name="fichaSeguridad" id="fichaSeguridad"
                                                        required tabindex="5">
                                                    <option value="" disabled selected>Seleccionar</option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="cantidad">Observaciones (opcional)</label>
                                        <input class="form-control" name="observaciones" id="observaciones" type="text"
                                               placeholder="Por favor entregar en horas de la tarde" tabindex="6">
                                    </div>

                                    <?php
                                    include_once '../facades/PuntosEntregaFacade.php';
                                    $facade=new PuntosEntregaFacade();
                                    $puntos=$facade->buscarPunto("clientes.nit",$cotizacion[0]['Nit'],1);
                                    ?>
                                    <div class="row">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="fichaSeguridad">Dirección de entrega:*</label>
                                                <select class="form-control select2" name="direccion" id="fichaSeguridad"
                                                        required tabindex="7">
                                                    <option value="" disabled selected>Seleccionar</option>
                                                    <?php
                                                    foreach ($puntos as $punto) {?>
                                                        <option value="<?php echo $punto['nombrePuntoEntrega']?>"><?php echo $punto['nombrePuntoEntrega'].' | '.$punto['direccionPuntoEntrega']?></option>

                                                    <?php
                                                    }


                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    </div>




                                    <div class="box-footer">
                                        <input type="button" class="btn bg-yellow-gradient pull-right" tabindex="9"
                                               onclick="location.href='index.php'" value="Cancelar"/>
                                        <button type="submit" class="btn bg-green-gradient pull-left" tabindex="8"
                                                value="guardar" name=" guardar" id="guardar">Guardar Orden
                                        </button>


                                        <script>
                                            function orden(){
                                                if( $('#ordenCompra').is(":visible") ){
                                                    $('#ordenCompra').hide();
                                                    document.getElementById('mostrarOrden').textContent="Agregar orden";


                                                }else{
                                                    $('#ordenCompra').show();
                                                    document.getElementById('mostrarOrden').textContent="Ocultar orden";
                                                }

                                            }

                                            function comprobar(){
                                                if ($('#cambiar').prop('checked')){
                                                    document.getElementById('guardar').textContent="Continuar";
                                                    $('#defaultForm').attr('action','../controllers/ControladorOrdenCompra.php?crear=true&change=true');

                                                }else{
                                                    document.getElementById('guardar').textContent="Guardar Orden";
                                                    $('#defaultForm').attr('action','../controllers/ControladorOrdenCompra.php?crear=true');
                                                }
                                            }


                                        </script>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <?php
                    }
                    ?>

                </div>

                <!-- /.box -->
            </div>
    </div>

    <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div><!-- /.content-wrapper -->


<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->

    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
</footer>

<!-- Control Sidebar -->

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $(document).ready(function() {
        $('#ordenCompra').hide();

        function randomNumber(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function generateCaptcha() {
            $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
        }

        generateCaptcha();
        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                cedula: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                meta: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },


            }
        });
    });

</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

</html>
