<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->


<html>
  <head>
    <meta charset="UTF-8">
    <title>Ver Catalogo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>



    <link rel="stylesheet" href="../../date/jquery-ui.css">
  <script src="../../date/jquery-ui.js"></script>
  <script src="../../date/jquery-ui.theme.css"></script>
<!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
        <header class="main-header">

            <?php include_once 'header.php'; ?>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <?php include_once 'menu.php'; ?>
            </section>
            <!-- /.sidebar -->
        </aside>

      <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Catálogo de Productos
                    <!--            <small>Optional description</small>-->
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li><a href="#">Productos</a></li>
                    <li class="active">Catalogo</li>
                </ol>

            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Horizontal Form -->
                <div class="row">
                    <div class="col-md-10">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <p>

                                <h2 class="box-title">Listado de Productos </h2></p>

                                <!-- /.box-header -->

                                <table id="example" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    include_once '../facades/FacadeProducto.php';
                                    $producto = new FacadeProducto();

                                    if(isset($_GET['resultado'])) {
                                        $Productos = $producto->buscarProducto($_GET['resultado']);

                                    }
                                    else{
                                        $Productos = $producto->getProductos();
                                    }
                                    foreach ( $Productos as $iterator) { ?>


                                               <ul class="col-xs-6 col-centered" style="list-style-type: none">
                                                    <li class="span4">
                                                        <div class="thumbnail" style="text-align: center">
                                                            <span
                                                                class="badge badge-inverse pull-right price">$<?php echo $iterator['ValorBase']; ?></span>
                                                            <?php if (!strpos($iterator['rutaImagen'], 'sinImagen.jpg') !== false) { ?>
                                                                <img src="<?php echo $iterator['rutaImagen'] ?>"
                                                                     alt="Unicorn Flux" class="img-responsive">
                                                            <?php } else { ?>
                                                                <img src="../images/55e920f1d7044placeholder.png"
                                                                     alt="Unicorn Flux" class="img-responsive">
                                                            <?php } ?>
                                                            <h3 class="clearfix">
                                                              <span class="pull-left">
                                                                <?php echo $iterator['NombreProducto'] ?>
                                                                  <small><?php echo $producto->presentacionId($iterator['IdPresentacionProductos'])['NombrePresentacion']; ?></small>
                                                              </span>


                                                            </h3>
                                                            <p>
                                                                <?php echo $iterator['DescripcionProducto'] ?>
                                                            </p>


                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>

                                    <?php
                                    }?>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th></th>

                                    </tr>
                                    </tfoot>

                                </table>


                                <!-- /content-panel -->
                            </div>
                        </div>
                    </div>

            </section>
            <!-- /.content -->
        </div>


      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->
  </body>
  <script>
      $("#modificar").click(function () {
          var n = noty({
              text: 'Desea eliminar este Registro',
              theme: 'relax',
              layout: 'center',
              closeWith: ['click', 'hover'],
              buttons: [
                  {
                      addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {

                      // this = button element
                      // $noty = $noty element

                      $noty.close();
                      noty({text: 'You clicked "Ok" button', type: 'success'});
                  }
                  },
                  {
                      addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                      $noty.close();
                      noty({text: 'You clicked "Cancel" button', type: 'error'});
                  }
                  }
              ],
              type: 'confirm',
              animation: {
                  open: 'animated bounceInRight', // Animate.css class names
                  close: 'animated bounceOutRight', // Animate.css class names
              }

          });

      });


  </script>
  <script>
      $(document).ready(function() {

          $('#example1').dataTable( {
              bSort: true,
              aoColumns: [ { sWidth: "45%" }, { sWidth: "45%" }, { sWidth: "10%", bSearchable: false, bSortable: false } ],
              "scrollY":        "500px",
              "scrollCollapse": true,
              "info":           true,
              "paging":         true
          } );
      } );
  </script>
</html>
