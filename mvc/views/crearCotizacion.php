<?php
include_once'../facades/ClienteFacade.php';
$dao=new ClienteFacade();
session_start();
unset($_SESSION['productoDatos']);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registrar cotización</title>

    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons --
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>-->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons --
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!--<link rel="stylesheet" href="../../plugins/ionic/css/ionic.css" type="text/css">
    <script type="text/javascript" src="../../plugins/ionic/js/ionic.js"></script>-->
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->


    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>Cotización</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Cotizaciones</a></li>
                <li class="active">Crear cotización</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }
                    ?>
                    <form id="defaultForm" action="crearCotizacion2.php" method="get">

                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Indicaciones de registro</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para registrar la información.<br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->


                        <div class="box box-solid box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registrar Cotización</h3>
                            </div>

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="idcliente">Seleccione el cliente*</label>
                                            <select class="form-control select22 " style="width: 100%" name="idcliente" id="idcliente" required>
                                                <option selected disabled>Seleccionar cliente..</option>
                                                <?php
                                                $listado=$dao->listarTodos();
                                                foreach ($listado as $clientes ) { ?>
                                                    <option <?php if(isset($_GET['nit'])&&$_GET['nit']==$clientes['Nit']){echo 'selected';} ?>
                                                        value="<?php echo $clientes['Nit']?>" <?php if($clientes['estadoCliente']=='Inactivo'){echo ' disabled';} ?> >
                                                        <?php echo $clientes['RazonSocial'].' | '.$clientes['Nit']; if($clientes['estadoCliente']=='Inactivo'){echo ' | Inactivo';} ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="idcliente">Seleccione empresa a cotizar*</label>
                                            <select class="form-control select2 " style="width: 100%" name="empresaCotiza" id="idcliente" required>
                                                <option value="1" selected disabled>Seleccionar empresa..</option>
                                                <option value="MAT">MAT Química</option>
                                                <option value="BIO">BIO Support</option>
                                                <option value="MAP">MAP Company</option>


                                            </select>
                                        </div>



                                    </div>





                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Clic para continuar*</label>
                                            <button type="submit" class="btn bg-green-gradient btn-block" tabindex="2"
                                                    value="guardar" name="guardar" id="guardar">Siguiente
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="box-footer">
                                <input type="button" class="btn bg-yellow-gradient pull-right" tabindex="3"
                                       onclick="location.href='index.php'" value="Cancelar"/>
                            </div>

                        </div>
                </div>
                </form>

            </div>
            <!--/.col (right) -->


            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
    <script>
        $(document).ready(function() {

            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {

                    idcliente: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    },
                    relleno: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }
                        }
                    }


                }
            });
        });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>
    <script type="text/javascript">
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2({
                templateResult: formatState
            });
        });
        $(function () {
            //Initialize Select2 Elements
            $(".select22").select2({
                language: "es",
                minimumInputLength: 3
            });
        });

        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img src="../images/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
            );
            return $state;
        }
    </script>
</body>
</html>
