<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->


<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Modificar Empleado</title>

    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons --
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>-->
    <link rel="stylesheet" href="../../plugins/font-awesome/css/font-awesome.min.css" type="text/css">
    <!-- Ionicons --
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!--<link rel="stylesheet" href="../../plugins/ionic/css/ionic.css" type="text/css">
    <script type="text/javascript" src="../../plugins/ionic/js/ionic.js"></script>-->
    <!-- daterange picker -->
    <link href="../../plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../../plugins/iCheck/all.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Color Picker -->
    <link href="../../plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap time Picker -->
    <link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->

    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Formulario de registro
                <small>empleados</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Empleados</a></li>
                <li class="active">Registrar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-10">

                    <?php
                    if (isset($_GET['mensaje'])) {
                        ?>
                        <div class="alert
                      <?php if ($_GET['error'] == 1) {
                            echo 'alert-error';
                        } else {
                            echo 'alert-info';
                        } ?>
                      alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Resultado del proceso:</h4>
                            <?php
                            echo $mensaje = $_GET['mensaje'] ?>
                        </div>

                        <?php
                        if (isset($_GET['detalleerror']) && $_GET['error'] == 1) {
                            ?>

                            <div class="box box-danger collapsed-box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ver detalle del error</h3>

                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <p>
                                        <?php echo $mensaje = $_GET['detalleerror'] ?>
                                    </p>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    Contacte al administrador para corregir el inconveniente: admin@sigco.com
                                </div>
                            </div><!-- /.box -->
                            <?php
                        }
                    }

                    include_once'../facades/FacadeEmpleado.php';
                    $facade=new FacadeEmpleado();
                    $persona=$_GET['id'];
                    $empleado=$facade->obtenerUsuario($persona);
                    ?>

                    <form id="defaultForm" action="../controllers/controladorUsuario.php?modificar=true&idPersona=<?php echo $empleado['IdPersona']; ?>" method="post">


                        <div class="box box-default box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Indicaciones para el registro</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>

                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para modificar un
                                        empleado.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>



                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modificar Empleado</h3>
                            </div>

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cc">Documento*</label>
                                            <input class="form-control" name="cc" id="cc" type="text" value="<?php echo $empleado['CedulaPersona']?>" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="names">Nombres*</label>
                                            <input class="form-control" name="nombres" id="names" type="text" value="<?php echo $empleado['Nombres']?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="apellido">Apellidos*</label>
                                            <input class="form-control" name="apellidos" id="apellido" type="text" maxlength="20" value="<?php echo $empleado['Apellidos']?>">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="celular">Celular*</label>
                                            <input class="form-control" name="celular" id="celular" type="text" maxlength="20" value="<?php echo $empleado['CelularPersona']?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="cargo">Cargo*</label>
                                            <input class="form-control" type="text" id="cargo" tabindex="" name="cargo" required value="<?php echo $empleado['Cargo'] ?>">
                                        </div>
                                    </div>

                                    <?php
                                    include_once'../facades/FacadeRol.php';
                                    $rolfaca=new FacadeRol();
                                    $roles=$rolfaca->listarTodos();
                                    ?>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="rol">Rol*</label>
                                            <select class="form-control select2" name="rol" id="rol" required tabindex="6">
                                                <?php
                                                foreach($roles as $rol){
                                                    if($rol['NombreRol']!='Cliente'){
                                                        ?>
                                                        <option <?php if($rol['IdRol']==$empleado['IdRol']){echo 'selected';} ?> value="<?php echo $rol['IdRol'] ?>"><?php echo $rol['NombreRol'] ?></option>
                                                    <?php } } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdDepartamento">Departamento*</label>
                                            <select class="form-control select2 depto" name="IdDepartamento" id="IdDepartamento" required tabindex="5">
                                                <option value="0" disabled selected>Seleccione...</option>
                                                <?php
                                                include_once '../facades/DepartamentosFacade.php';
                                                $deptosFac = new DepartamentosFacade();
                                                $todos = $deptosFac->listarTodos();
                                                foreach ($todos as $depto) {
                                                    ?>
                                                    <option <?php if($depto['idDepartamento']==$empleado['idDepartamentoLugar']){echo ' selected ';}; ?>
                                                        value="<?php echo $depto['idDepartamento'] ?>">
                                                        <?php echo $depto['nombreDepartamento'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="IdLugar">Lugar*</label>
                                            <select class="form-control select2"  name="IdLugar" id="IdLugar" required tabindex="6">
                                                <option value="0" disabled selected>Seleccione primero un departamento...</option>
                                                <?php
                                                include_once'../facades/LugaresFacade.php';
                                                $lugaresFaca=new LugaresFacade();
                                                $lugares=$lugaresFaca->listarTodos();
                                                foreach($lugares as $lugar) {
                                                    ?>
                                                    <option <?php if ($lugar['IdLugar'] == $empleado['IdLugar']) {
                                                        echo 'selected';
                                                    }; ?>
                                                        value="<?php echo $lugar['IdLugar'] ?>">
                                                        <?php echo $lugar['NombreLugar'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="email">Email*</label>
                                            <input class="form-control" name="email" id="email" type="text" value="<?php echo $empleado['EmailPersona']?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="imagen">Imagen</label>
                                            <input  name="imagen" id="imagen" type="file" multiple="true" class="file"  value="<?php echo $empleado['RutaImagenPersona']?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="estadoEmpleado">Estado del empleado</label><br>
                                            <select class="form-control" name="estadoEmpleado" id="estadoEmpleado">
                                                <option value="Activo" <?php if($empleado['EstadoPersona']=='Activo'){echo 'selected';} ?>>Activo</option>
                                                <option value="Inactivo" <?php if($empleado['EstadoPersona']=='Inactivo'){echo 'selected';} ?>>Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="fechaNacimiento">Fecha de nacimiento*</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="fechaNacimiento" id="fechaNacimiento" value="<?php echo $empleado['fechaNacimiento'] ?>"
                                                       class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="changepass">Cambiar contraseña</label><br>
                                            <label>
                                                <input type="checkbox" name="changepass" id="changepass" value="1" tabindex="17" onchange="javascript:showContent()">
                                                ¿Cambiar la contraseña?
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="newpass" style="display: none;">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="pass1">Nueva contraseña*</label>
                                            <input class="form-control" name="pass1" id="pass1" type="password" maxlength="20" required title="Este campo es requerido">
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="pass2">Confirmar contraseña*</label>
                                            <input class="form-control" name="pass2" id="pass2" type="password" maxlength="20" required title="Este campo es requerido" >
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn bg-green-gradient pull-left" tabindex="14"
                                            value="guardar" name=" guardar" id="guardar">Modificar empleado
                                    </button>
                                    <input type="button" class="btn bg-yellow-gradient pull-right" tabindex="15"
                                           onclick="location.href='index.php'" value="Cancelar"/>
                                </div>



                            </div>

                        </div>
                    </form>

                </div>

                <!-- /.box -->
            </div>
            <!-- /.content -->
        </section>
    </div>



    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- jQuery 2.1.4--
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js" type="text/javascript"></script>
    <script src="../../dist/js/demo.js" type="text/javascript"></script>

    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>



    <script type="text/javascript">
        function showContent() {
            element = document.getElementById("newpass");
            check = document.getElementById("changepass");
            if (check.checked) {
                element.style.display='block';
            }
            else {
                element.style.display='none';
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#IdDepartamento").change(function () {
                $('#IdLugar').removeAttr("disabled");
                $("#IdDepartamento option:selected").each(function () {
                    idDepto = $(this).val();
                    //$("#IdLugar").selectedIndex=0;
                    //document.getElementById("#IdLugar").selectedIndex=0;
                    $('#IdLugar').val('0');
                    $('#IdLugar').change();
                    $.post("lugaresDepto.php", { idDepto: idDepto }, function(data){
                        $("#IdLugar").html(data);
                    });
                });
            });


            $('#defaultForm').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                locale: 'es_ES',

                fields: {
                    documento: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'Este campo debe tener mínimo 6 carácteres y máximo 15.'
                            },
                            between: {
                                min: 99999,
                                max: 1000000000000000,
                                message: 'Debe ser un número de documento válido'
                            }
                        }
                    },
                    celular: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es necesario.'
                            },
                            integer: {
                                message: 'Sólo se permite el ingreso de números.'
                            },
                            stringLength: {
                                min: 7,
                                max: 10,
                                message: 'Este campo debe tener mínimo 7 carácteres y máximo 10.'
                            },
                            between: {
                                min: 999999,
                                max: 10000000000,
                                message: 'Debe ser un número de teléfono válido'
                            }
                        }
                    },
                    nombres: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 30,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 30'
                            }
                        }
                    },
                    apellidos: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            },
                            regexp: {
                                regexp: /^[a-z\sñÑ]+$/i,
                                message: 'Solo se permiten letras'
                            },
                            stringLength: {
                                min: 3,
                                max: 30,
                                message: 'Este campo debe tener mínimo 3 caracteres y máximo 30'
                            }
                        }
                    },

                    cargo:{
                        validators:{
                            notEmpty:{
                                message: 'Este campo es requerido'
                            }

                        }


                    },
                    rol:{
                        validators:{
                            notEmpty:{
                                message: 'Este campo es requerido'
                            }

                        }


                    },


                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido.'
                            },
                            emailAddress: {
                                message: 'Ingrese un correo electrónico válido.'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            }
                        }
                    },
                    pass1:{
                        validators:{
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }

                    },

                    pass2:{
                        validators:{
                            notEmpty: {
                                message: 'Este campo es requerido'
                            },
                            identical: {
                                field: 'pass1',
                                message: 'La contraseña debe ser identica a la anterior'
                            }

                        }

                    },

                    captcha: {
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            },
                            integer:{
                                message: 'Solo se permiten números'
                            },
                            callback: {
                                message: 'Respuesta inválida',
                                callback: function(value, validator, $field) {
                                    var items = $('#captchaOperation').html().split(' '),
                                        sum   = parseInt(items[0]) + parseInt(items[2]);
                                    return value == sum;
                                }
                            }
                        }
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            //Datemask dd/mm/yyyy
            $("#fechaNacimiento").inputmask("yyyy-mm-dd", {"placeholder": "aaaa-mm-dd"});
            //Initialize Select2 Elements
            $(".select2").select2();
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
</body>
</html>
