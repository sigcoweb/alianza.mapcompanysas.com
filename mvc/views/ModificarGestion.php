<?php
include_once '../facades/FacadeGestion.php';
include_once '../facades/FacadeProducto.php';
include_once '../facades/FacadeEmpleado.php';
$empresa = new FacadeGestion();
$persona = new FacadeEmpleado();
$id = $_GET['id'];
$empresas = $empresa->obtenerGestion($id,0);
$factory = $empresa->obtenerEmpresas();
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Modificar gestión</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/png" href="../../favicon.ico">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../plugins/datepicker/bootstrap-datetimepicker.css">

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>
    <script type="text/javascript" src="../../plugins/select2/select2.js"></script>


    <link rel="stylesheet" href="../../css/cirular-buttons.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css">-->


    <!-- FORMVALIDATION -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper supreme-container">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar iterator panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $_SESSION['datosLogin']['NombreRol']?></a>
                </div>
            </div>

            <?php include_once 'menu.php' ?>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Visitas
                <small> Formulario de modificación</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Gestiones</a></li>
                <li class="active">Modificar</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <?php $idviejo = $_GET['id']; ?>
                <!-- right column -->
                <div class="col-md-10">
                    <form id="defaultForm" action="../controllers/ControladorGestion.php?idv=<?php echo $idviejo ?>"
                          method="post">

                        <div class="box box-info box-solid collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modificar Gestión</h3>

                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <p>
                                        Por favor diligencie el siguiente formulario para actualizar la información
                                        de una Gestión.<br><br>
                                        Recuerde que este formulario contiene campos obligatorios(*).
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- general form elements disabled -->
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Modificar Visita</h3>
                                <?php if(!$empresas['existe']&&$_SESSION['rol']['rol']==4||!$empresas['existe']&&$_SESSION['rol']['rol']==1){ ?>
                                    <button class="fab click btn" data-toggle="tooltip" title="Añadir comentario" style="color: #fff;"><i class="fa fa-plus" style="margin: 0 -3px"></i></button>
                                <?php }
                                else if($empresas['existe']){
                                    ?>
                                    <button class="fab btn pop" data-toggle="popover" title="Observación:  <?php $name=$persona->getUserData($empresas['persona']);print '<br>'.$name['Nombres'].' '.$name['Apellidos'] ?>" data-tog="<?php print $empresas['Comentario'] ?>" style="color: #fff;"><i class="fa fa-weixin" style="margin: 0 -5px"></i></button>
                                <?php }?>
                            </div>

                            <div class="box-body">

                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="estado">Estado de visita</label>
                                        <select class="form-control select2" style="width: 100%" name="estado" <? if($empresas['EstadoGestiones']!='Pendiente'){ print 'disabled';} ?>   id="estado"
                                                required>
                                            <option
                                                value="Pendiente" <?php if ($empresas['EstadoGestiones'] == 'Pendiente') {
                                                echo 'selected';
                                            } ?>>Pendiente
                                            </option>
                                            <option
                                                value="Cancelada" <?php if ($empresas['EstadoGestiones'] == 'Cancelada') {
                                                echo 'selected';
                                            } ?>>Cancelada
                                            </option>
                                            <option
                                                value="Realizada"<?php if ($empresas['EstadoGestiones'] == 'Realizada') {
                                                echo 'selected';
                                            } ?>>Realizada
                                            </option>
                                        </select>
                                    </div>

                                    <label for="cc">Nit*</label>
                                    <select class="form-control select2" style="width: 100%" name="idCliente" id="idCliente" disabled>

                                        <?php foreach ($factory as $f) { ?>
                                            <option
                                                value="<?php echo $f['Nit']; ?>" <?php if ($empresas['NitClienteGestiones'] == $f['Nit']) {
                                                echo 'selected';
                                            } ?>><?php echo $f['Nit'] . "|" . $f['nombreComercial']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group ocl">
                                    <label for="apellido">Tipo visita*</label>
                                    <select class="form-control select2" style="width: 100%" name="tipoVisita" id="tipoVisita" disabled>

                                        <option value="Asesoría"<?php if ($empresas['TipoGestiones'] == 'Asesoría') {
                                            echo 'selected';
                                        } ?>> Asesoría Tecnica
                                        </option>
                                        <option
                                            value="Capacitación" <?php if ($empresas['TipoGestiones'] == 'Capacitación') {
                                            echo 'selected';
                                        } ?>>Capacitación
                                        </option>

                                    </select>

                                </div>
                                <div class="form-group ocl" id="producto">
                                    <label for="cargo">Producto*</label>
                                    <select class="form-control select2 " style="width: 100%" multiple disabled  name="temaproducto">
                                        <?php
                                        $producto = new FacadeProducto();
                                        $productos = $producto->getProductos();
                                        $currentProducts=$empresa->obtenerGestion($id,1);
                                        foreach ($currentProducts as $resp){
                                            foreach ($productos as $iterator2) { ?>
                                                <option
                                                    value="<?php echo $iterator2['IdProducto']; ?>" <?php if ($resp['Asunto'] == $iterator2['IdProducto']||$empresas['Asunto'] == $iterator2['IdProducto']) {
                                                    echo ' selected';
                                                } ?>><?php echo $iterator2['IdProducto']; ?></option>
                                                <?php
                                            }
                                        }?>
                                    </select>
                                </div>
                                <div class="form-group ocl" id="tema">
                                    <label for="email">Tema*</label>
                                    <input class="form-control" name="tema" value="<?php echo $empresas['Asunto'] ?>"
                                           type="text" maxlength="50" placeholder="Desengrasantes" disabled>
                                </div>

                                <div class="form-group ocl">
                                    <label for="pass1">Asistentes*</label>
                                    <input class="form-control" name="asistentes"
                                           value="<?php echo $empresas['Asistentes'] ?>" id="asistentes" type="number"
                                           placeholder="" min="1" disabled>
                                </div>

                                <div class="form-group ocl">
                                    <label for="pass2">Descripccion*</label>
                                    <textarea class="form-control" name="observaciones" id="observaciones" type="text"
                                              maxlength="100" placeholder=""
                                              rows="5" disabled><?php echo $empresas['ObservacionesGestiones'] ?> </textarea>

                                </div>
                                <div class="form-group ocl">
                                    <label for="pass1">Fecha*</label>

                                    <div class='input-group date'>
                                        <input type='text' class="form-control" name="fechaVisita"
                                               value="<?php print $empresas['FechaProgramada'] ?>" id="fechaVisita"
                                               placeholder="YYYY/MM/DD h:m A" disabled/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>
                                </div>
                                <div class="form-group ocl">
                                    <label for="pass1">Lugar*</label>
                                    <input class="form-control" name="lugar" id="lugar"
                                           value="<?php echo $empresas['LugarGestiones'] ?>" type="text" maxlength="50" disabled>
                                </div>

                                <div class="form-group" <?php if($empresas['EstadoGestiones']=='Pendiente'){print 'hidden';} ?>  id="resultado">
                                    <label for="pass2" id="label">Resultado de Actividad*</label>
                                    <textarea class="form-control" name="resultadoGestion" <?php if($empresas['EstadoGestiones']!='Pendiente'){print ' readonly';} ?> id="resultadoGestion" type="text"
                                              maxlength="100" placeholder=""
                                              rows="5" required><?php print $empresas['ResultadoGestiones'] ?> </textarea>

                                </div>

                                <?php if($empresas['EstadoGestiones']=='Pendiente'){ ?>
                                    <div class="box-footer">


                                        <button type="submit" class="btn btn-success btn-block" tabindex="14"
                                                value="registrar" name="modificar" id="guardar">Guardar Gestión
                                        </button>
                                    </div>
                                <?php }?>
                            </div>


                        </div>
                    </form>

                </div>

                <!-- /.box -->
                <!--/.col (right) -->
            </div>
            <!-- /.row -->

            <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Main Footer -->
    <?php include_once 'footer.php' ?>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title" id="modal-register-label">Agregar comentario</h3>
                <p>llene el siguiente campo con maximo 100 caracteres:</p>
            </div>

            <div class="modal-body">

                <form role="form" action="../controllers/ControladorGestion.php" method="post" class="registration-form">
                    <input hidden type="text" value="<?php print $_GET['id']?>" name="idGestion">
                    <div class="form-group">
                        <label class="sr-only" for="form-about-yourself">About yourself</label>
	                        	<textarea  placeholder="obsevaciones..."
                                           class="form-about-yourself form-control" name="comentario"></textarea>
                    </div>
                    <button type="submit" name="comentar" class="btn btn-success btn-block">Guardar comentario</button>
                </form>

            </div>

        </div>
    </div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/daterangepicker/moment.js"></script>
<script src="../../plugins/datepicker/bootstrap-datetimepicker.js"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script type="text/javascript">
    $('.pop').popover({placement: 'bottom',content: $('.pop').data('tog'), html: true});
    function visitType () {
        if ($('#tipoVisita').val() == 'Capacitación') {
            $('#tema').hide();
            $('#producto').show();
        } else {
            $('#producto').hide();
            $('#tema').show();

        }
    }
    function showResult(){
        var resultado =$('#resultado');
        if($(resultado).val()!='Pendiente'){
            resultado.show();
            $('#resultadoGestion').attr('readonly',true);
            if($(resultado).val()=='Cancelada'){
                $('#label').text('Motivo de cancelación*');
            }

        }
    }
    $(document).ready(function () {

        $('#defaultForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            locale: 'es_ES',

            fields: {
                idCliente: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        },
                        integer: {
                            message: 'Solo se permite el ingreso de números'
                        }
                    }
                },
                temaproducto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }

                    }
                },
                tema: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }

                    }
                },
                tipoVisita: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Este campo debe tener mínimo 3 caracteres y máximo 30'
                        }
                    }
                },

                observaciones: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }

                    }


                },

                asistentes: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        },
                        integer: {
                            message: 'solo se permiten números'
                        }
                    }
                },

                fechaVisita: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        },
                        date: {
                            format: 'YYYY-MM-DD HH:mm',
                            message: 'Fecha no Valida'
                        }

                    }

                }

            }
        });
        visitType();
    });
    $(".select2").select2();


</script>
<script>

    $('#idCliente').on('change', function () {
        $.post("../controllers/ControladorGestion.php",
            {
                reload: $('#idCliente').val()
            },
            function (data) {
                $('#cliente').val(data);
            });

    });
    $('#tipoVisita').on('change', function () {
        if ($('#tipoVisita').val() == 'Capacitación') {
            $('#tema').hide();
            $('#producto').show();
        } else {
            $('#producto').hide();
            $('#tema').show();

        }
    });
    $('#estado').change (function(){
        var resultado =$('#resultado');
        if($(this).val()!='Pendiente'){
            resultado.show();
            $('.ocl').hide(1000);
            $('#label').text('Resultado de Actividad*');
            if($(this).val()=='Cancelada'){
                $('#label').text('Motivo de cancelación*');
            }

        }else{
            $(resultado).hide();
            $('.ocl').show(1000);
        }
        visitType();
    });

    $('.click').on('click',function(){
        $('#modal-register').modal();
    });

</script>
</html>
