<!DOCTYPE html> <?php session_start(); ?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<?php


if ( ($_SESSION['hora']+20000)> time()){
    $_SESSION['hora']=time();
}else{
    $nombre_archivo = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
    if ( strpos($nombre_archivo, '/') !== FALSE )
        $nombre_archivo = array_pop(explode('/', $nombre_archivo));
    $_SESSION['pagina']=$nombre_archivo;
    header('location: ../../lock_screen.php?expire=true');
}
include_once'../models/daoGraficas.php';
$dao=new dao();
$fechaInicio=$_POST['fechaInicio'];
$fechaFin=$_POST['fechaFin'];
$cantidad=$dao->ventasAsesores($fechaInicio,$fechaFin);

?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Total ventas asesores</title>
    <link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="../../plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>

    <!-- Theme style -->
    <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="../../dist/css/skins/skin-blue.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css"/>

    <!-- FORMVALIDATION -->
    <script type="text/javascript" src="../../plugins/jQuery/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/formValidation.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/framework/bootstrap.js"></script>
    <script type="text/javascript" src="../../plugins/formvalidation/language/es_ES.js"></script>

    <link rel="stylesheet" href="../../date/jquery-ui.css">
    <script src="../../date/jquery-ui.js"></script>
    <script src="../../date/jquery-ui.theme.css"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/ju-1.11.4/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,af-2.1.0,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.css"/>

    <link href="../../plugins/animate/animate.css" rel="stylesheet" type="text/css"/>
    <script src="../../plugins/messajes/jquery.noty.packaged.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <?php include_once 'header.php'; ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <?php include_once 'menu.php'; ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Panel
                <small>Productos mas vendidos</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i>Inicio</a></li>
                <li>Panel</li>
                <li class="active">Productos mas vendidos</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- right column -->
                <div class="col-md-12">

                    <script type="text/javascript">
                        $(function () {
                            // Create the chart
                            $('#container').highcharts({

                                chart: {
                                    type: 'column',
                                    events: {
                                        click: function () {
                                            $('#tablaGrafica').show();
                                        }
                                    }
                                },
                                title: {
                                    text: 'Total ventas asesores fechas: <?php echo $fechaInicio.' - '.$fechaFin?>'
                                },
                                subtitle: {

                                },
                                xAxis: {
                                    type: 'category'
                                },
                                yAxis: {
                                    title: {
                                        text: 'Dinero recaudado'
                                    }

                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                    series: {
                                        borderWidth: 0,
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.y:}'
                                        }
                                    }
                                },

                                tooltip: {
                                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> <br/>'
                                },


                                series: [{
                                    name: "Dinero recaudado",
                                    colorByPoint: true,
                                    data: [

                                        <?php
                                        foreach ($cantidad as $resultado) {?>
                                        {

                                        <?php echo 'name:"'.$resultado['Nombres'].'"'   ?>,
                                        <?php echo 'y:'.$resultado['TotalVendido'].''   ?>,


                                    },
                                    <?php } ?>
                                        ]
                                }],
                            });
                        });
                    </script>


                    <!-- general form elements disabled -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Productos mas vendidos</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <p>
                                En la siguiente gráfica encontrará el total de productos mas vendidos, con sus respectivas descripciones de cada una<br><br>
                            </p>

                        </div>
                    </div>


                    <div class="box">

                        <div class="box-body">

                            <div  id="container" style="min-width: 310px; height: 600px; margin: 0 auto">

                            </div>

                        </div>
                        </div>


                    <div class="box box-primary box-solid" hidden id="tablaGrafica">
                        <div class="box-header with-border">
                            <h3 class="box-title">Total ventas asesores (Fechas: <?php echo $fechaInicio.' - '.$fechaFin?>)</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <br>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="Empresas">
                                    <table id="example1"
                                           class="table table-striped hover order-column row-border compact display">
                                        <thead>
                                        <tr>
                                            <th>Nombre asesor</th>
                                            <th>Total vendido</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($cantidad as $item) {

                                            ?>
                                            <tr>
                                                <td><?php echo  $item['Nombres'] ?></td>
                                                <td><?php echo "$".number_format($item['TotalVendido']) ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Nombre asesor</th>
                                            <th>Total vendido</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div> <!-- panel activo-->


                    </div>












                </div><!--/.col (right) -->
            </div>   <!-- /.row -->




        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <strong>Copyright &copy; <?php echo  date("Y", time()); ?> <a href='mailto:angelsv@hotmail.com'>Grupo MAT</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script type="text/javascript" src="https://cdn.datatables.net/s/ju-1.11.4/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.10,af-2.1.0,b-1.1.0,b-colvis-1.1.0,b-flash-1.1.0,b-html5-1.1.0,b-print-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,kt-2.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.js"></script>

<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../plugins/highcharts/js/highcharts.js"></script>
<script src="../../plugins/highcharts/js/modules/exporting.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>


<!-- jQuery 2.1.4 -->
<!-- Bootstrap 3.3.2 JS -->
<script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Select2 -->
<!-- AdminLTE App -->



<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
</body>
<script>
    function mostrar(){
        $('#jaja').show();
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {


        var table = $('#example1').DataTable({
            responsive: true,
            stateSave: true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            'sPageEllipsis': 'paginate_ellipsis',
            'sPageNumber': 'paginate_number',
            'sPageNumbers': 'paginate_numbers',
            "processing": true,
            "deferLoading": 57,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "lengthMenu": [
                [10, 25, 50, -1],
                ["10 registros", "25 registros", "50 registros", "Todos"]
            ],
            /*
             "aoColumns": [
             { "orderSequence": [ "desc", "asc", "asc" ] },
             null,
             null,
             null,
             null,
             null,
             null,
             null
             ],
             */

            "order": [[ 0, 'desc' ]],

            dom:
                'Bfrtip',

            buttons: [
                {
                    extend: 'pageLength',
                    text: '<i class="fa fa-filter"> </i> Cantidad',
                    titleAttr: 'Cantidad de registros'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye-slash"></i>',
                    titleAttr: 'Mostrar/ocultar columnas'
                }
                <?php if($_SESSION['datosLogin']['NombreRol']=='Administrador'||$_SESSION['datosLogin']['NombreRol']=='Coordinador'){ ?>
                ,{
                    extend: 'print',
                    text: '<i class="fa fa-print"> </i>',
                    titleAttr: 'Imprimir',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' )
                            .prepend(
                                '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    }
                },
                {
                    extend: 'collection',
                    text: '<i class="fa fa-file"> </i>  Exportar',
                    buttons: [
                        {
                            extend: 'copy',
                            text: '<i class="fa fa-files-o"> </i>  Copiar',
                            titleAttr: 'Copiar en el portapapeles',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        },
                        {
                            extend: 'pdf',
                            message: 'Ventas asesores periodo xxx',
                            text: '<i class="fa fa-file-pdf-o"> </i> PDF',
                            orientation: 'landscape',
                            pageSize: 'LETTER',
                            /*
                             customize: function ( doc ) {
                             doc.content.splice( 1, 0, {
                             margin: [ 0, 0, 0, 12 ],
                             alignment: 'center',
                             image: 'http://datatables.net/media/images/logo-fade.png'
                             } );
                             },
                             */
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-file-text-o"> </i>  CSV',
                            titleAttr: 'Exportar valores separados por comas',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        },
                        {
                            extend: 'excel',
                            text:      '<i class="fa fa-file-excel-o"></i>  Excel',
                            titleAttr: 'a Ms Excel',
                            exportOptions: {
                                columns: ':visible',
                                modifier: {
                                    page: 'current'
                                }
                            }
                        }
                    ],
                    fade: true
                }
                <?php } ?>
            ]


        });
        table.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
        });
        table.on( 'responsive-resize', function ( e, datatable, columns ) {
            var count = columns.reduce( function (a,b) {
                return b === false ? a+1 : a;
            }, 0 );
            console.log( count +' column(s) are hidden' );
        });


    });


</script>
</html>
