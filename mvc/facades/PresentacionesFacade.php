<?php
include_once '../models/PresentacionesDao.php';
include_once '../utilities/Conexion.php';
class PresentacionesFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new PresentacionesDao();
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function buscarConCriterio($criterio,$busqueda,$comobuscar){
        return $this->objDao->buscarConCriterio($criterio,$busqueda,$comobuscar,$this->con);
    }

    public function traerPresentacion($id){
        return $this->objDao->traerPresentacion($id,$this->con);
    }


}