<?php
include_once '../models/ConcentracionesDao.php';
include_once '../utilities/Conexion.php';
class ConcentracionesFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new ConcentracionesDao();
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function buscarConCriterio($criterio,$busqueda,$comobuscar){
        return $this->objDao->buscarConCriterio($criterio,$busqueda,$comobuscar,$this->con);
    }

    public function concentracionesProducto($id){
        return $this->objDao->concentracionesProducto($id,$this->con);
    }

    public function traerConcentracion($id){
        return $this->objDao->traerConcentracion($id,$this->con);
    }

}