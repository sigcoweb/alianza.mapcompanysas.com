<?php
include_once '../models/ColoresDao.php';
include_once '../utilities/Conexion.php';
class ColoresFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new ColoresDao();
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function buscarConCriterio($criterio,$busqueda,$comobuscar){
        return $this->objDao->buscarConCriterio($criterio,$busqueda,$comobuscar,$this->con);
    }

    public function coloresProducto($idProducto){
        return $this->objDao->coloresProducto($idProducto,$this->con);
    }

    public function traerColor($id){
        return $this->objDao->traerColor($id,$this->con);
    }

}