<?php
include_once '../models/DepartamentosDao.php';
include_once '../utilities/Conexion.php';
class DepartamentosFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new DepartamentosDao();
    }

    public function registrarDepartamento(DepartamentosDto $departamentoDto){
        return $this->objDao->registrarDepartamento($departamentoDto, $this->con);
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function obtenerDepartamento($idDepartamento){
        return $this->objDao->obtenerDepartamento($idDepartamento, $this->con);
    }

    public function buscarIdDepartamento($idDepartamento){
        return $this->objDao->buscarIdDepartamento($idDepartamento, $this->con);
    }
    
    public function buscarDepartamento($criterio, $busqueda, $comobuscar){
        return $this->objDao->buscarDepartamento($criterio, $busqueda, $comobuscar, $this->con);
    }

    public function modificarDepartamento(DepartamentosDto $departamentoDto){
        return $this->objDao->modificarDepartamento($departamentoDto, $this->con);
    }


}