<?php
include_once '../models/AromasDao.php';
include_once '../utilities/Conexion.php';
class AromasFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new AromasDao();
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function buscarConCriterio($criterio,$busqueda,$comobuscar){
        return $this->objDao->buscarConCriterio($criterio,$busqueda,$comobuscar,$this->con);
    }

    public function aromasProducto($id){
    return $this->objDao->aromasProducto($id,$this->con);
    }

    public function traerAroma($id){
        return $this->objDao->traerAroma($id,$this->con);
    }

    }