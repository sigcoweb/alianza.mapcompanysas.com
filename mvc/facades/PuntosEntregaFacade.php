<?php
include_once '../models/PuntosEntregaDao.php';
include_once '../utilities/Conexion.php';
class PuntosEntregaFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new PuntosEntregaDao();
    }

    public function registrarPunto(PuntosEntregaDto $puntosEntregaDto){
        return $this->objDao->registrarPunto($puntosEntregaDto, $this->con);
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function obtenerPunto($nombrePunto, $nitEmpresa){
        return $this->objDao->obtenerPunto($nombrePunto, $nitEmpresa, $this->con);
    }

    public function buscarPunto($criterio, $busqueda, $comobuscar){
        return $this->objDao->buscarPunto($criterio, $busqueda, $comobuscar, $this->con);
    }

    public function modificarPunto(PuntosEntregaDto $puntosEntregaDto){
        return $this->objDao->modificarPunto($puntosEntregaDto, $this->con);
    }


}