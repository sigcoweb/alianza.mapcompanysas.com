<?php
/**
 * Created by PhpStorm.
 * User: probook
 * Date: 12/08/15
 * Time: 01:42 AM
 */
include_once '../utilities/Conexion.php';
include_once  '../models/GestionDao.php';
Class FacadeGestion {
    private $conexion;
    private $gestionDao;

    public function __Construct(){

        $this->conexion=Conexion::getConexion();
        $this->gestionDao=new GestionDao();
    }

    public function registrarGestion(GestionDto $productoDto){
        return $this->gestionDao->registrarGestion($productoDto,$this->conexion);

    }
    public  function getGestiones($idUsuario,$opt){
        return $this->gestionDao->listarGestion($idUsuario,$opt,$this->conexion);
    }
    public  function  obtenerGestion($userId,$opt){
        return $this->gestionDao->buscarGestion($userId,$opt,$this->conexion);
    }
    public function modificarGestion(GestionDto $usuarioDto,$idGestion){
        return $this->gestionDao->modificarGestion($usuarioDto,$this->conexion,$idGestion);
    }
    public function  comentarGestion(GestionDto $gestion){
        return $this->gestionDao->comentarGestion($gestion,$this->conexion);
    }
    public function  getEvents($busqueda,$id){
        return $this->gestionDao->getallEvents($this->conexion,$busqueda,$id);
    }
    public function  obtenerEmpresasById($criteria){
        return $this->gestionDao->obtenerEmpresaById($criteria,$this->conexion);

    }
    public function  obtenerEmpresas(){
        return $this->gestionDao->obtenerEmpresas($this->conexion);
    }
    public function  obtenerCapacitacion(){
        return $this->gestionDao->obtenerCapacitaciones($this->conexion);
    }
    public function  completeGestion($id){
        return $this->gestionDao->completeGestion($id,$this->conexion);
    }
    public function buscarGestion ($id,$option){
        return $this->gestionDao->buscarGestion($id,$option,$this->conexion);
    }
    public function buscarGestionPorFecha($id){
        return $this->gestionDao->buscarGestionCriterio('gestiones.FechaProgramada',$id.':00',1,$this->conexion);
    }

    public function buscarConCriterio($criterio,$busqueda,$comobuscar){
        return $this->gestionDao->buscarGestionCriterio($criterio,$busqueda,$comobuscar,$this->conexion);
    }
}