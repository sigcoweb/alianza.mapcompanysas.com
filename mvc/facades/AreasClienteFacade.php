<?php
include_once '../models/AreasClienteDao.php';
include_once '../utilities/Conexion.php';
class AreasClienteFacade{
    private $con;
    private $objDao;

    public function __Construct(){
        $this->con=Conexion::getConexion();
        $this->objDao=new AreasClienteDao();
    }

    public function registrarArea(AreasClienteDto $areasClienteDto){
        return $this->objDao->registrarArea($areasClienteDto, $this->con);
    }

    public function listarTodos(){
        return $this->objDao->listarTodos($this->con);
    }

    public function obtenerArea($nombreArea, $nitEmpresa){
        return $this->objDao->obtenerArea($nombreArea, $nitEmpresa, $this->con);
    }

    public function buscarArea($criterio, $busqueda, $comobuscar){
        return $this->objDao->buscarArea($criterio, $busqueda, $comobuscar, $this->con);
    }

    public function modificarArea(AreasClienteDto $areasClienteDto){
        return $this->objDao->modificarArea($areasClienteDto, $this->con);
    }


}