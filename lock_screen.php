<?php
session_start();
if ($_SESSION['datosLogin']==null || $_SESSION['datosLogin']['EstadoPersona']=="Inactivo") /*|| $_SESSION['rol']['rol']!=3)*/{
    header('location: mvc/views/Invalido.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Grupo MAT - Bloqueado</title>
      <script src="plugins/sweetalert/sweetalert.min.js"></script>
      <link rel="stylesheet" type="text/css" href="plugins/sweetalert/sweetalert.css">

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
      <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>


    <![endif]-->
  </head>

  <body onload="getTime()">

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
	  	<div class="container">
	  		<div id="showtime">
            </div>
	  			<div class="col-lg-4 col-lg-offset-4">
	  				<div class="lock-screen">

                        <a data-toggle="modal" href="#myModal" tabindex="2"><h2><i class="fa fa-lock"></i></a></h2>
                        <a data-toggle="modal" href="#myModal" tabindex="1" onfocus="break"><h4 >Desbloquear</h4></a>
                        <br><br>
                        <a href="mvc/controllers/controladorLogin.php?account=true" tabindex="3"><h5>Ingresar con una cuenta diferente</h5></a>

				          <!-- Modal -->
				          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
				              <div class="modal-dialog">
				                  <div class="modal-content">
				                      <div class="modal-header">
				                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                          <h4 class="modal-title"><?php echo $_SESSION['datosLogin']['Nombres'].' '.$_SESSION['datosLogin']['Apellidos'] ?></h4>

				                      </div>
				                      <div class="modal-body">
				                          <p class="centered"><img class="img-circle" width="80" src="dist/img/<?php echo $_SESSION['datosLogin']['RutaImagenPersona'] ?>"></p>
				                         <form action="mvc/controllers/controladorLogin.php?unlock=true" method="post">
				                           <input type="password" name="pass" placeholder="Digite su contraseña" autocomplete="off" class="form-control placeholder-no-fix" autofocus tabindex="1">

				                      </div>
				                      <div class="modal-footer centered">
				                          <button data-dismiss="modal" class="btn btn-theme02" type="button" tabindex="2">Cancelar</button>
				                          <button class="btn btn-theme" type="submit" tabindex="3">Iniciar sesión</button>
				                      </div>
                                      </form>
				                  </div>
				              </div>
				          </div>
				          <!-- modal -->
	  				</div><! --/lock-screen -->
	  			</div><!-- /col-lg-4 -->

	  	</div><!-- /container -->
      <?php
      if (isset($_GET['login'])){
          ?>
          <script type="text/javascript">
              swal({   title: "Datos de ingreso incorrectos",   text: "Por favor inténtelo de nuevo",   type: "error",   showCancelButton: false,   confirmButtonColor: "#1B0092",   confirmButtonText: "Regresar",   closeOnConfirm: true }, function(){ });
          </script>
          <?php
      }
      ?>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>

    <script>
        function getTime()
        {
            var today=new Date();
            var h=today.getHours();
            var m=today.getMinutes();
            var s=today.getSeconds();
            // add a zero in front of numbers<10
            m=checkTime(m);
            s=checkTime(s);
            document.getElementById('showtime').innerHTML=h+":"+m+":"+s;
            t=setTimeout(function(){getTime()},500);
        }

        function checkTime(i)
        {
            if (i<10)
            {
                i="0" + i;
            }
            return i;
        }
    </script>

  </body>
</html>
